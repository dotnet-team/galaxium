/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

using Anculus.Core;

namespace Galaxium.Client
{
	/// <summary>
	/// Abstract window manager that defines the basic functionality of a
	/// toolkit/platform specific manager.
	/// </summary>
	public abstract class AbstractWindowManager<Widget> : IWindowManager<Widget>
	{
	// CHECKTHIS
	//	public event EventHandler WindowGroupBehaviorChanged;
		
		protected IList<IContainerWindow<Widget>> _windows;
		
		protected const string _uid = "*";
		
		protected AbstractWindowManager ()
		{
			_windows = new List<IContainerWindow<Widget>> ();
		}
		
		public virtual IEnumerable<IContainerWindow<Widget>> Windows
		{
			get { return _windows; }
		}
		
		public virtual void Destroy ()
		{
			Clear ();
		}
		
		public virtual void Clear ()
		{
			//we must take a copy because the window.Close raises an event that causes the window to be removed
			List<IContainerWindow<Widget>> copy = new List<IContainerWindow<Widget>> (_windows);
			foreach (IContainerWindow<Widget> window in copy) {
				window.Close (true);
				window.Destroy ();
			}
			copy.Clear ();
			_windows.Clear ();
		}
		
		public virtual void RemoveContainerWindow (IContainerWindow<Widget> window)
		{
			_windows.Remove (window);
		}
		
		public virtual IContainerWindow<Widget> GetContainerWindow (string uid)
		{
			foreach (IContainerWindow<Widget> window in _windows)
				if (window.UniqueIdentifier == uid)
					return window;
			
			return null;
		}
		
		public virtual IContainerWindow<Widget> GetContainerWindow (IConversation conversation)
		{
			ISession session = conversation.Session;
			
			string uid = null;
			
			WindowGroupBehavior groupBehavior = (WindowGroupBehavior)Configuration.Conversation.Section.GetInt (Configuration.Conversation.WindowGroupBehavior.Name, Configuration.Conversation.WindowGroupBehavior.Default);
			
			switch (groupBehavior)
			{
				case WindowGroupBehavior.DontGroupWindows:
					uid = conversation.UniqueIdentifier.ToString ();
					// This could be an alternative... not using it since i fixed the issues elsewhere.
					//uid = session.Account.Protocol.Name+":"+session.Account.UniqueIdentifier+":"+conversation.PrimaryContact.UniqueIdentifier;
					break;
				case WindowGroupBehavior.GroupAllWindows:
					uid = _uid;
					break;
				case WindowGroupBehavior.GroupProtocolWindows:
					uid = session.Account.Protocol.Name;
					break;
				case WindowGroupBehavior.GroupSessionWindows:
					uid = session.Account.UniqueIdentifier;
					break;
			}
			
			IContainerWindow<Widget> window = GetContainerWindow (uid);
			
			if (window == null)
			{
				window = CreateContainerWindow (uid, session, conversation);
				_windows.Add (window);
			}
			
			return window;
		}
		
		protected abstract IContainerWindow<Widget> CreateContainerWindow (string uid, ISession session, IConversation conversation);
	}
}
