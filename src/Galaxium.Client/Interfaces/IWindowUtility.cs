// IWindowUtility.cs created with MonoDevelop
// User: draek at 5:20 P 17/03/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Core;

namespace Galaxium.Client
{
	/// <summary>
	/// Window utilities provide the creation and management of container
	/// windows that are needed by the client.
	/// </summary>
	public interface IWindowUtility<Widget>
	{
		IWindowManager<Widget> WindowManager { get; }
		IProtocol LastConversationProtocol { get; set; }
		
		void UpdateAll ();
		void CloseAll ();
		void Activate (IConversation conversation, bool requested);
		void RemoveWindow (IContainerWindow<Widget> window);
		void RemoveWindows (ISession session);
		IContainerWindow<Widget> GetWindow ();
		IContainerWindow<Widget> GetWindow (IConversation conversation);
	}
}
