/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Mono.Addins;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Client
{
	/// <summary>
	/// Utility used to perform platform specific operations.
	/// </summary>
	public static class PlatformUtility
	{
		private static IPlatform _activePlatform;
		
		public static void Initialize ()
		{
			foreach (PlatformExtension ext in AddinManager.GetExtensionNodes ("/Galaxium/Platforms")) {
				IPlatform platform = ext.Platform;

				if (platform == null)
					continue;

				_activePlatform = platform;
				_activePlatform.Initialize ();
					
				Log.Debug ("Platform: {0}", _activePlatform.Identifier);
					
				break;
			}
			
			_activePlatform = new DefaultPlatform ();
			_activePlatform.Initialize ();
		}
		
		public static void Unload ()
		{
			_activePlatform.Unload ();
		}
		
		public static IPlatform ActivePlatform
		{
			get {  return _activePlatform; }
		}
		
		public static void OpenFile (string filename)
		{
			_activePlatform.OpenFile (filename);
		}
		
		public static void OpenUrl (string url)
		{
			_activePlatform.OpenUrl (url);
		}
	}
}