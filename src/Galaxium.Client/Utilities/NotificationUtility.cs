/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Protocol;

using Mono.Addins;

namespace Galaxium.Client
{
	/// <summary>
	/// Utility used to manage notifications using a specific backend.
	/// </summary>
	public static class NotificationUtility
	{
		private static INotificationBackend _activeBackend;
		private static int _displayTime = 5;

		public static int DisplayTime
		{
			get { return _displayTime; }
			set
			{
				ThrowUtility.ThrowIfNotInRange ("DisplayTime", value, 3, 100);
				if (_displayTime != value)
					_displayTime = value;
			}
		}

		internal static void Initialize ()
		{
			foreach (TypeExtensionNode node in AddinUtility.GetExtensionNodes("/Galaxium/Backends/Notification"))
			{
				ActiveBackend = (INotificationBackend)node.GetInstance();
				break;
			}
		}
		
		internal static void Unload ()
		{
			ActiveBackend = null;
		}
		
		public static INotificationBackend ActiveBackend
		{
			get { return _activeBackend; }
			set
			{
				if (_activeBackend != value) {
					if (_activeBackend != null)
						_activeBackend.Unload ();
					_activeBackend = value;
					if (_activeBackend != null)
						_activeBackend.Initialize ();
				}
			}
		}
		
		public static void Show (INotification notification, int time)
		{
			if (_activeBackend != null)
				_activeBackend.ShowNotification(notification, time);

		}
		
		public static void UpdateStatus ()
		{
			if (_activeBackend != null)
				_activeBackend.UpdateStatus ();
		}
		
		public static void Show (INotification notification)
		{
			if (_activeBackend != null)
				_activeBackend.ShowNotification(notification);
		}
	}
}