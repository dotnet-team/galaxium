/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Galaxium.Protocol;

namespace Galaxium.Client
{
	/// <summary>
	/// Specifies the different types of input commands that can be used by the client.
	/// </summary>
	public enum InputCommandType
	{
		//1 handler for all protocols
		GlobalUnique, //the last registered handler is the only handler that is called
		GlobalSupplement, //all handlers are called, unless GlobalUnique is defined
		
		//command handler precedes all global handlers
		PerProtocolUnique, //the last registered handler is the only handler that is called
		PerProtocolSupplement //all handlers are called, unless GlobalUnique is defined
	}
}