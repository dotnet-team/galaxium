/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Protocol;

namespace Galaxium.Client
{
	/// <summary>
	/// Represents a collection of input commands available to the client.
	/// </summary>
	public sealed class InputCommandCollection
	{
		private Dictionary<string, List<InputCommand>> _commands;
		
		public InputCommandCollection ()
		{
			_commands = new Dictionary<string, List<InputCommand>> ();
		}
		
		public void Add (InputCommand ic)
		{
			if (ic == null)
				throw new ArgumentNullException ("ic");

			bool contains = _commands.ContainsKey (ic.Command);
			List<InputCommand> lst = null;
			
			if (!contains) {
				lst = new List<InputCommand> ();
				_commands.Add (ic.Command, lst);
			} else {
				lst = _commands[ic.Command];
			}
			
			if (ic.IsUniqueCommand && contains)
				lst.Clear ();

			lst.Add (ic);
		}
		
		public bool ContainsCommand (string command)
		{
			if (command == null)
				throw new ArgumentNullException ("command");
			
			return _commands.ContainsKey (command);
		}
		
		public List<InputCommand> GetCommands (string command)
		{
			if (command == null)
				throw new ArgumentNullException ("command");
			
			command = command.ToLower ();
			
			List<InputCommand> lst = null;
			if (_commands.TryGetValue (command, out lst))
				return lst;
			else
				return new List<InputCommand> ();
		}
	}
}