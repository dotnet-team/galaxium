/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Galaxium.Protocol;

namespace Galaxium.Client
{
	/// <summary>
	/// Input commands are used by the client to perform specific actions available
	/// by the client, or sessions that are connected. They are normally typed by
	/// the user in a text field.
	/// </summary>
	public sealed class InputCommand
	{
		private ICommandInputHandler _commandHandler;
		
		private string _command;
		private string _description;
		private string _sample;
		private int _requiredParams;
		
		private InputCommandType _commandType;
		
		private bool _allProtocols;
		private IProtocol[] _protocols;

		public InputCommand (ICommandInputHandler commandHandler, string command, string description, string sample, int required, InputCommandType commandType, params IProtocol[] protocols)
		{
			if (commandHandler == null)
				throw new ArgumentNullException ("commandHandler");
			if (command == null)
				throw new ArgumentNullException ("command");
			if (description == null)
				throw new ArgumentNullException ("description");
			if (sample == null)
				throw new ArgumentNullException ("sample");
			
			if (protocols == null || protocols.Length == 0)
				_allProtocols = true;
			
			_commandHandler = commandHandler;
			_command = command.ToLower ();
			_description = description;
			_sample = sample;
			_requiredParams = required;
			_commandType = commandType;
			_protocols = protocols;
		}
		
		public InputCommand (ICommandInputHandler commandHandler, string command, string description, string sample, int required, params IProtocol[] protocols)
			: this (commandHandler, command, description, sample, required, InputCommandType.GlobalSupplement, protocols)
		{
		}

		public ICommandInputHandler CommandHandler
		{
			get { return _commandHandler; }
		}
		
		public string Command
		{
			get { return _command; }
		}
		
		public string Description
		{
			get { return _description; }
		}
		
		public string Sample
		{
			get { return _sample; }
		}
		
		public int RequiredParams
		{
			get { return _requiredParams; }
		}
		
		public InputCommandType CommandType
		{
			get { return _commandType; }
		}
		
		public bool IsUniqueCommand
		{
			get { return _commandType == InputCommandType.GlobalUnique || _commandType == InputCommandType.PerProtocolUnique; }
		}
		
		public IProtocol[] Protocols
		{
			get { return _protocols; }
		}
		
		public bool SupportsAllProtocols
		{
			get { return _allProtocols; }
		}
		
		public bool IsProtocolSupported (IProtocol protocol)
		{
			if (protocol == null)
				throw new ArgumentNullException ("protocol");
			
			if (_allProtocols)
				return true;
			
			foreach (IProtocol p in _protocols)
			{
				if (p == protocol)
					return true;
			}
			
			return false;
		}
	}
}