/*
 * Galaxium Messenger
 * Copyright (C) 2008 Jensen Somers <jensen.somers@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using NDesk.DBus;
using org.freedesktop.DBus;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Client
{	
	/// <summary>
	/// Song information backend used to obtain information from the rhythmbox
	/// media player.
	/// </summary>
	public sealed class RhythmboxSongInformationBackend : AbstractSongInformationBackend
	{	
		[Interface ("org.gnome.Rhythmbox.Shell")]
		interface IRhythmboxShell
		{
			IDictionary<string, object> getSongProperties(string uri);
		}
		
		[Interface ("org.gnome.Rhythmbox.Player")]
		interface IRhythmboxPlayer
		{
			string getPlayingUri();
			bool getPlaying();
			
			event PlayingChangedHandler playingChanged;
			event PlayingUriChangedHandler playingUriChanged;
		}
		
		delegate void PlayingChangedHandler(bool changed);
		delegate void PlayingUriChangedHandler(bool changed);		
		
		private IRhythmboxPlayer player;
		private IRhythmboxShell shell;
		
		private bool _dbus;
		
		private IRhythmboxPlayer FindRhythmboxPlayerInstance()
		{
			if (!Bus.Session.NameHasOwner("org.gnome.Rhythmbox"))
				return null;
			
			return Bus.Session.GetObject<IRhythmboxPlayer>("org.gnome.Rhythmbox", new ObjectPath("/org/gnome/Rhythmbox/Player"));
		}
		
		private IRhythmboxShell FindRhythmboxShellInstance ()
		{
			if (!Bus.Session.NameHasOwner("org.gnome.Rhythmbox"))
				return null;
			
			return Bus.Session.GetObject<IRhythmboxShell>("org.gnome.Rhythmbox", new ObjectPath("/org/gnome/Rhythmbox/Shell"));
		}
		
		public override string Name
		{
			get { return GettextCatalog.GetString ("Rhythmbox Music Player"); }
		}
		
		public override void Initialize()
		{
			try
			{
				BusG.Init ();
				_dbus = true;
			}
			catch
			{
			}
		}
		
		public override void Unload()
		{
		}

		public override bool RequiresPolling
		{
			get { return false; }
		}
		
		public override SongInformation GetCurrentSong()
		{
			try
			{
				if (!player.getPlaying())
					return null;
				
				IDictionary<string, object> dict = shell.getSongProperties(player.getPlayingUri());
				
				string title = null;
				string album = null;
				string artist = null;
				int duration  = 0;
				
				if (dict.ContainsKey("title"))
					title = dict["title"].ToString();
				
				if (dict.ContainsKey("album"))
					album = dict["album"].ToString();
				
				if (dict.ContainsKey("artist"))
					artist = dict["artist"].ToString();
				
				if (dict.ContainsKey("duration"))
					duration = Convert.ToInt32(dict["duration"].ToString());

				return new SongInformation(artist, title, album, duration);
			}
			catch
			{
				return null;
			}					
		}
		
		public override bool IsAvailable()
		{
			if (!_dbus)
				return false;
			
			try
			{
				if (player != null)
					player.playingChanged -= EmitSongChanged;
				
				player = FindRhythmboxPlayerInstance();
				if (player != null)
				{
					shell = FindRhythmboxShellInstance();
					if (shell != null)
						player.playingChanged += EmitSongChanged;
				}
			}
			catch
			{
				player = null;
				shell = null;
			}
			
			if ((player == null) || (shell == null)) {
				return false;
			}
			
			EmitSongChanged(true);
			return true;
		}
		
		private void EmitSongChanged(bool changed)
		{
			if (changed)
			{
				SongInformation info = GetCurrentSong();
				OnSongChanged(new SongInformationEventArgs(info));
			}
		}
	}
}
