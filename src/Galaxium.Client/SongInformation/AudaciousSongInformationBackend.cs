/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using NDesk.DBus;
using org.freedesktop.DBus;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Client
{
	/// <summary>
	/// Song information backend used to obtain information from the audacious
	/// media player.
	/// </summary>
	public sealed class AudaciousSongInformationBackend : AbstractSongInformationBackend
	{
		const string bus = "org.mpris.audacious";
		const string objectPath = "/Player";
		
		private IAudaciousPlayer player;
		private bool _dbus;
		
		public override string Name
		{
			get { return "Audacious"; }
		}
		
		public override bool RequiresPolling
		{
			get { return false; }
		}

		private IAudaciousPlayer FindAudaciousInstance ()
		{
			if (!Bus.Session.NameHasOwner (bus))
				return null;

			return Bus.Session.GetObject<IAudaciousPlayer>(bus, new ObjectPath (objectPath));
		}

		public override bool IsAvailable ()
		{
			if (!_dbus)
				return false;
			
			try
			{
				if (player != null)
					player.StatusChange -= EmitSongChanged;
				
				player = FindAudaciousInstance ();
				player.StatusChange += EmitSongChanged;
			} catch {
				player = null;
			}
			
			EmitSongChanged (1); //emit the initial song info
			return player != null;
		}
		
		private void EmitSongChanged (int state)
		{
			SongInformation info = GetCurrentSong ();
			OnSongChanged (new SongInformationEventArgs (info));
		}

		public override SongInformation GetCurrentSong ()
		{
			try {
				int state = player.GetStatus ();

				if (state != 0) //0=playing
					return null;
				
				IDictionary<string,object> dict = player.GetMetadata ();

				string title = null;
				string album = null;
				string artist = null;
				int length = 0;
				
				if (dict.ContainsKey ("title"))
					title = dict["title"].ToString ();
				if (dict.ContainsKey ("artist"))
					artist = dict["artist"].ToString ();
				if (dict.ContainsKey ("album"))
					album = dict["album"].ToString ();
				
				if (dict.ContainsKey ("length"))
					length = (int)dict["length"] / 1000;
	
				SongInformation info = new SongInformation (artist, title, album, length);
				return info;
			} catch {
				return null;
			}
		}

		public override void Initialize ()
		{
			try
			{
				BusG.Init ();
				_dbus = true;
			}
			catch
			{
			}
		}

		public override void Unload ()
		{
		}
		
		[Interface ("org.freedesktop.MediaPlayer")]
		interface IAudaciousPlayer
		{
			IDictionary<string,object> GetMetadata (); //title, genre, album, artist, length
			
			int GetStatus ();
			
			event StatusChangeHandler StatusChange;
		}
		
		delegate void StatusChangeHandler (int state);
	}
}