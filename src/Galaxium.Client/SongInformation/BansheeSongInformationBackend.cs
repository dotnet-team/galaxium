/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using NDesk.DBus;
using org.freedesktop.DBus;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Client
{
	/// <summary>
	/// Song information backend used to obtain information from the banshee
	/// media player.
	/// </summary>
	public sealed class BansheeSongInformationBackend : AbstractSongInformationBackend
	{
		const string bus = "org.bansheeproject.Banshee";
		const string objectPath = "/org/bansheeproject/Banshee/PlayerEngine";
		
		private IBansheePlayer player;
		private bool _dbus;
		
		public override string Name
		{
			get { return GettextCatalog.GetString ("Banshee Media Player"); }
		}
		
		public override bool RequiresPolling
		{
			get { return false; }
		}

		private IBansheePlayer FindBansheeInstance ()
		{
			if (!Bus.Session.NameHasOwner (bus))
				return null;

			return Bus.Session.GetObject<IBansheePlayer>(bus, new ObjectPath (objectPath));
		}

		public override bool IsAvailable ()
		{
			if (!_dbus)
				return false;
			
			try
			{
				if (player != null)
					player.StateChanged -= EmitSongChanged;
				
				player = FindBansheeInstance ();
				player.StateChanged += EmitSongChanged;
			} catch {
				player = null;
			}
			
			EmitSongChanged (); //emit the initial song info
			return player != null;
		}
		
		private void EmitSongChanged ()
		{
			SongInformation info = GetCurrentSong ();
			OnSongChanged (new SongInformationEventArgs (info));
		}

		public override SongInformation GetCurrentSong ()
		{
			try {
				string state = player.GetCurrentState ();
				
				if (state != "playing")
					return null;
				
				IDictionary<string,object> dict = player.GetCurrentTrack ();
				int length = player.GetLength ();
				
				string title = null;
				string album = null;
				string artist = null;
				
				if (dict.ContainsKey ("name"))
					title = dict["name"].ToString ();
				if (dict.ContainsKey ("artist"))
					artist = dict["artist"].ToString ();
				if (dict.ContainsKey ("album"))
					album = dict["album"].ToString ();
	
				SongInformation info = new SongInformation (artist, title, album, length);
				return info;
			} catch {
				return null;
			}
		}

		public override void Initialize ()
		{
			try
			{
				BusG.Init ();
				_dbus = true;
			}
			catch
			{
			}
		}

		public override void Unload ()
		{
		}
		
		[Interface ("org.bansheeproject.Banshee.PlayerEngine")]
		interface IBansheePlayer
		{
			IDictionary<string,object> GetCurrentTrack (); //album, name, artist, ...
			int GetLength ();
			
			string GetCurrentState ();
			
			event StateChangedHandler StateChanged;
		}
		
		delegate void StateChangedHandler ();
	}
}