/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Client
{
	/// <summary>
	/// Abstract song information backend used to provide basic functionality
	/// to other backends that are created for specific media players.
	/// </summary>
	public abstract class AbstractSongInformationBackend : ISongInformationBackend
	{
		public event EventHandler<SongInformationEventArgs> SongChanged;

		public abstract string Name { get; }

		public virtual bool RequiresPolling
		{
			get { return true; }
		}

		public abstract bool IsAvailable ();

		public abstract SongInformation GetCurrentSong ();

		public virtual void Initialize ()
		{
		}

		public virtual void Unload ()
		{
		}

		protected virtual void OnSongChanged (SongInformationEventArgs args)
		{
			if (SongChanged != null)
				SongChanged (this, args);
		}
		
		protected static int ParseLengthString (string length)
		{
			if (String.IsNullOrEmpty (length))
				return 0;
			
			int colonPos = length.IndexOf (":");
			
			if (colonPos < 0)
			{
				int s = 0;
				
				if (int.TryParse (length, out s))
					return s;
			}
			else
			{
				int m = 0;
				int s = 0;
				
				if (!int.TryParse (length.Substring (0, colonPos), out m))
					m = 0;
				if (!int.TryParse (length.Substring (colonPos + 1), out s))
					s = 0;
				
				return (m * 60) + s;
			}
			
			return 0;
		}
	}
}