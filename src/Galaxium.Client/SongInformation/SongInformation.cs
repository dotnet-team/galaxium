/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Client
{
	/// <summary>
	/// Container used to pass around song information from the backends
	/// to the client.
	/// </summary>
	public sealed class SongInformation : IEquatable<SongInformation>
	{
		private string _artist;
		private string _title;
		private string _album;
		private int _duration;

		public SongInformation (string artist, string title, string album, int duration)
		{
			this._artist = artist;
			this._title = title;
			this._album = album;
			this._duration = duration;
		}

		public string Artist
		{
			get { return this._artist; }
		}

		public string Title
		{
			get { return this._title; }
		}

		public string Album
		{
			get { return this._album; }
		}

		public int Duration
		{
			get { return this._duration; }
		}
		
		public override bool Equals (object other)
		{
			return Equals (other as SongInformation);
		}
		
		public bool Equals (SongInformation other)
		{
			if ((object)other == null)
				return false;
			
			return this == other;
		}
		
		public override int GetHashCode ()
		{
			return _artist.GetHashCode () ^
				_title.GetHashCode () ^
				_album.GetHashCode () ^
				_duration.GetHashCode ();
		}

		public static bool operator == (SongInformation x, SongInformation y)
		{
			if ((object)x == null && (object)y == null)
				return true;
			if ((object)x == null || (object)y == null)
				return false;
			
			return x._title == y._title &&
				x._artist == y._artist &&
				x._album == y._album &&
				x._duration == y._duration;
		}
		
		public static bool operator != (SongInformation x, SongInformation y)
		{
			return !(x == y);
		}
	}
}