/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;

[TestFixture]
public class PangoUtilityTests
{
	[Test]
	public void SplitWrapTest ()
	{
		List<IMessageChunk> chunks = PangoUtility.Split ("<span weight=\"light\" size=\"smaller\">Test http://www.test.com</span> Text", null, null, null);
		
		Assert.AreEqual (3, chunks.Count);
		Assert.IsTrue (chunks[0] is ITextMessageChunk, "Chunk 0 is not an ITextMessageChunk");
		Assert.IsTrue (chunks[1] is IURIMessageChunk, "Chunk 1 is not an IURIMessageChunk");
		Assert.IsTrue (chunks[2] is ITextMessageChunk, "Chunk 2 is not an ITextMessageChunk");
		Assert.AreEqual ("<span weight=\"light\" size=\"smaller\">Test </span>", (chunks[0] as ITextMessageChunk).Text);
		Assert.AreEqual ("<span weight=\"light\" size=\"smaller\">http://www.test.com</span>", (chunks[1] as IURIMessageChunk).Text);
		Assert.AreEqual ("http://www.test.com", (chunks[1] as IURIMessageChunk).URI);
		Assert.AreEqual (" Text", (chunks[2] as ITextMessageChunk).Text);
	}
}
