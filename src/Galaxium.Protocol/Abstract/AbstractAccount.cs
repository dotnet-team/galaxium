/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public abstract class AbstractAccount: AbstractEntity, IAccount
	{
		public event EventHandler<PropertyEventArgs> DisplaySettingsChange;
		
		protected string _password;
		protected IPresence _initial_presence;
		protected bool _remember_password;
		protected bool _auto_connect;
		
		protected ContactTreeDetailLevel _detail_level = ContactTreeDetailLevel.Detailed;
		
		protected bool _sort_alpha = true;
		protected bool _sort_asc = true;
		
		protected bool _show_profile_details = true;
		protected bool _show_offline_contacts = true;
		protected bool _show_empty_groups = true;
		protected bool _group_offline_contacts = false;
		protected bool _view_by_group = true;
		protected bool _show_display_images = true;
		protected bool _show_display_names = true;
		protected bool _show_display_messages = true;
		protected bool _show_nicknames = true;
		protected bool _show_search_bar = false;
		protected bool _use_default_list_view = true;
		
		protected IAccountCache _cache;
		
		protected AbstractAccount (ISession session, string uid): base(session, true, uid)
		{
			Cache = CacheUtility.GetAccountCache(this);
			
			if (Cache != null)
				Cache.GetAccountImage ();
		}

		protected AbstractAccount (ISession session, string uid, string password, string displayName, bool autoConnect, bool rememberPassword)
			: base(session, true, uid, displayName)
		{
			ThrowUtility.ThrowIfNull ("password", password);
			
			Cache = CacheUtility.GetAccountCache(this);
			
			if (Cache != null)
				Cache.GetAccountImage ();

			_password = password;
			_auto_connect = autoConnect;
			_remember_password = rememberPassword;
		}

		public abstract IProtocol Protocol { get; }

		public IAccountCache Cache
		{
			get { return _cache; }
			set { _cache = value; }
		}
		
		public string Password
		{
			get { return _password; }
			set { _password = value; }
		}

		public bool RememberPassword
		{
			get { return _remember_password; }
			set { _remember_password = value; }
		}

		public bool AutoConnect
		{
			get { return _auto_connect; }
			set { _auto_connect = value; }
		}
		
		public IPresence InitialPresence
		{
			get { return _initial_presence; }
			set
			{
				ThrowUtility.ThrowIfNull ("InitialPresence", value);
				_initial_presence = value;
			}
		}
		
		public ContactTreeDetailLevel DetailLevel
		{
			get { return _detail_level; }
			set {
				if (_detail_level != value) {
					_detail_level = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("DetailLevel"));
				}
			}
		}

		public bool SortAlphabetic
		{
			get { return _sort_alpha; }
			set {
				if (_sort_alpha != value) {
					_sort_alpha = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("SortAlphabetic"));
				}
			}
		}

		public bool SortAscending
		{
			get { return _sort_asc; }
			set {
				if (_sort_asc != value) {
					_sort_asc = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("SortAscending"));
				}
			}
		}
		
		public bool ShowProfileDetails
		{
			get { return _show_profile_details; }
			set {
				if (_show_profile_details != value) {
					_show_profile_details = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("ShowProfileDetails"));
				}
			}
		}
		
		public bool ShowOfflineContacts
		{
			get { return _show_offline_contacts; }
			set {
				if (_show_offline_contacts != value) {
					_show_offline_contacts = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("ShowOfflineContacts"));
				}
			}
		}
		
		public bool ShowEmptyGroups
		{
			get { return _show_empty_groups; }
			set {
				if (_show_empty_groups != value) {
					_show_empty_groups = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("ShowEmptyGroups"));
				}
			}
		}
		
		public bool GroupOfflineContacts
		{
			get { return _group_offline_contacts; }
			set {
				if (_group_offline_contacts != value) {
					_group_offline_contacts = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("GroupOfflineContacts"));
				}
			}
		}
		
		public bool ViewByGroup
		{
			get { return _view_by_group; }
			set
			{
				if (_view_by_group != value)
				{
					_view_by_group = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("ViewByGroup"));
				}
			}
		}
		
		public bool ShowDisplayImages
		{
			get { return _show_display_images; }
			set {
				if (_show_display_images != value) {
					_show_display_images = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("ShowDisplayImages"));
				}
			}
		}
		
		public bool ShowDisplayNames
		{
			get { return _show_display_names; }
			set {
				if (_show_display_names != value) {
					_show_display_names = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("ShowDisplayNames"));
				}
			}
		}
		
		public bool ShowPersonalMessages
		{
			get { return _show_display_messages; }
			set {
				if (_show_display_messages != value) {
					_show_display_messages = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("ShowPersonalMessages"));
				}
			}
		}
		
		public bool ShowNicknames
		{
			get { return _show_nicknames; }
			set {
				if (_show_nicknames != value) {
					_show_nicknames = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("ShowNicknames"));
				}
			}
		}
		
		public bool ShowSearchBar
		{
			get { return _show_search_bar; }
			set {
				if (_show_search_bar != value) {
					_show_search_bar = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("ShowSearchBar"));
				}
			}
		}
		
		public bool UseDefaultListView
		{
			get { return _use_default_list_view; }
			set {
				if (_use_default_list_view != value) {
					_use_default_list_view = value;
					OnDisplaySettingsChange (new PropertyEventArgs ("UseDefaultListView"));
				}
			}
		}
		
		protected virtual void OnDisplaySettingsChange (PropertyEventArgs args)
		{
			if (DisplaySettingsChange != null)
				DisplaySettingsChange (this, args);
		}
		
		public virtual void EmitDisplaySettingsChange (PropertyEventArgs args)
		{
			OnDisplaySettingsChange (args);
		}
		
		public int CompareTo (IAccount other)
		{
			return UniqueIdentifier.CompareTo(other.UniqueIdentifier);
		}
	}
}