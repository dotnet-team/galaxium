/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public abstract class AbstractAccountCache : IAccountCache
	{
		protected IAccount _account;
		protected string _directory;
		protected string _contactsDir = String.Empty;
		protected string _imagesDir = String.Empty;
		
		public IAccount Account
		{
			get { return this._account; }
		}

		public virtual string Directory
		{
			get { return this._directory; }
		}
		
		public virtual string ImagesDirectory
		{
			get { return _imagesDir; }
		}
		
		public virtual string ContactsDirectory
		{
			get { return _contactsDir; }
		}
		
		protected AbstractAccountCache (IAccount account, string directory)
		{
			ThrowUtility.ThrowIfNull ("account", account);
			ThrowUtility.ThrowIfEmpty ("directory", directory);
			
			_account = account;
			_directory = directory;
			_contactsDir = Path.Combine (Directory, "Contacts");
			_imagesDir = Path.Combine (Directory, "Images");
			
			if (!System.IO.Directory.Exists (_directory))
				System.IO.Directory.CreateDirectory(_directory);
			
			if(!System.IO.Directory.Exists(_contactsDir))
				System.IO.Directory.CreateDirectory(_contactsDir);
			
			if(!System.IO.Directory.Exists(_imagesDir))
				System.IO.Directory.CreateDirectory(_imagesDir);
		}
		
		public abstract void SaveAccountImage (string filename);
		public abstract void SaveAccountImage ();
		public abstract void GetAccountImage ();
		
		public abstract void SaveDisplayImage (IContact contact);
		public abstract IDisplayImage GetDisplayImage (IContact contact);
	}
}