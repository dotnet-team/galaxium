/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public enum BasePresence { Online = 6, Away = 5, Busy = 4, Idle = 3, Invisible = 2, Offline = 1, Unknown = 7 };
	
	public abstract class AbstractPresence : IPresence
	{
		public abstract string State { get; }
		
		public string Message
		{
			get { return String.Empty; }
		}
		
		protected BasePresence _basePresence;
		
		public BasePresence BasePresence
		{
			get { return(_basePresence); }
		}
		
		public virtual bool IsCustomPresence
		{
			get { return false; }
		}

		public int CompareTo (IPresence other)
		{
			return State.CompareTo (other.State);
		}

		public static bool operator == (AbstractPresence p1, AbstractPresence p2)
		{
			if ((p1 as object) == null)
				return (p2 as object) == null;

			return p1.Equals (p2);
		}

		public static bool operator != (AbstractPresence p1, AbstractPresence p2)
		{
			return !(p1 == p2);
		}

		public override bool Equals (object obj)
		{
			return Equals (obj as IPresence);
		}

		public override int GetHashCode ()
		{
			return State.GetHashCode ();
		}

		public bool Equals (IPresence other)
		{
			if (other == null) return false;

			return State.Equals (other.State);
		}
	}
}