/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public abstract class AbstractConnectionInfo : IConnectionInfo
	{
		protected string _hostName;
		protected int _port;
		protected bool _useHTTP;

		protected bool _isInbound;
		
		protected AbstractConnectionInfo ()
		{
		}
		
		public AbstractConnectionInfo (string hostname, int port, bool useHTTP)
		{
			HostName = hostname;
			Port = port;
			UseHTTP = useHTTP;
		}

		public string HostName
		{
			get { return this._hostName; }
			set
			{
				ThrowUtility.ThrowIfEmpty ("HostName", value);
				this._hostName = value;
			}
		}

		public int Port
		{
			get { return this._port; }
			set
			{
				ThrowUtility.ThrowIfNotInRange ("Port", value, 1, 65536);
				this._port = value;
			}
		}

		public bool UseHTTP
		{
			get { return this._useHTTP; }
			set { this._useHTTP = value; }
		}
		
		public bool IsInbound
		{
			get { return this._isInbound; }
			set { this._isInbound = value; }
		}
	}
}