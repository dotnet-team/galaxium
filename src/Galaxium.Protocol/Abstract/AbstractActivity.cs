/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Protocol
{
	public abstract class AbstractActivity : IActivity
	{
		ISession _session;
		IEntity _entity;
		bool _canQueue;
		bool _clearOthers;
		bool _requested;
		bool _handled;
		ActivityTypes _type;
		PriorityLevel _priority;
		
		public ISession Session
		{
			get { return _session; }
		}
		
		public IEntity Entity
		{
			get { return _entity; }
		}
		
		public bool Requested
		{
			get { return _requested; }
			set { _requested = value; }
		}
		
		public PriorityLevel Priority
		{
			get { return _priority; }
			set { _priority = value; }
		}
		
		public bool ClearOthers
		{
			get { return _clearOthers; }
		}
		
		public bool CanQueue
		{
			get { return _canQueue; }
		}
		
		public bool Handled
		{
			get { return _handled; }
			set { _handled = value; }
		}
		
		public ActivityTypes Type
		{
			get { return _type; }
		}
		
		public AbstractActivity (ISession session, IEntity entity, ActivityTypes type, bool canQueue, bool clearOthers, PriorityLevel level)
		{
			_session = session;
			_entity = entity;
			_type = type;
			_canQueue = canQueue;
			_clearOthers = clearOthers;
			_priority = level;
		}
	}
}