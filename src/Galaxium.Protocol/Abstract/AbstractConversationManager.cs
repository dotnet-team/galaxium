/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Collections.Generic;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Protocol
{
	public abstract class AbstractConversationManager : IConversationManager
	{
		public event EventHandler<ConversationEventArgs> ConversationAdded;
		public event EventHandler<ConversationEventArgs> ConversationRemoved;
		
		protected List<IConversation> _conversations;

		protected AbstractConversationManager ()
		{
			_conversations = new List<IConversation> ();
		}
		
		public virtual IConversation CreateConversation (IContact contact)
		{
			return null;
		}
		
		public void Add (IConversation conversation)
		{
			ThrowUtility.ThrowIfNull ("conversation", conversation);

			if (!_conversations.Contains (conversation))
			{
				//TODO: does anculus have a replacement for this and is it needed?
				//Sort.Insert (_conversations, conversation);
				
				_conversations.Add (conversation);
				
				OnConversationAdded (new ConversationEventArgs (conversation));
				
				//conversation.Closed += new ConversationEventHandler (ConversationClosed);
				// NEEDINFO: why is this commented out?
			}
		}

		public void Remove (IConversation conversation)
		{
			ThrowUtility.ThrowIfNull ("conversation", conversation);
			
			_conversations.Remove (conversation);
			OnConversationRemoved (new ConversationEventArgs (conversation));
		}

		public IEnumerator<IConversation> GetEnumerator ()
		{
			return _conversations.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return (_conversations as IEnumerable).GetEnumerator ();
		}
		
		public IConversation GetConversation (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);
			
			foreach (IConversation conv in _conversations)
				if (conv.PrimaryContact.UniqueIdentifier == contact.UniqueIdentifier)
					return conv;
			
			return null;
		}
		
		public IConversation GetPrivateConversation (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);
			
			foreach (IConversation conv in _conversations)
				if (conv.IsPrivateConversation && conv.PrimaryContact == contact)
					return conv;
			
			return null;
		}
		
		public IConversation GetInactiveConversation (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);
			
			foreach (IConversation conv in _conversations)
				if (!conv.Active && conv.PrimaryContact.UniqueIdentifier == contact.UniqueIdentifier)
					return conv;
			
			return null;
		}
		
		public IConversation GetInactiveConversation ()
		{
			foreach (IConversation conv in _conversations)
				if (!conv.Active)
					return conv;
			
			return null;
		}
		
		public IEnumerable<IConversation> GetGroupConversations (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);

			foreach (IConversation conv in _conversations) {
				if (!conv.IsPrivateConversation && conv.ContactCollection.Contains (contact))
					yield return conv;
			}
		}

		private class ConversationComparer : IPropertyComparer<IConversation, string>
		{
			public int Compare (IConversation t, string u)
			{
				return t.UniqueIdentifier.CompareTo (u);
			}
		}
	
	////	CHECKTHIS: Newer used
	//	
	//	private void ConversationClosed (object sender, ConversationEventArgs args)
	//	{
	//		Remove(args.Conversation);
	//	}
		
		protected virtual void OnConversationAdded (ConversationEventArgs args)
		{
			if (ConversationAdded != null)
				ConversationAdded (this, args);
		}
		
		protected virtual void OnConversationRemoved (ConversationEventArgs args)
		{
			if (ConversationRemoved != null)
				ConversationRemoved (this, args);
		}
	}
}