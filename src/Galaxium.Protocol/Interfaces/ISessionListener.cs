// ISessionListener.cs created with MonoDevelop
// User: draek at 6:46 P 15/06/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

namespace Galaxium.Protocol
{
	public interface ISessionListener
	{
		ISession Session { get; }
		
		void ProcessSessionMessage (ISessionMessage message);
	}
}
