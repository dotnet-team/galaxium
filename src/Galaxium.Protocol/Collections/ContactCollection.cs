/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public sealed class ContactCollection : ICollection<IContact>
	{
		private Dictionary<string, IContact> _contacts;

		public ContactCollection ()
		{
			_contacts = new Dictionary<string, IContact> ();
		}

		public IContact GetContact (string uid)
		{
			ThrowUtility.ThrowIfEmpty ("uid", uid);

			IContact contact = null;
			if (_contacts.TryGetValue (uid.ToLower(), out contact))
				return contact;
			return null;
		}
		
		public IContact GetContactByName (string name)
		{
			ThrowUtility.ThrowIfEmpty ("name", name);

			foreach (IContact contact in _contacts.Values)
				if (contact.DisplayName == name)
					return contact;
			return null;
		}

		public bool ContactExists (string uid)
		{
			ThrowUtility.ThrowIfEmpty ("uid", uid);

			return _contacts.ContainsKey (uid.ToLower());
		}

		public void Add (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);

			if (!_contacts.ContainsKey (contact.UniqueIdentifier))
				_contacts.Add (contact.UniqueIdentifier.ToLower(), contact);
		}

		public void Clear ()
		{
			_contacts.Clear ();
		}

		public bool Contains (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);

			return _contacts.ContainsKey (contact.UniqueIdentifier.ToLower());
		}

		public void CopyTo (IContact[] array, int arrayIndex)
		{
			int i = arrayIndex;
			
			foreach (IContact contact in this)
				array[i++] = contact;
		}

		public int Count
		{
			get { return _contacts.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}
		
		public bool Remove (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);

			return _contacts.Remove (contact.UniqueIdentifier.ToLower());
		}

		public IEnumerator<IContact> GetEnumerator ()
		{
			return _contacts.Values.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return (_contacts.Values as IEnumerable).GetEnumerator ();
		}
		
		public IContact[] ToArray ()
		{
			IContact[] arr = new IContact[_contacts.Count];
			CopyTo (arr, 0);
			
			return arr;
		}
	}
}