/*
 * Galaxium Messenger
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Protocol
{
	public class ReceivedOtherActivity: AbstractActivity
	{
		private IConversation _conversation;
		
		public IConversation Conversation { get { return(_conversation); } }
		
		public ReceivedOtherActivity (IConversation conversation, bool canQueue, bool clearOthers, PriorityLevel level) : base(conversation.Session, conversation.PrimaryContact, ActivityTypes.Alert, canQueue, clearOthers, level)
		{
			_conversation = conversation;
		}
	}
}
