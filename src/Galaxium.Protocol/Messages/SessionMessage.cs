// SystemMessage.cs created with MonoDevelop
// User: draek at 6:54 P 15/06/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

namespace Galaxium.Protocol
{
	public class SessionMessage : ISessionMessage
	{
		private ISession _session;
		protected string _message;
		private SessionMessageType _type;
		
		public ISession Session
		{
			get { return _session; }
		}
		
		public string Message
		{
			get { return _message; }
		}
		
		public SessionMessageType Type
		{
			get { return _type; }
		}
		
		public SessionMessage (SessionMessageType type, ISession session)
		{
			_type = type;
			_session = session;
		}
	}
}