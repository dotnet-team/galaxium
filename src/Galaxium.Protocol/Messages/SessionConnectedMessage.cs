// SessionConnectedMessage.cs created with MonoDevelop
// User: draek at 7:14 P 15/06/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

namespace Galaxium.Protocol
{
	public enum SessionMessageType { Connected, Disconnected, Other };
	
	public class SessionConnectedMessage : SessionMessage
	{
		public SessionConnectedMessage  (ISession session) : base (SessionMessageType.Connected, session)
		{
			_message = "Session ["+session.Account.UniqueIdentifier+"] on the ["+session.Account.Protocol.Name+"] protocol has come online.";
		}
	}
}