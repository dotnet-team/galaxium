/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public static class BufferUtility
	{
		private static BufferManager _mgrProtocol;
		private static BufferManager _mgrFileTransfer;

		static BufferUtility ()
		{
			_mgrProtocol = new BufferManager (1500, 4);
			_mgrFileTransfer = new BufferManager (4096, 0);
		}

		public static byte[] Allocate (BufferSize size)
		{
			if (size == BufferSize.Protocol)
				return _mgrProtocol.Allocate ();
			else
				return _mgrFileTransfer.Allocate ();
		}

		public static void Free (BufferSize size, ref byte[] buffer)
		{
			if (size == BufferSize.Protocol)
				_mgrProtocol.Free (ref buffer);
			else
				_mgrFileTransfer.Free (ref buffer);
		}

		public static void IncreaseCapacity (BufferSize size)
		{
			if (size == BufferSize.Protocol)
				_mgrProtocol.IncreaseCapacity (4);
			else
				_mgrFileTransfer.IncreaseCapacity (8);
		}

		public static void DecreaseCapacity (BufferSize size)
		{
			if (size == BufferSize.Protocol)
				_mgrProtocol.DecreaseCapacity (4);
			else
				_mgrFileTransfer.DecreaseCapacity (8);
		}
	}
}