/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#define DEBUGWHILE

using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Web;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public static class TextUtility
	{
		public const string ISO8601Format = "yyyy-MM-ddTHH:mm:ss.fffffffzzz";

		public static string GetParameter (byte[] data, int index)
		{
			return GetParameters (data, index)[0];
		}

		public static string[] GetParameterRange (byte[] data, int start, int count)
		{
			int[] indices = new int[count];
			int index = 0;
			for (int i = start; i < count; i++)
				indices[index++] = i;
			return GetParameters (data, indices);
		}

		public static string[] GetParameters (byte[] data, params int[] indices)
		{
			char[] chars = Encoding.UTF8.GetChars (data);
			string[] parameters = new string[indices.Length];

			int len = chars.Length;
			int paramIndex = 0;
			int prevParamCharIndex = 0;
			int returnIndex = 0;

			int index = 0;
			
#if DEBUGWHILE
			int iterations = 0;
#endif
			
			do {
#if DEBUGWHILE
				iterations++;
				
				if (iterations > 100)
					Anculus.Core.Log.Warn ("Iteration {0}", iterations);
#endif
				
				char c = chars[index];
				if (c == ' ' || c == '\r') { //TODO: allow a list of chars to be passed
					if (IsRequestedIndex (indices, paramIndex)) {
						string param = null;
						if (index == prevParamCharIndex) {
							param = String.Empty;
						} else {
							int strlen = index - prevParamCharIndex;
							char[] str = new char[strlen];
							Array.Copy (chars, prevParamCharIndex, str, 0, strlen);
							param = new string (str);
						}
						parameters[returnIndex++] = param;
					}

					prevParamCharIndex = (index + 1);
					paramIndex++;
				}

				index++;
			} while (index < len && returnIndex < indices.Length);

			//make sure the last parameter is also added
			if (IsRequestedIndex (indices, paramIndex)) {
				string param = null;
				if (index == prevParamCharIndex) {
					param = String.Empty;
				} else {
					int strlen = index - prevParamCharIndex;
					char[] str = new char[strlen];
					Array.Copy (chars, prevParamCharIndex, str, 0, strlen);
					param = new string (str);
				}
				parameters[returnIndex++] = param;
			}

			return parameters;
		}

		private static bool IsRequestedIndex (int[] indices, int index)
		{
			foreach (int i in indices)
				if (i == index)
					return true;
			return false;
		}
		
		public static string ReplaceNewlinesWithSpaces (string message)
		{
			ThrowUtility.ThrowIfNull ("message", message);
			
			StringBuilder sb = new StringBuilder ();

			int len = message.Length - 1;
			for (int i=0; i<=len; i++) {
				char c = message[i];
			
				if (c == '\n') { // \n
					sb.Append (' ');
				} else if (c == '\r') { // \r
					if ((i < len) && (message[i+1] == '\n')) // \r\n
						i++;
					
					sb.Append (' ');
				} else {
					sb.Append (c);
				}
			}
			
			return sb.ToString ();
		}
		
		public static string[] SplitStringOnSpace (string data, bool encapsulateWithQuotes, bool escapeWithBackspace)
		{
			//split a string on every occurance of a space char, with the exception of spaces inside quotes of escaped with a backspace
			//eg: ABC "C:\path\to\a directory" file\ name
			//will be splitted in the following parts:
			//   ABC
			//   C:\path\to\a directory (the \ handling is required for file transfer support on windows)
			//   file name
			
			if (data == null)
				throw new ArgumentNullException ("data");
			
			List<string> list = new List<string>  ();
			StringBuilder sb = new StringBuilder ();
			
			bool insideQuote = false;
			int len = data.Length;
			
			for (int i=0; i<len; i++) {
				char c = data[i];
				
				switch (c) {
				case ' ':
					if (!insideQuote) { //a space not inside quotes
						list.Add (sb.ToString ());
						sb.Remove (0, sb.Length);
					} else { //a space inside quotes
						sb.Append (' ');
					}
					break;
				case '"':
					if (encapsulateWithQuotes)
						insideQuote = !insideQuote;
					break;
				case '\\':
					if (escapeWithBackspace && (i < (len - 1))) {
						char next = data[i+1];
						if (next == ' ') { //if the next char is a space, this is an escaped space char
							sb.Append (' ');
							i++;
							break;
						}
					}
					//otherwise this is a normal backspace
					sb.Append ('\\');
					break;
				default:
					sb.Append (c);
					break;
				}
			}
			
			if (sb.Length > 0)
				list.Add (sb.ToString ());
			
			return list.ToArray ();
		}
	}
}