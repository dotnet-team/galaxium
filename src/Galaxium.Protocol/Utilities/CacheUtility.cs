/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public static class CacheUtility
	{
		private static string _cacheDirectory;

		static CacheUtility ()
		{
			_cacheDirectory = Path.Combine (CoreUtility.ConfigurationDirectory, "Cache");
		}

		internal static string CacheDirectory
		{
			get { return _cacheDirectory; }
		}

		public static IAccountCache GetAccountCache (IAccount account)
		{
			ThrowUtility.ThrowIfNull ("account", account);
			
			IProtocolFactory fac = ProtocolUtility.GetProtocolFactory (account.Protocol);
			
			if (fac == null)
				return null;
			
			return fac.CreateAccountCache (account, GetCacheDirectory (account));
		}

		public static string GetCacheDirectory (IAccount account)
		{
			ThrowUtility.ThrowIfNull ("account", account);

			return Path.Combine (_cacheDirectory, Path.Combine (
				account.Protocol.Name, BaseUtility.GetHashedName (account.UniqueIdentifier)
			));
		}

		public static string GetEmoticonCacheDirectory ()
		{
			return Path.Combine (_cacheDirectory, "emoticons");
		}
	}
}