/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	//FIXME: why the f*** are there 2 different ctors that have nothing to do with each other --> split this cls in 2 parts
	public class ContactEventArgs : EventArgs
	{
		private IContact _contact = null;
		private string _uniqueIdentifier = String.Empty;
		
		public ContactEventArgs (string uid)
		{
			ThrowUtility.ThrowIfEmpty("uid", uid);
			
			this._uniqueIdentifier = uid;
		}
		
		public ContactEventArgs (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);

			this._contact = contact;
		}

		public IContact Contact
		{
			get { return _contact; }
		}
		
		public string UniqueIdentifier
		{
			get { return _uniqueIdentifier; }
		}
	}
}