/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol
{
	public class ARGBColor
	{
		bool _none;
		byte _a, _r, _g, _b;
		
		public byte A
		{
			get { return _a; }
			set { _a = value; }
		}
		
		public byte R
		{
			get { return _r; }
			set { _r = value; }
		}
		
		public byte G
		{
			get { return _g; }
			set { _g = value; }
		}
		
		public byte B
		{
			get { return _b; }
			set { _b = value; }
		}
		
		public static ARGBColor None
		{
			get
			{
				ARGBColor c = new ARGBColor (0, 0, 0);
				c._none = true;
				return c;
			}
		}
		
		public ARGBColor (byte a, byte r, byte g, byte b)
		{
			_a = a;
			_r = r;
			_g = g;
			_b = b;
		}
		
		public ARGBColor (byte r, byte g, byte b)
			: this (255, r, g, b)
		{
		}
		
		public string ToHexString (bool alpha, bool bgr)
		{
			string ret = string.Empty;
			
			if (alpha)
				ret = _a.ToString ("x2");
			
			if (bgr)
				ret += string.Format ("{0:x2}{1:x2}{2:x2}", _b, _g, _r);
			else
				ret += string.Format ("{0:x2}{1:x2}{2:x2}", _r, _g, _b);
			
			return ret;
		}
		
#region Override equals & operator
		public override bool Equals (object o)
		{
			return Equals (o as ARGBColor);
		}
		
		public bool Equals (ARGBColor c)
		{
			if ((object)c == null)
				return false;
			
			return (_none == c._none) && (_a == c._a) && (_r == c._r) && (_g == c._g) && (_b == c._b);
		}
		
		public override int GetHashCode ()
		{
			return (_none ? 1 : 0) ^ _a ^ _r ^ _g ^ _b;
		}
		
		public static bool operator == (ARGBColor a, ARGBColor b)
		{
			if ((object)a == null)
				return ((object)b == null);
			
			if ((object)b == null)
				return false;
			
			return a.Equals (b);
		}
		
		public static bool operator != (ARGBColor a, ARGBColor b)
		{
			return !(a == b);
		}
#endregion
		
#region Implicit operators
		public static implicit operator int (ARGBColor val)
		{
			return (val.A << 24) & (val.R << 16) & (val.G << 8) & val.B;
		}
		
		public static implicit operator ARGBColor (int val)
		{
			byte a = (byte)((val & 0xFF000000) >> 24);
			byte r = (byte)((val & 0x00FF0000) >> 16);
			byte g = (byte)((val & 0x0000FF00) >> 08);
			byte b = (byte)((val & 0x000000FF) >> 00);
			
			return new ARGBColor (a, r, g, b);
		}
#endregion
	}
}
