/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading;

using Mono.Data.Sqlite;

using Anculus.Core;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public class SQLiteConversationLog : IConversationLog
	{
		enum ItemType : byte
		{
			Message = 0,
			Event = 1
		}
		
		const short _currentVersion = 2;
		
		string _dbFile;
		//bool _keepAlive;
		int _historyLength;
		
		SqliteConnection _dbConn;
		
		SqliteCommand _syncPragmaCommand;
		SqliteCommand _logMessageCommand;
		SqliteCommand _logEventCommand;
		
		object _lock = new object ();
		
		Queue<ConversationLogEntry> _history;
		List<ConversationLogEntry> _unread;
		
		/*public bool KeepAlive
		{
			get { return _keepAlive; }
			set { _keepAlive = value; }
		}*/
		
		private int DBVersion
		{
			get
			{
				int version = -1;
				
				using (SqliteCommand cmd = _dbConn.CreateCommand ())
				{
					cmd.CommandText = "SELECT version FROM dbinfo";
					IDataReader reader = cmd.ExecuteReader();
					if (reader.Read ())
						version = reader.GetInt32 (0);
					
					reader.Close ();
				}
				
				return version;
			}
		}
		
		public long ReadTimestamp
		{
			get
			{
				long timestamp = -1;
				
				try
				{
					using (SqliteCommand cmd = _dbConn.CreateCommand ())
					{
						cmd.CommandText = "SELECT timestamp FROM dbinfo";
						IDataReader reader = cmd.ExecuteReader();
						if (reader.Read ())
							timestamp = reader.GetInt64 (0);
						
						reader.Close ();
					}
				}
				catch (SqliteException ex)
				{
					Log.Error (ex, "Error getting timestamp");
				}
				
				return timestamp;
			}
			
			set
			{
				try
				{
					OpenDB ();
					
					using (SqliteCommand cmd = _dbConn.CreateCommand ())
					{
						cmd.CommandText = "UPDATE dbinfo SET timestamp="+value;
						cmd.ExecuteNonQuery ();
					}
				}
				catch (SqliteException ex)
				{
					Log.Error (ex, "Error setting timestamp");
				}
				finally
				{
					CloseDB ();
				}
			}
		}
		
		public bool WritingEnabled { get; set; }
		
		public SQLiteConversationLog (string directory, string logName, int historyLength) //bool keepAlive
		{
			//_keepAlive = keepAlive;
			_historyLength = historyLength;
			
			if (String.IsNullOrEmpty(directory))
			{
				Log.Warn ("Conversation log cannot be created without a directory!");
			}
			else
			{
				_dbFile = Path.Combine (directory, logName + ".db");
				
				_syncPragmaCommand = new SqliteCommand ("PRAGMA synchronous = NORMAL;");
				
				_logMessageCommand = new SqliteCommand (string.Format ("INSERT INTO items (timestamp, type, uid, displayname, data) VALUES(@timestamp, {0}, @uid, @display, @data);", (int)ItemType.Message));
				_logMessageCommand.Parameters.Add ("@timestamp", DbType.Int64);
				_logMessageCommand.Parameters.Add ("@uid", DbType.String);
				_logMessageCommand.Parameters.Add ("@display", DbType.String);
				_logMessageCommand.Parameters.Add ("@data", DbType.String);
				
				_logEventCommand = new SqliteCommand (string.Format ("INSERT INTO items (timestamp, type, data) VALUES(@timestamp, {0}, @data);", (int)ItemType.Event));
				_logEventCommand.Parameters.Add ("@timestamp", DbType.Int64);
				_logEventCommand.Parameters.Add ("@data", DbType.String);
				
				lock (_lock)
				{
					try
					{
						OpenDB ();
						CreateDBTables ();
						
						int dbVersion = DBVersion;
						
						if (dbVersion < _currentVersion)
							UpdateDB ();
						else if (dbVersion > _currentVersion)
							Log.Warn ("Database is newer than this version of Galaxium knows how to handle");
					}
					catch (SqliteException ex)
					{
						Log.Error (ex, "Error finding database version");
					}
					finally
					{
						CloseDB ();
					}
				}
							
				InitHistory ();
				InitUnread ();
			}
		}
		
		void InitHistory ()
		{
			lock (_lock)
			{
				var entries = new List<ConversationLogEntry> ();
				
				try
				{
					OpenDB ();
					
					using (SqliteCommand cmd = _dbConn.CreateCommand ())
					{
						cmd.CommandText = string.Format ("SELECT timestamp, type, uid, displayname, data FROM items WHERE timestamp <= "+ReadTimestamp+" ORDER BY timestamp DESC LIMIT {0}", _historyLength);
						
						IDataReader reader = cmd.ExecuteReader();
						
						while (reader.Read ())
						{
							long timestamp = reader.GetInt64 (0);
							ItemType type = (ItemType)reader.GetByte (1);
							
							if (type == ItemType.Message)
							{
								string uid = reader.GetString (2);
								string displayName = reader.GetString (3);
								string data = reader.GetString (4);
								
								ConversationLogEntry entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), uid, displayName, data);
								entries.Add (entry);
							}
							else
							{
								string data = reader.GetString (4);
								
								ConversationLogEntry entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), data);
								entries.Add (entry);
							}
						}
						
						reader.Close ();
						
						entries.Reverse ();
						
						_history = new Queue<ConversationLogEntry> (entries);
					}
				}
				catch (SqliteException ex)
				{
					Log.Error (ex, "Error reading log entries");
				}
				finally
				{
					CloseDB ();
				}
			}
		}
		
		void InitUnread ()
		{
			_unread = new List<ConversationLogEntry> ();
			
			lock (_lock)
			{
				try
				{
					OpenDB ();
					
					using (SqliteCommand cmd = _dbConn.CreateCommand ())
					{
						cmd.CommandText = string.Format ("SELECT timestamp, type, uid, displayname, data FROM items WHERE timestamp > "+ReadTimestamp+" ORDER BY timestamp ASC");
						IDataReader reader = cmd.ExecuteReader();
						
						while (reader.Read ())
						{
							long timestamp = reader.GetInt64 (0);
							ItemType type = (ItemType)reader.GetByte (1);
							
							if (type == ItemType.Message)
							{
								string uid = reader.GetString (2);
								string displayName = reader.GetString (3);
								string data = reader.GetString (4);
								
								ConversationLogEntry entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), uid, displayName, data);
								_unread.Add (entry);
							}
							else
							{
								string data = reader.GetString (4);
								
								ConversationLogEntry entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), data);
								_unread.Add (entry);
							}
						}
						
						reader.Close ();
					}
				}
				catch (SqliteException ex)
				{
					Log.Error (ex, "Error reading log entries");
				}
				finally
				{
					CloseDB ();
				}
			}
		}
		
		void OpenDB ()
		{
			if (_dbConn != null)
				return;
			
			try
			{
				_dbConn = new SqliteConnection ("Data Source=" + _dbFile);
				_dbConn.Open ();
				
				_syncPragmaCommand.Connection = _dbConn;
				_logMessageCommand.Connection = _dbConn;
				_logEventCommand.Connection = _dbConn;
				
				_syncPragmaCommand.ExecuteNonQuery ();
			}
			catch (SqliteException ex)
			{
				Log.Error (ex, "Unable to open database ({0})", ex.ErrorCode);
				throw ex;
			}
		}
		
		void CloseDB ()
		{
			if (_dbConn == null)
				return;
			
			try
			{
				_dbConn.Close ();
				_dbConn = null;
			}
			catch (SqliteException ex)
			{
				Log.Error (ex, "Unable to close database");
			}
		}
		
		void CreateDBTables ()
		{
			using (SqliteCommand cmd = _dbConn.CreateCommand ())
			{
				cmd.CommandText = "CREATE TABLE IF NOT EXISTS items (timestamp int64, type byte, uid text, displayname text, data text);";
				cmd.ExecuteNonQuery ();
			}
			
			using (SqliteCommand cmd = _dbConn.CreateCommand ())
			{
				cmd.CommandText = "CREATE TABLE IF NOT EXISTS dbinfo (version int16, timestamp int64);";
				cmd.ExecuteNonQuery ();
			}
			
			if (DBVersion == -1)
			{
				using (SqliteCommand cmd = _dbConn.CreateCommand ())
				{
					cmd.CommandText = string.Format ("INSERT INTO dbinfo VALUES({0},{1});", _currentVersion, DateTime.Now.Ticks);
					cmd.ExecuteNonQuery ();
				}
			}
		}
		
		void UpdateDB ()
		{
			Log.Debug ("Attempting to update the log database file: " + _dbFile);
			bool needUpgrade = false;
			
			using (SqliteCommand cmd = _dbConn.CreateCommand ())
			{
				cmd.CommandText = "SELECT timestamp FROM dbinfo LIMIT 1";
				
				try
				{
					cmd.ExecuteNonQuery ();
				}
				catch
				{
					needUpgrade = true;
				}
			}
			
			if (needUpgrade)
			{
				using (SqliteCommand cmd = _dbConn.CreateCommand ())
				{
					cmd.CommandText = "ALTER TABLE dbinfo ADD timestamp int64; UPDATE dbinfo SET timestamp="+DateTime.Now.Ticks;
					cmd.ExecuteNonQuery ();
				}
			}
			
			// Set to current version with a new timestamp.
			using (SqliteCommand cmd = _dbConn.CreateCommand())
			{
				cmd.CommandText = "UPDATE dbinfo SET version=2";
				cmd.ExecuteNonQuery ();
			}
		}
		
		void DumpUnread ()
		{
			foreach (var entry in _unread)
			{
				_history.Enqueue (entry);
			}
			_unread.Clear ();
			while (_history.Count > _historyLength)
				_history.Dequeue ();
		}
		
		public void LogMessage (IMessage msg, bool read)
		{
			lock (_lock)
			{
				ConversationLogEntry entry;
				if ((msg.Flags & MessageFlag.Message) == MessageFlag.Message)
				{
					entry = new ConversationLogEntry (this, 0, 0, msg.TimeStamp,
					                                  msg.Source.UniqueIdentifier,
					                                  msg.Source.DisplayName, msg.Markup);
				}
				else
				{
					entry = new ConversationLogEntry (this, 0, 0, msg.TimeStamp, msg.Markup);
				}
				
				if (read)
				{
					if (_unread.Count != 0)
					{
						Log.Warn ("Logging a read message when there are unread available.");
						DumpUnread ();
					}
					_history.Enqueue (entry);
					if (_history.Count > _historyLength)
						_history.Dequeue ();
				}
				else
				{
					_unread.Add (entry);
				}
				
				if (!WritingEnabled) return;
				
				ThreadUtility.CreateBackgroundTask (() => {
					lock (_lock) {
						try
						{
							OpenDB ();
							
							if ((msg.Flags & MessageFlag.Message) == MessageFlag.Message)
							{
								_logMessageCommand.Parameters["@timestamp"].Value = msg.TimeStamp.Ticks;
								_logMessageCommand.Parameters["@data"].Value = msg.Markup;
								_logMessageCommand.Parameters["@uid"].Value = msg.Source.UniqueIdentifier;
								_logMessageCommand.Parameters["@display"].Value = msg.Source.DisplayName;
								_logMessageCommand.ExecuteNonQuery ();
							}
							else
							{
								_logEventCommand.Parameters["@timestamp"].Value = msg.TimeStamp.Ticks;
								_logEventCommand.Parameters["@data"].Value = msg.Markup;
								_logEventCommand.ExecuteNonQuery ();
							}
						}
						catch (Exception ex)
						{
							Log.Error (ex, "Error logging message");
						}
						finally
						{
							CloseDB ();
							
							if (read)
								ReadTimestamp = msg.TimeStamp.Ticks;
						}
					}
				});
			}
		}
		
		public void Close ()
		{
			lock (_lock)
				CloseDB ();
		}
		
		public IEnumerable<ConversationLogEntry> GetHistoryEntries ()
		{
			return _history.ToArray ();
		}
		
		public IEnumerable<ConversationLogEntry> GetUnreadEntries ()
		{
			var unread = _unread.ToArray ();
			DumpUnread ();
			
			if (unread.Length > 0)
			{
				var ticks = unread [unread.Length - 1].TimeStamp.Ticks;
				// We have entries, lets mark them as all read now.
				ThreadUtility.CreateBackgroundTask (() => {
					lock (_lock)
					{
						ReadTimestamp = ticks;
					}
				});
			}
			
			return unread;
		}
		
		public IEnumerable<ConversationLogEntry> GetRangeEntries (DateTime from, DateTime to)
		{
			lock (_lock)
			{
				var entries = new List<ConversationLogEntry> ();
				
				try
				{
					OpenDB ();
					
					using (SqliteCommand cmd = _dbConn.CreateCommand ())
					{
						cmd.CommandText = string.Format ("SELECT timestamp, type, uid, displayname, data FROM items WHERE timestamp > "+from.Ticks+" AND timestamp < "+to.Ticks+" ORDER BY timestamp ASC");
						
						IDataReader reader = cmd.ExecuteReader();
						
						while (reader.Read ())
						{
							long timestamp = reader.GetInt64 (0);
							ItemType type = (ItemType)reader.GetByte (1);
							
							if (type == ItemType.Message)
							{
								string uid = reader.GetString (2);
								string displayName = reader.GetString (3);
								string data = reader.GetString (4);
								
								ConversationLogEntry entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), uid, displayName, data);
								entries.Add (entry);
							}
							else
							{
								string data = reader.GetString (4);
								
								ConversationLogEntry entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), data);
								entries.Add (entry);
							}
						}
						
						reader.Close ();
					}
				}
				catch (SqliteException ex)
				{
					Log.Error (ex, "Error reading log entries");
				}
				finally
				{
					CloseDB ();
				}
				
				return new Queue<ConversationLogEntry> (entries);
			}
			
			return null;
		}
		
		public IEnumerable<ConversationLogEntry> SearchAll (string keyword)
		{
			List<ConversationLogEntry> entries = new List<ConversationLogEntry> ();
			
			lock (_lock)
			{
				try
				{
					OpenDB ();
					
					using (SqliteCommand cmd = _dbConn.CreateCommand ())
					{
						cmd.CommandText = "SELECT timestamp, type, uid, displayname, data FROM items ORDER BY timestamp ASC WHERE data LIKE @search";
						cmd.Parameters.Add (new SqliteParameter ("@search", keyword));
						IDataReader reader = cmd.ExecuteReader();
						
						while (reader.Read ())
						{
							long timestamp = reader.GetInt64 (0);
							ItemType type = (ItemType)reader.GetByte (1);
							
							if (type == ItemType.Message)
							{
								string uid = reader.GetString (2);
								string displayName = reader.GetString (3);
								string data = reader.GetString (4);
								
								ConversationLogEntry entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), uid, displayName, data);
								entries.Add (entry);
							}
							else
							{
								string data = reader.GetString (4);
								
								ConversationLogEntry entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), data);
								entries.Add (entry);
							}
						}
						
						reader.Close ();
					}
				}
				catch (SqliteException ex)
				{
					Log.Error (ex, "Error reading log entries");
				}
				finally
				{
					CloseDB ();
				}
			}
			
			return entries;
		}
		
		public ConversationLogEntry Search (string keyword)
		{
			ConversationLogEntry entry = null;
			
			lock (_lock)
			{
				try
				{
					OpenDB ();
					
					using (SqliteCommand cmd = _dbConn.CreateCommand ())
					{
						cmd.CommandText = "SELECT timestamp, type, uid, displayname, data FROM items ORDER BY timestamp DESC WHERE data LIKE @search LIMIT 1";
						cmd.Parameters.Add (new SqliteParameter ("@search", keyword));
						IDataReader reader = cmd.ExecuteReader();
						
						while (reader.Read ())
						{
							long timestamp = reader.GetInt64 (0);
							ItemType type = (ItemType)reader.GetByte (1);
							
							if (type == ItemType.Message)
							{
								string uid = reader.GetString (2);
								string displayName = reader.GetString (3);
								string data = reader.GetString (4);
								
								entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), uid, displayName, data);
							}
							else
							{
								string data = reader.GetString (4);
								
								entry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), data);
							}
							break;
						}
						
						reader.Close ();
					}
				}
				catch (SqliteException ex)
				{
					Log.Error (ex, "Error reading log entries");
				}
				finally
				{
					CloseDB ();
				}
			}
			
			return entry;
		}
		
		public ConversationLogEntry SearchNext (string keyword, ConversationLogEntry entry)
		{
			ConversationLogEntry newEntry = null;
			
			lock (_lock)
			{
				try
				{
					OpenDB ();
					
					using (SqliteCommand cmd = _dbConn.CreateCommand ())
					{
						cmd.CommandText = string.Format ("SELECT timestamp, type, uid, displayname, data FROM items ORDER BY timestamp DESC WHERE timestamp < {0} AND data LIKE @search LIMIT 1", entry.TimeStamp.Ticks);
						cmd.Parameters.Add (new SqliteParameter ("@search", keyword));
						IDataReader reader = cmd.ExecuteReader();
						
						while (reader.Read ())
						{
							long timestamp = reader.GetInt64 (0);
							ItemType type = (ItemType)reader.GetByte (1);
							
							if (type == ItemType.Message)
							{
								string uid = reader.GetString (2);
								string displayName = reader.GetString (3);
								string data = reader.GetString (4);
								
								newEntry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), uid, displayName, data);
							}
							else
							{
								string data = reader.GetString (4);
								
								newEntry = new ConversationLogEntry (this, 0, 0, new DateTime (timestamp), data);
							}
							break;
						}
						
						reader.Close ();
					}
				}
				catch (SqliteException ex)
				{
					Log.Error (ex, "Error reading log entries");
				}
				finally
				{
					CloseDB ();
				}
			}
			
			return newEntry;
		}
	}
}
