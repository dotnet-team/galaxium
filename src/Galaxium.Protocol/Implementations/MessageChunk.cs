/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public abstract class MessageChunk : IMessageChunk
	{
		IMessageChunk _parent;
		List<IMessageChunk> _chunks = new List<IMessageChunk> ();
		IMessageStyle _style = new MessageStyle ();
		
		protected MessageChunk (IMessageChunk parent, IMessageStyle style)
		{
			_parent = parent;
			_style = style;
		}
		
		protected MessageChunk (IMessageChunk parent)
			: this (parent, new MessageStyle ())
		{
		}
		
		public IMessageChunk Parent
		{
			get { return _parent; }
		}
		
		public List<IMessageChunk> Chunks
		{
			get { return _chunks; }
		}
		
		public IMessageStyle Style
		{
			get { return _style; }
			set { _style = value; }
		}
	}
	
	public class TextMessageChunk : MessageChunk, ITextMessageChunk
	{
		string _text;
		
		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}
		
		public TextMessageChunk (IMessageChunk parent, IMessageStyle style, string text)
			: base (parent, style)
		{
			_text = text;
		}
	}
	
	public class URIMessageChunk : TextMessageChunk, IURIMessageChunk
	{
		string _uri;
		
		public string URI
		{
			get { return _uri; }
			set { _uri = value; }
		}
		
		public URIMessageChunk (IMessageChunk parent, IMessageStyle style, string uri, string text)
			: base (parent, style, text)
		{
			_uri = uri;
		}
		
		public URIMessageChunk (IMessageChunk parent, IMessageStyle style, string uri)
			: this (parent, style, uri, uri)
		{
		}
	}
	
	public class EmoticonMessageChunk : TextMessageChunk, IEmoticonMessageChunk
	{
		IEmoticon _emoticon;
		
		public IEmoticon Emoticon
		{
			get { return _emoticon; }
			set { _emoticon = value; }
		}
		
		public EmoticonMessageChunk (IMessageChunk parent, IMessageStyle style, IEmoticon emot, string equiv)
			: base (parent, style, equiv)
		{
			_emoticon = emot;
		}
		
		public EmoticonMessageChunk (IMessageChunk parent, IMessageStyle style, IEmoticon emot)
			: this (parent, style, emot, emot.Equivalents[0])
		{
		}
	}
}