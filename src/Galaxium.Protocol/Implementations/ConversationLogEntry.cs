/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public sealed class ConversationLogEntry
	{
		public IConversationLog Log { get; private set; }
		
		public int ArchiveIndex { get; private set; }
		public int StreamIndex  { get; private set; }

		public DateTime TimeStamp        { get; private set; }
		public string   UniqueIdentifier { get; private set; }
		public string   DisplayName      { get; private set; }
		public string   Message          { get; private set; }
		public bool     IsEvent          { get; private set; }
		public bool     WasDisplayed     { get; internal set; }
		
		internal ConversationLogEntry (IConversationLog log, int archiveIndex, int streamIndex,
		                               DateTime timestamp, string uid, string displayName,
		                               string message)
		{
			Log = log;
			ArchiveIndex = archiveIndex;
			StreamIndex = streamIndex;
			TimeStamp = timestamp;
			UniqueIdentifier = uid;
			DisplayName = displayName;
			Message = message;
			
			IsEvent = false;
		}

		internal ConversationLogEntry (IConversationLog log, int archiveIndex, int streamIndex,
		                               DateTime timestamp, string message)
			:this (log, archiveIndex, streamIndex, timestamp, null, null, message)
		{
			IsEvent = true;
		}
	}
}