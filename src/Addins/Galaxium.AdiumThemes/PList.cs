/*
 * Galaxium Messenger
 *
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;

using Anculus.Core;

using Galaxium.Core;

namespace Galaxium.AdiumThemes
{
	public class PList: ArrayList
	{
		public PList (string filename)
		{
			Load (filename);
		}
		
		void Load (string filename)
		{
			XmlTextReader reader = XmlTextReader.Create (filename) as XmlTextReader;
			reader.WhitespaceHandling = WhitespaceHandling.None;
			reader.ProhibitDtd = false;
			reader.XmlResolver = null;
			reader.MoveToContent ();
			reader.ReadStartElement ("plist");
			
			while (reader.IsStartElement ())
			{
				object obj = LoadObject (reader);
				
				if (obj != null)
					Add (obj);
			}
		}
		
		object LoadObject (XmlReader reader)
		{
			string name = reader.LocalName;
			
			if (name == "string")
				return reader.ReadElementContentAsString ();
			
			if (name == "integer")
				return reader.ReadElementContentAsInt ();
			
			if (name == "true")
			{
				reader.ReadStartElement ();
				return true;
			}
			
			if (name == "false")
			{
				reader.ReadStartElement ();
				return false;
			}
			
			if (name == "array")
			{
				ArrayList list = new ArrayList ();
				
				reader.ReadStartElement ();
				
				while (reader.IsStartElement ())
				{
					object obj = LoadObject (reader);
					
					if (obj != null)
						list.Add (obj);
				}
				
				reader.ReadEndElement ();
				
				return list;
			}
			
			if (name == "dict")
			{
				PListDict dict = new PListDict();
				string key = string.Empty;
				
				reader.ReadStartElement ();
				
				while (reader.IsStartElement ())
				{
					if (reader.LocalName != "key")
						Log.Warn ("Not a key! {0}", reader.LocalName);
					
					key = reader.ReadElementContentAsString ();

					if (string.IsNullOrEmpty (key))
					{
						Log.Warn ("No Key!");
						continue;
					}
					
					object obj = LoadObject (reader);
					
					if (obj != null)
						dict[key] = obj;
					
					key = string.Empty;
				}
				
				reader.ReadEndElement ();
						       
				return dict;
			}
			
			return null;
		}
	}
	
	public class PListDict : Dictionary <string, object>
	{
		public T Value<T> (string key, T def)
		{
			if (!ContainsKey (key))
				return def;
			
			object obj = this[key];
			
			try
			{
				return (T)Convert.ChangeType (obj, typeof (T));
			}
			catch
			{
				return def;
			}
		}
	}
}
