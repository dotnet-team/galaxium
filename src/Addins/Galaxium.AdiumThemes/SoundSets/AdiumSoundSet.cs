/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.AdiumThemes
{
	public class AdiumSoundSet: ISoundSet
	{
		string _archiveName;
		string _cacheDir;

		public AdiumSoundSet(string archivename, string cacheDir)
		{
			_archiveName = archivename;
			_cacheDir = Path.Combine(cacheDir, ArchiveUtility.GetArchiveNameWithoutExtension (archivename));
			
			if (!ArchiveUtility.Extract(_archiveName, _cacheDir))
				throw new ApplicationException("Unable to extract " + _archiveName);
				
			_name = ArchiveUtility.GetArchiveNameWithoutExtension (_archiveName);
			
			if (_name.Contains("."))
				_name = _name.Substring(0, _name.IndexOf("."));
			
			_sounds = new List<ISound>();
		}
		
		string _name;
		public string Name
		{
			get { return _name; }
		}
		
		string _creator = "Unknown";
		public string Creator
		{
			get { return _creator; }
		}
		
		string _description = string.Empty;
		public string Description
		{
			get { return _description; }
		}
		
		List<ISound> _sounds;
		public List<ISound> Sounds
		{
			get
			{
				if (_sounds.Count == 0)
					LoadSounds();

				return _sounds;
			}
		}
		
		public void LoadSounds()
		{
			_sounds.Clear();
			
			string plistFile = ArchiveUtility.FindFile(_cacheDir, "Sounds.plist", ".adiumsoundset");

			if (File.Exists(plistFile))
			{
				LoadFromPList(Path.GetDirectoryName(plistFile), plistFile);
				return;
			}
			
			string infoFile = ArchiveUtility.FindFile(_cacheDir, "*.txt", ".adiumsoundset");
				
			if (File.Exists(infoFile))
				LoadFromText(Path.GetDirectoryName(infoFile), infoFile);
		}
		
		void LoadFromText(string path, string infoFile)
		{
			StreamReader reader = File.OpenText(infoFile);
				
			string line = reader.ReadLine();
				
			while ((line.Trim().ToLower() != "soundset:") && (!reader.EndOfStream))
			{
				_description += line;
				line = reader.ReadLine();
			}
				
			if (reader.EndOfStream)
			{
				Log.Warn("Unable to parse text description file");
				return;
			}
				
			line = reader.ReadLine();
				
			while (!reader.EndOfStream)
			{
				string[] sections = line.Split(new char[] { '"' });
					
				try
				{
					_sounds.Add(new AdiumSound(sections[1].Trim(), Path.Combine(path, sections[2].Trim())));
				}
				catch (Exception)
				{
				}
				
				line = reader.ReadLine();
			}
		}
		
		void LoadFromPList(string path, string plistFile)
		{
			PList plist = new PList(plistFile);
			
			PListDict rootDict = (PListDict)plist[0];
			PListDict soundDict = (PListDict)rootDict["Sounds"];
			_description = rootDict.Value<string> ("Info", string.Empty);
			
			foreach (string soundType in soundDict.Keys)
				_sounds.Add(new AdiumSound(soundType, Path.Combine(path, (string)soundDict[soundType])));
		}
	}
}
