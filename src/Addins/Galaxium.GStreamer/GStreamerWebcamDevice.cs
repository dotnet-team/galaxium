/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using GLib;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.GStreamer
{
	public class GStreamerWebcamDevice : IWebcamDevice
	{
		NativeGalaxiumWebcamDevice _native;
		string _name;
		string _videoDevice;
		string _gstreamerSource;
		string _halUdi;
		List<GStreamerVideoFormat> _videoFormats = new List<GStreamerVideoFormat> ();
		
		internal NativeGalaxiumWebcamDevice Native
		{
			get { return _native; }
		}
		
		public string Name
		{
			get { return _name; }
		}
		
		public string VideoDevice
		{
			get { return _videoDevice; }
		}
		
		public string GStreamerSource
		{
			get { return _gstreamerSource; }
		}
		
		public string HalUdi
		{
			get { return _halUdi; }
		}
		
		public IEnumerable<IVideoFormat> VideoFormats
		{
			get { return _videoFormats.ToArray (); }
		}
		
		internal unsafe GStreamerWebcamDevice (NativeGalaxiumWebcamDevice native)
		{
			_native = native;
			
			_name = GLib.Marshaller.Utf8PtrToString (native.name);
			_videoDevice = GLib.Marshaller.Utf8PtrToString (native.video_device);
			_gstreamerSource = GLib.Marshaller.Utf8PtrToString (native.gstreamer_src);
			_halUdi = GLib.Marshaller.Utf8PtrToString (native.hal_udi);
			
			for (int i = 0; i < native.num_video_formats; i++)
			{
				GStreamerVideoFormat format = new GStreamerVideoFormat (native.video_formats[i]);
				
				if (!_videoFormats.Contains (format))
					_videoFormats.Add (format);
			}
		}
	}
}
