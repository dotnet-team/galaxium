/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Mono.Addins;
using Gtk;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Client;

namespace Galaxium.Client.GtkGui
{
	public class ActiveConversationSessionCondition : ConditionType
	{
		public ActiveConversationSessionCondition ()
		{			
		}

		public override bool Evaluate (NodeElement conditionNode)
		{
			IProtocol protocol = WindowUtility<Widget>.LastConversationProtocol;
			
			if (protocol != null)
			{
				string protocolCondition = conditionNode.GetAttribute ("protocol");
				
				if (String.IsNullOrEmpty (protocolCondition))
					return true;
				else
				{
					// Adding ability to specify multiple protocols.
					string[] protocols = protocolCondition.Split(',');
					
					for (int i = 0; i < protocols.Length; i++)
						if (protocols[i].ToLower() == protocol.Name.ToLower())
							return true;
				}
			}

			return false;
		}
	}
}