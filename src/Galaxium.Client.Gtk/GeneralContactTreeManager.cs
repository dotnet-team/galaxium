/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Text;

using Gdk;
using Gtk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;

namespace Galaxium.Client.GtkGui
{
	public class GeneralContactTreeManager : BasicContactTreeManager
	{
		Dictionary<IProtocol, IContactTreeManager> _treeManagers = new Dictionary<IProtocol, IContactTreeManager> ();
		
		IContactTreeManager GetManager (IProtocol protocol)
		{
			if (_treeManagers.ContainsKey (protocol))
				return _treeManagers[protocol];
			
			foreach (ContactTreeManagerExtension node in AddinUtility.GetExtensionNodes("/Galaxium/Gui/ContactTreeManagers"))
			{
				if (Array.IndexOf (node.Protocol.Split (','), protocol.Name) >= 0)
				{
					// We know this extension should be used for this contact.
					IContactTreeManager manager = node.CreateInstance () as IContactTreeManager;
					_treeManagers.Add (protocol, manager);
					return manager;
				}
			}
			
			Log.Warn ("Unable to find contact tree manager for protocol '{0}'", protocol.Name);
			
			return null;
		}
		
		public override void RenderText (object data, CellRendererContact renderer)
		{
			ISession session = null;
			IContactTreeManager manager = null;
			
			if (data is IContact)
				session = (data as IContact).Session;
			else if (data is ContactTreeRealGroup)
				session = (data as ContactTreeRealGroup).Group.Session;
			
			if (session != null)
				manager = GetManager (session.Account.Protocol);
			
			if (manager != null)
			{
				manager.Session = session;
				manager.RenderText (data, renderer);
			}
			else
				base.RenderText (data, renderer);
		}
		
		public override void RenderLeftImage (object data, CellRendererPixbuf renderer)
		{
			ISession session = null;
			IContactTreeManager manager = null;
			
			if (data is IContact)
				session = (data as IContact).Session;
			else if (data is ContactTreeRealGroup)
				session = (data as ContactTreeRealGroup).Group.Session;
			
			if (session != null)
				manager = GetManager (session.Account.Protocol);
			
			if (manager != null)
			{
				manager.Session = session;
				manager.RenderLeftImage (data, renderer);
			}
			else
				base.RenderLeftImage (data, renderer);
		}
		
		public override void RenderRightImage (object data, CellRendererPixbuf renderer)
		{
			ISession session = null;
			IContactTreeManager manager = null;
			
			if (data is IContact)
				session = (data as IContact).Session;
			else if (data is ContactTreeRealGroup)
				session = (data as ContactTreeRealGroup).Group.Session;
			
			if (session != null)
				manager = GetManager (session.Account.Protocol);
			
			if (manager != null)
			{
				manager.Session = session;
				manager.RenderRightImage (data, renderer);
			}
			else
				base.RenderRightImage (data, renderer);
		}
		
		public override string GetMenuExtensionPoint (object data)
		{
			return base.GetMenuExtensionPoint (data);
		}
				
		public override InfoTooltip GetTooltip (object data)
		{
			return base.GetTooltip (data);
		}
		
		public override int Compare (object data1, object data2)
		{
			return base.Compare (data1, data2);
		}
	}
}
