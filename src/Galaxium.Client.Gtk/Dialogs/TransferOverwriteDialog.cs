/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Gui;
using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public sealed class TransferOverwriteDialog
	{
		[Widget ("TransferOverwriteDialog")]
		private Dialog _dialog;
		[Widget ("dialogImage")]
		private Image _imgDialog;
		[Widget ("labelCountdown")]
		private Label _labelCountdown;
		[Widget ("labelFilename")]
		private Label _labelFilename;
		[Widget ("labelIncomingSize")]
		private Label _labelIncomingSize;
		[Widget ("labelExistingSize")]
		private Label _labelExistingSize;
		[Widget ("buttonReject")]
		private Button _buttonReject;
		[Widget ("buttonOverwrite")]
		private Button _buttonOverwrite;
		[Widget ("buttonRename")]
		private Button _buttonRename;
	
	// CHECKTHIS
	//	private IFileTransfer _transfer;
		private uint _countdownTimer;
		private int _countdown = 30;
		
		public TransferOverwriteDialog (IFileTransfer transfer, string destinationFolder, bool autoaccept)
		{
		//	_transfer = transfer;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (TransferOverwriteDialog).Assembly, "TransferOverwriteDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-warning", IconSizes.Small);
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-warning", IconSizes.Large);
			
			_labelFilename.Text = transfer.FileName;
			_labelIncomingSize.Text = transfer.TotalBytes.ToString() + GettextCatalog.GetString (" Bytes");
			
			FileInfo info = new FileInfo(Path.Combine(transfer.Destination, transfer.FileName));
			_labelExistingSize.Text = info.Length + GettextCatalog.GetString (" Bytes");
			
			_labelCountdown.Text = GettextCatalog.GetPluralString ("You have {0} second left to choose...", "You have {0} seconds left to choose...", _countdown);
			_labelCountdown.Visible = autoaccept;
			
			if (autoaccept)
				_countdownTimer = TimerUtility.RequestInfiniteCallback (CountdownTimerElapsed, 1000);
			
			_dialog.Show();
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		private void CountdownTimerElapsed ()
		{
			_labelCountdown.Text = GettextCatalog.GetPluralString ("You have {0} second left to choose...",
				"You have {0} seconds left to choose...", _countdown);
			
			if (--_countdown <= 0)
			{
				TimerUtility.RemoveCallback (_countdownTimer);
				_countdownTimer = 0;
				
				switch (FileTransferUtility.OverwriteDefault)
				{
					case DefaultOverwriteState.Decline:
						_buttonReject.Click ();
						break;
					
					case DefaultOverwriteState.Overwrite:
						_buttonOverwrite.Click ();
						break;
					
					case DefaultOverwriteState.Rename:
						_buttonRename.Click ();
						break;
				}
			}
		}
	}
}