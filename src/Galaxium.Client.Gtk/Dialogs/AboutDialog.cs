/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Reflection;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Client.GtkGui
{
	public class AboutDialog
	{
		[Widget ("treeVersions")]
		private TreeView _treeVersions;
		[Widget ("AboutDialog")]
		private Window _dialog;
		[Widget ("image")]
		private Image _image;
		[Widget ("image2")]
		private Image _image2;

		public AboutDialog ()
		{
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (AboutDialog).Assembly, "AboutDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			_dialog.DeleteEvent += OnWindowDelete;
			(gxml ["btnClose"] as Button).Clicked += OnCloseClicked;
			
			GtkUtility.EnableComposite (_dialog);

			_dialog.Icon = IconUtility.GetIcon ("galaxium-about", IconSizes.Small);

			_image.Pixbuf = IconUtility.GetIcon ("galaxium-about", IconSizes.Other);
			_image2.Pixbuf = IconUtility.GetIcon ("galaxium-mono", IconSizes.Other);
			
			_treeVersions.AppendColumn ("Name", new CellRendererText (), "text", 0);
			_treeVersions.AppendColumn ("Version", new CellRendererText (), "text", 1);
			
			ListStore versionStore = new ListStore (typeof (string), typeof (string));
			
			foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies ())
			{
				AssemblyName name = asm.GetName ();
				versionStore.AppendValues (name.Name, name.Version.ToString ());
			}
			
			versionStore.SetSortColumnId (0, SortType.Ascending);
			
			_treeVersions.Model = versionStore;
			
			_dialog.ShowAll ();
		}

		private void OnCloseClicked (object sender, EventArgs args)
		{
			_dialog.Destroy ();
		}

		private void OnWindowDelete (object sender, DeleteEventArgs args)
		{
			_dialog.Destroy ();
			args.RetVal = false;
		}
	}
}