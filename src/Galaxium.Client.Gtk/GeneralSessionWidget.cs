/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Text;

using Gtk;
using Gdk;
using Glade;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Client.GtkGui
{
	public class GeneralSessionWidget : BasicSessionWidget
	{
		internal GeneralSessionWidget (IControllerWindow<Widget> window, GeneralSession session) : base (window, session)
		{
			
		}
		
		public override void Initialize ()
		{
			base.Initialize ();
			
			// We need to setup a Jabber tree manager for our tree
			_tree_view.Manager = new GeneralContactTreeManager ();
			
			// We have to insert the protocol specific statuses
			_status_combo.Append (GeneralPresence.Online);
			_status_combo.Append (GeneralPresence.Away);
			_status_combo.Append (GeneralPresence.Busy);
			_status_combo.Append (GeneralPresence.Invisible);
			
			// Make sure everything is positioned properly.
			Update ();
		}
		
		public override IConversation CreateUsableConversation (IContact contact)
		{
			IConversation conversation = null;
			
			return conversation;
		}
		
		protected override string PresenceTextLookup (IPresence item)
		{
			return item.State;
		}
		
		protected override Gdk.Pixbuf PresenceImageLookup (IPresence item, IIconSize size)
		{
			return IconUtility.StatusLookup (item, size);
		}
		
		protected override void SetDisplayImage ()
		{
			//new SetDisplayImageCommand().Run();
		}
	}
}