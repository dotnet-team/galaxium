/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

namespace Galaxium.Client.GtkGui
{
	public class GtkWindowUtility : IWindowUtility<Widget>
	{
		private IConfigurationSection _config = Configuration.Conversation.Section;
		private IWindowManager<Widget> _windowManager;
		
		private IProtocol _lastConversationProtocol; 
		
		public GtkWindowUtility ()
		{
			_windowManager = new GtkWindowManager ();
		}
		
		public IWindowManager<Widget> WindowManager
		{
			get { return _windowManager; }
		}
		
		public IProtocol LastConversationProtocol
		{
			get { return _lastConversationProtocol; }
			set { _lastConversationProtocol = value; }
		}
		
		public void CloseAll ()
		{
			_windowManager.Destroy ();
		}
		
		public void UpdateAll ()
		{
			foreach (IContainerWindow<Widget> window in _windowManager.Windows)
				window.Update ();
		}
		
		public void Activate (IConversation conversation, bool requested)
		{
			if (conversation == null)
				throw new ArgumentNullException ("conversation");
			
			IContainerWindow<Widget> window = _windowManager.GetContainerWindow (conversation);
			
			if (window != null)
				Activate (window, conversation, requested);
		}
		
		private void Activate (IContainerWindow<Widget> window, IConversation conversation, bool requested)
		{
			window.WindowClosed += new EventHandler (ContainerWindowClosed);
			
			if (!window.ContainsConversation (conversation))
				window.AddConversation (conversation);
			else
				window.ShowConversation (conversation);
			
			if (requested)
			{
				window.ShowConversation (conversation);
				window.Show (false);
			}
			else
				window.Show (_config.GetBool (Configuration.Conversation.WindowMinimized.Name, Configuration.Conversation.WindowMinimized.Default));
		}
		
		private void ContainerWindowClosed (object sender, EventArgs args)
		{
			IContainerWindow<Widget> window = (IContainerWindow<Widget>)sender;
			
			_windowManager.RemoveContainerWindow (window);
			
			window.Destroy ();
		}
		
		public void RemoveWindow (IContainerWindow<Widget> window)
		{
			_windowManager.RemoveContainerWindow (window);
		}
		
		public void RemoveWindows (ISession session)
		{
			List<IContainerWindow<Widget>> clear = new List<IContainerWindow<Widget>> ();
			
			lock (_windowManager.Windows)
			{
				foreach (IContainerWindow<Widget> window in _windowManager.Windows)
				{
					window.RemoveConversations (session, false);
					
					if (window.ConversationCount == 0)
						clear.Add (window);
				}
			}
			
			foreach (IContainerWindow<Widget> window in clear)
			{
				window.Close(true);
				RemoveWindow (window);
			}
		}
		
		public IContainerWindow<Widget> GetWindow ()
		{
			foreach (IContainerWindow<Widget> window in _windowManager.Windows)
				return window;
			
			return null;
		}
		
		public IContainerWindow<Widget> GetWindow (IConversation conversation)
		{
			foreach (IContainerWindow<Widget> window in _windowManager.Windows)
				if (window.ContainsConversation (conversation))
					return window;
			
			return null;
		}
		
		// CHECKTHIS
		
	//	private void Organize ()
	//	{
	//		List<IChatWidget<Widget>> widgets = new List<IChatWidget<Widget>> ();
	//		
	//		foreach (IContainerWindow<Widget> window in _windowManager.Windows)
	//			foreach (IChatWidget<Widget> widget in window.Widgets)
	//				widgets.Add (widget);
	//		
	//		_windowManager.Clear ();
	//		
	//		foreach (IChatWidget<Widget> widget in widgets)
	//		{
	//			IContainerWindow<Widget> window = _windowManager.GetContainerWindow (widget.Conversation);
	//			
	//			window.WindowClosed += new EventHandler (ContainerWindowClosed);
	//			window.AddWidget (widget);
	//			
	//			window.Show (false);
	//		}
	//	}
		
		internal class GtkWindowManager : AbstractWindowManager<Widget>
		{
			public GtkWindowManager () : base ()
			{
				
			}
			
			protected override IContainerWindow<Widget> CreateContainerWindow (string uid, ISession session, IConversation conversation)
			{
				return new ContainerWindow (uid);
			}
		}
	}
}