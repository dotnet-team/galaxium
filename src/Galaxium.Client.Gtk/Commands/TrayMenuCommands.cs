/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;

using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public class ShowHideMainWindowCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			GalaxiumUtility.MainWindow.Visible = !GalaxiumUtility.MainWindow.Visible;
		}
	}
	
	public class MuteSoundsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			
		}
	}
	
	public class QueueAlwaysCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			RadioMenuItem item = MenuItem as RadioMenuItem;
			item.Active = Configuration.Conversation.Section.GetBool (Configuration.Conversation.QueueAlways.Name, Configuration.Conversation.QueueAlways.Default);
		}
		
		public override void Run ()
		{
			RadioMenuItem item = MenuItem as RadioMenuItem;
			Configuration.Conversation.Section.SetBool (Configuration.Conversation.QueueAlways.Name, item.Active);
		}
	}
	
	public class QueueWhenAwayCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			RadioMenuItem item = MenuItem as RadioMenuItem;
			item.Active = Configuration.Conversation.Section.GetBool (Configuration.Conversation.QueueWhenAway.Name, Configuration.Conversation.QueueWhenAway.Default);
		}
		
		public override void Run ()
		{
			RadioMenuItem item = MenuItem as RadioMenuItem;
			Configuration.Conversation.Section.SetBool (Configuration.Conversation.QueueWhenAway.Name, item.Active);
		}
	}
	
	public class QueueNeverCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			RadioMenuItem item = MenuItem as RadioMenuItem;
			item.Active = Configuration.Conversation.Section.GetBool (Configuration.Conversation.QueueNever.Name, Configuration.Conversation.QueueNever.Default);
		}
		
		public override void Run ()
		{
			RadioMenuItem item = MenuItem as RadioMenuItem;
			Configuration.Conversation.Section.SetBool (Configuration.Conversation.QueueNever.Name, item.Active);
		}
	}
	
	public class SetPresenceCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			MenuItem item = MenuItem as MenuItem;
			MenuItemExtension ext = MenuItemExtension.Extensions[item];
			
			BasePresence presence = BasePresence.Unknown;
			
			try
			{
				presence = (BasePresence)Enum.Parse(typeof(BasePresence), ext.Id, true);
			}
			catch (Exception)
			{
				return;
			}
			
			foreach (ISession session in SessionUtility.Sessions)
				session.SetPresence(presence);
		}
	}
}
