/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Gui;

namespace Galaxium.Client.GtkGui
{
	public abstract class DefaultMenuCommand : AbstractMenuCommand
	{
		public abstract void SetDefaultMenuItem ();
		protected bool _using_defaults = false;
		
		public override void SetMenuItem ()
		{
			if (SessionUtility.ActiveSession == null)
				return;
			
			if (MenuItem == null)
				return;
			
			_using_defaults = SessionUtility.ActiveSession.Account.UseDefaultListView;
			
			SetDefaultMenuItem ();
		}
		
		public abstract void RunDefault ();
		
		public override void Run ()
		{
			if (MenuItem == null && SessionUtility.ActiveSession == null)
				return;
			
			RunDefault ();
		}
	}
}
