/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Gui;

namespace Galaxium.Client.GtkGui
{
	public class ShowProfileDetailsCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowProfileDetails.Name, Configuration.ContactList.ShowProfileDetails.Default) : SessionUtility.ActiveSession.Account.ShowProfileDetails;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowProfileDetails.Name, (MenuItem as CheckMenuItem).Active);
				SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("ShowProfileDetails"));
			}
			else
				SessionUtility.ActiveSession.Account.ShowProfileDetails = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class DetailCompactCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = (_using_defaults ? (ContactTreeDetailLevel)Configuration.ContactList.Section.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default) : SessionUtility.ActiveSession.Account.DetailLevel) == ContactTreeDetailLevel.Compact;
		}
		
		public override void RunDefault ()
		{
			if ((MenuItem as RadioMenuItem).Active)
			{
				if (_using_defaults)
				{
					Configuration.ContactList.Section.SetInt (Configuration.ContactList.DetailLevel.Name, (int)ContactTreeDetailLevel.Compact);
					SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("DetailLevel"));
				}
				else
					SessionUtility.ActiveSession.Account.DetailLevel = ContactTreeDetailLevel.Compact;
			}
		}
	}

	public class DetailNormalCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = (_using_defaults ? (ContactTreeDetailLevel)Configuration.ContactList.Section.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default) : SessionUtility.ActiveSession.Account.DetailLevel) == ContactTreeDetailLevel.Normal;
		}
		
		public override void RunDefault ()
		{
			if ((MenuItem as RadioMenuItem).Active)
			{
				if (_using_defaults)
				{
					Configuration.ContactList.Section.SetInt (Configuration.ContactList.DetailLevel.Name, (int)ContactTreeDetailLevel.Normal);
					SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("DetailLevel"));
				}
				else
					SessionUtility.ActiveSession.Account.DetailLevel = ContactTreeDetailLevel.Normal;
			}
		}
	}

	public class DetailDetailedCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = (_using_defaults ? (ContactTreeDetailLevel)Configuration.ContactList.Section.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default) : SessionUtility.ActiveSession.Account.DetailLevel) == ContactTreeDetailLevel.Detailed;
		}
		
		public override void RunDefault ()
		{
			if ((MenuItem as RadioMenuItem).Active)
			{
				if (_using_defaults)
				{
					Configuration.ContactList.Section.SetInt (Configuration.ContactList.DetailLevel.Name, (int)ContactTreeDetailLevel.Detailed);
					SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("DetailLevel"));
				}
				else
					SessionUtility.ActiveSession.Account.DetailLevel = ContactTreeDetailLevel.Detailed;
			}
		}
	}

	public class SortByAlphabetCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.SortAlphabetic.Name, Configuration.ContactList.SortAlphabetic.Default) : SessionUtility.ActiveSession.Account.SortAlphabetic;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.SortAlphabetic.Name, (MenuItem as RadioMenuItem).Active);
				SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("SortAlphabetic"));
			}
			else
				SessionUtility.ActiveSession.Account.SortAlphabetic = (MenuItem as RadioMenuItem).Active;
		}
	}

	public class SortByStatusCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.SortStatus.Name, Configuration.ContactList.SortStatus.Default) : !SessionUtility.ActiveSession.Account.SortAlphabetic;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.SortStatus.Name, (MenuItem as RadioMenuItem).Active);
				SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("SortAlphabetic"));
			}
			else
				SessionUtility.ActiveSession.Account.SortAlphabetic = !(MenuItem as RadioMenuItem).Active;
		}
	}

	public class SortAscendingCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.SortAscending.Name, Configuration.ContactList.SortAscending.Default) : SessionUtility.ActiveSession.Account.SortAscending;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.SortAscending.Name, (MenuItem as RadioMenuItem).Active);
				SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("SortAscending"));
			}
			else
				SessionUtility.ActiveSession.Account.SortAscending = (MenuItem as RadioMenuItem).Active;
		}
	}

	public class SortDescendingCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.SortDescending.Name, Configuration.ContactList.SortDescending.Default) : !SessionUtility.ActiveSession.Account.SortAscending;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.SortDescending.Name, (MenuItem as RadioMenuItem).Active);
				SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("SortAscending"));
			}
			else
				SessionUtility.ActiveSession.Account.SortAscending = !(MenuItem as RadioMenuItem).Active;
		}
	}
}