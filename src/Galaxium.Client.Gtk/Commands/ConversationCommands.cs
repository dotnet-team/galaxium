/*
 * Galaxium Messenger
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Galaxium.Gui;
using Galaxium.Protocol;

using Gtk;

namespace Galaxium.Client.GtkGui
{
	public class AssimilateEmoticonCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			IEmoticon emot = (IEmoticon)Context.Object;
			Gtk.MenuItem item = (Gtk.MenuItem)MenuItem;
			
			item.Sensitive = (emot is ICustomEmoticon) && !EmoticonUtility.HaveCustom (emot as ICustomEmoticon);
		}
		
		public override void Run ()
		{
			IEmoticon emot = (IEmoticon)Context.Object;
			EmoticonUtility.AddCustom (emot.Name, emot.Equivalents, emot.Filename, null, null);
		}
	}
}
