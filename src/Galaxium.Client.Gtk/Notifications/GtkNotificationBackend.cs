/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Notifications;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

// TODO: reevaluate notifications' layout and use append-hint for the new Ubuntu 9.04

namespace Galaxium.Client.GtkGui
{
	public class GtkNotificationBackend : INotificationBackend, ITrayListener
	{
		private static GtkTrayWidget _trayWidget;
		private static bool _supress = false;

		private static bool _actions_supported;
		
		private bool _notifying;
		
		public static bool Supress { get { return(_supress); } set { _supress = value; } }
		
		private Queue<Notification> _notifications = new Queue<Notification> ();
		private Dictionary<string,INotification> _notificationActions = new Dictionary<string,INotification> ();
		
		static GtkNotificationBackend ()
		{
			_actions_supported = Array.IndexOf (Notifications.Global.Capabilities, "actions") != -1;
		}
		
		public void Initialize ()
		{
			_trayWidget = new GtkTrayWidget(this);
			
			// Listen to the activity utility's events.
			GtkActivityUtility.ActivityUpdated += ActivityUpdated;
			
			ShowIcon (IconUtility.GetIcon("galaxium-tray-offline"));
		}
		
		public void Unload ()
		{
			
		}
		
		private void ActivityUpdated (object sender, EventArgs args)
		{
			UpdateFlashingIcon();
		}
		
		private void UpdateFlashingIcon ()
		{
			if (GtkActivityUtility.Pending)
			{
				IActivity activity = GtkActivityUtility.PeekQueueItem ();
				
				if(activity != null)
				{
					switch (activity.Type)
					{
						case ActivityTypes.Alert:
							ShowIcon (IconUtility.GetIcon("galaxium-tray-alert"));
							break;
						case ActivityTypes.Message:
							ShowIcon (IconUtility.GetIcon("galaxium-tray-unread"));
							break;
						case ActivityTypes.Transfer:
							ShowIcon (IconUtility.GetIcon("galaxium-tray-transfer"));
							break;
					}
					
					_trayWidget.Blinking = true;
				}
			}
			else
			{
				UpdateStatus ();
				
				_trayWidget.Blinking = false;
			}
		}
		
		public void TrayActivated ()
		{
			if (GtkActivityUtility.Pending)
			{
				UpdateStatus ();
				_trayWidget.Blinking = false;
				
				GtkActivityUtility.ProcessQueue ();
				
				UpdateFlashingIcon ();
			}
			else
				GalaxiumUtility.MainWindow.Visible = !GalaxiumUtility.MainWindow.Visible;
			
			if (!GtkActivityUtility.Pending)
			{
				UpdateStatus ();
				
				_trayWidget.Blinking = false;
			}
		}
		
		public void TrayPopupActivated ()
		{
			Gtk.Menu menu = MenuUtility.CreateContextMenu("/Galaxium/Gui/Tray/Menu", new DefaultExtensionContext());
			
			_trayWidget.ShowMenu(menu);
		}
		
		public void ShowIcon (Gdk.Pixbuf pixbuf)
		{
			_trayWidget.Pixbuf = pixbuf;
		}
		
		private BasePresence EvaluateStatus (List<IPresence> presences)
		{
			// Evaluating the status should show the lowest status before offline.
			IPresence statusPresence = Galaxium.Protocol.UnknownPresence.Instance;
			
			foreach (IPresence presence in presences)
			{
				if (presence.BasePresence < statusPresence.BasePresence && presence.BasePresence != BasePresence.Offline)
					statusPresence = presence;
			}
			
			return statusPresence.BasePresence;
		}
		
		public void UpdateStatus ()
		{
			List<IPresence> list = new List<IPresence> ();
			
			foreach (ISession session in SessionUtility.Sessions)
				list.Add(session.Account.Presence);
			
			switch (EvaluateStatus (list))
			{
				case BasePresence.Online:
					ShowIcon(IconUtility.GetIcon("galaxium-tray-online", IconSizes.Other));
					break;
				case BasePresence.Offline:
					ShowIcon(IconUtility.GetIcon("galaxium-tray-offline", IconSizes.Other));
					break;
				
				case BasePresence.Away:
					ShowIcon(IconUtility.GetIcon("galaxium-tray-away", IconSizes.Other));
					break;
				
				case BasePresence.Busy:
					ShowIcon(IconUtility.GetIcon("galaxium-tray-busy", IconSizes.Other));
					break;
				
				case BasePresence.Invisible:
					ShowIcon(IconUtility.GetIcon("galaxium-tray-invisible", IconSizes.Other));
					break;
				
				case BasePresence.Idle:
					ShowIcon(IconUtility.GetIcon("galaxium-tray-idle", IconSizes.Other));
					break;
				
				case BasePresence.Unknown:
					ShowIcon(IconUtility.GetIcon("galaxium-tray-offline", IconSizes.Other));
					break;
				
				default:
					ShowIcon(IconUtility.GetIcon("galaxium-tray-offline", IconSizes.Other));
					break;
			}
		}
		
		public void ShowNotification (INotification notification)
		{
			ShowNotificationReal(notification, null);
		}
		
		public void ShowNotification (INotification notification, int displayTime)
		{
			ShowNotificationReal(notification, displayTime);
		}
		
		static int i = 0;
		
		public void ShowNotificationReal (INotification notification, int? displayTime)
		{
			Notification notifyobject = new Notification(notification.Title, notification.Body);
			
			notifyobject.Urgency = Urgency.Low;
			try {
				notifyobject.Icon = new Gdk.Pixbuf (notification.Icon);
			}
			catch {
				Log.Warn ("Unable to generate pixbuf from byte array from notification object.");
			}
			
			// Generate a new identity for this action.
			notification.Action = notification.Action + (++i).ToString();
			
			if (i > 50) i = 0;
			
			if (_actions_supported) {
				// Attach the action button to this notification, if it is supported
				notifyobject.AddAction (notification.Action, "Show", new ActionHandler (ActionHandle));
			}
			
			notifyobject.Closed += (s, e) => _notificationActions.Remove (notification.Action);			
			
			// Register the notification within our action hash.
			_notificationActions [notification.Action] = notification;
			
			// If we are already showing a notification, there is really no sense in showing another
			// one right on top. We should wait until the timeout is over, and then display the next etc
		
			var bubble = true; // TODO: give option to disable tray-bubble-like behavior
			
			if (bubble) {
				Gdk.Screen screen;
				Gdk.Rectangle area;
				_trayWidget.GetPosition(out screen, out area);
				notifyobject.SetGeometryHints(screen, area.X + (area.Width / 2), area.Y + (area.Height / 2));
				if (displayTime.HasValue)
					notifyobject.Timeout = displayTime.Value;
				notifyobject.Closed += (sender, e) => NextNotification ();
				_notifications.Enqueue (notifyobject);
				if (_notifying) return;
				NextNotification ();
			} else
				ThreadUtility.Dispatch (() => notifyobject.Show ());
		}

		private void NextNotification ()
		{
			_notifying = _notifications.Count != 0;
			if (_notifying)
				_notifications.Dequeue ().Show ();
		}
		
		private void ActionHandle (object sender, Notifications.ActionArgs args)
		{
			// At this point all we know is the action name,
			// we have to process that accordingly.
			if (_notificationActions.ContainsKey (args.Action))
			{
				var notification = _notificationActions[args.Action] as GtkNotification;
				
				// We will need to push this activity forward and process it.
				GtkActivityUtility.PushQueueItem (notification.Activity);
				
				_notificationActions.Remove (args.Action);
			}
			else
				Log.Error ("The action name was not present in the dictionary!");
		}
	}
}