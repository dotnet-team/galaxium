/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public class SongInformationPreferenceWidget : IPreferenceWidget<Widget>
	{
		[Widget ("buttonbox")]
		private VButtonBox _buttonBox;
		
		[Widget ("scrolledwindow")]
		private ScrolledWindow _scrolledWindow;
		[Widget ("listBackends")]
		private TreeView _listBackends;
		private ListStore _store;
		
		[Widget ("checkEnableSongInformation")]
		private CheckButton _checkEnableSongInformation;
		[Widget ("checkPollMusicPlayers")]
		private CheckButton _checkPollMusicPlayers;
		
		[Widget ("buttonSelectAll")]
		private Button _buttonSelectAll;
		[Widget ("buttonSelectNone")]
		private Button _buttonSelectNone;
		
		private Widget _nativeWidget;
		
		public void Initialize ()
		{
			_nativeWidget = GladeUtility.ExtractWidget<Widget> (GladeUtility.GetGladeResourceStream (typeof (SongInformationPreferenceWidget).Assembly, "SongInformationPreferenceWidget.glade"), "vbox", this);
			
			_buttonSelectAll.Clicked += SelectAllClicked;
			_buttonSelectNone.Clicked += SelectNoneClicked;
			_checkEnableSongInformation.Toggled += EnableSongInformationToggled;
			_checkPollMusicPlayers.Toggled += PollMusicPlayersToggled;
			
			_checkEnableSongInformation.Active = SongPlayingUtility.EnableSongInformation;
			_checkPollMusicPlayers.Active = SongPlayingUtility.PollAllBackends;
			ChangeListSensitiveState (!SongPlayingUtility.PollAllBackends);
			
			_store = new ListStore (typeof (bool), typeof (string), typeof (object));
			_listBackends.Model = _store;
			
			CellRendererToggle toggleRenderer = new CellRendererToggle ();
			toggleRenderer.Activatable = true;
			toggleRenderer.Toggled += new ToggledHandler (BackendToggled);
			
			CellRendererText nameRenderer = new CellRendererText ();
			TreeViewColumn nameColumn = new TreeViewColumn();
			nameColumn.Title = GettextCatalog.GetString ("Name");
			nameColumn.PackStart (nameRenderer, true);
			nameColumn.AddAttribute (nameRenderer, "text", 1);
			
			TreeViewColumn toggleColumn = new TreeViewColumn();
			toggleColumn.Title = GettextCatalog.GetString ("Active");
			toggleColumn.PackStart (toggleRenderer, true);
			toggleColumn.AddAttribute (toggleRenderer, "active", 0);

			_listBackends.AppendColumn (toggleColumn);
			_listBackends.AppendColumn (nameColumn);

			_store.SetSortFunc (1, new TreeIterCompareFunc (SortTextFunc));
			_store.SetSortColumnId (1, SortType.Ascending);
			
			foreach (ISongInformationBackend backend in SongPlayingUtility.Backends) {
				bool enabled = SongPlayingUtility.IsBackendEnabled (backend);
				_store.AppendValues (enabled, backend.Name, backend);
			}
			
			_nativeWidget.ShowAll ();
		}
		
		public Widget NativeWidget
		{
			get { return _nativeWidget; }
		}
		
		private void SelectAllClicked (object sender, EventArgs args)
		{
			ChangeBackendStates (true);
		}
		
		private void SelectNoneClicked (object sender, EventArgs args)
		{
			ChangeBackendStates (false);
		}
		
		private void EnableSongInformationToggled (object sender, EventArgs args)
		{
			SongPlayingUtility.EnableSongInformation = _checkEnableSongInformation.Active;
		}
		
		private void PollMusicPlayersToggled (object sender, EventArgs args)
		{
			SongPlayingUtility.PollAllBackends = _checkPollMusicPlayers.Active;
			ChangeListSensitiveState (!SongPlayingUtility.PollAllBackends);
		}
		
		private void ChangeBackendStates (bool enabled)
		{
			TreeIter iter;
			if (_store.GetIterFirst (out iter)) {
				do {
					ISongInformationBackend backend = _store.GetValue (iter, 2) as ISongInformationBackend;
					_store.SetValue (iter, 0, enabled);
					SongPlayingUtility.ChangeBackendState (backend, enabled);
				} while (_store.IterNext (ref iter));
			}
		}
		
		private int SortTextFunc (TreeModel model, TreeIter iter1, TreeIter iter2)
		{
			string name1 = model.GetValue (iter1, 1) as string;
			string name2 = model.GetValue (iter2, 1) as string;
		
			if (name1 == null && name2 == null) return 0;
			else if (name1 == null) return -1;
			else if (name2 == null) return 1;

			return name1.CompareTo (name2);
		}
		
		private void BackendToggled (object sender, ToggledArgs args)
		{
	 		TreeIter iter;
			if (_store.GetIterFromString (out iter, args.Path)) {
	 			bool val = (bool) _store.GetValue (iter, 0);
	 			_store.SetValue (iter, 0, !val);
				
				ISongInformationBackend backend = _store.GetValue (iter, 2) as ISongInformationBackend;
				SongPlayingUtility.ChangeBackendState (backend, !val);
	 		}
		}
		
		private void ChangeListSensitiveState (bool sensitive)
		{
			_buttonBox.Sensitive = sensitive;
			_scrolledWindow.Sensitive = sensitive;
		}
	}
}