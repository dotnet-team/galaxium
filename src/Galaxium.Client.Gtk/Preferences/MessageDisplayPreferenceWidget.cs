/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gdk;
using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;

using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public class MessageDisplayPreferenceWidget: IPreferenceWidget<Widget>
	{
		[Widget("vbDisplays")] VBox _vbDisplays;
		[Widget("swDisplayPrefs")] ScrolledWindow _swDisplayPrefs;
		
		Widget _nativeWidget;
		ImageComboBox<MessageDisplayExtension> _cbDisplays;
		IConfigurationSection _config;
		
		public void Initialize ()
		{
			_nativeWidget = GladeUtility.ExtractWidget<Widget> (GladeUtility.GetGladeResourceStream (typeof (GtkPreferenceWidget).Assembly, "MessageDisplayPreferenceWidget.glade"), "widget", this);
			
			_config = Configuration.MessageDisplay.Section;
			
			_cbDisplays = new ImageComboBox<MessageDisplayExtension>(new ImageComboTextLookup<MessageDisplayExtension>(DisplayTextLookup), new ImageComboPixbufLookup<MessageDisplayExtension>(DisplayImageLookup));
			_cbDisplays.Changed += DisplayChanged;
			
			_vbDisplays.PackStart(_cbDisplays, false, true, 0);
			
			// Load up the combos to include the themes available
			foreach (MessageDisplayExtension node in AddinUtility.GetExtensionNodes("/Galaxium/Gui/Widgets/MessageDisplay"))
			{
				_cbDisplays.Append(node);
				
				if (node.Id == _config.GetString(Configuration.MessageDisplay.Active.Name, Configuration.MessageDisplay.Active.Default))
					_cbDisplays.Select(node);
			}
			
			_nativeWidget.ShowAll ();
			
			_nativeWidget.Unrealized += delegate
			{
				if (_swDisplayPrefs.Child != null)
					_swDisplayPrefs.Remove(_swDisplayPrefs.Child);
			};
		}
		
		private void DisplayChanged (object sender, EventArgs args)
		{
			MessageDisplayExtension mde = _cbDisplays.GetSelectedItem();
			
			_config.SetString(Configuration.MessageDisplay.Active.Name, mde.Id);
			
			if (_swDisplayPrefs.Child != null)
				_swDisplayPrefs.Remove(_swDisplayPrefs.Child);
			
			MessageDisplayPreferencesExtension mdpe = AddinUtility.GetExtensionNode<MessageDisplayPreferencesExtension>("/Galaxium/Gui/Widgets/MessageDisplayPreference", mde.Id, false);
			
			if (mdpe != null)
			{
				IMessageDisplayPreferences mdp = mdpe.CreateInstance() as IMessageDisplayPreferences;
				
				if (mdp != null)
					_swDisplayPrefs.Add((Widget)mdp.Widget);
				else
					_swDisplayPrefs.Add(new Label(GettextCatalog.GetString ("Error")));

				_swDisplayPrefs.ShowAll();
			}
		}
		
		private string DisplayTextLookup(MessageDisplayExtension item)
		{
			return item.Name;
		}

		private Gdk.Pixbuf DisplayImageLookup(MessageDisplayExtension item, IIconSize size)
		{
			return IconUtility.GetIcon ("galaxium-preferences-themes", size);
		}

		public Widget NativeWidget
		{
			get { return _nativeWidget; }
		}
	}
}
