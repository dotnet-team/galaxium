/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Runtime.InteropServices;

using GLib;

namespace Swfdec
{
	public partial class Player : GLib.Object
	{
		Signal _advanceSignal;
		Signal _audioAddedSignal;
		Signal _audioRemovedSignal;
		Signal _fsCommandSignal;
		Signal _invalidateSignal;
		
		[CDeclCallback]
		delegate void FsCommandSignalDelegate (IntPtr arg0, IntPtr arg1, IntPtr arg2, IntPtr gch);
		
		[CDeclCallback]
		delegate void AdvanceSignalDelegate (IntPtr arg0, ulong arg1, uint arg2, IntPtr gch);
		
		[CDeclCallback]
		delegate void AudioSignalDelegate (IntPtr arg0, IntPtr arg1, IntPtr gch);
		
		[CDeclCallback]
		delegate void InvalidateSignalDelegate (IntPtr arg0, IntPtr arg1, IntPtr arg2, uint arg3, IntPtr gch);
		
		static void FsCommandSignalCallback (IntPtr arg0, IntPtr arg1, IntPtr arg2, IntPtr gch)
		{
			FsCommandEventArgs args = new FsCommandEventArgs ();
			
			try
			{
				Player player = GLib.Object.GetObject (arg0) as Player;
				
				args.Args = new object[2];
				args.Args[0] = GLib.Marshaller.Utf8PtrToString (arg1);
				args.Args[1] = GLib.Marshaller.Utf8PtrToString (arg2);

				FsCommandEventHandler handler = (FsCommandEventHandler)player._fsCommandSignal.Handler;
				handler (player, args);
			}
			catch (Exception e)
			{
				GLib.ExceptionManager.RaiseUnhandledException (e, false);
			}
		}
		
		static void AdvanceSignalCallback (IntPtr arg0, ulong arg1, uint arg2, IntPtr gch)
		{
			AdvanceEventArgs args = new AdvanceEventArgs ();
			
			try
			{
				Player player = GLib.Object.GetObject (arg0) as Player;
				
				args.Args = new object[2];
				args.Args[0] = arg1;
				args.Args[1] = arg2;

				AdvanceEventHandler handler = (AdvanceEventHandler)player._advanceSignal.Handler;
				handler (player, args);
			}
			catch (Exception e)
			{
				GLib.ExceptionManager.RaiseUnhandledException (e, false);
			}
		}
		
		static void AudioAddedSignalCallback (IntPtr arg0, IntPtr arg1, IntPtr gch)
		{
			AudioEventArgs args = new AudioEventArgs ();
			
			try
			{
				Player player = GLib.Object.GetObject (arg0) as Player;
				
				args.Args = new object[1];
				args.Args[0] = arg1;

				AudioEventHandler handler = (AudioEventHandler)player._audioAddedSignal.Handler;
				handler (player, args);
			}
			catch (Exception e)
			{
				GLib.ExceptionManager.RaiseUnhandledException (e, false);
			}
		}
		
		static void AudioRemovedSignalCallback (IntPtr arg0, IntPtr arg1, IntPtr gch)
		{
			AudioEventArgs args = new AudioEventArgs ();
			
			try
			{
				Player player = GLib.Object.GetObject (arg0) as Player;
				
				args.Args = new object[1];
				args.Args[0] = arg1;

				AudioEventHandler handler = (AudioEventHandler)player._audioRemovedSignal.Handler;
				handler (player, args);
			}
			catch (Exception e)
			{
				GLib.ExceptionManager.RaiseUnhandledException (e, false);
			}
		}
		
		static void InvalidateSignalCallback (IntPtr arg0, IntPtr arg1, IntPtr arg2, uint arg3, IntPtr gch)
		{
			InvalidateEventArgs args = new InvalidateEventArgs ();
			
			try
			{
				Player player = GLib.Object.GetObject (arg0) as Player;
				
				args.Args = new object[3];
				args.Args[0] = arg1;
				args.Args[1] = arg2;
				args.Args[2] = arg3;

				InvalidateEventHandler handler = (InvalidateEventHandler)player._invalidateSignal.Handler;
				handler (player, args);
			}
			catch (Exception e)
			{
				GLib.ExceptionManager.RaiseUnhandledException (e, false);
			}
		}
		
		[Signal ("fscommand")]
		public event FsCommandEventHandler FsCommand
		{
			add
			{
				if (_fsCommandSignal == null)
					_fsCommandSignal = Signal.Lookup (this, "fscommand", new FsCommandSignalDelegate (FsCommandSignalCallback));
				
				_fsCommandSignal.AddDelegate (value);
			}
			remove
			{
				if (_fsCommandSignal == null)
					_fsCommandSignal = Signal.Lookup (this, "fscommand", new FsCommandSignalDelegate (FsCommandSignalCallback));
				
				_fsCommandSignal.RemoveDelegate (value);
			}
		}
		
		[Signal ("advance")]
		public event AdvanceEventHandler Advance
		{
			add
			{
				if (_advanceSignal == null)
					_advanceSignal = Signal.Lookup (this, "advance", new AdvanceSignalDelegate (AdvanceSignalCallback));

				_advanceSignal.AddDelegate (value);
			}
			remove
			{
				if (_advanceSignal == null)
					_advanceSignal = Signal.Lookup (this, "advance", new AdvanceSignalDelegate (AdvanceSignalCallback));

				_advanceSignal.RemoveDelegate (value);
			}
		}
		
		[Signal ("audio-added")]
		public event AudioEventHandler AudioAdded
		{
			add
			{
				if (_audioAddedSignal == null)
					_audioAddedSignal = Signal.Lookup (this, "audio-added", new AudioSignalDelegate (AudioAddedSignalCallback));
				
				_audioAddedSignal.AddDelegate (value);
			}
			remove
			{
				if (_audioAddedSignal == null)
					_audioAddedSignal = Signal.Lookup (this, "audio-added", new AudioSignalDelegate (AudioAddedSignalCallback));
				
				_audioAddedSignal.RemoveDelegate (value);
			}
		}
		
		[Signal ("audio-removed")]
		public event AudioEventHandler AudioRemoved
		{
			add
			{
				if (_audioRemovedSignal == null)
					_audioRemovedSignal = Signal.Lookup (this, "audio-removed", new AudioSignalDelegate (AudioRemovedSignalCallback));
				
				_audioRemovedSignal.AddDelegate (value);
			}
			remove
			{
				if (_audioRemovedSignal == null)
					_audioRemovedSignal = Signal.Lookup (this, "audio-removed", new AudioSignalDelegate (AudioRemovedSignalCallback));
				
				_audioRemovedSignal.RemoveDelegate (value);
			}
		}
		
		[Signal ("invalidate")]
		public event InvalidateEventHandler Invalidate
		{
			add
			{
				if (_invalidateSignal == null)
					_invalidateSignal = Signal.Lookup (this, "invalidate", new InvalidateSignalDelegate (InvalidateSignalCallback));
				
				_invalidateSignal.AddDelegate (value);
			}
			remove
			{
				if (_invalidateSignal == null)
					_invalidateSignal = Signal.Lookup (this, "invalidate", new InvalidateSignalDelegate (InvalidateSignalCallback));
				
				_invalidateSignal.RemoveDelegate (value);
			}
		}
	}
}
