/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using GLib;
using GTK=Gtk;
using Gtk;

namespace Swfdec.Gtk
{
	public class SwfdecWidget : Widget
	{
		public static new GType GType
		{
			get { return new GType (GtkInterop.swfdec_gtk_widget_get_type ()); }
		}
		
		[Property ("player")]
		public GtkPlayer Player
		{
			get
			{
				IntPtr handle = GtkInterop.swfdec_gtk_widget_get_player (Handle);
				return handle == IntPtr.Zero ? null : (GtkPlayer)GLib.Object.GetObject (handle);
			}
			set { GtkInterop.swfdec_gtk_widget_set_player (Handle, value == null ? IntPtr.Zero : value.Handle); }
		}
		
		[Property ("interactive")]
		public bool Interactive
		{
			get { return GtkInterop.swfdec_gtk_widget_get_interactive (Handle); }
			set { GtkInterop.swfdec_gtk_widget_set_interactive (Handle, value); }
		}
		
		/*[Property ("renderer")]
		public Cairo.SurfaceType Renderer
		{
			get { return GtkInterop.swfdec_gtk_widget_get_renderer (Handle); }
			set { GtkInterop.swfdec_gtk_widget_set_renderer (Handle, value); }
		}*/
		
		[Property ("renderer-set")]
		public bool RendererSet
		{
			get
			{
				GLib.Value val = GetProperty ("renderer-set");
				bool ret = (bool) val;
				val.Dispose ();
				return ret;
			}
			set
			{
				GLib.Value val = new GLib.Value (value);
				SetProperty ("renderer-set", val);
				val.Dispose ();
			}
		}
		
		public SwfdecWidget (IntPtr widget)
			: base (widget)
		{
		}
		
		public SwfdecWidget (GtkPlayer player)
			: this (GtkInterop.swfdec_gtk_widget_new (player.Handle))
		{
		}
		
		public SwfdecWidget (string filename)
			: this (new GtkPlayer (filename))
		{
		}
		
		public SwfdecWidget ()
			: this (new GtkPlayer ())
		{
		}
		
		public void UnsetRenderer ()
		{
			GtkInterop.swfdec_gtk_widget_unset_renderer (Handle);
		}
	}
}
