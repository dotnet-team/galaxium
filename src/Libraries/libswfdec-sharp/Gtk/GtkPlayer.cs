/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using GLib;

namespace Swfdec.Gtk
{
	public class GtkPlayer : Player
	{
		public static new GType GType
		{
			get { return new GType (GtkInterop.swfdec_gtk_player_get_type ()); }
		}
		
		[Property ("playing")]
		public bool Playing
		{
			get { return GtkInterop.swfdec_gtk_player_get_playing (Handle); }
			set { GtkInterop.swfdec_gtk_player_set_playing (Handle, value); }
		}
		
		public GtkPlayer (IntPtr handle)
			: base (handle)
		{
		}
		
		public GtkPlayer ()
			: this (GtkInterop.swfdec_gtk_player_new (IntPtr.Zero))
		{
		}
		
		public GtkPlayer (string filename)
			: this ()
		{
			Url = Url.FromInput (filename);
		}
	}
}
