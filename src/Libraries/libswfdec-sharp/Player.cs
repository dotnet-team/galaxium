/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Runtime.InteropServices;

using GLib;

namespace Swfdec
{
	public partial class Player : GLib.Object
	{
		public static new GType GType
		{
			get { return new GType (Interop.swfdec_player_get_type ()); }
		}
		
		[Property ("initialized")]
		public bool Initialized
		{
			get { return Interop.swfdec_player_is_initialized (Handle); }
		}
		
		public Url Url
		{
			get
			{
				IntPtr handle = Interop.swfdec_player_get_url (Handle);
				return handle == IntPtr.Zero ? null : (Url)GLib.Opaque.GetOpaque (handle, typeof (Url), false);
			}
			set { Interop.swfdec_player_set_url (Handle, value == null ? IntPtr.Zero : value.Handle); }
		}
		
		public Url BaseUrl
		{
			get
			{
				IntPtr handle = Interop.swfdec_player_get_base_url (Handle);
				return handle == IntPtr.Zero ? null : (Url)GLib.Opaque.GetOpaque (handle, typeof (Url), false);
			}
			set { Interop.swfdec_player_set_base_url (Handle, value == null ? IntPtr.Zero : value.Handle); }
		}
		
		[Property ("alignment")]
		public Alignment Alignment
		{
			get { return Interop.swfdec_player_get_alignment (Handle); }
			set { Interop.swfdec_player_set_alignment (Handle, value); }
		}
		
		[Property ("scale-mode")]
		public ScaleMode ScaleMode
		{
			get { return Interop.swfdec_player_get_scale_mode (Handle); }
			set { Interop.swfdec_player_set_scale_mode (Handle, value); }
		}
		
		[Property ("background-color")]
		public uint BackgroundColor
		{
			get { return Interop.swfdec_player_get_background_color (Handle); }
			set { Interop.swfdec_player_set_background_color (Handle, value); }
		}
		
		public Player (IntPtr handle)
			: base (handle)
		{
		}
		
		public Player ()
			: this (Interop.swfdec_player_new (IntPtr.Zero))
		{
		}
		
		public void Render (Cairo.Context context, double x, double y, double width, double height)
		{
			Interop.swfdec_player_render (Handle, context.Handle, x, y, width, height);
		}
		
		public void SetSize (int width, int height)
		{
			Interop.swfdec_player_set_size (Handle, width, height);
		}
	}
}
