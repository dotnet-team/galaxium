/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using GLib;

namespace Swfdec
{
	//typedef enum {
	//  SWFDEC_ALIGNMENT_TOP_LEFT,
	//  SWFDEC_ALIGNMENT_TOP,
	//  SWFDEC_ALIGNMENT_TOP_RIGHT,
	//  SWFDEC_ALIGNMENT_LEFT,
	//  SWFDEC_ALIGNMENT_CENTER,
	//  SWFDEC_ALIGNMENT_RIGHT,
	//  SWFDEC_ALIGNMENT_BOTTOM_LEFT,
	//  SWFDEC_ALIGNMENT_BOTTOM,
	//  SWFDEC_ALIGNMENT_BOTTOM_RIGHT
	//} SwfdecAlignment;
	
	
	[GType (typeof (AlignmentGType))]
	public enum Alignment
	{
		TopLeft,
		Top,
		TopRight,
		Left,
		Center,
		Right,
		BottomLeft,
		Bottom,
		BottomRight
	}

	internal class AlignmentGType
	{
		public static GType GType
		{
			get { return new GType (Interop.swfdec_alignment_get_type ()); }
		}
	}
}
