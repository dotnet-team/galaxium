/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Runtime.InteropServices;

namespace Swfdec
{
	internal static class Interop
	{
		const string DllName = "libswfdec-0.6";
		
		//void                swfdec_init                         (void);
		[DllImport (DllName)]
		internal static extern void swfdec_init ();
		
#region swfdec_player
		[DllImport(DllName)]
		internal static extern IntPtr swfdec_player_get_type();
		
		//SwfdecPlayer*       swfdec_player_new                   (SwfdecAsDebugger *debugger);
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_player_new (IntPtr debugger);
		
		//gboolean            swfdec_player_is_initialized        (SwfdecPlayer *player);
		[DllImport (DllName)]
		internal static extern bool swfdec_player_is_initialized (IntPtr player);
		
		//const SwfdecURL*    swfdec_player_get_url               (SwfdecPlayer *player);
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_player_get_url (IntPtr player);
		
		//void                swfdec_player_set_url               (SwfdecPlayer *player,
		//                                                         const SwfdecURL *url);
		[DllImport (DllName)]
		internal static extern void swfdec_player_set_url (IntPtr player, IntPtr url);
		
		//const SwfdecURL*    swfdec_player_get_base_url          (SwfdecPlayer *player);
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_player_get_base_url (IntPtr player);
		
		//void                swfdec_player_set_base_url          (SwfdecPlayer *player,
		//                                                         const SwfdecURL *url);
		[DllImport (DllName)]
		internal static extern void swfdec_player_set_base_url (IntPtr player, IntPtr url);
		
		//const char*         swfdec_player_get_variables         (SwfdecPlayer *player);
		//void                swfdec_player_set_variables         (SwfdecPlayer *player,
		//                                                         const char *variables);
		//double              swfdec_player_get_rate              (SwfdecPlayer *player);
		//void                swfdec_player_get_default_size      (SwfdecPlayer *player,
		//                                                         guint *width,
		//                                                         guint *height);
		
		//void                swfdec_player_get_size              (SwfdecPlayer *player,
		//                                                         int *width,
		//                                                         int *height);
		
		//void                swfdec_player_set_size              (SwfdecPlayer *player,
		//                                                         int width,
		//                                                         int height);
		[DllImport (DllName)]
		internal static extern void swfdec_player_set_size (IntPtr player, int width, int height);
		
		//glong               swfdec_player_get_next_event        (SwfdecPlayer *player);
		
		//guint               swfdec_player_get_background_color  (SwfdecPlayer *player);
		[DllImport (DllName)]
		internal static extern uint swfdec_player_get_background_color (IntPtr player);
		
		//void                swfdec_player_set_background_color  (SwfdecPlayer *player,
		//                                                         guint color);
		[DllImport (DllName)]
		internal static extern void swfdec_player_set_background_color (IntPtr player, uint color);
		
		//SwfdecScaleMode     swfdec_player_get_scale_mode        (SwfdecPlayer *player);
		[DllImport (DllName)]
		internal static extern ScaleMode swfdec_player_get_scale_mode (IntPtr player);
		
		//void                swfdec_player_set_scale_mode        (SwfdecPlayer *player,
		//                                                         SwfdecScaleMode mode);
		[DllImport (DllName)]
		internal static extern void swfdec_player_set_scale_mode (IntPtr player, ScaleMode mode);
		
		//SwfdecAlignment     swfdec_player_get_alignment         (SwfdecPlayer *player);
		[DllImport (DllName)]
		internal static extern Alignment swfdec_player_get_alignment (IntPtr player);
		
		//void                swfdec_player_set_alignment         (SwfdecPlayer *player,
		//                                                         SwfdecAlignment align);
		[DllImport (DllName)]
		internal static extern void swfdec_player_set_alignment (IntPtr player, Alignment align);
		
		//SwfdecPlayerScripting* swfdec_player_get_scripting      (SwfdecPlayer *player);
		//void                swfdec_player_set_scripting         (SwfdecPlayer *player,
		//                                                         SwfdecPlayerScripting *scripting);
		
		//void                swfdec_player_render                (SwfdecPlayer *player,
		//                                                         cairo_t *cr,
		//                                                         double x,
		//                                                         double y,
		//                                                         double width,
		//                                                         double height);
		[DllImport (DllName)]
		internal static extern void swfdec_player_render (IntPtr player, IntPtr cr, double x, double y, double width, double height);
		
		//gulong              swfdec_player_advance               (SwfdecPlayer *player,
		//                                                         gulong msecs);
		//gboolean            swfdec_player_mouse_move            (SwfdecPlayer *player,
		//                                                         double x,
		//                                                         double y);
		//gboolean            swfdec_player_mouse_press           (SwfdecPlayer *player,
		//                                                         double x,
		//                                                         double y,
		//                                                         guint button);
		//gboolean            swfdec_player_mouse_release         (SwfdecPlayer *player,
		//                                                         double x,
		//                                                         double y,
		//                                                         guint button);
		//gboolean            swfdec_player_key_press             (SwfdecPlayer *player,
		//                                                         guint keycode,
		//                                                         guint character);
		//gboolean            swfdec_player_key_release           (SwfdecPlayer *player,
		//                                                         guint keycode,
		//                                                         guint character);
		//void                swfdec_player_render_audio          (SwfdecPlayer *player,
		//                                                         gint16 *dest,
		//                                                         guint start_offset,
		//                                                         guint n_samples);
		//const GList*        swfdec_player_get_audio             (SwfdecPlayer *player);
		//gulong              swfdec_player_get_maximum_runtime   (SwfdecPlayer *player);
		//void                swfdec_player_set_maximum_runtime   (SwfdecPlayer *player,
		//                                                         gulong msecs);
#endregion
		
#region swfdec_url
		[DllImport(DllName)]
		internal static extern IntPtr swfdec_url_get_type();
		
		//SwfdecURL*          swfdec_url_new                      (const char *string);
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_url_new (IntPtr str);
		
		//SwfdecURL*          swfdec_url_new_from_input           (const char *input);
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_url_new_from_input (IntPtr input);
		
		//SwfdecURL*          swfdec_url_new_components           (const char *protocol,
		//                                                         const char *hostname,
		//                                                         guint port,
		//                                                         const char *path,
		//                                                         const char *query);
		//SwfdecURL*          swfdec_url_new_relative             (const SwfdecURL *url,
		//                                                         const char *string);
		//SwfdecURL*          swfdec_url_new_parent               (const SwfdecURL *url);
		
		//SwfdecURL*          swfdec_url_copy                     (const SwfdecURL *url);
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_url_copy (IntPtr url);
		
		//void                swfdec_url_free                     (SwfdecURL *url);
		[DllImport (DllName)]
		internal static extern void swfdec_url_free (IntPtr url);
		
		//const char*         swfdec_url_get_protocol             (const SwfdecURL *url);
		//gboolean            swfdec_url_has_protocol             (const SwfdecURL *url,
		//                                                         const char *protocol);
		//const char*         swfdec_url_get_host                 (const SwfdecURL *url);
		//guint               swfdec_url_get_port                 (const SwfdecURL *url);
		//const char*         swfdec_url_get_path                 (const SwfdecURL *url);
		//const char*         swfdec_url_get_query                (const SwfdecURL *url);
		//const char*         swfdec_url_get_url                  (const SwfdecURL *url);
		//gboolean            swfdec_url_is_local                 (const SwfdecURL *url);
		//gboolean            swfdec_url_is_parent                (const SwfdecURL *parent,
		//                                                         const SwfdecURL *child);
		//char*               swfdec_url_format_for_display       (const SwfdecURL *url);
		
		//gboolean            swfdec_url_equal                    (gconstpointer a,
		//                                                         gconstpointer b);
		[DllImport (DllName)]
		internal static extern bool swfdec_url_equal (IntPtr a, IntPtr b);
		
		//guint               swfdec_url_hash                     (gconstpointer url);
		//gboolean            swfdec_url_path_is_relative         (const char *path);
#endregion
		
#region Enums
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_alignment_get_type ();
		
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_scale_mode_get_type ();
#endregion
	}
}
