/*
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GALAXIUM_DEVICES_H__
#define __GALAXIUM_DEVICES_H__

#include <stdbool.h>

typedef struct 
{
	int numerator;
	int denominator;
} GalaxiumFramerate;

typedef struct 
{
	char *mimetype;
	int width;
	int height;
	int num_framerates;
	GalaxiumFramerate *framerates; 
} GalaxiumVideoFormat;

typedef struct
{
	char *video_device;
	char *hal_udi;
	char *gstreamer_src;
	char *name;
	int num_video_formats;
	GalaxiumVideoFormat *video_formats;
} GalaxiumWebcamDevice;

extern GalaxiumWebcamDevice *libgalaxium_find_webcams (int *numdevices);

#endif
