/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Gui
{
	public interface IControllerWindow<Widget>
	{
		//event EventHandler WindowClosed;
		
		//IEnumerable<ISession> Sessions { get; }
		//int SessionCount { get; }

		//ISessionWidget<Widget> CurrentWidget { get; }
		//IEnumerable<ISessionWidget<Widget>> Widgets { get; }
		Widget Toolbar { get; }
		Widget Menubar { get; }
		
		//void AddWidget (ISessionWidget<Widget> widget);
		
		//void AddSession (ISession session);
		
		//void ShowSession (ISession session);
		
		//void RemoveSession (ISession session, bool closeWindow);
		//void RemoveSessions (bool closeWindow);
		
		//bool ContainsSession (ISession session);
		void UpdateAll ();
		void UpdateMenu ();
		
		void Shake ();
		
		void GenerateAlert ();
	}
}