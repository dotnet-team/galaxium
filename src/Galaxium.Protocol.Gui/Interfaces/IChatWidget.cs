/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Gui
{
	public interface IChatWidget<T>
	{
		event EventHandler BecomingActive;
		event EventHandler BecomeActive;
		
		void Initialize ();
		void Destroy ();
		void SwitchTo ();
		void SwitchFrom ();
		void Focus ();
		void Update ();
		
		IConversation Conversation { get; }
		IContainerWindow<T> ContainerWindow { get; }
		IEntity GetEntity (string uid, string name);
		
		bool ShowActionToolbar { get; set; }
		bool ShowInputToolbar { get; set; }
		bool ShowAccountImage { get; set; }
		bool ShowContactImage { get; set; }
		bool ShowPersonalMessage { get; set; }
		bool ShowTimestamps { get; set; }
		bool UseDefaultView { get; set; }
		bool EnableSounds { get; set; }
		
		T NativeWidget { get; }
	}
}
