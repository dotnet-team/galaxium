/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Gui
{
	//TODO: how useful is this class ?
	public static class ProtocolGuiUtility
	{
		public static ISessionWidget<T> CreateSessionWidget<T> (IProtocol protocol, IControllerWindow<T> window, ISession session)
		{
			IProtocolGuiFactory<T> fac = ProtocolUtility.GetProtocolFactory (protocol) as IProtocolGuiFactory<T>;
			if (fac != null)
				return fac.CreateSessionWidget (window, session);
			return null;
		}

		public static IAccountWidget<T> CreateAccountWidget<T> (IProtocol protocol)
		{
			IProtocolGuiFactory<T> fac = ProtocolUtility.GetProtocolFactory (protocol) as IProtocolGuiFactory<T>;
			if (fac != null)
				return fac.CreateAccountWidget ();
			return null;
		}
		
		public static IChatWidget<T> CreateChatWidget<T> (IProtocol protocol, IContainerWindow<T> window, IConversation conversation)
		{
			IProtocolGuiFactory<T> fac = ProtocolUtility.GetProtocolFactory (protocol) as IProtocolGuiFactory<T>;
			if (fac != null)
				return fac.CreateChatWidget (window, conversation);
			return null;
		}
	}
}