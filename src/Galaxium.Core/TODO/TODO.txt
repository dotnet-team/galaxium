Galaxium.Client.Gtk
===================

- Finish the tray icon menu items that do not work, Presence and Supress Activities.
  - Update: Presence is almost done, we just have to work out a few kinks between individual protocols.

- Tray icon Preferences option should not work if we are in a preference panel, or other model window.

- Evaluate Keyboard Shortcuts that are present and missing. (ESC to close chat window?)

- Must save what account was last used (including its protocol) so we dont always have to switch the list.

- We must save the account details when Connect is clicked, not when it successfully logs in. It also has to commit the changes to file, otherwise if the app crashes, they lose their settings.

- Having a way to identify images in the choose image window might be nice. maybe a tooltip?

- The contact tree performance is way too slow, we need to improve it.

Galaxium.Protocol
=================

- Automatic network state discovery (listen for events if a network cable is disconnected, ...) to detect disconnects earlier

- Group chat needs to be totally re-evaluated. I dont think converting normal chats into group chats is
  the answer. We need to also create a general way that galaxium will handle group chats in all protocols.

- The standard settings of each protocol account should be figured out
  for example: "Show Display Names+Images" should be enabled by default

Galaxium.Protocol.Msn
=====================

- Handle all incoming messages

- Support for offline messages and email notifications

- We need to start dealing with NAK's so we notify users that message did not reach destination.

- We have to fix outgoing file transfers with pidgin, they dont work, incoming does however.

- We need to fix file transfers for HTTP method. The initialize but dont work.

- Switchboard connection gets disconnected unexpectedly when receiving a file transfer, and then
  the switchboard stays alive with a disconnected socket. This makes it so we never can get rid of it,
  and we can never chat the user again, since a new connection will never be made.
  
Galaxium.Protocol.Msn.Gtk
=========================

- MsnPlus features for text coloring and perhaps some other things like slash-commands etc.

- Allow dropping of files into the chat window to send files out.

- The contact list doesnt expand when you log in, expand states have to be manipulated correctly, stored
  and returned the way its supposed to be.
  
Galaxium.Protocol.Irc
=====================

- When an IRC session is closed, it should send the QUIT command instead of a PART command to each and every channel followed by a QUIT

Other
================

- remove all .so files and include the source of the library (otherwise galaxium won't work on other architectures)

- Buddy Pounce in Pidgin is a great feature,
  and we need to come up with our own way to do this. It will be the same for all protocols and doesn't
  deal with any protocol specific details, so it will be general.