/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Reflection;

using Mono.Addins;

namespace Galaxium.Core
{
	public class StaticMethodExtension: ExtensionNode
	{
		[NodeAttribute]
		string method = null;
		
		public Type CallType
		{
			get
			{
				string typename = method.Substring(0, method.LastIndexOf("."));
				return Addin.GetType(typename);
			}
		}
		
		public MethodInfo CallMethod
		{
			get
			{
				string methodname = method.Substring(method.LastIndexOf(".") + 1);
				return CallType.GetMethod(methodname, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
			}
		}
		
		public void InvokeMethod()
		{
			CallMethod.Invoke(null, null);
		}
	}
}
