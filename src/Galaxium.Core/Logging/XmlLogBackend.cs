/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

using Anculus.Core;

namespace Galaxium.Core
{
	public class XmlLogBackend : ILogBackend, IEquatable<XmlLogBackend>
	{
		object _sync = new object ();
		FileStream _stream;
		string _closeStr;
		byte[] _close;
		
		public XmlLogBackend (string filename, bool append)
		{
			_closeStr = "</log>";
			_close = Encoding.UTF8.GetBytes (_closeStr);
			
			if (!(File.Exists (filename) && append))
				File.WriteAllText (filename, "<log>" + Environment.NewLine + _closeStr);
			
			_stream = File.OpenWrite (filename);
			_stream.Seek (_stream.Length - _close.Length, SeekOrigin.Begin);
		}
		
		public void Log (string timestamp, LogLevel level, string logger, object message, Exception ex)
		{
			lock (_sync)
			{
				StringBuilder entry = new StringBuilder ();
				
				entry.AppendFormat ("  <entry timestamp=\"{0}\" level=\"{1}\" logger=\"{2}\">{3}",
				                    XmlUtility.Encode (timestamp),
				                    level,
				                    XmlUtility.Encode (logger),
				                    Environment.NewLine);
				
				entry.AppendFormat ("    {0}{1}", XmlUtility.Encode (message.ToString ()), Environment.NewLine);
				
				WriteException (entry, ex, "    ");
				
				entry.AppendLine ("  </entry>");
				entry.Append (_closeStr);
				
				byte[] entryData = Encoding.UTF8.GetBytes (entry.ToString ());
				
				_stream.Seek (_stream.Length - _close.Length, SeekOrigin.Begin);
				_stream.Write (entryData, 0, entryData.Length);
			}
		}
		
		void WriteException (StringBuilder entry, Exception ex, string indent)
		{
			if (ex == null)
				return;
			
			entry.AppendLine (string.Format ("{0}<exception message=\"{1}\" source=\"{2}\">", indent, XmlUtility.Encode (ex.Message), XmlUtility.Encode (ex.Source)));
			entry.AppendLine (string.Format ("{0}  <stacktrace>", indent));
			entry.AppendLine (string.Format ("{0}    {1}", indent, XmlUtility.Encode (ex.StackTrace)));
			entry.AppendLine ("</stacktrace>");
			
			WriteException (entry, ex.InnerException, indent + "  ");
			
			entry.AppendLine (string.Format ("{0}</exception>", indent));
		}
		
		public override bool Equals (object obj)
		{
			if (obj == null)
				return false;
			
			return obj is XmlLogBackend;
		}
		
		public bool Equals (XmlLogBackend other)
		{
			return true;
		}
		
		public override int GetHashCode ()
		{
			return typeof (XmlLogBackend).GetHashCode ();
		}
	}
}
