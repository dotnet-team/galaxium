/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using Anculus.Core;

namespace Galaxium.Core
{
	public class GalaxiumLogBackend : ILogBackend
	{
		public void Log (string timestamp, LogLevel level, string logger, object message, Exception ex)
		{
			Console.Write (timestamp);
			
			if ((message != null) && (message.ToString ().StartsWith ("<<")) || (message.ToString ().StartsWith (">>")))
			{
				Console.Write (" ");
			}
			else
			{
				switch (level)
				{
					case LogLevel.Debug:
						Console.Write (" [DEBUG] ");
						break;
					case LogLevel.Error:
						Console.Write (" [ERROR] ");
						break;
					case LogLevel.Fatal:
						Console.Write (" [FATAL] ");
						break;
					case LogLevel.Info:
						Console.Write (" [INFO] ");
						break;
					case LogLevel.Warning:
						Console.Write (" [WARNING] ");
						break;
				}
				
				Console.Write ("[");
				Console.Write (logger);
				Console.Write ("] ");
			}
			
			if (message != null)
				Console.WriteLine (message.ToString ());
			else
				Console.WriteLine ();

			while (ex != null)
			{
				Console.WriteLine ("{0}: {1}", ex.GetType().Name, ex.Message);
				Console.WriteLine (ex.StackTrace);

				ex = ex.InnerException;
			}
		}
	}
}