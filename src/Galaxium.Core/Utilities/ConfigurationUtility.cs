/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Anculus.Core;

namespace Galaxium.Core
{
	public static class ConfigurationUtility
	{
		private static string _generalConfigFile;
		private static string _accountsConfigFile;
		private static string _pluginsConfigFile;
		
		private static IConfiguration _generalConfiguration;
		private static IConfiguration _accountsConfiguration;
		private static IConfiguration _pluginsConfiguration;

		public static void Initialize ()
		{
			_generalConfigFile = CoreUtility.GetConfigurationDirectoryFile ("General.xml");
			_accountsConfigFile = CoreUtility.GetConfigurationDirectoryFile ("Accounts.xml");
			_pluginsConfigFile = CoreUtility.GetConfigurationDirectoryFile ("Plugins.xml");
			
			_generalConfiguration = new XmlConfiguration (_generalConfigFile);
			_accountsConfiguration = new XmlConfiguration (_accountsConfigFile);
			_pluginsConfiguration = new XmlConfiguration (_pluginsConfigFile);
			
			_generalConfiguration.Load ();
			_accountsConfiguration.Load ();
			_pluginsConfiguration.Load ();
		}

		public static void Save ()
		{
			_generalConfiguration.Save ();
			_accountsConfiguration.Save ();
			_pluginsConfiguration.Save ();
		}
		
		public static void SaveGeneral ()
		{
			_generalConfiguration.Save ();
		}
		
		public static void SaveAccounts ()
		{
			_accountsConfiguration.Save ();
		}
		
		public static void SavePlugins ()
		{
			_pluginsConfiguration.Save ();
		}

		public static IConfiguration General
		{
			get { return _generalConfiguration; }
		}
		
		public static IConfiguration Accounts
		{
			get { return _accountsConfiguration; }
		}
		
		public static IConfiguration Plugins
		{
			get { return _pluginsConfiguration; }
		}
	}
}