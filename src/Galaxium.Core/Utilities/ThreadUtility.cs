/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Threading;

using Anculus.Core;

namespace Galaxium.Core
{
	public class ExceptionEventArgs: EventArgs
	{
		public Exception Exception { get; private set; }
		public ExceptionEventArgs (Exception e) { Exception = e; }
	}
	
	public static class ThreadUtility
	{
		static IThreadDispatcher _dispatcher;
		
		static event EventHandler<ExceptionEventArgs> UnhandledExceptionThrown;
		
		public static IThreadDispatcher Dispatcher
		{
			get { return _dispatcher; }
			set { _dispatcher = value; }
		}
		
		public static void Check ()
		{
			if (!_dispatcher.Check ())
				Log.Fatal ("Not executing in main thread:\n" + Environment.StackTrace);
		}
		
		public static void Dispatch (VoidDelegate d)
		{
			_dispatcher.Dispatch (d, null);
		}
		
		public static void Dispatch (ObjectDelegate d, params object[] parameters)
		{
			_dispatcher.Dispatch (d, parameters);
		}
		
		public static object SyncDispatch (VoidDelegate d)
		{
			return _dispatcher.SyncDispatch (d, null);
		}
		
		public static object SyncDispatch (ObjectDelegate d, params object[] parameters)
		{
			return _dispatcher.SyncDispatch (d, parameters);
		}
		
		public static void CreateBackgroundTask (VoidDelegate d)
		{
			ThreadPool.QueueUserWorkItem ((o) => {
				try
				{
					(o as VoidDelegate) ();
				}
				catch (Exception e)
				{
					Log.Error (e, "Unhandled exception in a background thread");
					if (UnhandledExceptionThrown != null)
						UnhandledExceptionThrown (null, new ExceptionEventArgs (e));
				}
			}, d);
		}
	}
}
