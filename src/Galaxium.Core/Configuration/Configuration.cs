/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Anculus.Core;

namespace Galaxium.Core
{
	public static class Configuration
	{
		public static class Protocol
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Protocol"];
			
			public static DefaultValue<string> LastUsed = new DefaultValue<string> ("LastUsed", String.Empty);
			public static DefaultValue<string> LastAccount = new DefaultValue<string> ("LastAccount", String.Empty);
		}
		
		public static class Addin
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Addin"];
			
		}
		
		public static class Account
		{
			public static IConfigurationSection Section = ConfigurationUtility.Accounts;
			
			public static DefaultValue<string> Password = new DefaultValue<string> ("Password", String.Empty);
			public static DefaultValue<bool> RememberPassword = new DefaultValue<bool> ("RememberPassword", false);
			public static DefaultValue<bool> AutoConnect = new DefaultValue<bool> ("AutoConnect", false);
			public static DefaultValue<List<List<string>>> AutoConnectList = new DefaultValue<List<List<string>>> ("AutoConnectList", new List<List<string>> ());
			public static DefaultValue<string> DisplayName = new DefaultValue<string> ("DisplayName", String.Empty);
			public static DefaultValue<string> DisplayMessage = new DefaultValue<string> ("DisplayMessage", String.Empty);
			public static DefaultValue<string> DisplayImage = new DefaultValue<string> ("DisplayImage", String.Empty);
			public static DefaultValue<string> Nickname = new DefaultValue<string> ("Nickname", String.Empty);
			public static DefaultValue<string> InitialPresence = new DefaultValue<string> ("InitialPresence", String.Empty);
			public static DefaultValue<bool> ViewByGroup = new DefaultValue<bool> ("ViewByGroup", true);
			public static DefaultValue<bool> ViewByStatus = new DefaultValue<bool> ("ViewByStatus", false);
			public static DefaultValue<int> DetailLevel = new DefaultValue<int> ("DetailLevel", 0);
			public static DefaultValue<bool> SortAlphabetic = new DefaultValue<bool> ("SortAlphabetic", true);
			public static DefaultValue<bool> SortStatus = new DefaultValue<bool> ("SortStatus", false);
			public static DefaultValue<bool> SortAscending = new DefaultValue<bool> ("SortAscending", false);
			public static DefaultValue<bool> SortDescending = new DefaultValue<bool> ("SortDescending", true);
			public static DefaultValue<bool> ShowProfileDetails = new DefaultValue<bool> ("ShowProfileDetails", true);
			public static DefaultValue<bool> ShowSearchBar = new DefaultValue<bool> ("ShowSearchBar", false);
			public static DefaultValue<bool> ShowContactImages = new DefaultValue<bool> ("ShowContactImages", true);
			public static DefaultValue<bool> ShowContactNames = new DefaultValue<bool> ("ShowContactNames", true);
			public static DefaultValue<bool> ShowContactNicknames = new DefaultValue<bool> ("ShowContactNicknames", true);
			public static DefaultValue<bool> ShowContactMessages = new DefaultValue<bool> ("ShowContactMessages", true);
			public static DefaultValue<bool> ShowEmptyGroups = new DefaultValue<bool> ("ShowEmptyGroups", false);
			public static DefaultValue<bool> ShowOfflineContacts = new DefaultValue<bool> ("ShowOfflineContacts", false);
			public static DefaultValue<bool> GroupOfflineContacts = new DefaultValue<bool> ("GroupOfflineContacts", false);
			public static DefaultValue<bool> UseDefaultListView = new DefaultValue<bool> ("UseDefaultListView", true);
		}
		
		public static class Contact
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Contact"];
			
			public static DefaultValue<bool> ShowActionToolbar = new DefaultValue<bool> ("ShowActionToolbar", true);
			public static DefaultValue<bool> ShowInputToolbar = new DefaultValue<bool> ("ShowInputToolbar", true);
			public static DefaultValue<bool> ShowAccountImage = new DefaultValue<bool> ("ShowAccountImage", false);
			public static DefaultValue<bool> ShowContactImage = new DefaultValue<bool> ("ShowContactImage", false);
			public static DefaultValue<bool> ShowIdentification = new DefaultValue<bool> ("ShowIdentification", true);
			public static DefaultValue<bool> EnableLogging = new DefaultValue<bool> ("EnableLogging", true);
			public static DefaultValue<bool> EnableSounds = new DefaultValue<bool> ("EnableSounds", true);
			public static DefaultValue<bool> SupressImage = new DefaultValue<bool> ("SupressImage", false);
			public static DefaultValue<bool> SupressName = new DefaultValue<bool> ("SupressName", false);
			public static DefaultValue<bool> SupressMessage = new DefaultValue<bool> ("SupressMessage", false);
			public static DefaultValue<bool> AutoAccept = new DefaultValue<bool> ("AutoAccept", false);
			public static DefaultValue<string> DisplayName = new DefaultValue<string> ("DisplayName", String.Empty);
			public static DefaultValue<string> DisplayMessage = new DefaultValue<string> ("DisplayMessage", String.Empty);
			public static DefaultValue<string> Nickname = new DefaultValue<string> ("Nickname", String.Empty);
		}
		
		public static class Emoticon
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Emoticon"];
			
			public static DefaultValue<string> Active = new DefaultValue<string> ("Active", "Tango");
		}
		
		public static class MessageDisplay
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["MessageDisplay"];
			
			public static DefaultValue<string> Active = new DefaultValue<string> ("Active", "Text");
			public static DefaultValue<string> Family = new DefaultValue<string> ("Family", "sans-serif");
			public static DefaultValue<double> Size = new DefaultValue<double> ("Size", 10);
			public static DefaultValue<int> Color = new DefaultValue<int> ("Color", 0);
			public static DefaultValue<bool> Bold = new DefaultValue<bool> ("Bold", false);
			public static DefaultValue<bool> Italic = new DefaultValue<bool> ("Italic", false);
			public static DefaultValue<bool> Underline = new DefaultValue<bool> ("Underline", false);
			public static DefaultValue<bool> Strikethrough = new DefaultValue<bool> ("Strikethrough", false);
		}
		
		public static class Sound
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Sound"];
			
			public static DefaultValue<string> Active = new DefaultValue<string> ("Active", "Ambience");
		}
		
		public static class Logging
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Logging"];
			
			public static DefaultValue<int> HistorySize = new DefaultValue<int> ("HistorySize", 5);
			public static DefaultValue<bool> ShowHistory = new DefaultValue<bool> ("ShowHistory", true);
			public static DefaultValue<bool> EnableLogging = new DefaultValue<bool> ("EnableLogging", true);
			public static DefaultValue<bool> EnableEventLogging = new DefaultValue<bool> ("EnableEventLogging", true);
			public static DefaultValue<int> MaximumLogSize = new DefaultValue<int> ("MaximumLogSize", 1024 * 1024 * 2);
		}
		
		public static class Proxy
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Proxy"];
			
			public static DefaultValue<int> SocksType = new DefaultValue<int> ("SocksType", 0);
			
			public static DefaultValue<bool> UseProxy = new DefaultValue<bool> ("UseProxy", false);
			public static DefaultValue<bool> UseSame = new DefaultValue<bool> ("UseSame", false);
			
			public static DefaultValue<string> SocksHost = new DefaultValue<string> ("SocksHost", String.Empty);
			public static DefaultValue<int> SocksPort = new DefaultValue<int> ("SocksPort", 0);
			public static DefaultValue<string> SocksUsername = new DefaultValue<string> ("SocksUsername", String.Empty);
			public static DefaultValue<string> SocksPassword = new DefaultValue<string> ("SocksPassword", String.Empty);
			public static DefaultValue<string> HttpsHost = new DefaultValue<string> ("HttpsHost", String.Empty);
			public static DefaultValue<int> HttpsPort = new DefaultValue<int> ("HttpsPort", 0);
			public static DefaultValue<string> HttpsUsername = new DefaultValue<string> ("HttpsUsername", String.Empty);
			public static DefaultValue<string> HttpsPassword = new DefaultValue<string> ("HttpsPassword", String.Empty);
			public static DefaultValue<string> HttpHost = new DefaultValue<string> ("HttpHost", String.Empty);
			public static DefaultValue<int> HttpPort = new DefaultValue<int> ("HttpPort", 0);
			public static DefaultValue<string> HttpUsername = new DefaultValue<string> ("HttpUsername", String.Empty);
			public static DefaultValue<string> HttpPassword = new DefaultValue<string> ("HttpPassword", String.Empty);
		}
		
		public static class Transfer
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Transfer"];
			
			public static DefaultValue<int> Automation = new DefaultValue<int> ("Automation", 0);
			public static DefaultValue<int> Overwrite = new DefaultValue<int> ("Overwrite", 0);
			public static DefaultValue<int> OverwriteDefault = new DefaultValue<int> ("OverwriteDefault", 0);
			public static DefaultValue<string> DestinationFolder = new DefaultValue<string> ("DestinationFolder", String.Empty);
			public static DefaultValue<string> CompleteFolder = new DefaultValue<string> ("CompleteFolder", String.Empty);
			public static DefaultValue<bool> AutoDestination = new DefaultValue<bool> ("AutoDestination", true);
			public static DefaultValue<bool> AutoClear = new DefaultValue<bool> ("AutoClear", false);
			public static DefaultValue<bool> AutoClose = new DefaultValue<bool> ("AutoClose", false);
		}
		
		public static class Activity
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Interface"]["Activity"];
			
			public static DefaultValue<bool> NewContacts = new DefaultValue<bool> ("ShowNewContactsActivities", true);
			public static DefaultValue<bool> NewConversations = new DefaultValue<bool> ("ShowNewConversationsActivities", true);
			public static DefaultValue<bool> PresenceChanges = new DefaultValue<bool> ("ShowPresenceChangesActivities", true);
			public static DefaultValue<bool> NameChanges = new DefaultValue<bool> ("ShowNameChangesActivities", false);
			public static DefaultValue<bool> ImageChanges = new DefaultValue<bool> ("ShowImageChangesActivities", false);
			public static DefaultValue<bool> MessageChanges = new DefaultValue<bool> ("ShowMessageChangesActivities", false);
			public static DefaultValue<bool> Transfers = new DefaultValue<bool> ("ShowTransfersActivities", true);
			public static DefaultValue<bool> WebcamAudio = new DefaultValue<bool> ("ShowWebcamAudioActivities", true);
			public static DefaultValue<bool> ProtocolSpecific = new DefaultValue<bool> ("ShowProtocolSpecificActivities", true);
			public static DefaultValue<bool> DetectHardware = new DefaultValue<bool> ("ShowDetectHardwareActivities", true);
			public static DefaultValue<bool> ShowImages = new DefaultValue<bool> ("ShowImages", true);
			public static DefaultValue<int> NotificationLength = new DefaultValue<int> ("NotificationLength", 3000);
		}
		
		public static class ContactList
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Interface"]["ContactList"];
			
			public static DefaultValue<bool> ShowOnStartup = new DefaultValue<bool> ("ShowOnStartup", true);
			public static DefaultValue<bool> ViewByGroup = new DefaultValue<bool> ("ViewByGroup", true);
			public static DefaultValue<bool> ViewByStatus = new DefaultValue<bool> ("ViewByStatus", false);
			public static DefaultValue<int> DetailLevel = new DefaultValue<int> ("DetailLevel", 0);
			public static DefaultValue<bool> SortAlphabetic = new DefaultValue<bool> ("SortAlphabetic", true);
			public static DefaultValue<bool> SortStatus = new DefaultValue<bool> ("SortStatus", false);
			public static DefaultValue<bool> SortAscending = new DefaultValue<bool> ("SortAscending", true);
			public static DefaultValue<bool> SortDescending = new DefaultValue<bool> ("SortDescending", false);
			public static DefaultValue<bool> ShowProfileDetails = new DefaultValue<bool> ("ShowProfileDetails", true);
			public static DefaultValue<bool> ShowSearchBar = new DefaultValue<bool> ("ShowSearchBar", false);
			public static DefaultValue<bool> ShowContactImages = new DefaultValue<bool> ("ShowContactImages", true);
			public static DefaultValue<bool> ShowContactNames = new DefaultValue<bool> ("ShowContactNames", true);
			public static DefaultValue<bool> ShowContactNicknames = new DefaultValue<bool> ("ShowContactNicknames", true);
			public static DefaultValue<bool> ShowContactMessages = new DefaultValue<bool> ("ShowContactMessages", true);
			public static DefaultValue<bool> ShowEmptyGroups = new DefaultValue<bool> ("ShowEmptyGroups", false);
			public static DefaultValue<bool> ShowOfflineContacts = new DefaultValue<bool> ("ShowOfflineContacts", false);
			public static DefaultValue<bool> GroupOfflineContacts = new DefaultValue<bool> ("GroupOfflineContacts", false);
		}
		
		public static class Conversation
		{
			public static IConfigurationSection Section = ConfigurationUtility.General["Interface"]["Conversation"];
			
			public static DefaultValue<int> TabPlacement = new DefaultValue<int> ("TabPlacement", 0);
			public static DefaultValue<bool> QueueNever = new DefaultValue<bool> ("QueueNever", true);
			public static DefaultValue<bool> QueueAlways = new DefaultValue<bool> ("QueueAlways", false);
			public static DefaultValue<bool> QueueWhenAway = new DefaultValue<bool> ("QueueWhenAway", false);
			public static DefaultValue<bool> WindowMinimized = new DefaultValue<bool> ("WindowMinimized", false);
			public static DefaultValue<bool> WindowShake = new DefaultValue<bool> ("WindowShake", true);
			public static DefaultValue<int> WindowGroupBehavior = new DefaultValue<int> ("WindowGroupBehavior", 0);
			public static DefaultValue<bool> ShowActivityToolbar = new DefaultValue<bool> ("ShowActivityToolbar", true);
			public static DefaultValue<bool> ShowInputToolbar = new DefaultValue<bool> ("ShowInputToolbar", true);
			public static DefaultValue<bool> ShowAccountImage = new DefaultValue<bool> ("ShowAccountImage", false);
			public static DefaultValue<bool> ShowContactImage = new DefaultValue<bool> ("ShowContactImage", false);
			public static DefaultValue<bool> ShowIdentification = new DefaultValue<bool> ("ShowIdentification", true);
			public static DefaultValue<bool> EnableLogging = new DefaultValue<bool> ("EnableLogging", true);
			public static DefaultValue<bool> EnableSounds = new DefaultValue<bool> ("EnableSounds", true);
		}
	}
}