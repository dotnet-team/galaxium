/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2005-2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	public sealed class MsnGroup : AbstractGroup, IComparable<MsnGroup>
	{
		public MsnGroup (MsnSession session, string uid, string name)
			: base (session, uid, name, false)
		{
		}
		
		public MsnGroup (MsnSession session, string uid, string name, bool isVirtual)
			: base (session, uid, name, isVirtual)
		{
		}

		public override int OnlineCount
		{
			get
			{
				int count = 0;
				foreach (MsnContact contact in this) {
					if (contact.Presence.CompareTo (MsnPresence.Offline) != 0)
						count++;
				}

				return count;
			}
		}

		public override int OfflineCount
		{
			get
			{
				int count = 0;
				foreach (MsnContact contact in this) {
					if (contact.Presence.CompareTo (MsnPresence.Offline) == 0)
						count++;
				}

				return count;
			}
		}

		public int CompareTo (MsnGroup other)
		{
			return UniqueIdentifier.CompareTo (other.UniqueIdentifier);
		}
	}
}