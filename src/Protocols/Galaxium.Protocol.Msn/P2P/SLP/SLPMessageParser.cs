/*
 * Galaxium Messenger
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	public static class SLPMessageParser
	{
		public static SLPMessage Parse (MsnSession session, byte[] data)
		{
			int lineLen = MsnByteArrayUtility.IndexOf (data, "\r\n");
			
			if (lineLen < 0)
				return null;
			
			try
			{
				byte[] lineData = new byte[lineLen];
				Array.Copy (data, lineData, lineLen);
				string line = Encoding.UTF8.GetString (lineData);
			
				if (!line.Contains ("MSNSLP"))
					return null;
			
				if (line.StartsWith ("MSNSLP/1.0"))
					return new SLPStatusMessage (session, data);
				else
					return new SLPRequestMessage (session, data);
			}
			catch
			{
				return null;
			}
		}
	}
}
