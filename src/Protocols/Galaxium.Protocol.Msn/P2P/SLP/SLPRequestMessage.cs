/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	public class SLPRequestMessage : SLPMessage
	{
		string _method = "UNKNOWN";
		string _version = "MSNSLP/1.0";
		
		public string Method
		{
			get { return _method; }
			set { _method = value; }
		}
		
		public string Version
		{
			get { return _version; }
			set { _version = value; }
		}
		
		protected override string StartLine
		{
			get { return string.Format ("{0} {1}:{2} {3}\r\n", _method, MsnNetworkUtility.Name (To.Network).ToUpper (), To.UniqueIdentifier, _version); }
			set
			{
				string[] chunks = value.Split (new string [] { " ", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
				
				_method = chunks[0];
				_version = chunks[2];
			}
		}
		
		public SLPRequestMessage (IMsnEntity to, string method)
			: base (to)
		{
			_method = method;
		}
		
		public SLPRequestMessage (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
