/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	public class SLPStatusMessage : SLPMessage
	{
		string _version = "MSNSLP/1.0";
		int _code = 0;
		string _phrase = "Unknown";
		
		public int Code
		{
			get { return _code; }
			set { _code = value; }
		}
		
		public string Phrase
		{
			get { return _phrase; }
			set { _phrase = value; }
		}
		
		public string Version
		{
			get { return _version; }
			set { _version = value; }
		}
		
		protected override string StartLine
		{
			get { return string.Format ("{0} {1} {2}\r\n", _version, _code, _phrase); }
			set
			{
				string[] chunks = value.Split (new string [] { " ", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
				
				_version = chunks[0];
				
				if (!int.TryParse (chunks[1], out _code))
					Log.Warn ("Unable to parse SLP status message");
				
				_phrase = string.Empty;
				
				for (int i = 2; i < chunks.Length; i++)
					_phrase += chunks[i] + " ";
				
				_phrase = _phrase.Trim ();
			}
		}
		
		public SLPStatusMessage (IMsnEntity to, int code, string phrase)
			: base (to)
		{
			_code = code;
			_phrase = phrase;
		}
		
		public SLPStatusMessage (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
