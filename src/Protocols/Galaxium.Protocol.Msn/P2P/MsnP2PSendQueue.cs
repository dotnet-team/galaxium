/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

namespace Galaxium.Protocol.Msn
{
	public struct MsnP2PSendItem
	{
		public MsnContact Remote;
		public P2PMessage Message;
		
		public MsnP2PSendItem (MsnContact remote, P2PMessage message)
		{
			Remote = remote;
			Message = message;
		}
	}
	
	public class MsnP2PSendQueue : Queue<MsnP2PSendItem>
	{
		public void Enqueue (MsnContact remote, P2PMessage message)
		{
			Enqueue (new MsnP2PSendItem (remote, message));
		}
	}
	
	public class MsnP2PSendList : List<MsnP2PSendItem>
	{
		public void Add (MsnContact remote, P2PMessage message)
		{
			Add (new MsnP2PSendItem (remote, message));
		}
		
		public bool Contains (P2PMessage msg)
		{
			foreach (MsnP2PSendItem item in this)
			{
				if (item.Message == msg)
					return true;
			}
			
			return false;
		}
		
		public void Remove (P2PMessage msg)
		{
			foreach (MsnP2PSendItem item in this)
			{
				if (item.Message == msg)
				{
					Remove (item);
					return;
				}
			}
			
			throw new ArgumentException ("msg not found in queue");
		}
	}
}
