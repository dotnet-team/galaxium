/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;

using Anculus.Core;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	[MsnP2PApplication (8, "5E288469-7693-4B64-A8DC-C56648E4397D")]
	public class P2PObjectLookup : AbstractMsnP2PApplication
	{
		Guid _contentID;
		
		public override bool AutoAccept
		{
			get { return true; }
		}
		
		public P2PObjectLookup (MsnContact remote, Guid contentID)
			: base (remote.Session.Account, remote)
		{
			_contentID = contentID;
		}
		
		public P2PObjectLookup (MsnP2PSession p2pSession)
			: base (p2pSession)
		{
		}
		
		public override bool CheckInvite (SLPRequestMessage invite)
		{
			return base.CheckInvite (invite);
		}
		
		public override string CreateInviteContext ()
		{
			MessageBuilder mb = new MessageBuilder (20);
			
			mb.Append (_contentID.ToByteArray ());
			mb.Append (0); // What is this?
			
			return Convert.ToBase64String (mb.GetByteArray ());
		}
		
		public override void Begin ()
		{
			base.Begin ();
			
			//SendString (new byte[] { 0x80, 0xcc, 0xcc, 0xcc }, "request");
		}
		
		public override bool ProcessMessage (IMsnP2PBridge bridge, P2PMessage msg)
		{
			if (msg.Payload.Length <= 10)
			{
				Log.Debug ("Received payload: {0}", BaseUtility.BytesToString (msg.Payload));
				
				if (msg.Payload.Length == 4)
				{
					Log.Debug ("As Int16: {0} {1}", BitUtility.ToInt16 (msg.Payload, 0, false), BitUtility.ToInt16 (msg.Payload, 2, false));
					Log.Debug ("As Int32: {0}", BitUtility.ToInt32 (msg.Payload, 0, false));
				}
				
				return false;
			}
			
			byte[] first4 = new byte [4];
			Array.Copy (msg.Payload, first4, 4);
			
			Log.Debug ("First 4 bytes: {0} ({1})", BaseUtility.BytesToString (first4), BitUtility.ToInt32 (first4, 0, false));
			
			int appID = (msg.Payload[5] << 8) & msg.Payload[4];
			Log.Debug ("AppID {0}", appID);
			
			int strLen = BitUtility.ToInt32 (msg.Payload, 6, false);
			Log.Debug ("String Length: {0}", strLen);
			
			if (strLen + 10 < msg.Payload.Length)
			{
				Log.Error ("Message payload too short");
				return false;
			}
			
			byte[] strData = new byte[strLen];
			Array.Copy (msg.Payload, 10, strData, 0, strData.Length);
			string str = Encoding.Unicode.GetString (strData);
			
			return HandleString (str);
		}
		
		bool HandleString (string str)
		{
			if (str.EndsWith ("\0"))
				str = str.Substring (0, str.Length - 1);
			
			Log.Debug (str);
			
			return false;
		}
		
	// CHECHTHIS
	//	
	//	void SendString (byte[] first4, string str)
	//	{
	//		MessageBuilder mb = new MessageBuilder ();
	//		
	//		byte[] strData = Encoding.Unicode.GetBytes (str + "\0");
	//		
	//		mb.Append (first4);
	//		mb.Append ((Int16)AppID);
	//		mb.Append (strData.Length);
	//		mb.Append (strData);
	//		
	//		P2PMessage msg = new P2PMessage (Session);
	//		msg.Payload = mb.GetByteArray ();
	//		
	//		Send (msg);
	//	}
	}
}
