/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	[MsnP2PApplication (4, "4BD96FC0-AB17-4425-A14A-439185962DC8")]
	public class P2PViewWebcam : P2PWebcamBase
	{
		protected override bool Producer
		{
			get { return P2PSession.Invite.From != Session.Account; }
		}
		
		public P2PViewWebcam (MsnP2PSession p2pSession)
			: base (p2pSession)
		{
			Conversation.EmitSeeWebcamInvite (this);
		}
	}
}
