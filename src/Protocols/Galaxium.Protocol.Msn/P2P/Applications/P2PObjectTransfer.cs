/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Security.Cryptography;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Client;

namespace Galaxium.Protocol.Msn
{
	[MsnP2PApplication (1, "A4268EEC-FEC5-49E5-95C3-F126696BDBF6")]
	public class P2PObjectTransfer : AbstractMsnP2PApplication
	{
		MsnObject _object;
		bool _sending;
		MemoryStream _inStream;
		
		public override bool AutoAccept
		{
			get { return true; }
		}
		
		public MsnObject Object
		{
			get { return _object; }
		}
		
		public bool Sending
		{
			get { return _sending; }
		}
		
		public P2PObjectTransfer (MsnP2PSession p2pSession)
			: base (p2pSession)
		{
			_object = MsnObject.Load (Session, EncodingUtility.Base64DecodeSafe (P2PSession.Invite.MIMEBody["Context"].Value));
			
			if (_object == null)
			{
				// Sometimes (especially with bots) the invite doesn't give us the object in the context
				// so we accept that and send them the account display pic if we have one
				
				_object = Local.DisplayImage as MsnDisplayImage;
			}
			
			_sending = true;
			
			// For some reason object transfers use BaseID-3, BaseID-2 etc after the invite
			// We subtract 4 because the ID is incremented after sending the invite
			P2PSession.LocalID -= 4;
		}
		
		public P2PObjectTransfer (MsnObject obj)
			: base (obj.Session.Account, obj.Creator as MsnContact)
		{
			_object = obj;
			_sending = false;
		}
		
		public override bool CheckInvite (SLPRequestMessage invite)
		{
			bool ret = base.CheckInvite (invite);
			MsnObject obj = MsnObject.Load (invite.Session, EncodingUtility.Base64DecodeSafe (invite.MIMEBody["Context"].Value));
			
			if (obj != null)
			{
				// Invite is valid if the objects creator is the local account & the object has data
				ret &= (obj.Creator == invite.Session.Account) && (obj.Data != null) && (obj.Data.Length > 0);
			}
			else
			{
				// Sometimes (especially with bots) the invite doesn't give us the object in the context
				// so we accept that and send them the account display pic if we have one
				
				ret &= invite.Session.Account.DisplayImage is MsnDisplayImage;
			}
			
			return ret;
		}
		
		public override string CreateInviteContext ()
		{
			return EncodingUtility.Base64Encode (_object.Context);
		}
		
		public override void Begin ()
		{
			base.Begin ();
			
			if (Sending)
			{
				P2PMessage msg = new P2PMessage (Session);
				msg.Payload = new byte[] { 0, 0, 0, 0 };
				
				Send (msg, delegate
				{
					msg = new P2PMessage (Session);
					msg.Header.Flags = P2PHeaderFlag.Data;
					msg.Payload = _object.Data;
					
					Send (msg, delegate
					{
						OnComplete ();
					});
				});
			}
			else
				_inStream = new MemoryStream ();
		}
		
		public override bool ProcessMessage (IMsnP2PBridge bridge, P2PMessage msg)
		{
			if ((_inStream.Length == 0) && (msg.Payload.Length == 4) && (BitUtility.ToInt32 (msg.Payload, 0, true) == 0))
			{
				// Data prep
				
				return true;
			}
			else if (((msg.Header.Flags & P2PHeaderFlag.Data) == P2PHeaderFlag.Data) ||
			         (msg.Header.Flags == P2PHeaderFlag.Normal)) // Some screwy bots do this... Virtual Secretary for example
			{
				_inStream.Write (msg.Payload, 0, msg.Payload.Length);
				
				Log.Debug ("Received {0} of {1} bytes", _inStream.Length, _object.Size);
				
				if (_inStream.Length == _object.Size)
				{
					// Finished transfer
					
					byte[] data = new byte[_object.Size];
					
					_inStream.Seek (0, SeekOrigin.Begin);
					_inStream.Read (data, 0, data.Length);
					
					string dataSha = Convert.ToBase64String (new SHA1Managed ().ComputeHash (data));
					
					if (dataSha != _object.Sha)
					{
						Log.Warn ("Object hash doesn't match data hash, data invalid");
						return false;
					}
					
					// Data is ok, update MsnObject
					_object.Data = data;
					
					OnComplete ();
					P2PSession.Close ();
					
					if (_object is MsnDisplayImage)
					{
						//FIXME: this isnt the best place, but its easy right now...
						//Emit the contact display image has officially changed using the session.
						MsnDisplayImage image = _object as MsnDisplayImage;
						
						Local.Session.EmitContactDisplayImageChanged (new EntityChangeEventArgs<IDisplayImage> (image.Creator, image, null));
						
						// We have finished the transfer of a Display Image.
						ActivityUtility.EmitActivity (this, new EntityImageChangeActivity (this.Remote, null));
					}
				}
				
				return true;
			}
			
			return false;
		}
	}
}
