/*
 * Galaxium Messenger
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

namespace Galaxium.Protocol.Msn
{
	public interface IMsnP2PBridge : IDisposable
	{
		event EventHandler BridgeOpened;
		event EventHandler BridgeClosed;
		event EventHandler<P2PMessageEventArgs> BridgeSent;
		
		bool Open { get; }
		int MaxDataSize { get; }
		
		Dictionary<MsnP2PSession, MsnP2PSendQueue> SendQueues { get; }
		
		void StopSending (MsnP2PSession session);
		void ResumeSending (MsnP2PSession session);
		void MigrateQueue (MsnP2PSession session, IMsnP2PBridge newBridge);
		void AddQueue (MsnP2PSession session, MsnP2PSendQueue queue);
		
		bool SuitableFor (MsnP2PSession session);
		bool Ready (MsnP2PSession session);
		void Send (MsnP2PSession session, MsnContact remote, P2PMessage msg);
	}
}
