/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Galaxium.Core;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	public class MsnDisplayImage : MsnObject, IDisplayImage
	{
		public string Filename
		{
			get { return CacheFilename; }
			set
			{
				if (File.Exists (value))
					Data = File.ReadAllBytes (value);
				else
					Data = new byte[0];
				
				Location = Path.GetFileName (value);
			}
		}

		public byte[] ImageBuffer
		{
			get { return Data; }
		}
		
		public byte[] AvatarContentID
		{
			get
			{
				if (!Xml.HasAttribute ("avatarcontentid"))
					return null;
				
				return Convert.FromBase64String (Xml.GetAttribute ("avatarcontentid"));
			}
		}
		
		public Guid AvatarID
		{
			get
			{
				if (!Xml.HasAttribute ("avatarid"))
					return Guid.Empty;
				
				return new Guid (Xml.GetAttribute ("avatarid"));
			}
		}
		
		public MsnDisplayImage (MsnSession session, string context)
			: base (session, context)
		{
			Type = MsnObjectType.UserDisplay;
		}
		
		public MsnDisplayImage (MsnSession session)
			: base (session)
		{
			Type = MsnObjectType.UserDisplay;
		}
	}
}