/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

namespace Galaxium.Protocol.Msn
{
	internal class SecurityToken
	{
		string _domain;
		string _policyReference;
		
		public string Domain
		{
			get { return _domain; }
			set { _domain = value; }
		}
		
		public string PolicyReference
		{
			get { return _policyReference; }
			set { _policyReference = value; }
		}
		
		internal SecurityToken (string domain, string policyReference)
		{
			_domain = domain;
			_policyReference = policyReference;
		}

		static Dictionary<string, SecurityToken> _tokens;
		
		public const string PassportTb = "http://Passport.NET/tb";
		public const string Messenger = "messenger.msn.com";
		public const string MessengerClear = "messengerclear.live.com";
		public const string MessengerSecure = "messengersecure.live.com";
		public const string Contacts = "contacts.msn.com";
		public const string Spaces = "spaces.live.com";
		public const string Storage = "storage.msn.com";
		
		static SecurityToken ()
		{
			_tokens = new Dictionary<string, SecurityToken> ();
			
			RegisterToken (PassportTb, string.Empty);
			RegisterToken (Messenger, "?id=507");
			RegisterToken (MessengerClear, string.Empty);
			RegisterToken (MessengerSecure, "MBI_SSL");
			RegisterToken (Contacts, "MBI");
			RegisterToken (Spaces, "MBI");
			RegisterToken (Storage, "MBI");
		}
		
		public static void RegisterToken (string domain, string policyRef)
		{
			_tokens[domain] = new SecurityToken (domain, policyRef);
		}
		
		public static string GetPolicy (string domain)
		{
			if (!_tokens.ContainsKey (domain))
				return string.Empty;
			
			return _tokens[domain].PolicyReference;
		}
	}
}
