/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	public class MsnActivityEntry
	{
		public long ID;
		public string SubscriptionUrl;
		public string Error;
		public string Locale;
		public int Kids;
		public int Page;
		public long CategoryID;
		public long Sequence;
		public string Name;
		public string Description;
		public string Url;
		public string IconUrl;
		public string AppIconUrl;
		public long PassportSiteID;
		public string Type;
		public int Width;
		public int Height;
		public string Location;
		public int MinUsers;
		public int MaxUsers;
		public bool EnableIP;
		public bool ActiveX;
		public bool SendFile;
		public bool SendIM;
		public bool ReceiveIM;
		public bool ReplaceIM;
		public bool Windows;
		public int MaxPacketRate;
		public bool UserProperties;
		public Version ClientVersion;
		public int AppType;
		public bool Hidden;
	}
	
	public class MsnActivityCategory
	{
		public long ID;
		public string Locale;
		public string IconURL;
		public string Name;
		public string Description;
	}
}
