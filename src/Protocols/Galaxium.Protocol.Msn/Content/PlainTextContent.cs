/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	[MsnContent ("text/plain")]
	public class PlainTextContent : AbstractMsnContent
	{
		IMessage _message;
		
		public IMessage Message
		{
			get
			{
				if (_message == null)
				{
					_message = new Message (MessageFlag.Message, Source, Session.Account, DateTime.Now);
					_message.SetMarkup (DataString, null);
					_message.Style = Style;
				}
				
				return _message;
			}
			set
			{
				_message = value;
				Source = value.Source as IMsnEntity;
				Style = value.Style as MsnMessageStyle;
				DataString = value.Markup;
			}
		}
		
		public MsnMessageStyle Style
		{
			get
			{
				MsnMessageStyle style = new MsnMessageStyle ();
				
				if (MIMEHeader.ContainsKey ("X-MMS-IM-Format"))
				{
					MIMEValue val = MIMEHeader["X-MMS-IM-Format"];
					
					if (val.HasAttribute ("FN"))
						style.Font = EncodingUtility.UrlDecode (val["FN"]);
					
					if (val.HasAttribute ("EF"))
					{
						style.Bold = val["EF"].Contains ("B");
						style.Italic = val["EF"].Contains ("I");
						style.Underline = val["EF"].Contains ("U");
						style.Strikethrough = val["EF"].Contains ("S");
					}
					
					if (val.HasAttribute ("CO"))
						style.Foreground = (ARGBColor)Convert.ToInt32 (BGRToRGB (val["CO"]), 16);
					
					if (val.HasAttribute ("PF"))
						style.PitchAndFamily = (MsnPitchFamily)Convert.ToInt32 (val["PF"], 16);
					
					if (val.HasAttribute ("CS"))
						style.Charset = (MsnCharacterSet)Convert.ToInt32 (val["CS"], 16);

					if (val.HasAttribute ("RL"))
						style.IsRightAlligned = val["RL"] == "1";
				}
				
				return style;
			}
			set
			{
				_message = null;
				
				MIMEValue val = new MIMEValue ();
				
				if (!string.IsNullOrEmpty (value.Font))
					val["FN"] = EncodingUtility.UrlEncode (value.Font);
				
				val["EF"] = (value.Bold ? "B" : string.Empty) +
					        (value.Italic ? "I" : string.Empty) +
						    (value.Underline ? "U" : string.Empty) +
						    (value.Strikethrough ? "S" : string.Empty);
				
				if (value.Foreground != ARGBGradient.None)
					val["C0"] = ((ARGBColor)value.Foreground).ToHexString (false, true);
				
				val["CS"] = string.Format ("{0:x2}", (int)value.Charset);
				val["PF"] = string.Format ("{0:x2}", (int)value.PitchAndFamily);
			
				if (value.IsRightAlligned)
					val["RL"] = "1";
				
				MIMEHeader["X-MMS-IM-Format"] = val;
			}
		}
		
		public PlainTextContent (MsnSession session)
			: base (session)
		{
			SendEncoding = true;
		}
		
		public PlainTextContent (ContentCommand cmd)
			: base (cmd)
		{
		}
		
		protected override void WriteCommand (ContentCommand cmd)
		{
			base.WriteCommand (cmd);
			
			if (cmd is MSGCommand)
				(cmd as MSGCommand).AckType = MSGAckType.Always;
		}
		
		string BGRToRGB (string str)
		{
			if (str.Length != 6)
				return "000000";
			
			return str.Substring (4, 2) + str.Substring (2, 2) + str.Substring (0, 2);
		}
	}
}
