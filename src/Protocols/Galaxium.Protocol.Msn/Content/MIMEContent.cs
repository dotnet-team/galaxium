/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	public class MIMEContent : AbstractMsnContent
	{
		MIMECollection _mimeBody = new MIMECollection ();
		
		public MIMECollection MIMEBody
		{
			get { return _mimeBody; }
		}
		
		public MIMEContent (MsnSession session)
			: base (session)
		{
		}
		
		public MIMEContent (ContentCommand cmd)
			: base (cmd)
		{
		}
		
		protected override void ReadCommand (ContentCommand cmd)
		{
			base.ReadCommand (cmd);
			
			_mimeBody.Clear ();
			int mimeLen = _mimeBody.Parse (Data);
			
			byte[] newData = new byte[Data.Length - mimeLen];
			Array.Copy (Data, mimeLen, newData, 0, newData.Length);
			
			Data = newData;
		}
		
		protected override void WriteCommand (ContentCommand cmd)
		{
			base.WriteCommand (cmd);
			
			cmd.Payload = System.Text.Encoding.UTF8.GetBytes (_mimeBody.ToString () + "\r\n");
		}
	}
}
