/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	public static class MsnPresence
	{
		private static IPresence _online;
		private static IPresence _offline;
		private static IPresence _away;
		private static IPresence _brb;
		private static IPresence _busy;
		private static IPresence _idle;
		private static IPresence _phone;
		private static IPresence _lunch;
		private static IPresence _invisible;

		public static IPresence Online
		{
			get
			{
				if (_online == null)
					_online = new OnlinePresence ();
				return _online;
			}
		}

		public static IPresence Offline
		{
			get
			{
				if (_offline == null)
					_offline = new OfflinePresence ();
				return _offline;
			}
		}

		public static IPresence Away
		{
			get
			{
				if (_away == null)
					_away = new AwayPresence ();
				return _away;
			}
		}

		public static IPresence Brb
		{
			get
			{
				if (_brb == null)
					_brb = new BrbPresence ();
				return _brb;
			}
		}

		public static IPresence Busy
		{
			get
			{
				if (_busy == null)
					_busy = new BusyPresence ();
				return _busy;
			}
		}

		public static IPresence Idle
		{
			get
			{
				if (_idle == null)
					_idle = new IdlePresence ();
				return _idle;
			}
		}

		public static IPresence Phone
		{
			get
			{
				if (_phone == null)
					_phone = new PhonePresence ();
				return _phone;
			}
		}

		public static IPresence Lunch
		{
			get
			{
				if (_lunch == null)
					_lunch = new LunchPresence ();
				return _lunch;
			}
		}

		public static IPresence Invisible
		{
			get
			{
				if (_invisible == null)
					_invisible = new InvisiblePresence ();
				return _invisible;
			}
		}

		public static bool IsValidMsnPresence (IPresence presence)
		{
			if (presence == null)
				return false;

			return presence.Equals (_online) || presence.Equals (_offline)
				|| presence.Equals (_away) || presence.Equals (_brb)
				|| presence.Equals (_busy) || presence.Equals (_idle)
				|| presence.Equals (_phone) || presence.Equals (_lunch)
				|| presence.Equals (_invisible);
		}

		public static bool IsValidMsnPresence (string presence)
		{
			if (String.IsNullOrEmpty (presence))
				return false;

			return String.Compare (presence, "NLN", true) == 0
				|| String.Compare (presence, "BSY", true) == 0
				|| String.Compare (presence, "IDL", true) == 0
				|| String.Compare (presence, "BRB", true) == 0
				|| String.Compare (presence, "AWY", true) == 0
				|| String.Compare (presence, "PHN", true) == 0
				|| String.Compare (presence, "LUN", true) == 0
				|| String.Compare (presence, "FLN", true) == 0
				|| String.Compare (presence, "HDN", true) == 0;
		}

		public static IPresence ParseMsnPresence (string presence)
		{
			ThrowUtility.ThrowIfEmpty ("presence", presence);

			if (String.Compare (presence, "NLN", true) == 0) {
				return Online;
			} else if (String.Compare (presence, "BSY", true) == 0) {
				return Busy;
			} else if (String.Compare (presence, "IDL", true) == 0) {
				return Idle;
			} else if (String.Compare (presence, "BRB", true) == 0) {
				return Brb;
			} else if (String.Compare (presence, "AWY", true) == 0) {
				return Away;
			} else if (String.Compare (presence, "PHN", true) == 0) {
				return Phone;
			} else if (String.Compare (presence, "LUN", true) == 0) {
				return Lunch;
			} else if (String.Compare (presence, "FLN", true) == 0) {
				return Offline;
			} else if (String.Compare (presence, "HDN", true) == 0) {
				return Invisible;
			}

			return UnknownPresence.Instance;
		}

		public static string GetMsnPresenceCode (IPresence presence)
		{
			if (presence.Equals (MsnPresence.Online))
				return "NLN";
			else if (presence.Equals (MsnPresence.Away))
				return "AWY";
			else if (presence.Equals (MsnPresence.Brb))
				return "BRB";
			else if (presence.Equals (MsnPresence.Busy))
				return "BSY";
			else if (presence.Equals (MsnPresence.Idle))
				return "IDL";
			else if (presence.Equals (MsnPresence.Lunch))
				return "LUN";
			else if (presence.Equals (MsnPresence.Phone))
				return "PHN";
			else if (presence.Equals (MsnPresence.Invisible))
				return "HDN";
			else
				return "FLN";
		}
		
		public static IPresence Get(BasePresence basePresence)
		{
			if (basePresence == BasePresence.Online)
				return Online;
			if (basePresence == BasePresence.Away)
				return Away;
			if (basePresence == BasePresence.Busy)
				return Busy;
			if (basePresence == BasePresence.Idle)
				return Idle;
			if (basePresence == BasePresence.Invisible)
				return Invisible;
			if (basePresence == BasePresence.Offline)
				return Offline;
			
			return null;
		}
	}

	internal sealed class OnlinePresence : AbstractPresence
	{
		public OnlinePresence ()
		{
			base._basePresence = BasePresence.Online; 
		}
		
		public override string State
		{
			get { return "Online"; }
		}
	}

	internal sealed class OfflinePresence : AbstractPresence
	{
		public OfflinePresence ()
		{
			_basePresence = BasePresence.Offline; 
		}
		
		public override string State
		{
			get { return "Offline"; }
		}
	}

	internal sealed class BusyPresence : AbstractPresence
	{
		public BusyPresence ()
		{
			_basePresence = BasePresence.Busy; 
		}
		
		public override string State
		{
			get { return "Busy"; }
		}
	}

	internal sealed class BrbPresence : AbstractPresence
	{
		public BrbPresence ()
		{
			_basePresence = BasePresence.Away; 
		}
		
		public override string State
		{
			get { return "Be Right Back"; }
		}
	}

	internal sealed class IdlePresence : AbstractPresence
	{
		public IdlePresence ()
		{
			_basePresence = BasePresence.Idle; 
		}
		
		public override string State
		{
			get { return "Idle"; }
		}
	}

	internal sealed class AwayPresence : AbstractPresence
	{
		public AwayPresence ()
		{
			_basePresence = BasePresence.Away; 
		}
		
		public override string State
		{
			get { return "Away"; }
		}
	}

	internal sealed class PhonePresence : AbstractPresence
	{
		public PhonePresence ()
		{
			_basePresence = BasePresence.Busy; 
		}
		
		public override string State
		{
			get { return "On The Phone"; }
		}
	}

	internal sealed class LunchPresence : AbstractPresence
	{
		public LunchPresence ()
		{
			_basePresence = BasePresence.Busy; 
		}
		
		public override string State
		{
			get { return "Out To Lunch"; }
		}
	}

	internal sealed class InvisiblePresence : AbstractPresence
	{
		public InvisiblePresence ()
		{
			_basePresence = BasePresence.Invisible; 
		}
		
		public override string State
		{
			get { return "Invisible"; }
		}
	}
}