/*
 * Galaxium Messenger
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	public delegate void AckHandler (P2PMessage ack);
	
	public static class MsnP2PUtility
	{
		struct P2PAppInfo
		{
			public Type appType;
			public Guid eufGuid;
			public uint appId;
		}
		
		static List<P2PAppInfo> _apps = new List<P2PAppInfo> ();
		static List<MsnP2PSession> _sessions = new List<MsnP2PSession> ();
		
		static Dictionary<uint, MemoryStream> _messageStreams = new Dictionary<uint, MemoryStream> ();
		
		static Dictionary<uint, KeyValuePair<P2PMessage, AckHandler>> _ackHandlers = new Dictionary<uint, KeyValuePair<P2PMessage, AckHandler>> ();
		
		static List<IMsnP2PBridge> _bridges = new List<IMsnP2PBridge> ();
		
		public static IEnumerable<MsnP2PSession> Sessions
		{
			get { return _sessions; }
		}
		
		static MsnP2PUtility ()
		{
			try
			{
				AutoAddAssembly (Assembly.GetExecutingAssembly ());
			}
			catch (Exception ex)
			{
				Log.Error (ex, "Exception initializing MsnP2PUtility");
			}
		}
		
		public static void AutoAddAssembly (Assembly asm)
		{
			foreach (Type t in asm.GetTypes ())
			{
				if (t.GetCustomAttributes (typeof (MsnP2PApplicationAttribute), false).Length > 0)
					AddType (t);
			}
		}
		
		public static void AddType (Type t)
		{
			foreach (MsnP2PApplicationAttribute att in t.GetCustomAttributes (typeof (MsnP2PApplicationAttribute), false))
			{
				P2PAppInfo appInfo = new P2PAppInfo ();
				appInfo.appType = t;
				appInfo.appId = att.AppID;
				appInfo.eufGuid = att.EufGuid;
				
				_apps.Add (appInfo);
			}
		}
		
		public static MsnP2PSession Add (IMsnP2PApplication app)
		{
			MsnP2PSession session = new MsnP2PSession (app);
			session.Closed += SessionClosed;
			
			_sessions.Add (session);
			
			return session;
		}
		
		public static MsnP2PSession FindSession (P2PMessage msg)
		{
			uint sessionID = msg.Header.SessionID;
			
			if ((sessionID == 0) && (msg.SLPMessage != null))
			{
				if (msg.SLPMessage.MIMEBody.ContainsKey ("SessionID"))
				{
					if (!uint.TryParse (msg.SLPMessage.MIMEBody["SessionID"].Value, out sessionID))
					{
						Log.Warn ("Unable to parse SLP message SessionID");
						sessionID = 0;
					}
				}
				
				if (sessionID == 0)
				{
					// We don't get a session ID in BYE requests
					// so we need to find the session by its call ID
					
					foreach (MsnP2PSession session in _sessions)
					{
						if (session.Invite.CallID == msg.SLPMessage.CallID)
							return session;
					}
				}
			}
			
			// Sometimes we only have a message ID to find the session with...
			// e.g. the waiting (flag 4) messages wlm sends sometimes
			if ((sessionID == 0) && (msg.Header.MessageID != 0))
			{
				foreach (MsnP2PSession session in _sessions)
				{
					uint expected = session.RemoteID + 1;
					
					if (expected == session.RemoteBaseID)
						expected++;
					
					//Log.Debug ("MessageID {0}, expected {1}", msg.Header.MessageID, expected);
					
					if (msg.Header.MessageID == expected)
						return session;
				}
			}
			
			if (sessionID == 0)
				return null;
			
			foreach (MsnP2PSession session in _sessions)
			{
				if (session.SessionID == sessionID)
					return session;
			}
			
			return null;
		}
		
		static bool ShouldAck (P2PMessage msg)
		{
			if (!msg.Complete)
				return false;
			if (msg.Header.AckUID != 0)
				return false;
			if (msg.SLPMessage != null)
				return false;
			if ((msg.Header.Flags & P2PHeaderFlag.Waiting) == P2PHeaderFlag.Waiting)
				return false;
			if ((msg.Header.Flags & P2PHeaderFlag.CloseSession) == P2PHeaderFlag.CloseSession)
				return false;
			
			return true;
		}
		
		public static void ProcessMessage (IMsnP2PBridge bridge, MsnContact source, P2PMessage msg)
		{
			if (HandleSplitMessage (ref msg))
				return;
			
			// ACK this message (if it isn't an ack itself and isn't an SLP message)
			
			if (ShouldAck (msg))
				bridge.Send (null, source, msg.CreateAck ());
			
			if (msg.SLPMessage != null)
			{
				if (!CheckSLPMessage (bridge, source, msg, msg.SLPMessage))
					return;
			}
			
			if (msg.Header.AckUID != 0)
			{
				// This is an ack
				
				if (_ackHandlers.ContainsKey (msg.Header.AckUID))
				{
					// An AckHandler has been registered for this ack
					
					KeyValuePair<P2PMessage, AckHandler> pair = _ackHandlers[msg.Header.AckUID];
					
					if (pair.Value != null)
						pair.Value (msg);
					else
						Log.Debug ("No AckHandler registered for ack {0}", msg.Header.AckUID);
					
					_ackHandlers.Remove (msg.Header.AckUID);
				}
				else
					Log.Debug ("No AckHandler pair for ack {0}", msg.Header.AckUID);
				
				return;
			}
			
			Log.Debug ("Received MSG:\n"+msg.ToString(true));
			
			MsnP2PSession session = FindSession (msg);
			
			if (session != null)
			{
				if (!session.ProcessMessage (bridge, msg))
				{
					Log.Warn ("P2PSession {0} could not process message\n{1}", session.SessionID, msg);
					
					//SendStatus (bridge, msg, session.Remote, 500, "Internal Error");
				}
				
				return;
			}
			
			if (msg.SLPMessage != null)
			{
				if (ProcessSLPMessage (bridge, source, msg, msg.SLPMessage))
					return;
			}
			
			if ((msg.Header.Flags & P2PHeaderFlag.Waiting) == P2PHeaderFlag.Waiting)
			{
				Log.Debug ("Received P2P waiting message");
				return;
			}
			
			if (msg.SLPMessage != null)
				return;
			
			Log.Warn ("Unhandled P2P message!\n{0}", msg);
		}
		
		static bool CheckSLPMessage (IMsnP2PBridge bridge, MsnContact source, P2PMessage msg, SLPMessage slp)
		{
			if (slp.From != source)
			{
				Log.Warn ("Received message from '{0}', differing from source '{1}'", slp.From.UniqueIdentifier, source.UniqueIdentifier);
				Log.Warn ("Discarding message");
				
				return false;
			}
			else if (slp.To != msg.Session.Account)
			{
				Log.Debug ("Received P2P message intended for {0}, not us\n{1}", slp.To.UniqueIdentifier, msg);
				
				if (slp.From.Local)
					Log.Error ("We received a message from ourselves?");
				else
				{
					bridge.Send (null, source, msg.CreateAck ());
					SendStatus (bridge, msg, source, 404, "Not Found");
				}
				
				return false;
			}
			
			return true;
		}
		
		public static bool ProcessSLPMessage (IMsnP2PBridge bridge, MsnContact source, P2PMessage msg, SLPMessage slp)
		{
			if (slp is SLPRequestMessage)
			{
				SLPRequestMessage req = slp as SLPRequestMessage;
				
				if (req.Method == "INVITE")
				{
					if (req.ContentType == "application/x-msnmsgr-sessionreqbody")
					{
						// Start a new session
						
						MsnP2PSession session = new MsnP2PSession (req, msg);
						session.Closed += SessionClosed;
						
						lock (_sessions)
							_sessions.Add (session);
						
						return true;
					}
				}
			}
			
			return false;
		}
		
		static void SessionClosed (object sender, EventArgs args)
		{
			MsnP2PSession session = sender as MsnP2PSession;
			
			session.Closed -= SessionClosed;
			
			Log.Debug ("P2PSession {0} closed, removing", session.SessionID);
			
			lock (_sessions)
				_sessions.Remove (session);
			
			session.Dispose ();
		}
		
		static void SendStatus (IMsnP2PBridge bridge, P2PMessage msg, MsnContact dest, int code, string phrase)
		{
			SLPMessage slp = new SLPStatusMessage (dest, code, phrase);
			
			if (msg.SLPMessage != null)
			{
				slp.Branch = msg.SLPMessage.Branch;
				slp.CallID = msg.SLPMessage.CallID;
				slp.From = msg.SLPMessage.To;
				slp.ContentType = msg.SLPMessage.ContentType;
			}
			else
				slp.ContentType = "null";
				
			P2PMessage response = new P2PMessage (msg.Session);
			response.SLPMessage = slp;
			
			RegisterAckHandler (msg, delegate {});
			bridge.Send (null, dest, msg);
		}
		
		public static void RegisterAckHandler (P2PMessage msg, AckHandler handler)
		{
			_ackHandlers[msg.Header.AckID] = new KeyValuePair<P2PMessage, AckHandler> (msg, handler);
		}
		
		public static P2PMessage[] SplitMessage (P2PMessage msg, int maxChunkSize)
		{
			if (msg.Payload.Length <= maxChunkSize)
				return new P2PMessage[] { msg };
			
			List<P2PMessage> chunks = new List<P2PMessage> ();
			ulong offset = 0;
			
			while (offset < (ulong)msg.Payload.Length)
			{
				P2PMessage chunk = new P2PMessage (msg);
				
				uint chunkSize = Math.Min ((uint)maxChunkSize, (uint)((ulong)msg.Payload.Length - offset));
				byte[] chunkPayload = new byte [chunkSize];
				
				Array.Copy (msg.Payload, (long)offset, chunkPayload, 0, chunkSize);
				
				chunk.Header.ChunkOffset = offset;
				chunk.Header.ChunkSize = chunkSize;
				chunk.Header.TotalSize = (ulong)msg.Payload.Length;
				
				chunk.Payload = chunkPayload;
				
				chunks.Add (chunk);
				
				offset += chunkSize;
			}
			
			return chunks.ToArray ();
		}
		
		static bool HandleSplitMessage (ref P2PMessage msg)
		{
			// If this is not a split message, return false
			if (((msg.Header.Flags & P2PHeaderFlag.Data) == P2PHeaderFlag.Data) || (msg.Payload.Length == 0) || (msg.Header.ChunkSize == msg.Header.TotalSize))
				return false;
			
			Log.Debug ("Caught split P2P message");
			
			if (!_messageStreams.ContainsKey (msg.Header.MessageID))
				_messageStreams.Add (msg.Header.MessageID, new MemoryStream ());
				
			_messageStreams[msg.Header.MessageID].Write (msg.Payload, 0, msg.Payload.Length);
			
			if ((msg.Header.ChunkOffset + msg.Header.ChunkSize) >= msg.Header.TotalSize)
			{
				// We have the whole message
				
				msg.Header.ChunkOffset = 0;
				msg.Header.ChunkSize = (uint)msg.Header.TotalSize;
				
				msg.Payload = _messageStreams[msg.Header.MessageID].ToArray ();
				
				_messageStreams[msg.Header.MessageID].Close ();
				_messageStreams.Remove (msg.Header.MessageID);
				
				return false;
			}
			
			return true;
		}
		
		internal static Guid GetEufGuid (IMsnP2PApplication app)
		{
			foreach (P2PAppInfo info in _apps)
			{
				if (info.appType == app.GetType ())
					return info.eufGuid;
			}
			
			return new Guid ();
		}
		
		internal static uint GetAppID (IMsnP2PApplication app)
		{
			foreach (P2PAppInfo info in _apps)
			{
				if (info.appType == app.GetType ())
					return info.appId;
			}
			
			return 0;
		}
		
		internal static Type GetApp (Guid eufGuid)
		{
			foreach (P2PAppInfo info in _apps)
			{
				if (info.eufGuid == eufGuid)
					return info.appType;
			}
			
			return null;
		}
		
		internal static IMsnP2PBridge GetBridge (MsnP2PSession session)
		{
			foreach (IMsnP2PBridge existing in _bridges)
				if (existing.SuitableFor (session))
					return existing;
			
			IMsnP2PBridge bridge = new SBBridge (session);
			bridge.BridgeClosed += BridgeClosed;
			
			_bridges.Add (bridge);
			
			return bridge;
		}
		
		internal static IMsnP2PBridge GetBridge (SBConnection sb)
		{
			foreach (IMsnP2PBridge existing in _bridges)
			{
				if (!(existing is SBBridge))
					continue;
				
				if ((existing as SBBridge).Switchboard == sb)
					return existing;
			}
			
			IMsnP2PBridge bridge = new SBBridge (sb);
			bridge.BridgeClosed += BridgeClosed;
			
			_bridges.Add (bridge);
			
			return bridge;
		}
		
		static void BridgeClosed (object sender, EventArgs args)
		{
			IMsnP2PBridge bridge = sender as IMsnP2PBridge;
			
			if (!_bridges.Contains (bridge))
			{
				Log.Error ("Closed bridge not found in list");
				return;
			}
			
			_bridges.Remove (bridge);
		}
	}
}
