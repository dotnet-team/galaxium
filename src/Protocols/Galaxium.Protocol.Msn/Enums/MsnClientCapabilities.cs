/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	[Flags]
	public enum MsnClientCapabilities : long
	{
		Unknown                      = 0,
		MobileDevice                 = 0x01,
		MSN8User                     = 0x02,           // What's this?
		SupportInk_GIF               = 0x04,
		SupportInk_ISF               = 0x08,
		HasWebcam                    = 0x10,
		SupportMultiPacket           = 0x20,
		MsnMobileDevice              = 0x40,
		MsnDirectDevice              = 0x80,
		WebBasedMessenger            = 0x100,
		// 0x200 unknown
		// 0x400 unknown
		ConnectedViaTGW              = 0x800,          // What's this?
		// 0x1000 unknown
		MCEUser                      = 0x2000,
		DirectIM                     = 0x4000,
		CanReceiveWinks              = 0x8000,
		SupportSharedSearch          = 0x10000,
		Bot                          = 0x20000,
		SupportVoiceClips            = 0x40000,
		SupportSChannel              = 0x80000,
		SupportSIPInvitations        = 0x100000,
		// 0x200000 unknown
		SupportSDrive                = 0x400000,
		// 0x800000 unknown
		HasOneCare                   = 0x1000000,
		SupportP2PTURN               = 0x2000000,
		SupportP2PUUNBootstrap       = 0x4000000,
		// 0x8000000 unknown
		
		SupportsMSNC1  = 0x10000000,
		SupportsMSNC2  = 0x20000000,
		SupportsMSNC3  = 0x30000000,
		SupportsMSNC4  = 0x40000000,
		SupportsMSNC5  = 0x50000000,
		SupportsMSNC6  = 0x60000000,
		SupportsMSNC7  = 0x70000000,
		SupportsMSNC8  = 0x80000000,
		SupportsMSNC9  = 0x90000000,
		SupportsMSNC10 = 0xA0000000,
		
		Default = SupportsMSNC8 | DirectIM | SupportMultiPacket | CanReceiveWinks | SupportInk_GIF
				| SupportVoiceClips | SupportP2PUUNBootstrap | SupportSIPInvitations | SupportSDrive
	}
	
	[Flags]
	public enum MsnClientCapabilitiesExtended : long
	{
		None                   = 0,
		RTCVideoEnabled        = 0x10,
		P2PV2                  = 0x20,
	}
}