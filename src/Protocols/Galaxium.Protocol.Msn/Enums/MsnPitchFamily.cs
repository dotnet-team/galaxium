/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	//http://www.hypothetic.org/docs/msn/client/plaintext.php?printable=gray
	//examples:
	//12
	//    Times New Roman, MS Serif, Bitstream Vera Serif
	//22
	//    Arial, Verdana, MS Sans Serif, Bitstream Vera Sans
	//31
	//    Courier New, Courier
	//42
	//    Comic Sans MS 
	[Flags]
	public enum MsnPitchFamily : int
	{
		DontCare = 0x00, //Specifies a generic family name. This name is used when information about a font does not exist or does not matter. The default font is used.
		Roman = 0x10, //Specifies a proportional (variable-width) font with serifs. An example is Times New Roman.
		Swiss = 0x20, //Specifies a proportional (variable-width) font without serifs. An example is Arial.
		Modern = 0x30, //Specifies a monospace font with or without serifs. Monospace fonts are usually modern; examples include Pica, Elite, and Courier New.
		Script = 0x40, //Specifies a font that is designed to look like handwriting; examples include Script and Cursive.
		Decorative = 0x50, //Specifies a novelty font. An example is Old English.

		DefaultPitch = 0x00, //Specifies a generic font pitch. This name is used when information about a font does not exist or does not matter. The default font pitch is used.
		FixedPitch = 0x01, //Specifies a fixed-width (monospace) font. Examples are Courier New and Bitstream Vera Sans Mono.
		VariablePitch = 0x02, //Specifies a variable-width (proportional) font. Examples are Times New Roman and Arial.

		Default = DontCare | DefaultPitch
	}
}