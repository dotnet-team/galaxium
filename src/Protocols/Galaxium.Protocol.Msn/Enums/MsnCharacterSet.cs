/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	//http://www.hypothetic.org/docs/msn/client/plaintext.php?printable=gray
	public enum MsnCharacterSet : int
	{
		Ansi		= 0x00, //ANSI characters
		Default		= 0x01, //Font is chosen based solely on name and size. If the described font is not available on the system, Windows will substitute another font.
		Symbol		= 0x02, // Standard symbol set
		Mac		= 0x4D, //Macintosh characters
		ShiftJIS	= 0x80, //Japanese shift-JIS characters
		Hangeul		= 0x81, //Korean characters (Wansung)
		Johab		= 0x82, //Korean characters (Johab)
		GB2312		= 0x86, //Simplified Chinese characters (Mainland China)
		ChineseBig5	= 0x88, //Traditional Chinese characters (Taiwanese)
		Greek		= 0xA1, //Greek characters
		Turkish		= 0xA2, //Turkish characters
		Vietnamese	= 0xA3, //Vietnamese characters
		Hebrew		= 0xB1, //Hebrew characters
		Arabic		= 0xB2, //Arabic characters
		Baltic		= 0xBA, //Baltic characters
		Russian		= 0xCC, //Cyrillic characters
		Thai		= 0xDE, //Thai characters
		EastEurope	= 0xEE, //Sometimes called the "Central European" character set, this includes diacritical marks for Eastern European countries
		OemDefault	= 0xFF //Depends on the codepage of the operating system
	}
}