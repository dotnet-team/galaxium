/*
 * Galaxium Messenger
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	public enum P2PHeaderFlag : uint
	{
		Normal                  = 0,
		NegativeAck             = 0x1,
		Ack                     = 0x2,
		Waiting                 = 0x4,
		Error                   = 0x8,
		File                    = 0x10,
		Data                    = 0x20,
		CloseSession            = 0x40,
		TlpError                = 0x80,
		DirectHandshake         = 0x100,
		Encrypted               = 0x200,                       //http://telepathy.freedesktop.org/wiki/Pymsn/MSNP2P
		FileData                = 0x01000000 | File | Data
	}
}
