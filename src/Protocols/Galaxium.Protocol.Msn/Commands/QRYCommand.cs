/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("QRY")]
	[TransactionCommand]
	[PayloadCommand (false, true)]
	public class QRYCommand: AbstractMsnCommand
	{
		string _challenge;
		
		public override byte[] Payload
		{
			get { return Encoding.UTF8.GetBytes (_challenge); }
			set { _challenge = Encoding.UTF8.GetString (value); }
		}
		
		public string Challenge
		{
			get { return _challenge; }
			set { _challenge = value; }
		}
		
		public string ProductId
		{
			get { return GetArgument (0); }
			set { SetArgument (0, value); }
		}
		
		public QRYCommand (MsnSession session, string productId, string challenge)
			: base (session)
		{
			ProductId = productId;
			Challenge = challenge;
		}
		
		public QRYCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
