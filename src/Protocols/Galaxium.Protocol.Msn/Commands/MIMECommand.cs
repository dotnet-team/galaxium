/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;

namespace Galaxium.Protocol.Msn
{
	public abstract class MIMECommand : AbstractMsnCommand
	{
		MIMECollection _mimeHeader = new MIMECollection ();
		byte[] _payload = new byte[0];
		
		public MIMECollection MIMEHeader
		{
			get { return _mimeHeader; }
			protected set { _mimeHeader = value; }
		}
		
		public override byte[] Payload
		{
			get { return _payload; }
			set { _payload = value; }
		}
		
		protected MIMECommand (MsnSession session)
			: base (session)
		{
			MIMEHeader["MIME-Version"] = "1.0";
		}
		
		protected MIMECommand (MsnSession session, byte[] data)
			: base (session, data)
		{
			if (_payload.Length > 0)
			{
				// After AbstractMsnCommand has parsed the command, the MIME header is part of the payload
				// Here, we parse the MIMEHeader & leave Payload containing everything after it
				
				_mimeHeader.Clear ();
				int mimeLen = _mimeHeader.Parse (_payload);
				
				byte[] payload = new byte[_payload.Length - mimeLen];
				
				if (payload.Length > 0)
					Array.Copy (_payload, mimeLen, payload, 0, payload.Length);
				
				Payload = payload;
			}
		}
		
		public override string ToCommandString ()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append (Command);
			
			if ((Incoming && IsTransIn) || ((!Incoming) && IsTransOut))
				sb.Append (" " + TransactionID.ToString ());
			
			foreach (string arg in Arguments)
				sb.Append (" " + arg);
			
			int payloadLen = Encoding.UTF8.GetByteCount (_mimeHeader.ToString ());
			if (payloadLen > 0)
				payloadLen += 2;
			
			if ((Incoming && IsPayloadIn) || ((!Incoming) && IsPayloadOut))
				payloadLen += Payload.Length;
			
			sb.Append (" " + payloadLen.ToString ());
			
			return sb.ToString ();
		}
		
		public override byte[] ToByteArray ()
		{
			MessageBuilder mb = new Galaxium.Protocol.MessageBuilder ();
			
			mb.Append (ToCommandString ());
			mb.Append ("\r\n");
			
			if (_mimeHeader.Count > 0)
			{
				mb.Append (_mimeHeader.ToString ());
				mb.Append ("\r\n");
			}
			
			if ((Incoming && IsPayloadIn) || ((!Incoming) && IsPayloadOut))
				mb.Append (Payload);
			
			return mb.GetByteArray ();
		}
	}
}
