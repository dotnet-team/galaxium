/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand (null)]
	[TransactionCommand]
	public class ErrorCommand: AbstractMsnCommand
	{
		int _errorCode;
		byte[] _rawData;
		
		public override string Command
		{
			get { return _errorCode.ToString (); }
		}
		
		public int ErrorCode
		{
			get { return _errorCode; }
		}

		public byte[] RawData
		{
			get { return _rawData; }
		}
		
		public string RawString
		{
			get { return Encoding.UTF8.GetString (_rawData); }
		}
				
		public ErrorCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
			_rawData = data;
			int.TryParse (TextUtility.GetParameter (data, 0), out _errorCode);
		}
		
		public override string ToCommandString ()
		{
			return RawString;
		}
	}
}
