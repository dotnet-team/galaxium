/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("ADL")]
	[TransactionCommand]
	[PayloadCommand]
	public class ADLCommand: ListCommand
	{
		public ADLCommand (MsnSession session)
			: base (session)
		{
		}
		
		public ADLCommand (MsnSession session, bool initial)
			: base (session, initial)
		{
		}
			
		public ADLCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
		
		protected override void CorrectLists (ref MsnListType lists)
		{
			// Here we remove Reverse and Pending lists, ADL doesn't care about them
			lists &= ~MsnListType.Reverse;
			lists &= ~MsnListType.Pending;
		}
	}
}
