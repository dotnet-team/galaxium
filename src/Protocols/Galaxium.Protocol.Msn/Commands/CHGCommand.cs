/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("CHG")]
	[TransactionCommand]
	public class CHGCommand: AbstractMsnCommand
	{
		public IPresence Presence
		{
			get { return MsnPresence.ParseMsnPresence (GetArgument (0)); }
			set { SetArgument (0, MsnPresence.GetMsnPresenceCode (value)); }
		}
		
		public MsnClientCapabilities ClientId
		{
			get
			{
				if (Arguments.Count < 2)
					return 0;
				
				return (MsnClientCapabilities)long.Parse (GetArgument (1));
			}
			set { SetArgument (1, ((long)value).ToString ()); }
		}

		public MsnDisplayImage DisplayImage
		{
			get
			{
				string context = GetArgument (2);
				
				if (string.IsNullOrEmpty(context) || (context == "0"))
					return null;
				
				return new MsnDisplayImage (Session, context);
			}
			set { SetArgument (2, value != null ? value.ContextEncoded : "0"); }
		}
		
		public CHGCommand (MsnSession session, IPresence presence, MsnClientCapabilities clientId, MsnDisplayImage displayImage)
			: base (session)
		{
			Presence = presence;
			ClientId = clientId;
			DisplayImage = displayImage;
		}

		public CHGCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
