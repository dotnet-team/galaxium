/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("JOI")]
	public class JOICommand: AbstractMsnCommand
	{
		public MsnContact Contact
		{
			get
			{
				//FIXME: We end up getting both for some reason... i'm not sure why.
				string contact = GetArgument (0);
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
					if (contact.IndexOf (';') >= 0)
						contact = contact.Split(';')[0];
				
				return Session.FindContact (contact);
			}
		}

		public string DisplayName
		{
			get { return EncodingUtility.UrlDecode (GetArgument (1)); }
		}

		public MsnClientCapabilities ClientIdentifier
		{
			get
			{
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
				{
					string[] capabilities = GetArgument (2).Split (':');
					
					return (MsnClientCapabilities)long.Parse (capabilities[0]);
				}
				
				return (MsnClientCapabilities)long.Parse (GetArgument (2));
			}
		}
		
		public MsnClientCapabilitiesExtended ClientCapabilitiesExtended
		{
			get
			{
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
				{
					string[] capabilities = GetArgument (2).Split (':');
					
					return (MsnClientCapabilitiesExtended)long.Parse (capabilities[1]);
				}
				
				return MsnClientCapabilitiesExtended.None;
			}
		}

		public JOICommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
