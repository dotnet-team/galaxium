/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("PRP")]
	[TransactionCommand]
	public class PRPCommand: AbstractMsnCommand
	{
		public string Type
		{
			get { return GetArgument (0); }
			set { SetArgument (0, value); }
		}

		public string Value
		{
			get { return EncodingUtility.UrlDecodeSafe (GetArgument (1)); }
			set { SetArgument (1, EncodingUtility.UrlEncode (value)); }
		}

		private PRPCommand (MsnSession session)
			: base (session)
		{
			Type = "MFN";
		}
		
		public PRPCommand (MsnSession session, string value)
			: base (session)
		{
			Type = "MFN";
			Value = value;
		}
		
		public PRPCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
