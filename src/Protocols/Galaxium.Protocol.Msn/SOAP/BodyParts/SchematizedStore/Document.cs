/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	public enum DocumentItemType { Unknown, Photo }
	
	[XmlInclude (typeof (Photo))]
	public class Document
	{
		string _itemType;
		bool _itemTypeSpecified;
		string _resourceID;
		bool _resourceIDSpecified;
		DateTime _dateModified;
		bool _dateModifiedSpecified;
		string _name;
		bool _nameSpecified;
		DocumentStreamCollection _documentStreams = new DocumentStreamCollection ();
		
		[XmlElement ("ItemType")]
		public string _ItemType
		{
			get { return _itemType; }
			set
			{
				_itemType = value;
				ItemTypeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public DocumentItemType ItemType
		{
			get
			{
				try
				{
					return (DocumentItemType)Enum.Parse (typeof (DocumentItemType), _itemType);
				}
				catch
				{
					Log.Warn ("Unknown value '{0}'", _itemType);
					return DocumentItemType.Unknown;
				}
			}
			set { _ItemType = value.ToString (); }
		}
		
		[XmlIgnore]
		public bool ItemTypeSpecified
		{
			get { return _itemTypeSpecified; }
			set { _itemTypeSpecified = value; }
		}
		
		public string ResourceID
		{
			get { return _resourceID; }
			set
			{
				_resourceID = value;
				ResourceIDSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool ResourceIDSpecified
		{
			get { return _resourceIDSpecified; }
			set { _resourceIDSpecified = value; }
		}
		
		public DateTime DateModified
		{
			get { return _dateModified; }
			set
			{
				_dateModified = value;
				DateModifiedSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool DateModifiedSpecified
		{
			get { return _dateModifiedSpecified; }
			set { _dateModifiedSpecified = value; }
		}
		
		public string Name
		{
			get { return _name; }
			set
			{
				_name = value;
				NameSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool NameSpecified
		{
			get { return _nameSpecified; }
			set { _nameSpecified = value; }
		}
		
		[XmlArray ("DocumentStreams"), XmlArrayItem ("DocumentStream", typeof (DocumentStream))]
		public DocumentStreamCollection DocumentStreams
		{
			get { return _documentStreams; }
			set { _documentStreams = value; }
		}
	}
	
	public class Photo : Document
	{
		public Photo ()
		{
		}
	}
}
