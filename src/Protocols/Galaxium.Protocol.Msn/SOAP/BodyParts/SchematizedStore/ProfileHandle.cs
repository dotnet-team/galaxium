/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsStorage)]
	public class ProfileHandle : ObjectHandle
	{
		ProfileAlias _alias;
		bool _aliasSpecified;
		string _relationshipName;
		bool _relationshipNameSpecified;
		
		public ProfileAlias Alias
		{
			get { return _alias; }
			set
			{
				_alias = value;
				AliasSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool AliasSpecified
		{
			get { return _aliasSpecified; }
			set { _aliasSpecified = value; }
		}
		
		public string RelationshipName
		{
			get { return _relationshipName; }
			set
			{
				_relationshipName = value;
				RelationshipNameSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool RelationshipNameSpecified
		{
			get { return _relationshipNameSpecified; }
			set { _relationshipNameSpecified = value; }
		}
	}
}
