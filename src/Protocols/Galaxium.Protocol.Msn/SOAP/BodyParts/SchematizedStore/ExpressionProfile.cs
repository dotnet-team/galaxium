/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	public class ExpressionProfile
	{
		string _resourceID;
		bool _resourceIDSpecified;
		DateTime _dateModified;
		bool _dateModifiedSpecified;
		string _freeText;
		bool _freeTextSpecified;
		long _flags;
		bool _flagsSpecified;
		string _displayName;
		bool _displayNameSpecified;
		DateTime _displayNameLastModified;
		bool _displayNameLastModifiedSpecified;
		string _personalStatus;
		bool _personalStatusSpecified;
		DateTime _personalStatusLastModified;
		bool _personalStatusLastModifiedSpecified;
		string _staticUserTilePublicURL;
		bool _staticUserTilePublicURLSpecified;
		Document _photo;
		bool _photoSpecified;
		
		public string ResourceID
		{
			get { return _resourceID; }
			set { _resourceID = value; }
		}

		[XmlIgnore]
		public bool ResourceIDSpecified
		{
			get { return _resourceIDSpecified; }
			set { _resourceIDSpecified = value; }
		}
		
		public DateTime DateModified
		{
			get { return _dateModified; }
			set { _dateModified = value; }
		}
		
		[XmlIgnore]
		public bool DateModifiedSpecified
		{
			get { return _dateModifiedSpecified; }
			set { _dateModifiedSpecified = value; }
		}
		
		public string FreeText
		{
			get { return _freeText; }
			set
			{
				_freeText = value;
				FreeTextSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool FreeTextSpecified
		{
			get { return _freeTextSpecified; }
			set { _freeTextSpecified = value; }
		}
		
		public long Flags
		{
			get { return _flags; }
			set
			{
				_flags = value;
				FlagsSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool FlagsSpecified
		{
			get { return _flagsSpecified; }
			set { _flagsSpecified = value; }
		}
		
		public string DisplayName
		{
			get { return _displayName; }
			set 
			{
				_displayName = value;
				DisplayNameSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool DisplayNameSpecified
		{
			get { return _displayNameSpecified; }
			set { _displayNameSpecified = value; }
		}
		
		public DateTime DisplayNameLastModified
		{
			get { return _displayNameLastModified; }
			set
			{
				_displayNameLastModified = value;
				DisplayNameLastModifiedSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool DisplayNameLastModifiedSpecified
		{
			get { return _displayNameLastModifiedSpecified; }
			set { _displayNameLastModifiedSpecified = value; }
		}
		
		public string PersonalStatus
		{
			get { return _personalStatus; }
			set
			{
				_personalStatus = value;
				PersonalStatusSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool PersonalStatusSpecified
		{
			get { return _personalStatusSpecified; }
			set { _personalStatusSpecified = value; }
		}
		
		public DateTime PersonalStatusLastModified
		{
			get { return _personalStatusLastModified; }
			set
			{
				_personalStatusLastModified = value;
				PersonalStatusLastModifiedSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool PersonalStatusLastModifiedSpecified
		{
			get { return _personalStatusLastModifiedSpecified; }
			set { _personalStatusLastModifiedSpecified = value; }
		}
		
		public string StaticUserTilePublicURL
		{
			get { return _staticUserTilePublicURL; }
			set
			{
				_staticUserTilePublicURL = value;
				StaticUserTilePublicURLSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool StaticUserTilePublicURLSpecified
		{
			get { return _staticUserTilePublicURLSpecified; }
			set { _staticUserTilePublicURLSpecified = value; }
		}
		
		public Document Photo
		{
			get { return _photo; }
			set
			{
				_photo = value;
				PhotoSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool PhotoSpecified
		{
			get { return _photoSpecified; }
			set { _photoSpecified = value; }
		}
	}
}
