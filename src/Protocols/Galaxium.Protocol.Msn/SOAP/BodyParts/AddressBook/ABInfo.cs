/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class ABInfo
	{
		long _ownerPuid;
		bool _ownerPuidSpecified = false;
		long _ownerCID;
		bool _ownerCIDSpecified = false;
		string _ownerEmail;
		bool _ownerEmailSpecified = false;
		string _name;
		bool _nameSpecified = false;
		bool _fDefault;
		bool _fDefaultSpecified = false;
		
		[XmlElement ("ownerPuid")]
		public long OwnerPuid
		{
			get { return _ownerPuid; }
			set
			{
				_ownerPuid = value;
				_ownerPuidSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool OwnerPuidSpecified
		{
			get { return _ownerPuidSpecified; }
			set { _ownerPuidSpecified = value; }
		}
		
		public long OwnerCID
		{
			get { return _ownerCID; }
			set
			{
				_ownerCID = value;
				_ownerCIDSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool OwnerCIDSpecified
		{
			get { return _ownerCIDSpecified; }
			set { _ownerCIDSpecified = value; }
		}
		
		[XmlElement ("ownerEmail")]
		public string OwnerEmail
		{
			get { return _ownerEmail; }
			set
			{
				_ownerEmail = value;
				_ownerEmailSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool OwnerEmailSpecified
		{
			get { return _ownerEmailSpecified; }
			set { _ownerEmailSpecified = value; }
		}
		
		[XmlElement ("name")]
		public string Name
		{
			get { return _name; }
			set
			{
				_name = value;
				_nameSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool NameSpecified
		{
			get { return _nameSpecified; }
			set { _nameSpecified = value; }
		}
		
		[XmlElement ("fDefault")]
		public bool FDefault
		{
			get { return _fDefault; }
			set
			{
				_fDefault = value;
				_fDefaultSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool FDefaultSpecified
		{
			get { return _fDefaultSpecified; }
			set { _fDefaultSpecified = value; }
		}
	}
}
