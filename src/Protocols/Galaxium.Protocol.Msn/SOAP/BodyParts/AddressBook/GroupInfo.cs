/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class GroupInfo
	{
		AnnotationCollection _annotations = null;
		bool _annotationsSpecified = false;
		Guid _type = new Guid ();
		bool _typeSpecified = false;
		string _name;
		bool _nameSpecified = false;
		bool _messenger;
		bool _messengerSpecified = false;

		[XmlArray ("annotations")]
		[XmlArrayItem (Type=typeof (Annotation))]
		public AnnotationCollection Annotations
		{
			get { return _annotations; }
			set
			{
				_annotations = value;
				_annotationsSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool AnnotationsSpecified
		{
			get { return _annotationsSpecified; }
			set { _annotationsSpecified = value; }
		}
		
		[XmlElement ("groupType")]
		public Guid Type
		{
			get { return _type; }
			set
			{
				_type = value;
				_typeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool TypeSpecified
		{
			get { return _typeSpecified; }
			set { _typeSpecified = value; }
		}
		
		[XmlElement ("name")]
		public string Name
		{
			get { return _name; }
			set
			{
				_name = value;
				_nameSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool NameSpecified
		{
			get { return _nameSpecified; }
			set { _nameSpecified = value; }
		}
		
		[XmlElement ("fMessenger")]
		public bool Messenger
		{
			get { return _messenger; }
			set
			{
				_messenger = value;
				_messengerSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool MessengerSpecified
		{
			get { return _messengerSpecified; }
			set { _messengerSpecified = value; }
		}
		
		public GroupInfo Clone ()
		{
			GroupInfo g = new GroupInfo ();
			
			g._annotations = (_annotations != null) ? _annotations.Clone () : null;
			g._annotationsSpecified = _annotationsSpecified;
			g._messenger = _messenger;
			g._messengerSpecified = _messengerSpecified;
			g._name = _name;
			g._nameSpecified = _nameSpecified;
			g._type = _type;
			g._typeSpecified = _typeSpecified;
			
			return g;
		}
	}
}
