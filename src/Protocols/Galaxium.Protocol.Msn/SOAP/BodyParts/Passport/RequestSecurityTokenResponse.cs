/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot ("RequestSecurityTokenResponse", Namespace=SoapConstants.nsWst)]
	public class RequestSecurityTokenResponse
	{
		string _tokenType;
		AppliesTo _appliesTo = new AppliesTo ();
		LifeTime _lifeTime = new LifeTime ();
		RequestedSecurityToken _requestedSecurityToken = new RequestedSecurityToken ();
		RequestedProofToken _requestedProofToken = new RequestedProofToken ();
		
		public string TokenType
		{
			get { return _tokenType; }
			set { _tokenType = value; }
		}
		
		[XmlElement (Namespace=SoapConstants.nsWsp)]
		public AppliesTo AppliesTo
		{
			get { return _appliesTo; }
			set { _appliesTo = value; }
		}
		
		public LifeTime LifeTime
		{
			get { return _lifeTime; }
			set { _lifeTime = value; }
		}
		
		public RequestedSecurityToken RequestedSecurityToken
		{
			get { return _requestedSecurityToken; }
			set { _requestedSecurityToken = value; }
		}
		
		public RequestedProofToken RequestedProofToken
		{
			get { return _requestedProofToken; }
			set { _requestedProofToken = value; }
		}
	}
	
	[XmlRoot (Namespace=SoapConstants.nsWst)]
	public class RequestSecurityTokenResponseCollection : List<RequestSecurityTokenResponse>
	{
	}
}
