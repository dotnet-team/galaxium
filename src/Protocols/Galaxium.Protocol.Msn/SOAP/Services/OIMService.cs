/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Galaxium.Protocol.Msn.Soap.BodyParts;
using Galaxium.Protocol.Msn.Soap.Headers;

namespace Galaxium.Protocol.Msn.Soap
{
	[WebServiceBinding (Name="soap:Envelope", Namespace="http://schemas.xmlsoap.org/soap/envelope/")]
	internal class OIMService : MsnSoapService
	{
		public PassportCookieHeader passportHeader;
		
		public OIMService (MsnSession session)
			: base (session)
		{
			passportHeader = new PassportCookieHeader (session);
			
			AddUrl ("https://rsi.hotmail.com/rsi/rsi.asmx");
			UserAgent = SoapConstants.UserAgent;
		}
		
		[SoapDocumentMethod ("http://www.hotmail.msn.com/ws/2004/09/oim/rsi/GetMessage", RequestNamespace="http://www.hotmail.msn.com/ws/2004/09/oim/rsi", ResponseNamespace="http://www.hotmail.msn.com/ws/2004/09/oim/rsi")]
		[SoapHeader ("passportHeader")]
		public string GetMessage (string messageId, bool alsoMarkAsRead)
		{
			object[] results = this.Invoke ("GetMessage", new object[] { messageId, alsoMarkAsRead });
			return (string)results[0];
		}
		
		public IAsyncResult BeginGetMessage (string messageId, bool alsoMarkAsRead, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("GetMessage", new object[] { messageId, alsoMarkAsRead }, callback, asyncState);
		}
		
		public string EndGetMessage (IAsyncResult asyncResult)
		{
			object[] results = this.EndInvoke (asyncResult);
            return (string)results[0];
		}
		
		[SoapDocumentMethod ("http://www.hotmail.msn.com/ws/2004/09/oim/rsi/DeleteMessages", RequestNamespace="http://www.hotmail.msn.com/ws/2004/09/oim/rsi", ResponseNamespace="http://www.hotmail.msn.com/ws/2004/09/oim/rsi")]
		[SoapHeader ("passportHeader")]
		public void DeleteMessages ([XmlArrayItem (ElementName="messageId")] string[] messageIds)
		{
			this.Invoke ("DeleteMessages", new object[] { messageIds });
		}
		
		public IAsyncResult BeginDeleteMessages ([XmlArrayItem (ElementName="messageId")] string[] messageIds, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("DeleteMessages", new object[] { messageIds }, callback, asyncState);
		}
		
		public void EndDeleteMessages (IAsyncResult asyncResult)
		{
			this.EndInvoke (asyncResult);
		}
		
		[SoapDocumentMethod ("http://www.hotmail.msn.com/ws/2004/09/oim/rsi/GetMetadata", RequestNamespace="http://www.hotmail.msn.com/ws/2004/09/oim/rsi", ResponseNamespace="http://www.hotmail.msn.com/ws/2004/09/oim/rsi")]
		[SoapHeader ("passportHeader")]
		public string GetMetadata ()
		{
			object[] results = this.Invoke ("GetMetadata", new object[0]);
			return (string)results[0];
		}
		
		public IAsyncResult BeginGetMetadata (AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("GetMetadata", new object[0], callback, asyncState);
		}
		
		public string EndGetMetadata (IAsyncResult asyncResult)
		{
			object[] results = this.EndInvoke (asyncResult);
			return (string)results[0];
		}
	}
}
