/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Galaxium.Protocol.Msn.Soap.BodyParts;
using Galaxium.Protocol.Msn.Soap.Headers;

namespace Galaxium.Protocol.Msn.Soap
{
	[WebServiceBinding (Name="soap:Envelope", Namespace="http://schemas.xmlsoap.org/soap/envelope/")]
	internal class OIMStoreService : MsnSoapService
	{
		public FromHeader fromHeader;
		public ToHeader toHeader;
		public TicketHeader ticketHeader;
		public SequenceHeader sequenceHeader;
		
		public OIMStoreService (MsnSession session)
			: base (session)
		{
			fromHeader = new FromHeader (session);
			toHeader = new ToHeader ();
			ticketHeader = new TicketHeader (session);
			sequenceHeader = new SequenceHeader ();
			
			AddUrl ("https://ows.messenger.msn.com/OimWS/oim.asmx");
			UserAgent = SoapConstants.UserAgent;
		}
		
		[SoapDocumentMethod ("http://messenger.live.com/ws/2006/09/oim/Store2", RequestNamespace=SoapConstants.nsOIM, ResponseNamespace=SoapConstants.nsOIM, ParameterStyle=SoapParameterStyle.Bare)]
		[SoapHeader ("sequenceHeader")]
		[SoapHeader ("ticketHeader")]
		[SoapHeader ("toHeader")]
		[SoapHeader ("fromHeader")]
		public StoreResponse Store2 ([XmlElement ("MessageType", Namespace=SoapConstants.nsOIM)] string messageType, [XmlElement ("Content", Namespace=SoapConstants.nsOIM)] string content)
		{
			object[] results = this.Invoke ("Store2", new object[] { messageType, content });
			return (StoreResponse)results[0];
		}
		
		public IAsyncResult BeginStore2 ([XmlElement ("MessageType", Namespace=SoapConstants.nsOIM)] string messageType, [XmlElement ("Content", Namespace=SoapConstants.nsOIM)] string content, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("Store2", new object[] { messageType, content }, callback, asyncState);
		}
		
		public StoreResponse EndStore2 (IAsyncResult asyncResult)
		{
			object[] results = this.EndInvoke (asyncResult);
            return (StoreResponse)results[0];
		}
	}
}
