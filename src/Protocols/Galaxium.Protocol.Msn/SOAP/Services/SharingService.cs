/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Galaxium.Protocol.Msn.Soap.BodyParts;
using Galaxium.Protocol.Msn.Soap.Headers;

namespace Galaxium.Protocol.Msn.Soap
{
	[WebServiceBinding (Name="soap:Envelope", Namespace="http://schemas.xmlsoap.org/soap/envelope/")]
	public class SharingService : MsnSoapService
	{
		public ABApplicationHeader appHeader;
		public ABAuthHeader authHeader;
		public ServiceHeader serviceHeader;
		
		public SharingService (MsnSession session)
			: base (session)
		{
			AddUrl ("http://contacts.msn.com/abservice/SharingService.asmx");
			AddUrl ("https://omega.contacts.msn.com/abservice/SharingService.asmx");
			AddUrl ("https://by4.omega.contacts.msn.com/abservice/SharingService.asmx");
			
			appHeader = new ABApplicationHeader ();
			authHeader = new ABAuthHeader (session);
		}
		
		void ProcessServiceHeader ()
		{
			if (serviceHeader != null)
			{
				if (!string.IsNullOrEmpty (serviceHeader.PreferredHostName))
				{
					UpdateDomain (serviceHeader.PreferredHostName);
					Session.ABService.UpdateDomain (serviceHeader.PreferredHostName);
				}
				if (serviceHeader.CacheKeyChanged)
					appHeader.CacheKey = serviceHeader.CacheKey;
				
				serviceHeader = null;
			}
		}
		
		internal void UpdateDomain (string domain)
		{
			SwitchUrl ("http://" + domain + "/abservice/SharingService.asmx");
		}
		
		[SoapDocumentMethod ("http://www.msn.com/webservices/AddressBook/FindMembership", RequestNamespace=SoapConstants.nsAddressBook, ResponseNamespace=SoapConstants.nsAddressBook)]
		[SoapHeader ("appHeader", Direction=SoapHeaderDirection.In)]
		[SoapHeader ("authHeader", Direction=SoapHeaderDirection.In)]
		[SoapHeader ("serviceHeader", Direction=SoapHeaderDirection.Out)]
		[RequireSecurityTokens (SecurityToken.Contacts)]
		public FindMembershipResult FindMembership (ServiceFilter serviceFilter)
		{
			object[] results = this.Invoke ("FindMembership", new object[] { serviceFilter });
			ProcessServiceHeader ();
			return (FindMembershipResult)results[0];
		}
		
		public IAsyncResult BeginFindMembership (ServiceFilter serviceFilter, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("FindMembership", new object[] { serviceFilter }, callback, asyncState);
		}
		
		public FindMembershipResult EndFindMembership (IAsyncResult asyncResult)
		{
			object[] results = this.EndInvoke (asyncResult);
            return (FindMembershipResult)results[0];
		}
		
		[SoapDocumentMethod ("http://www.msn.com/webservices/AddressBook/FindMembership",
		                     RequestElementName="FindMembership", ResponseElementName="FindMembershipResponse",
		                     RequestNamespace=SoapConstants.nsAddressBook, ResponseNamespace=SoapConstants.nsAddressBook)]
		[SoapHeader ("appHeader", Direction=SoapHeaderDirection.In)]
		[SoapHeader ("authHeader", Direction=SoapHeaderDirection.In)]
		[SoapHeader ("serviceHeader", Direction=SoapHeaderDirection.Out)]
		[RequireSecurityTokens (SecurityToken.Contacts)]
		public FindMembershipResult FindMembershipDelta (ServiceFilter serviceFilter, ABView View, bool deltasOnly, string lastChange)
		{
			object[] results = this.Invoke ("FindMembershipDelta", new object[] { serviceFilter, View, deltasOnly, lastChange });
			ProcessServiceHeader ();
			return (FindMembershipResult)results[0];
		}
		
		public IAsyncResult BeginFindMembershipDelta (ServiceFilter serviceFilter, ABView View, bool deltasOnly, string lastChange, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("FindMembershipDelta", new object[] { serviceFilter, View, deltasOnly, lastChange }, callback, asyncState);
		}
		
		public FindMembershipResult EndFindMembershipDelta (IAsyncResult asyncResult)
		{
			object[] results = this.EndInvoke (asyncResult);
            return (FindMembershipResult)results[0];
		}
		
		[SoapDocumentMethod ("http://www.msn.com/webservices/AddressBook/AddMember", RequestNamespace=SoapConstants.nsAddressBook, ResponseNamespace=SoapConstants.nsAddressBook)]
		[SoapHeader ("appHeader", Direction=SoapHeaderDirection.In)]
		[SoapHeader ("authHeader", Direction=SoapHeaderDirection.In)]
		[SoapHeader ("serviceHeader", Direction=SoapHeaderDirection.Out)]
		[RequireSecurityTokens (SecurityToken.Contacts)]
		public void AddMember (ServiceHandle serviceHandle, MembershipCollection memberships)
		{
			appHeader.PartnerScenario = ABPartnerScenario.BlockUnblock;
			this.Invoke ("AddMember", new object[] { serviceHandle, memberships });
			ProcessServiceHeader ();
		}
		
		public IAsyncResult BeginAddMember (ServiceHandle serviceHandle, MembershipCollection memberships, AsyncCallback callback, object asyncState)
		{
			appHeader.PartnerScenario = ABPartnerScenario.BlockUnblock;
			return this.BeginInvoke ("AddMember", new object[] { serviceHandle, memberships }, callback, asyncState);
		}
		
		public void EndAddMember (IAsyncResult asyncResult)
		{
			this.EndInvoke (asyncResult);
		}
		
		[SoapDocumentMethod ("http://www.msn.com/webservices/AddressBook/DeleteMember", RequestNamespace=SoapConstants.nsAddressBook, ResponseNamespace=SoapConstants.nsAddressBook)]
		[SoapHeader ("appHeader", Direction=SoapHeaderDirection.In)]
		[SoapHeader ("authHeader", Direction=SoapHeaderDirection.In)]
		[SoapHeader ("serviceHeader", Direction=SoapHeaderDirection.Out)]
		[RequireSecurityTokens (SecurityToken.Contacts)]
		public void DeleteMember (ServiceHandle serviceHandle, MembershipCollection memberships)
		{
			appHeader.PartnerScenario = ABPartnerScenario.BlockUnblock;
			this.Invoke ("DeleteMember", new object[] { serviceHandle, memberships });
			ProcessServiceHeader ();
		}
		
		public IAsyncResult BeginDeleteMember (ServiceHandle serviceHandle, MembershipCollection memberships, AsyncCallback callback, object asyncState)
		{
			appHeader.PartnerScenario = ABPartnerScenario.BlockUnblock;
			return this.BeginInvoke ("DeleteMember", new object[] { serviceHandle, memberships }, callback, asyncState);
		}
		
		public void EndDeleteMember (IAsyncResult asyncResult)
		{
			this.EndInvoke (asyncResult);
		}
	}
}
