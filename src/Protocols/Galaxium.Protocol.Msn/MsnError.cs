/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2005-2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	public static class MsnError
	{
		public static bool RequiresUserNotification (int errorCode)
		{
			switch (errorCode) {
				case 923: case 924: case 928:
				case 911: case 917: case 900:
				case 912: case 918: case 919:
				case 921: case 922: case 601:
				case 605: case 914: case 915:
				case 916: case 604:
					return true;
				default:
					return false;
			}
		}

		public static string GetErrorDescription (int errorCode)
		{
			switch (errorCode) {
				case 200: return "Invalid syntax.";
				case 201: return "Invalid parameter.";
				case 205: return "Contact does not exist.";
				case 206: return "Domain name is missing.";
				case 207: return "Already logged in.";
				case 208: return "Invalid account name.";
				case 210: return "Contact list is full.";
				case 215: return "Contact already in list.";
				case 216: return "Contact not in list.";
				case 217: return "Contact not online.";
				case 218: return "Already in mode.";
				case 219: return "Contact in opposite list.";
				case 223: return "Group list full.";
				case 224: return "Group does not exist.";
				case 225: return "Contact not in group.";
				case 229: return "Invalid group name.";
				case 230: return "Cannot remove group 0.";
				case 231: return "Invalid Group.";
				case 280: return "Switchboard failure.";
				case 281: return "Switchboard transfer failure.";

				case 300: return "Required field missing.";
				case 302: return "Not logged in.";

				case 500: return "Internal server error.";
				case 501: return "Database server error.";
				case 502: return "Command disabled.";
				case 510: return "Internal file operation failed.";
				case 520: return "Internal memory allocation failed.";
				case 540: return "Challenge response failed.";

				case 600: return "Server busy.";
				case 602: return "Peer nameserver down.";
				case 603: return "Database connection failed.";
				case 604: return "Server is going down.";

				case 707: return "Could not create connection.";
				case 710: return "Bad CVR message.";
				case 711: return "Write is blocking.";
				case 712: return "Session overloaded.";
				case 713: return "Calling too rapidly.";
				case 714: return "Too many sessions.";
				case 715: return "Unexpected PRP or REA.";
				case 717: return "Bad friend file.";
				case 731: return "Unexpected CVR.";

				case 800: return "Changing too rapidly.";

				case 913: return "Not allowed when hiding.";
				case 920: return "Not accepting new contacts.";
				case 923: return "Kid without parental consent.";
				case 924: return "Passport account not yet verified.";
				case 928: return "Bad ticket.";

				case 911: case 917:
					return "Authentication failed.";

				case 900: case 912:
				case 918: case 919:
				case 921: case 922:
					return "Server Too Busy.";

				case 601: case 605:
				case 914: case 915: case 916:
					return "Server unavailable.";
			}
			return String.Empty;
		}
	}
}