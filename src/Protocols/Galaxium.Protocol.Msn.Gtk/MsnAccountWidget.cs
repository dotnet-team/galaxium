/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using G=Gtk;
using Gtk;
using Glade;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class MsnAccountWidget : BasicAccountWidget
	{
		protected bool _http = false;
		
		public MsnAccountWidget () : base (MsnProtocol.Instance)
		{
			
		}
		
		public override void Initialize ()
		{
			_presenceCombo = CreateStatusCombo (MsnPresence.Online, //the selected one
					MsnPresence.Online, MsnPresence.Away, MsnPresence.Brb, MsnPresence.Busy,
					MsnPresence.Phone, MsnPresence.Lunch, MsnPresence.Invisible);
			
			base.Initialize ();
			
			_accountCombo.TooltipText =
				GettextCatalog.GetString ("Provide the address registered with Microsoft's Passport service. Example: account@hotmail.com or account@msn.com");
		}
		
		protected override IAccount SetAccount ()
		{
			MsnAccount account = GetAccount(_accountCombo.Entry.Text) as MsnAccount;
			
			if (account != null)
			{
				account.Password = _passwordEntry.Text;
				account.AutoConnect = _autoconnectCheck.Active;
				account.RememberPassword = _rememberPassCheck.Active;
				account.InitialPresence = GetInitialPresence ();
				account.UseHTTP = _http;
				
				return account;
			}
			else
			{
				MsnAccount newAccount = new MsnAccount (_accountCombo.Entry.Text, _passwordEntry.Text, string.Empty, _autoconnectCheck.Active, _rememberPassCheck.Active);
				newAccount.InitialPresence = GetInitialPresence ();
				newAccount.UseHTTP = _http;
				
				AccountUtility.AddAccount (newAccount);
				
				return newAccount;
			}
		}
		
		protected override void LoadAccountInfo ()
		{
			base.LoadAccountInfo ();
			
			// Here we would load all the MSN specific settings into the UI.
			
			MsnAccount account = Account as MsnAccount;
			
			if (account != null)
			{
				_http = account.UseHTTP;
			}
		}
		
		public override void EnableFields ()
		{
			base.EnableFields ();
			
			// Also enable custom protocol widgets.
		}
		
		public override void DisableFields (bool omit_cancel)
		{
			base.DisableFields (omit_cancel);
			
			// Also disable custom protocol widgets.
		}
		
		protected override Gdk.Pixbuf PresenceImageLookup (IPresence item, IIconSize size)
		{
			return IconUtility.StatusLookup (item, size);
		}
		
		protected override void ConnectButtonClicked (object sender, EventArgs args)
		{
			_current_session = CreateSession () as MsnSession;
			
			if (_current_session == null)
				return;
			
			(_current_session as MsnSession).Connection.ErrorOccurred += ConnectionErrorOccurred;
			(_current_session as MsnSession).Usurped += SessionUsurped;
			
			base.ConnectButtonClicked (sender, args);
		}
		
		protected override void SettingsButtonClicked (object sender, EventArgs args)
		{
			// Settings button is custom to this protocol
			
			ResponseType response = ResponseType.None;
			AccountSettingsDialog dialog = new AccountSettingsDialog ();
			
			dialog.HTTP = _http;
			
			while(response == ResponseType.None)
			{
				response = (ResponseType)dialog.Run();
				
				if (response == ResponseType.Ok)
				{
					_http = dialog.HTTP;
					
					dialog.Destroy();
					break;
				}
				else
				{
					dialog.Destroy();
					break;
				}
			}
		}
		
		private void SessionUsurped (object sender, SessionEventArgs args)
		{
			// This one is MSN specific, it doesn't happen to other protocols.
			
			EnableFields ();
			
			_progressBar.Fraction = 0.0;
			_progressBar.Text = GettextCatalog.GetString ("Session Usurped!");
			
			OnCloseSessionWidget (args);
			
			SoundSetUtility.Play(Sound.Disconnected);
		}
		
		protected virtual void ConnectionErrorOccurred (object sender, ConnectionErrorEventArgs args)
		{
			ErrorOccurred (args.Description);
		}
	}
}