/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gtk;
using Glade;

using Anculus.Core;

using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public partial class MsnPreferenceWidget
	{
		[Widget ("radioDisplay")]  RadioButton _radioDisplay = null;
		[Widget ("radioHide")]     RadioButton _radioHide = null;
		[Widget ("radioIgnore")]   RadioButton _radioIgnore = null;
		
		void InitInterface ()
		{
			int _plusFormatMode = _config.GetInt ("PlusFormatMode", 0);
			
			if (_plusFormatMode == 0)
				_radioDisplay.Active = true;
			else if (_plusFormatMode == 1)
				_radioHide.Active = true;
			else
				_radioIgnore.Active = true;
		}
		
#region Glade Event Handlers
#pragma warning disable 169
		void OnPlusFormatToggled (object sender, EventArgs args)
		{
			int plusFormatMode = _radioDisplay.Active ? 0 : (_radioHide.Active ? 1 : 2);
			_config.SetInt ("PlusFormatMode", plusFormatMode);
			
			// Force the markup to be regenerated
			//PangoUtility.ClearMarkupCache ();
		}
#pragma warning restore 169
#endregion
	}
}
