/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;

using Anculus.Core;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public static class WinkStorageUtility
	{
		static IConfigurationSection _config;
		static string _cacheDir;
		
		static WinkStorageUtility ()
		{
			_config = Configuration.Protocol.Section["MSN"];
			
			_cacheDir = Path.Combine(CoreUtility.GetConfigurationSubDirectory("Cache"), "Winks");
			BaseUtility.CreateDirectoryIfNeeded(_cacheDir);
		}
		
		public static MsnWink AddWink (MsnAccount account, string filename, bool forAll)
		{
			MsnWink wink = MsnWink.FromPackage (account != null ? account.Session : null, filename);
			
			if (wink == null)
				return null;
			
			string shaname = BaseUtility.GetHashedName (wink.Sha);
			
			if (_config[forAll ? "*" : account.UniqueIdentifier]["Winks"].ContainsSection (shaname))
			{
				Log.Warn ("Already have wink");
				return null;
			}
			
			string cacheFilename = Path.Combine (_cacheDir, shaname);
			File.Copy (filename, cacheFilename, true);
			
			IConfigurationSection section = _config[forAll ? "*" : account.UniqueIdentifier]["Winks"][shaname];
			section.SetString ("Filename", cacheFilename);
			
			return wink;
		}
		
		public static void RemoveWink (MsnWink wink)
		{
			List<IConfigurationSection> toRemove = new List<IConfigurationSection> ();
			
			if (wink.Session != null)
			{
				foreach (IConfigurationSection section in _config[wink.Session.Account.UniqueIdentifier]["Winks"].Sections)
				{
					if (section.Name != BaseUtility.GetHashedName (wink.Sha))
						continue;
					
					// This section references the wink
					
					if (File.Exists (section.GetString ("Filename")))
						File.Delete (section.GetString ("Filename"));
					
					toRemove.Add (section);
					break;
				}
			}
			
			foreach (IConfigurationSection section in _config["*"]["Winks"].Sections)
			{
				if (section.Name != BaseUtility.GetHashedName (wink.Sha))
					continue;
				
				// This section references the wink

				if (File.Exists (section.GetString ("Filename")))
					File.Delete (section.GetString ("Filename"));

				toRemove.Add (section);
				break;
			}
			
			foreach (IConfigurationSection section in toRemove)
				section.Parent.RemoveSection (section.Name);
		}
		
		public static List<MsnWink> GetWinks (MsnAccount account)
		{
			List<MsnWink> winks = new List<MsnWink> ();
			
			foreach (IConfigurationSection section in _config[account.UniqueIdentifier]["Winks"].Sections)
			{
				MsnWink wink = MsnWink.FromPackage (account.Session, section.GetString ("Filename"));
				
				if (wink == null)
				{
					Log.Warn ("Unable to load wink {0}", section.GetString ("Filename"));
					continue;
				}
				
				winks.Add (wink);
			}
			
			foreach (IConfigurationSection section in _config["*"]["Winks"].Sections)
			{
				MsnWink wink = MsnWink.FromPackage (account.Session, section.GetString ("Filename"));
				
				if (wink == null)
				{
					Log.Warn ("Unable to load wink {0}", section.GetString ("Filename"));
					continue;
				}
				
				winks.Add (wink);
			}
			
			return winks;
		}
	}
}
