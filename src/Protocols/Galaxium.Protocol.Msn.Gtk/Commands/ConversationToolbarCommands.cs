/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Timers;

using G=Gtk;
using Cairo;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class InviteContactToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as G.ToolButton).Sensitive = (chatWidget != null) && (chatWidget.Conversation as MsnConversation).CanInvite;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				new InviteContactDialog (chatWidget.Conversation, delegate (IContact c)
				{
					MsnContact contact = c as MsnContact;
					
					return (contact.Network == Network.WindowsLive) &&
							(contact.Presence != MsnPresence.Offline) &&
							(!chatWidget.Conversation.ContactCollection.Contains (contact));
				},
				delegate (IContact c)
				{
					MsnContact contact = c as MsnContact;

					if (contact.Presence == MsnPresence.Idle)
						return IconUtility.GetIcon ("galaxium-idle", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Offline)
						return IconUtility.GetIcon ("galaxium-offline", IconSizes.Small);
					else if (!contact.IsInList(MsnListType.Reverse))
						return IconUtility.GetIcon ("galaxium-status-reverse", IconSizes.Small);
					else if (contact.IsBlocked)
						return IconUtility.GetIcon ("galaxium-status-block", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Away)
						return IconUtility.GetIcon ("galaxium-status-away", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Brb)
						return IconUtility.GetIcon ("galaxium-status-brb", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Busy)
						return IconUtility.GetIcon ("galaxium-status-busy", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Lunch)
						return IconUtility.GetIcon ("galaxium-status-lunch", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Online)
						return IconUtility.GetIcon ("galaxium-contact", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Phone)
						return IconUtility.GetIcon ("galaxium-status-phone", IconSizes.Small);
					
					return null;
				});
			}
		}
	}
	
	public class SendNudgeToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as G.ToolButton).Sensitive = (chatWidget != null) && (chatWidget.Conversation as MsnConversation).CanSendNudge;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
				chatWidget.SendNudge ();
		}
	}
	
	public class SendFileToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as G.ToolButton).Sensitive = (chatWidget != null) &&
													chatWidget.Conversation.IsPrivateConversation &&
													((chatWidget.Conversation.PrimaryContact as MsnContact).Network == Network.WindowsLive) &&
													(chatWidget.Conversation.PrimaryContact.Presence != MsnPresence.Offline);
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
				chatWidget.SendFile ();
		}
	}
	
	public class StartActivityCommand : AbstractMenuCommand, IPopupMenuBuilder
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as G.ToolButton).Sensitive = (chatWidget != null) && (chatWidget.Conversation as MsnConversation).CanStartActivity;
		}
		
		string GetIconCacheName (MsnActivityEntry entry)
		{
			string dir = System.IO.Path.Combine (System.IO.Path.Combine (CoreUtility.GetConfigurationSubDirectory ("Cache"), "MSN"), "ActivityIcons");
			
			BaseUtility.CreateDirectoryIfNeeded (dir);
			
			return System.IO.Path.Combine (dir, entry.ID.ToString ());
		}
		
		void RequestImage (Gtk.Image img, MsnActivityEntry entry)
		{
			string cacheFile = GetIconCacheName (entry);
			
			if (File.Exists (cacheFile))
			{
				img.File = cacheFile;
				return;
			}
			
			string url = string.IsNullOrEmpty (entry.AppIconUrl) ? entry.IconUrl : entry.AppIconUrl;
			
			if (string.IsNullOrEmpty (url))
				return;
			
			WebClient client = new WebClient ();
			client.DownloadDataCompleted += ImageDataDownloaded;
			client.DownloadDataAsync (new Uri (url), new KeyValuePair<Gtk.Image, MsnActivityEntry> (img, entry));
		}
		
		void ImageDataDownloaded (object sender, DownloadDataCompletedEventArgs args)
		{
			if (args.Cancelled || (args.Error != null))
				return;
			
			KeyValuePair<Gtk.Image, MsnActivityEntry> pair = (KeyValuePair<Gtk.Image, MsnActivityEntry>)args.UserState;
			
			string cacheFile = GetIconCacheName (pair.Value);
			File.WriteAllBytes (cacheFile, args.Result);
			
			pair.Key.File = cacheFile;
		}
		
		public void Build (Gtk.Menu menu)
		{
			foreach (Gtk.Widget child in menu.Children)
				menu.Remove (child);
			
			foreach (long categoryID in MsnActivityUtility.Activities.Keys)
			{
				foreach (MsnActivityEntry entry in MsnActivityUtility.Activities[categoryID])
				{
					Gtk.ImageMenuItem item = new G.ImageMenuItem (entry.Name);
					item.Image = new Gtk.Image ();
					
					RequestImage (item.Image as Gtk.Image, entry);
					
					menu.Add (item);
				}
			}
			
			menu.ShowAll ();
		}
		
		public override void Run ()
		{
		}
	}
	
	public class ViewWebcamCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			//MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			//(MenuItem as G.MenuItem).Sensitive = (chatWidget != null) && (chatWidget.Conversation as MsnConversation).CanViewWebcam;
			(MenuItem as G.MenuItem).Sensitive = false;
		}
		
		public override void Run ()
		{
			//MsnChatWidget chatWidget = Object as MsnChatWidget;
			
		}
	}
	
	public class ShowWebcamCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as G.MenuItem).Sensitive = (chatWidget != null) && (chatWidget.Conversation as MsnConversation).CanShowWebcam;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			chatWidget.ShowMyWebcam ();
		}
	}
}
