//
//  Copyright © 2007 Ben Motmans <ben.motmans@gmail.com>
//  Copyright © 2007 Philippe Durand <draekz@gmail.com>
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol.Gui;

using Anculus.Core;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	/*public class InviteContactCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			JabberChatWidget chatWidget = Object as JabberChatWidget;
			
			//(MenuItem as ImageMenuItem).Sensitive = (chatWidget != null)
			// && (chatWidget.Conversation as JabberConversation).CanInvite;
		}
		
		public override void Run ()
		{
			JabberChatWidget chatWidget = Object as JabberChatWidget;
			
			if (chatWidget != null)
			{
				new InviteContactDialog (chatWidget.Conversation, delegate (IContact c)
				{
					JabberContact contact = c as JabberContact;
					
					return (contact.Presence != JabberPresence.Offline) &&
							(!chatWidget.Conversation.ContactCollection.Contains (contact));
				},
				delegate (IContact c)
				{
					JabberContact contact = c as JabberContact;

					if (contact.Presence == JabberPresence.Idle)
						return IconUtility.GetIcon ("galaxium-idle", IconSizes.Small);
					else if (contact.Presence == JabberPresence.Offline)
						return IconUtility.GetIcon ("galaxium-offline", IconSizes.Small);
					//else if (!contact.IsInList(JabberListType.Reverse))
						//return IconUtility.GetIcon ("galaxium-status-reverse", IconSizes.Small);
					//else if (contact.IsBlocked)
						//return IconUtility.GetIcon ("galaxium-status-block", IconSizes.Small);
					else if (contact.Presence == JabberPresence.Away)
						return IconUtility.GetIcon ("galaxium-status-away", IconSizes.Small);
					//else if (contact.Presence == JabberPresence.Brb)
						//return IconUtility.GetIcon ("galaxium-status-brb", IconSizes.Small);
					else if (contact.Presence == JabberPresence.Busy)
						return IconUtility.GetIcon ("galaxium-status-busy", IconSizes.Small);
					//else if (contact.Presence == JabberPresence.Lunch)
						//return IconUtility.GetIcon ("galaxium-status-lunch", IconSizes.Small);
					else if (contact.Presence == JabberPresence.Online)
						return IconUtility.GetIcon ("galaxium-contact", IconSizes.Small);
					//else if (contact.Presence == JabberPresence.Phone)
						//return IconUtility.GetIcon ("galaxium-status-phone", IconSizes.Small);
					
					return null;
				});
			}
		}
	}*/
	
	public class ClearConversationCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as ImageMenuItem).Sensitive = false;
		}
		
		public override void Run ()
		{
			// TODO
		}
	}
	
	public class ViewLogsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			XmppChatWidget chatWidget = Object as XmppChatWidget;
			
			if (chatWidget != null && chatWidget.Conversation != null)
			{
				ViewHistoryDialog dialog = new ViewHistoryDialog (chatWidget.Conversation, chatWidget);
				ResponseType response = (ResponseType)dialog.Run();
				
				if (response == ResponseType.Apply)
				{
					
				}
				
				dialog.Destroy ();
			}
		}
	}
	
	public class FindTextCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as ImageMenuItem).Sensitive = false;
		}
		
		public override void Run ()
		{
			// TODO
		}
	}
	
	public class SendFileCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as ImageMenuItem).Sensitive = false;
			
		//	JabberChatWidget chatWidget = Object as JabberChatWidget;
			
		//	(MenuItem as ImageMenuItem).Sensitive = (chatWidget != null) &&(chatWidget != null) &&
		//											chatWidget.Conversation.IsPrivateConversation &&
		//											(chatWidget.Conversation.PrimaryContact.Presence != JabberPresence.Offline);
		}
		
		public override void Run ()
		{
			XmppChatWidget chatWidget = Object as XmppChatWidget;
			
			if (chatWidget != null)
			{
				XmppContact contact = chatWidget.Conversation.PrimaryContact as XmppContact;
				XmppSession session = contact.Session as XmppSession;
				
				if (contact != null && contact.DisplayName != session.Account.DisplayName)
				{
					SendFileDialog dialog = new SendFileDialog (contact);
					
					// Here we would fill the dialog with the options we intend to use to send the file.
					
					ResponseType response = (ResponseType) dialog.Run ();
					
					if (response == ResponseType.Apply)
					{
						if (dialog.Filename != null && dialog.Filename.Length > 0)
						{
							contact.Session.SendFile (contact, dialog.Filename);
						}
					}
					
					dialog.Destroy();
				}
			}
		}
	}
	
	public class CloseWindowCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var chat_widget = Object as XmppChatWidget;
			
			if (chat_widget != null)
				chat_widget.ContainerWindow.Close(false);
		}
	}
	
	public class CloseConversationCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var chat_widget = Object as XmppChatWidget;
			
			if (chat_widget != null)
				chat_widget.Close();
		}
	}

	//////////////////////////////////////////////////////////////////////////////////

	
	public class ShowActionToolbarCommand: ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = UsingDefaultView
				? _config.GetBool (Configuration.Conversation.ShowActivityToolbar.Name,
				                   Configuration.Conversation.ShowActivityToolbar.Default)
					: _chat_widget.ShowActionToolbar;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Conversation.ShowActivityToolbar.Name,
				                 (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowActionToolbar = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class ShowInputToolbarCommand: ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = UsingDefaultView
				? _config.GetBool (Configuration.Contact.ShowInputToolbar.Name,
				                   Configuration.Contact.ShowInputToolbar.Default)
					: _chat_widget.ShowInputToolbar;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Contact.ShowInputToolbar.Name,
				                 (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowInputToolbar = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class ShowAccountImageCommand: ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			if (!_chat_widget.Conversation.IsPrivateConversation)
			{
				(MenuItem as CheckMenuItem).Active = false;
				(MenuItem as CheckMenuItem).Sensitive = false;
			}
			else
				(MenuItem as CheckMenuItem).Active = UsingDefaultView
					? _config.GetBool (Configuration.Contact.ShowAccountImage.Name,
					                   Configuration.Contact.ShowAccountImage.Default)
					: _chat_widget.ShowAccountImage;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Contact.ShowAccountImage.Name,
				                 (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowAccountImage = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class ShowContactImageCommand: ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			if (!_chat_widget.Conversation.IsPrivateConversation)
			{
				(MenuItem as CheckMenuItem).Active = false;
				(MenuItem as CheckMenuItem).Sensitive = false;
			}
			else
				(MenuItem as CheckMenuItem).Active = UsingDefaultView
					? _config.GetBool (Configuration.Contact.ShowContactImage.Name,
					                   Configuration.Contact.ShowContactImage.Default)
					: _chat_widget.ShowContactImage;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Contact.ShowContactImage.Name,
				                 (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowContactImage = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class ShowPersonalMessageCommand: ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			if (!_chat_widget.Conversation.IsPrivateConversation)
			{
				(MenuItem as CheckMenuItem).Active = false;
				(MenuItem as CheckMenuItem).Sensitive = false;
			}
			else
				(MenuItem as CheckMenuItem).Active = UsingDefaultView
					? _config.GetBool (Configuration.Contact.ShowIdentification.Name,
					                   Configuration.Contact.ShowIdentification.Default)
					: _chat_widget.ShowPersonalMessage;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Contact.ShowIdentification.Name, (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowPersonalMessage = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class UseDefaultViewCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var chat_widget = Object as XmppChatWidget;
			
			if (chat_widget != null)
				(MenuItem as CheckMenuItem).Active = chat_widget.UseDefaultView;
		}
		
		public override void Run ()
		{
			var chat_widget = Object as XmppChatWidget;
			
			if (chat_widget != null) {
				chat_widget.UseDefaultView = (MenuItem as CheckMenuItem).Active;
				chat_widget.Conversation.PrimaryContact.Save ();
				chat_widget.SwitchTo ();
				chat_widget.Update ();
			}
		}
	}
	
	public class EnableLoggingCommand: ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = UsingDefaultSettings
				? _config.GetBool (Configuration.Contact.EnableLogging.Name,
				                   Configuration.Contact.EnableLogging.Default)
					: _chat_widget.Conversation.EnableLogging;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultSettings)
			{
				_config.SetBool (Configuration.Contact.EnableLogging.Name,
				                 (MenuItem as CheckMenuItem).Active);
				_chat_widget.Conversation.EnableLogging = (MenuItem as CheckMenuItem).Active;
			}
			else
			{
				_chat_widget.Conversation.EnableLogging = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class EnableSoundsCommand: ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = UsingDefaultSettings
				? _config.GetBool (Configuration.Contact.EnableSounds.Name,
				                   Configuration.Contact.EnableSounds.Default)
					: _chat_widget.EnableSounds;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultSettings)
				_config.SetBool (Configuration.Contact.EnableSounds.Name,
				                 (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.EnableSounds = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class UseDefaultSettingsCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var chat_widget = Object as XmppChatWidget;
			
			if (chat_widget != null)
				(MenuItem as CheckMenuItem).Active = chat_widget.Conversation.UseDefaultSettings;
		}
		
		public override void Run ()
		{
			var chat_widget = Object as XmppChatWidget;
			
			if (chat_widget != null)
			{
				chat_widget.Conversation.UseDefaultSettings = (MenuItem as CheckMenuItem).Active;
				chat_widget.Conversation.PrimaryContact.Save ();
				chat_widget.SwitchTo ();
				chat_widget.Update ();
			//	chat_widget.ReportLogging (); FIXME
			}
		}
	}
}