//
//  Copyright © 2007 Ben Motmans <ben.motmans@gmail.com>
//  Copyright © 2007 Philippe Durand <draekz@gmail.com>
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;

using G=Gtk;
using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Galaxium.Protocol.Xmpp.Library;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class InviteContactToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		//	var chat_widget = Object as XmppChatWidget;

			(MenuItem as G.ToolButton).Sensitive = false;
		//	(MenuItem as G.ToolButton).Sensitive = (chat_widget != null) && (chat_widget.Conversation as XmppConversation).CanInvite;
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;

			CanInviteDelegate can_invite = delegate (IContact c) {
				var contact = c as XmppContact;
				return contact.Presence != XmppPresence.Offline
					&& contact.Presence != XmppPresence.Error
					&& contact.Presence != XmppPresence.Unknown
					&& !chat_widget.Conversation.ContactCollection.Contains (contact);
			};

			GetPixbufDelegate get_pixbuf = delegate (IContact c) {
				var contact = c as XmppContact;
				return XmppProtocolFactory.StatusLookup (contact.Presence, IconSizes.Small);
			};

			new InviteContactDialog (chat_widget.Conversation, can_invite, get_pixbuf);
		}
	}
	
	public class SendFileToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var chat_widget = Object as XmppChatWidget;
			
			if (chat_widget == null) {
				(MenuItem as G.ToolButton).Sensitive = false;
				return;
			}
			
			var conversation = chat_widget.Conversation;
			var contact = conversation.PrimaryContact as XmppContact;
			
			if (contact != null && conversation != null)
			{
				if (contact.InternalContact != null)
					(MenuItem as G.ToolButton).Sensitive =
						contact.InternalContact.IsOnline && (conversation.ActiveResource == null ||
						contact.InternalContact.GetResource (conversation.ActiveResource).Supports (Namespaces.SIFileTransfer));
				else
					(MenuItem as G.ToolButton).Sensitive = false;
			}
		}
		
		public override void Run ()
		{
			var chat_widget = Object as XmppChatWidget;
			var contact = chat_widget.Conversation.PrimaryContact as XmppContact;
			var intern = contact.InternalContact;
			
			var uid = new JabberID (intern.Jid.Node, intern.Jid.Domain,
			                        chat_widget.Conversation.ActiveResource);

			var dialog = new FileChooserDialog(GettextCatalog.GetString ("Select File"),
			                                   null, FileChooserAction.Open, Stock.Cancel,
			                                   ResponseType.Cancel, Stock.Apply, ResponseType.Accept);
			
			dialog.SetCurrentFolder (Environment.GetFolderPath (Environment.SpecialFolder.Personal));
				
			if (dialog.Run () == (int) ResponseType.Accept &&
			    !String.IsNullOrEmpty (dialog.Filename)) {
				
				if (uid.Resource == null)
					contact.Session.SendFile (contact, dialog.Filename);
				else
					contact.Session.SendFile (uid, dialog.Filename);
			}
			
			dialog.Destroy();
		}
	}
}
