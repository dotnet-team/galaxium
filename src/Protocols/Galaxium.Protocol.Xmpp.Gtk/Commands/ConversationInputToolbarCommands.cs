//
//  Copyright © 2008 Philippe Durand <draekz@gmail.com>
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;

using Gtk;
using Pango;

using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class BoldToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var chat_widget = Object as XmppChatWidget;

			(MenuItem as ToggleToolButton).Sensitive = false;
			(MenuItem as ToggleToolButton).Active =
				(chat_widget != null) && chat_widget.MessageEntry.Bold;
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;
			chat_widget.MessageEntry.Bold = (MenuItem as ToggleToolButton).Active;
			chat_widget.SaveFont ();
		}
	}
	
	public class ItalicToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var chat_widget = Object as XmppChatWidget;

			(MenuItem as ToggleToolButton).Sensitive = false;
			(MenuItem as ToggleToolButton).Active =
				(chat_widget != null) && chat_widget.MessageEntry.Italic;
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;

			var chat_widget = Object as XmppChatWidget;
			chat_widget.MessageEntry.Italic = (MenuItem as ToggleToolButton).Active;
			chat_widget.SaveFont ();
		}
	}
	
	public class UnderlineToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var chat_widget = Object as XmppChatWidget;

			(MenuItem as ToggleToolButton).Sensitive = false;
			(MenuItem as ToggleToolButton).Active =
				(chat_widget != null) && chat_widget.MessageEntry.Underline;
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;
			chat_widget.MessageEntry.Underline = (MenuItem as ToggleToolButton).Active;
			chat_widget.SaveFont ();
		}
	}
	
	public class StrikethroughToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var chat_widget = Object as XmppChatWidget;

			(MenuItem as ToggleToolButton).Sensitive = false;
			(MenuItem as ToggleToolButton).Active =
				(chat_widget != null) && chat_widget.MessageEntry.Strikethrough;
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;
			chat_widget.MessageEntry.Strikethrough = (MenuItem as ToggleToolButton).Active;
			chat_widget.SaveFont ();
		}
	}
	
	public class ForegroundToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as ToggleToolButton).Sensitive = false;
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;

			var dialog = new ColorSelectionDialog (String.Empty);
			dialog.Modal = true;
			dialog.ColorSelection.CurrentColor = chat_widget.MessageEntry.Color;
				
			if (dialog.Run () == (int) ResponseType.Ok) {
				chat_widget.MessageEntry.Color = dialog.ColorSelection.CurrentColor;
				chat_widget.SaveFont ();
			}
				
			dialog.Destroy ();
		}
	}
	
	public class FontToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;

			var dialog = new FontSelectionDialog (String.Empty);
			dialog.Modal = true;
			
			var desc = new FontDescription ();
			desc.Family = chat_widget.MessageEntry.Family;
			if (chat_widget.MessageEntry.Bold)
				desc.Weight = Pango.Weight.Bold;
			if (chat_widget.MessageEntry.Italic)
				desc.Style = Pango.Style.Italic;
			dialog.SetFontName (desc.ToString ());
				
			if (dialog.Run () == (int) ResponseType.Ok) {
				desc = FontDescription.FromString (dialog.FontName);
					
				chat_widget.MessageEntry.Family = desc.Family;
				chat_widget.MessageEntry.Bold = desc.Weight == Pango.Weight.Bold;
				chat_widget.MessageEntry.Italic = desc.Style == Pango.Style.Italic;
					
				chat_widget.SaveFont ();
			}
				
			dialog.Destroy ();
		}
	}
	
	public class ResetToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;
			chat_widget.MessageEntry.ResetFont ();
			chat_widget.SaveFont ();
		}
	}
	
	public class ClearToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;
			chat_widget.MessageEntry.Buffer.Clear ();
		}
	}
	
	public class SpellCheckToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as ToolButton).Sensitive = false;
		}
		
		public override void Run ()
		{
		//	if (!(Object is XmppChatWidget)) return;
			
		//	var chat_widget = Object as XmppChatWidget;
			// TODO
		}
	}
	
	public class EmoticonToolCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;

			var dialog = new EmoticonPopupDialog (chat_widget.Conversation.Session.Account,
			                                      chat_widget.Conversation.PrimaryContact,
			                                      EmoticonPopupMode.Common);
			dialog.Selected += EmoticonSelected;
			dialog.TransientFor = chat_widget.NativeWidget.Toplevel as Gtk.Window;
			dialog.Show ();
		}
		
		void EmoticonSelected (object sender, IEmoticon emoticon)
		{
			if (!(Object is XmppChatWidget)) return;
			
			var chat_widget = Object as XmppChatWidget;
			var cursor_pos = chat_widget.MessageEntry.Buffer.CursorPosition;
			chat_widget.MessageEntry.InsertEmoticon (emoticon, cursor_pos);
			(sender as EmoticonPopupDialog).Destroy ();
		}
	}
}
