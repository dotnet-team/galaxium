// 
//   Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//  
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as
//   published by the Free Software Foundation, either version 3 of the
//   License, or (at your option) any later version.
//  
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//  
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  
//  

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Client;

using Anculus.Core;

using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class ContactTreeOpenChannelCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			context.TreeView.ActivateRow (context.Path, null);
		}
	}
	
	public class ContactTreeRenameChannelCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
		//	var context = Object as ContactTreeContext;
		//	context.TreeView.ActivateRow (context.Path, null);
		}
	}
	
	public class ContactTreeChangeChannelPasswordCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
		}
	}
		
	public class ContactTreeJoinChannelCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppConferenceContact;
			contact.Join ();
		}
	}
	
		
	public class ContactTreePartChannelCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppConferenceContact;
			contact.Leave ();
		}
	}
	
		
	public class ContactTreeRemoveChannelCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as MenuItem).Sensitive = false;
		}
		
		public override void Run ()
		{
		}
	}
		
	public class ContactTreeChannelPersistentCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
		}
	}
	
	public class ContactTreeChannelAutoJoinCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
		}
	}
	
	public class ContactTreeChannelDetailsCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
		}
	}
}