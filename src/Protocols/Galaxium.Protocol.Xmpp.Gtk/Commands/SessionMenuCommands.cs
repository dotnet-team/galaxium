//
//  Copyright © 2007 Ben Motmans <ben.motmans@gmail.com>
//  Copyright © 2008 Philippe Durand <draekz@gmail.com>
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.IO;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol.Xmpp;

using Galaxium.Protocol.Xmpp.Library;

using Anculus.Core;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class OpenNewChatCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (!(SessionUtility.ActiveSession is XmppSession)) return;
			var session = SessionUtility.ActiveSession as XmppSession;
			
			var jid = EnterJidDialog.Open ();
			if (jid == JabberID.Empty) return;
			var contact = session.GetContact (jid);
			var conversation = session.Conversations.GetConversation (contact);
			WindowUtility<Widget>.Activate(conversation, true);
		}
	}

	public class ComposeNewMessageCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (!(SessionUtility.ActiveSession is XmppSession)) return;
			var client = (SessionUtility.ActiveSession as XmppSession).Client;
			Messages.MessageWindow.ComposeNew (client, JabberID.Empty);
		}
	}
	
	public class ShowMessagesCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (!(SessionUtility.ActiveSession is XmppSession)) return;
			var client = (SessionUtility.ActiveSession as XmppSession).Client;
			new Messages.MessageListWindow (client);
		}
	}

	public class AddContactCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (!(SessionUtility.ActiveSession is XmppSession)) return;
			var client = (SessionUtility.ActiveSession as XmppSession).Client;
			AddContactDialog.Open (null, client);
		}
	}
	
	public class SetVCardCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as ImageMenuItem).Sensitive = false;
		}
		
		public override void Run ()
		{
			// TODO
		}
	}

	public class PrivacyListsCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as ImageMenuItem).Sensitive = false;
		}
		
		public override void Run ()
		{
			// TODO
		}
	}
	
	public class SetAvatarCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			// TODO: error handling
			
			if (SessionUtility.ActiveSession is XmppSession)
			{
				var dialog = new SetDisplayImageDialog((XmppSession)SessionUtility.ActiveSession);
				
				if (dialog.Run () == (int) ResponseType.Ok)
				{
					var image = new XmppDisplayImage (dialog.Filename);
					var session = SessionUtility.ActiveSession as XmppSession;
					session.Account.DisplayImage = image;
					session.Client.SetAvatar (null, image.ImageBuffer);
					session.Account.Cache.SaveAccountImage();
				}
				
				dialog.Destroy ();
			}
		}
	}
	
	public class ManageServicesCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (!(SessionUtility.ActiveSession is XmppSession)) return;
			
			new BrowseServicesDialog ((SessionUtility.ActiveSession as XmppSession).Client);
		}
	}
	
	public class DisplayServiceLogCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (!(SessionUtility.ActiveSession is XmppSession)) return;
			var session = SessionUtility.ActiveSession as XmppSession;
			
			new ProtocolLogWindow (session.CommunicationLog, JabberID.Empty);
		}
	}
	
	public class BrowseChatroomsCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (!(SessionUtility.ActiveSession is XmppSession)) return;
			var session = SessionUtility.ActiveSession as XmppSession;
			
			new MucListWindow (session.Client, null);
		}
	}
}