//
//  Copyright © 2007 Ben Motmans <ben.motmans@gmail.com>
//  Copyright © 2008 Philippe Durand <draekz@gmail.com>
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class ViewByGroupCommand: SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = _using_defaults
				? Configuration.ContactList.Section.GetBool (Configuration.ContactList.ViewByGroup.Name,
				                                             Configuration.ContactList.ViewByGroup.Default)
					: SessionUtility.ActiveSession.Account.ViewByGroup;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ViewByGroup.Name,
				                                           (MenuItem as RadioMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				(SessionUtility.ActiveSession.Account as XmppAccount).ViewByGroup = (MenuItem as RadioMenuItem).Active;
		}
	}
	
	public class ViewByStatusCommand: SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = _using_defaults ? !Configuration.ContactList.Section.GetBool (Configuration.ContactList.ViewByGroup.Name, Configuration.ContactList.ViewByGroup.Default) : !SessionUtility.ActiveSession.Account.ViewByGroup;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ViewByGroup.Name, !(MenuItem as RadioMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				(SessionUtility.ActiveSession.Account as XmppAccount).ViewByGroup = !(MenuItem as RadioMenuItem).Active;
		}
	}
	
	public class ShowSearchCheckCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowSearchBar.Name, Configuration.ContactList.ShowSearchBar.Default) : SessionUtility.ActiveSession.Account.ShowSearchBar;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowSearchBar.Name, (MenuItem as CheckMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				SessionUtility.ActiveSession.Account.ShowSearchBar = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class ShowDisplayImagesCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowContactImages.Name, Configuration.ContactList.ShowContactImages.Default) : SessionUtility.ActiveSession.Account.ShowDisplayImages;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowContactImages.Name, (MenuItem as CheckMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				SessionUtility.ActiveSession.Account.ShowDisplayImages = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class ShowDisplayNamesCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowContactNames.Name, Configuration.ContactList.ShowContactNames.Default) : SessionUtility.ActiveSession.Account.ShowDisplayNames;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowContactNames.Name, (MenuItem as CheckMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				SessionUtility.ActiveSession.Account.ShowDisplayNames = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class ShowPersonalMessagesCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowContactMessages.Name, Configuration.ContactList.ShowContactMessages.Default) : SessionUtility.ActiveSession.Account.ShowPersonalMessages;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowContactMessages.Name, (MenuItem as CheckMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				SessionUtility.ActiveSession.Account.ShowPersonalMessages = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class ShowContactNicknamesCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowContactNicknames.Name, Configuration.ContactList.ShowContactNicknames.Default) : SessionUtility.ActiveSession.Account.ShowNicknames;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowContactNicknames.Name, (MenuItem as CheckMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				SessionUtility.ActiveSession.Account.ShowNicknames = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class ShowEmptyGroupsCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowEmptyGroups.Name, Configuration.ContactList.ShowEmptyGroups.Default) : SessionUtility.ActiveSession.Account.ShowEmptyGroups;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowEmptyGroups.Name, (MenuItem as CheckMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				SessionUtility.ActiveSession.Account.ShowEmptyGroups = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class ShowOfflineContactsCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowOfflineContacts.Name, Configuration.ContactList.ShowOfflineContacts.Default) : SessionUtility.ActiveSession.Account.ShowOfflineContacts;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowOfflineContacts.Name, (MenuItem as CheckMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				SessionUtility.ActiveSession.Account.ShowOfflineContacts = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class GroupOfflineContactsCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  _using_defaults ? Configuration.ContactList.Section.GetBool (Configuration.ContactList.GroupOfflineContacts.Name, Configuration.ContactList.GroupOfflineContacts.Default) : SessionUtility.ActiveSession.Account.GroupOfflineContacts;
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.GroupOfflineContacts.Name, (MenuItem as CheckMenuItem).Active);
				XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession).Update ();
			}
			else
				SessionUtility.ActiveSession.Account.GroupOfflineContacts = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class UseDefaultListViewCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			if (MenuItem != null && SessionUtility.ActiveSession != null)
				(MenuItem as CheckMenuItem).Active = SessionUtility.ActiveSession.Account.UseDefaultListView;
		}
		
		public override void Run ()
		{
			if (MenuItem != null && SessionUtility.ActiveSession != null)
			{
				SessionUtility.ActiveSession.Account.UseDefaultListView = (MenuItem as CheckMenuItem).Active;
				XmppSessionWidget widget = XmppWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as XmppSession);
				widget.Update ();
				widget.Window.UpdateMenu ();
			}
		}
	}
}