//  
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;

using Gtk;
using Glade;

using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp.GtkGui
{	
	public partial class InformationDialog
	{
		public static void Open (Contact contact)
		{
			var dialog = new InformationDialog (contact);
			dialog.Show ();
		}
		
		private Contact _contact;

		[Widget] private Dialog dialog;
		[Widget] private Notebook resources_notebook;
		[Widget] private Label jid_label;
		[Widget] private Label name_label;
		[Widget] private Label subscription_label_1;
		[Widget] private Label subscription_label_2;
		[Widget] private Label subscription_label_3;
	//	[Widget] private Image avatar_image;
		[Widget] private Alignment vcard_placeholder;

		public InformationDialog (Contact contact)
		{
			_contact = contact;
			var xml = new XML (null, "InformationDialog.glade", "dialog", null);
			xml.Autoconnect (this);

			jid_label.Text = contact.Jid;

			if (contact.Name == null) {
				name_label.Text = "-- no name set --";
				name_label.Sensitive = false;
			} else {
				name_label.Text = contact.Name;
			}

			string subs_string;
			if (!contact.IsInRoster)
				subs_string = "Contact is not in your list.";
			else switch (contact.Subscription) {
			case SubscriptionState.None:
				subs_string = "Neither of you is subscribed."; break;
			case SubscriptionState.From:
				subs_string = "The contact is subscribed to your status."; break;
			case SubscriptionState.To:
				subs_string = "You are subscribed to contacts status."; break;
			case SubscriptionState.Both:
				subs_string = "You are both subscribed."; break;
			default:
				subs_string = String.Empty; break;
			}
			
			subscription_label_1.Text = subs_string;
			if (!contact.IsAsking)
				subscription_label_2.Destroy ();
			if (!contact.IsWaiting)
				subscription_label_3.Destroy (); 
			
			LoadResources ();

			vcard_placeholder.Add (new vCardWidget (contact.Client, contact.Jid));
		}

		public void Show ()
		{
			dialog.ShowAll ();
		}

		private void LoadResources ()
		{
			if (_contact.ResourceCount == 0) return;

			if (_contact.ResourceCount > 1)
				resources_notebook.ShowTabs = true;
			resources_notebook.RemovePage (0);

			foreach (var resource in _contact.Resources) {
				var widget = ResourceBox.Load (resource);
				var label = new Label (resource.Identifier);
				resources_notebook.AppendPage (widget, label);
			}
		}
		
		protected void OnCloseClicked (object sender, EventArgs args)
		{
			dialog.Destroy ();
		}
	}
}
