// 
// SubscriptionRequestDisplayDialog.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// SubscriptionRequestDisplayDialog.cs is part of SharpXMPP.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using Glade;

using Lib=Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;
using Galaxium.Protocol.Xmpp.Library.Utility;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class SubscriptionRequestDisplayDialog: Window
	{
		[Widget] private Label    description_label = null;
		[Widget] private TextView message_textview  = null;

		private Lib.Client _client;
		private Presence _request;

		public static void Open (Window parent, Lib.Client client, Presence request)
		{
			var dialog = new SubscriptionRequestDisplayDialog (client, request);
			dialog.TransientFor = parent;
			dialog.Modal = true;
			dialog.Show ();
		}
		
		private SubscriptionRequestDisplayDialog (Lib.Client client, Presence request)
			:base ("Subscription request")
		{
			var gxml = new XML (null, "SubscriptionRequestDisplayDialog.glade", "main", null);
			gxml.Autoconnect (this);
			Add (gxml.GetWidget ("main"));

			_client = client;
			_request = request;

			var contact = _client.Roster.GetContact (_request.From.Bare ());
			var name = String.IsNullOrEmpty (contact.Name) ? (string) contact.Jid :
				contact.Name + " (" + contact.Jid + ")";

			description_label.Text = String.Format (description_label.Text, name);
			if (!String.IsNullOrEmpty (_request.Status)) {
				message_textview.Buffer.Text = _request.Status;
			} else {
				message_textview.Buffer.Text = "--- no message ---";
				message_textview.Sensitive = false;
			}
		}

		protected void OnApproveButtonClicked (object sender, EventArgs e)
		{
			_client.Roster.ApproveSubscription (_request.From.Bare ());
			Destroy ();
		}
		
		protected void OnDeclineButtonClicked (object sender, EventArgs e)
		{
			_client.Roster.DenySubscription (_request.From.Bare ());
			Destroy ();
		}

		protected void OnIgnoreButtonClicked (object sender, EventArgs e)
		{
			Destroy ();
		}
		
		protected void OnHelpButtonClicked (object sender, EventArgs e)
		{
			// TODO: help
		}
	}
}
