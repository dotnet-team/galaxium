// 
// QuestionDialog.cs
// 
// Copyright © 2008, 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// QuestionDialog.cs is part of SpiderIM.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using Gtk;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class QuestionDialog: Dialog
	{
		public static void Open (Window parent, string question, System.Action action)
		{
			var dialog = new QuestionDialog (parent, question, action);
			if (dialog.Run () == 1)
				action.Invoke ();
			dialog.Destroy ();
		}

		private QuestionDialog (Window parent, string question, System.Action action)
			:base ("", parent, DialogFlags.Modal | DialogFlags.NoSeparator,
			       "gtk-cancel", 0, "gtk-ok", 1)
		{
			Resizable = false;
			HBox box = new HBox ();
			box.PackStart (new Image ("gtk-dialog-question", IconSize.Dialog), false, false, 12);
			box.PackStart (new Label (question), true, true, 24);
			VBox.Add (box);
			VBox.ShowAll ();
		}
	}
}
