// 
// AddContactDialog.cs
// 
// Copyright © 2008, 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// AddContactDialog.cs is part of SpiderIM.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using Gtk;
using Glade;

using Galaxium.Core;

using Lib=Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;

// TODO: sort groups

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class AddContactDialog
	{
		[Widget] private Dialog dialog = null;
		
		[Widget] private Table table = null;
		[Widget] private Entry jid_entry = null;
		[Widget] private Label jid_error_label = null;
		[Widget] private Entry nick_entry = null;
		[Widget] private ComboBoxEntry group_combo = null;
		[Widget] private Entry group_entry = null;
		[Widget] private CheckButton request_check = null;
		[Widget] private CheckButton approve_check = null;
		[Widget] private TextView request_message = null;

		[Widget] private Button ok_button = null;
		[Widget] private Button cancel_button = null;

		private Lib.Client _client;
		
		public static void Open (Window parent, Lib.Client client)
		{
			var dialog = new AddContactDialog (parent, client, JabberID.Empty);
			new System.Threading.Thread (() => dialog.Run ()).Start ();
		}
		
		private AddContactDialog (Window parent, Lib.Client client, JabberID jid)
		{
			Glade.XML gxml = new Glade.XML (null, "AddContactDialog.glade", null, null);
			gxml.Autoconnect (this);

			_client = client;
			
			dialog.TransientFor = parent;
			
			group_combo.RemoveText (0);
			foreach (string grp in client.Roster.Groups)
				group_combo.AppendText (grp);

			jid_entry.Text = jid;
		}

		private void Run ()
		{
			JabberID jid;
			
			do {
				EnableAll ();

				int result = 0;
				ThreadUtility.SyncDispatch (() => result = dialog.Run ());
				if (result != 1) break;
				
				try   { jid = new JabberID (jid_entry.Text).Bare (); }
				catch {
					SetError ("Invalid Jabber ID.");
					continue;
				}
				
				if (_client.Roster.GetContact (jid).IsInRoster) {
					SetError ("Contact is already present in roster.");
					continue;
				}
				
				DisableAll ();
				
				SetMessage ("Adding contact...");

				try {
					_client.Roster.AddContact (jid, nick_entry.Text, group_entry.Text,
					                           approve_check.Active, request_check.Active,
					                           request_message.Buffer.Text);
				}
				catch (Exception e) {
					SetError (e.Message);
					continue;
				}
				
				break;				
			} while (true);

			ThreadUtility.Dispatch (() => dialog.Destroy ());
		}

		private void SetMessage (string message)
		{
			ThreadUtility.Dispatch (() => {
				jid_error_label.Markup = "<b><span color='green'>" + message + "</span></b>";
			});
		}
		
		private void SetError (string error)
		{
			ThreadUtility.Dispatch (() => {
				jid_error_label.Markup = "<b><span color='red'>Error: " + error + "</span></b>";
			});
		}

		private void DisableAll ()
		{
			ThreadUtility.Dispatch (() => {
				table.Sensitive = ok_button.Sensitive = cancel_button.Sensitive = false;
			});
		}

		private void EnableAll ()
		{
			ThreadUtility.Dispatch (() => {
				table.Sensitive = ok_button.Sensitive = cancel_button.Sensitive = true;
			});
		}
		
		protected void OnJidChange (object sender, EventArgs args)
		{
			jid_error_label.Text = " ";
		}

		protected void OnRequestToggled (object sender, EventArgs args)
		{
			request_message.Sensitive = request_check.Active;
		}
	}
}
