// 
// XmppSessionWidget.cs
//
// Copyright © 2007 Philippe Durand <draekz@gmail.com>
// Copyright © 2007 Ben Motmans <ben.motmans@gmail.com>
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Text;

using Gtk;
using Gdk;
using Glade;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Protocol.Xmpp;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Galaxium.Protocol.Xmpp.GtkGui.Messages;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class XmppSessionWidget: BasicSessionWidget
	{
		internal XmppSessionWidget (IControllerWindow<Widget> window, XmppSession session)
			:base (window, session)
		{
			XmppWidgetUtility.AddSessionWidget(session, this);
			
			// Map some specialized events for service registration;
		//	session.ServiceRegistrationInstructions += ReceivedInstructions;
		//	session.ServiceRegistrationError += ReceivedRegistrationError;
		}
		
		public override void Initialize ()
		{
			base.Initialize ();
			
			// We need to setup a Xmpp tree manager for our tree
			_tree_view.Manager = new XmppContactTreeManager ();
			
			// We have to insert the protocol specific statuses
			_status_combo.Append (XmppPresence.Online);
			_status_combo.Append (XmppPresence.Away);
			_status_combo.Append (XmppPresence.XA);
			_status_combo.Append (XmppPresence.Chat);
			_status_combo.Append (XmppPresence.Dnd);
		//	_status_combo.Append (XmppPresence.Invisible);

			_status_combo.Select (_session.Account.InitialPresence);

			var button = new MessageButton ((_session as XmppSession).Client);
			button.OnlyUnread = true;
			_profile_table.Attach (button, 0, 3, 4, 5);
			
			// Make sure everything is positioned properly.
			Update ();
		}
		
		public override IConversation CreateUsableConversation (IContact contact)
		{
		//	IConversation conversation = new XmppConversation (contact as XmppContact, Session);
		//	_session.Conversations.Add (conversation);
		//	(_session as XmppSession).RegisterConversation (conversation);
		//	return conversation;
			throw new NotImplementedException ();
		}
		
		protected override string PresenceTextLookup (IPresence item)
		{
			return item.State;
		}
		
		protected override Gdk.Pixbuf PresenceImageLookup (IPresence item, IIconSize size)
		{
			return XmppProtocolFactory.StatusLookup (item, size);
		}
		
		protected override void SetDisplayImage ()
		{
			var dialog = new SetDisplayImageDialog((XmppSession)SessionUtility.ActiveSession);
				
				if (dialog.Run () == (int) ResponseType.Ok)
				{
					var image = new XmppDisplayImage (dialog.Filename);
					var session = SessionUtility.ActiveSession as XmppSession;
					session.Account.DisplayImage = image;
					session.Client.SetAvatar (null, image.ImageBuffer);
					session.Account.Cache.SaveAccountImage();
				}
				
				dialog.Destroy ();
		}
		
		/*private void ReceivedInstructions (object sender, ServiceRegistrationEventArgs args)
		{
			RegisterServiceDialog dialog = new RegisterServiceDialog (_session as XmppSession, args);
			
			if (dialog.Run () == 1)
			{
				Dictionary<string,string> values = dialog.GetValues ();
				
				if (values != null && values.Count > 0)
				{
					foreach (ServiceInstructionField field in args.Instructions.Fields)
					{
						if (!string.IsNullOrEmpty (field.Variable))
							if (values.ContainsKey (field.Variable))
								field.Value = values[field.Variable];
					}
				}
				
				(_session as XmppSession).Services.RegisterService (args.Identity, args.Instructions);
			}
			
			dialog.Destroy ();
		}
		
		private void ReceivedRegistrationError (object sender, ServiceRegistrationErrorArgs args)
		{
			MessageDialog dialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, "{0}: {1}", args.Code, args.Message);
			dialog.Run();
			dialog.Destroy ();
		}*/
		
		/*private void ShowNewContactDialog (NewContactActivity activity)
		{
			XmppSession session = activity.Session as XmppSession;
			string passport = String.Empty;
			bool enable_addition = true;
			
			if (activity.Contact != null)
			{
				passport = activity.Contact.UniqueIdentifier;
				
				if (_session.ContactCollection.GetContact (activity.Contact.UniqueIdentifier) != null)
					enable_addition = false;
			}
			else
			{
				passport = activity.UniqueIdentifier;
				
				if (_session.ContactCollection.GetContact (activity.UniqueIdentifier) != null)
					enable_addition = false;
			}
			
			NewContactDialog dialog = new NewContactDialog (session, passport, enable_addition, true);
			ResponseType response = ResponseType.None;
			
			while(response == ResponseType.None)
			{
				response = (ResponseType)dialog.Run();
				
				if (response == ResponseType.Ok || response == ResponseType.DeleteEvent)
				{
					string error = String.Empty;
					bool success = false;
					bool block = response == ResponseType.DeleteEvent;
					
					if (enable_addition)
					{
						if (dialog.SelectedGroup == "Other Contacts")
							session.AddContact (dialog.Address, dialog.Nickname);
						else
							session.AddContact (dialog.Address, dialog.Nickname, new string[] { dialog.SelectedGroup });
					}
					else
					{
						session.AdjustSubscription (passport, dialog.AuthAccept);
					}
					
					dialog.Destroy();
					break;
				}
				else
				{
					dialog.Destroy();
					break;
				}
			}
		}*/
		
		internal static void OnNewContactActivity (object sender, NewContactActivity activity)
		{
			/*
			if (!(activity.Session is XmppSession))
				return;
				
			XmppSessionWidget widget = XmppWidgetUtility.GetSessionWidget((XmppSession)activity.Session);
			widget.ShowNewContactDialog(activity);
			 */
		}
	}
}