
using System;
using System.IO;
using System.Web;
using System.Threading;
using System.Collections.Generic;

using Gtk;
using Glade;
using Pango;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Messaging;


namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class XmppPrivateConversationWidget: BasicChatWidget
	{
		public new XmppPrivateConversation Conversation {
			get { return base.Conversation as XmppPrivateConversation; }
		}
		
		
		public XmppPrivateConversationWidget (IContainerWindow<Widget> window,
		                                      XmppPrivateConversation conversation)
			:base (window, conversation)
		{
		}
		
		public override void Initialize ()
		{
			base.Initialize ();
			
			foreach (var msg in Conversation.GetHistory ()) {
				MessageDisplay.AddMessage (msg);
				msg.Flags = msg.Flags | MessageFlag.History;
			}
			
			Conversation.MessageReceived += HandleMessageReceived;
			
			Conversation.Active = true;
		}

		void HandleMessageReceived(object sender, MessageEventArgs e)
		{
			MessageDisplay.AddMessage (e.Message);
			e.Message.Flags = e.Message.Flags | MessageFlag.History;
			EmitBecomeActive ();
		}

		public override void SwitchFrom ()
		{
		}
		
		public override void SwitchTo ()
		{
			MenuUtility.FillToolBar ("/Galaxium/Gui/XMPP/ContainerWindow/PrivateConversationToolbar",
			                         new DefaultExtensionContext (this),
			                         _window.Toolbar as Toolbar);
			MenuUtility.FillMenuBar ("/Galaxium/Gui/XMPP/ContainerWindow/PrivateConversationMenu",
			                         new DefaultExtensionContext (this),
			                         _window.Menubar as MenuBar);
		}

		public override void Destroy ()
		{
			Conversation.MessageReceived -= HandleMessageReceived;
			Conversation.Active = false;
		}
		
		public override IEntity GetEntity (string uid, string name)
		{
			IContact contact = _conversation.ContactCollection.GetContactByName (name);
			
			if (contact == null)
			{
				contact = new XmppMucContact (Session as XmppSession, _conversation as XmppConference, new JabberID ("blah", "blah", name), XmppPresence.Offline);
			}
			
			return contact;
		}
		
		protected override void MessageEntryTextSubmitted (object sender, SubmitTextEventArgs args)
		{
			var msg = new Message (MessageFlag.Message, Session.Account,
			                       Conversation.PrimaryContact, DateTime.Now);
			msg.SetMarkup (args.Text, null);
			MessageDisplay.AddMessage (msg);
			msg.Flags = msg.Flags | MessageFlag.History;
			Conversation.Send (msg);
			args.Submitted = true;
		}

		protected override void OwnImageButtonPressEvent (object sender, ButtonPressEventArgs args)
		{
			base.OwnImageButtonPressEvent(sender, args);
		}

		public override void LoadFont ()
		{
		}

		public override void SaveFont ()
		{
		}

		public override void SendFile (string filename)
		{
			throw new System.NotImplementedException();
		}
	}
}
