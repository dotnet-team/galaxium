// 
// XmppPreferenceWidget.cs
//
// Copyright © 2007 Philippe Durand <draekz@gmail.com>
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

using Anculus.Core;

// TODO: settings

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class XmppPreferenceWidget: IPreferenceWidget<Widget>
	{
		private Widget _nativeWidget;
	//	private IConfigurationSection _config;
		
		public void Initialize ()
		{
			_nativeWidget = new XML ("XmppPreferenceWidget.glade", "widget") ["widget"];
			
	//		_config = Configuration.Protocol.Section ["Xmpp"];
			
			_nativeWidget.ShowAll ();
		}

		public Widget NativeWidget
		{
			get { return _nativeWidget; }
		}
	}
}
