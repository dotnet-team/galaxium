<Addin
	id="Galaxium.Protocol.Xmpp.GtkGui"
	version="0.1"
	category="core,protocol"
	name="Jabber/XMPP GTK+ Interface"
	description="Provides a GTK+ based interface for the XMPP protocol."
	author="Philippe Durand, Jiří Zárevúcky"
	copyright="GNU General Public License v3">
	
	<!-- ======================================================================= -->
	<!-- ADDIN: You must provide the id, version, category, name, description,
		author and copyright tags to fully describe a working addin. -->
	<!-- ======================================================================= -->
	
	<!-- =================================================================== -->
	<!-- ASSEMBLY: Must point to the DLL file of the assembly to be loaded. -->
	<!-- =================================================================== -->
	
	<Runtime>
		<Import assembly="Galaxium.Protocol.Xmpp.GtkGui.dll"/>
	</Runtime>
	
	<!-- =================================================================== -->
	<!-- DEPENDENCIES: List all the other addin ids and their versions that
		you need available to this one. -->
	<!-- =================================================================== -->
	
	<Dependencies>
		<Addin id="Galaxium.Core" version="0.8" />
		<Addin id="Galaxium.Gui" version="0.8" />
		<Addin id="Galaxium.Gui.GtkGui" version="0.8" />
		<Addin id="Galaxium.Protocol" version="0.8" />
		<Addin id="Galaxium.Protocol.Xmpp" version="0.1" />
		<Addin id="Galaxium.Client" version="0.8" />
		<Addin id="Galaxium.Client.GtkGui" version="0.8" />
	</Dependencies>
	
	<!-- =================================================================== -->
	<!-- PROTOCOL FACTORY: Here you must specify the protocol factory that will
		provide the application with objects useful in handling sessions,
		contacts, conversations and more within the application. -->
	<!-- =================================================================== -->
	
	<Extension path = "/Galaxium/Protocols">
		<Protocol type="Galaxium.Protocol.Xmpp.GtkGui.XmppProtocolFactory" />
	</Extension>
	
	<!-- =================================================================== -->
	<!-- ICONS: Any icons that need to be described for this specific addin. -->
	<!-- =================================================================== -->
	
	<Extension path = "/Galaxium/Icons">
		<GtkIcon _theme="default" _id="galaxium-jabber" _resource="small_galaxium-jabber.png" _iconSize="small" />
		<GtkIcon _theme="default" _id="galaxium-googletalk" _resource="small_galaxium-googletalk.png" _iconSize="small" />

		<GtkIcon _theme="default" _id="search-services" _resource="small_search-services.png" _iconSize="small" />
		<GtkIcon _theme="default" _id="search-services" _resource="large_search-services.png" _iconSize="large" />

		<GtkIcon _theme="default" _id="service-conference" _resource="service-conference.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-conference_irc" _resource="service-conference_irc.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-directory_user" _resource="service-directory_user.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-error" _resource="service-error.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway" _resource="service-gateway.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_aim" _resource="service-gateway_aim.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_gadu-gadu" _resource="service-gateway_gadu-gadu.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_http-ws" _resource="service-gateway_http-ws.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_icq" _resource="service-gateway_icq.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_mrim" _resource="service-gateway_mrim.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_msn" _resource="service-gateway_msn.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_rss" _resource="service-gateway_rss.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_simple" _resource="service-gateway_simple.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_sip" _resource="service-gateway_sip.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_sms" _resource="service-gateway_sms.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_smtp" _resource="service-gateway_smtp.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_tv" _resource="service-gateway_tv.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-gateway_yahoo" _resource="service-gateway_yahoo.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-proxy_bytestreams" _resource="service-proxy_bytestreams.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-pubsub_service" _resource="service-pubsub_service.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-store" _resource="service-store.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-unknown" _resource="service-unknown.png" _iconSize="medium" />
		<GtkIcon _theme="default" _id="service-registered" _resource="service-registered.png" _iconSize="medium" />
	</Extension>
	
	<!-- =================================================================== -->
	<!-- PREFERENCE PANEL: The preference panel option that shows up inside the
		dialog. The name, category, icon, and type must be specified. -->
	<!-- =================================================================== -->
	
	<Extension path = "/Galaxium/Gui/Widgets/Preference">
		<Widget name="Xmpp" category="Protocols" icon="galaxium-jabber" type="Galaxium.Protocol.Xmpp.GtkGui.XmppPreferenceWidget"/>
	</Extension>
	
	<!-- =================================================================== -->
	<!-- CONTACT TREE MANAGER: The contact tree items are rendered using
		instructions from these managers, you will need to provide one for
		this protocol if you want to render the items with custom details. -->
	<!-- =================================================================== -->
	
	<Extension path = "/Galaxium/Gui/ContactTreeManagers">
		<Manager protocol="XMPP" type="Galaxium.Protocol.Xmpp.GtkGui.XmppContactTreeManager" />
	</Extension>
	
	<!-- =================================================================== -->
	<!-- ACTIVITIES: Any special activity that is performed within the protocol
		that needs to get back to the activity utility must be defined here to
		be picked up by the application. -->
	<!-- =================================================================== -->

	<Extension path = "/Galaxium/Activities/PreProcessors">
		<Processor activity="Galaxium.Protocol.ReceivedMessageActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityPreProcessor.OnReceivedMessageActivity"/>
				
		<Processor activity="Galaxium.Protocol.Xmpp.ContactAddedActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityPreProcessor.OnContactAddedActivity"/>
		<Processor activity="Galaxium.Protocol.Xmpp.ContactRemovedActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityPreProcessor.OnContactRemovedActivity"/>
		<Processor activity="Galaxium.Protocol.Xmpp.MessageReceivedActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityPreProcessor.OnMessageReceivedActivity"/>
		<Processor activity="Galaxium.Protocol.Xmpp.StatusChangedActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityPreProcessor.OnStatusChangedActivity"/>
		<Processor activity="Galaxium.Protocol.Xmpp.SubscriptionApprovedActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityPreProcessor.OnSubscriptionApprovedActivity"/>
		<Processor activity="Galaxium.Protocol.Xmpp.SubscriptionDeclinedActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityPreProcessor.OnSubscriptionDeclinedActivity"/>
		<Processor activity="Galaxium.Protocol.Xmpp.SubscriptionRequestedActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityPreProcessor.OnSubscriptionRequestedActivity"/>
	</Extension>

	<Extension path = "/Galaxium/Activities/Processors">
		<Processor activity="Galaxium.Protocol.Xmpp.SubscriptionRequestedActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityListener.OnSubscriptionRequestedActivity"/>
		<Processor activity="Galaxium.Protocol.Xmpp.MessageReceivedActivity" method="Galaxium.Protocol.Xmpp.GtkGui.XmppActivityListener.OnMessageReceivedActivity" />
	</Extension>
	
	<!-- =================================================================== -->
	<!-- MENU EXTENSION POINTS: The following are the extension points for the
		menus/toolbars in this protocol. They will be used in the menu
		definitions below.-->
	<!-- =================================================================== -->

	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/Conference">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>

	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/Contact">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/ConferenceContact">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/Group">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/NRGroup">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/ConferenceGroup">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/Group">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>
		
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContainerWindow/Menu">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContainerWindow/Toolbar">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.ToolItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContainerWindow/ConferenceMenu">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContainerWindow/ConferenceToolbar">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.ToolItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContainerWindow/PrivateConversationMenu">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.MenuItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContainerWindow/PrivateConversationToolbar">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.ToolItemSet"/>
	</ExtensionPoint>
	
	<ExtensionPoint path = "/Galaxium/Gui/XMPP/ContainerWindow/InputToolbar">
		<ExtensionNodeSet id="Galaxium.Gui.GtkGui.ToolItemSet"/>
	</ExtensionPoint>
	
	<!-- =================================================================== -->
	<!-- MENU DEFINITIONS: The actual menus that will show up in the application
		when this protocol is selected. There are specific extension points you
		should be defining menus for that are requirements:
		
		- /Galaxium/Gui/MainWindow/Menu/Session
		- /Galaxium/Gui/MainWindow/Menu/View
		
		As well, you should define menus for the extension points that you have
		provided in th section above. You must also put a condition on some
		menus to check for the proper protocol, see other working addins for
		examples of menu definitions. -->
	<!-- =================================================================== -->
	
	<Extension path = "/Galaxium/Gui/MainWindow/Menu/Session">
		<Condition id="ActiveSession" protocol="XMPP">
			<MenuItem id="open_new_chat" label="_Start Conversation..." accel_key="control+o" icon="galaxium-conversation" event_handler="Galaxium.Protocol.Xmpp.GtkGui.OpenNewChatCommand" />
			<MenuItem id="compose_new_message" label="_Compose Message" accel_key="control+c" icon="galaxium-message" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ComposeNewMessageCommand" />
			<MenuItem id="show_messages" label="_Show Messages" accel_key="control+s" icon="galaxium-messages" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowMessagesCommand" />
			<MenuSeparator />
			<MenuItem id="add_contact" label="_Add Contact..." icon="galaxium-add-contact" accel_key="control+a" event_handler="Galaxium.Protocol.Xmpp.GtkGui.AddContactCommand" />
			<MenuSeparator />
			<MenuItem id="edit_privacy" label="Edit _Privacy Lists" accel_key="control+p" icon="galaxium-privacy" event_handler="Galaxium.Protocol.Xmpp.GtkGui.PrivacyListsCommand" />
			<MenuItem id="modify_vcard" label="Modify vCar_d" accel_key="control+d" icon="galaxium-user-info" event_handler="Galaxium.Protocol.Xmpp.GtkGui.SetVCardCommand" />
			<MenuItem id="set_avatar" label="Set A_vatar..." accel_key="control+v" icon="galaxium-images" event_handler="Galaxium.Protocol.Xmpp.GtkGui.SetAvatarCommand" />
			<MenuSeparator />
			<MenuItem id="manage_services" label="Manage Services" icon="search-services" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ManageServicesCommand" />
			<MenuItem id="browse_chatrooms" label="Browse Chatrooms" icon="search-services" event_handler="Galaxium.Protocol.Xmpp.GtkGui.BrowseChatroomsCommand" />
			<MenuItem id="service_log" label="Display Service Log" event_handler="Galaxium.Protocol.Xmpp.GtkGui.DisplayServiceLogCommand" />
		</Condition>
	</Extension>
	
	<Extension path = "/Galaxium/Gui/MainWindow/Menu/View">
		<Condition id="ActiveSession" protocol="XMPP">
			<MenuRadio insertbefore="detail_level" id="by_group" label="By Group" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ViewByGroupCommand" />
			<MenuRadio id="by_status" label="By Status" group="by_group" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ViewByStatusCommand" />
		
			<MenuSeparator />
		
			<MenuCheck insertafter="show_profile_details" id="show_search" label="Search Bar" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowSearchCheckCommand" />
		
			<MenuSeparator />
		
			<MenuCheck id="show_display_images" label="Contact Images" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowDisplayImagesCommand" />
			<MenuCheck id="show_display_names" label="Contact Names" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowDisplayNamesCommand" />
			<MenuCheck id="show_personal_messages" label="Contact Messages" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowPersonalMessagesCommand" />
			<MenuCheck id="show_nicknames" label="Contact Nicknames" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowContactNicknamesCommand" />
		
			<MenuSeparator />
		
			<MenuCheck id="show_empty_groups" label="Empty Groups" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowEmptyGroupsCommand" />
			<MenuCheck id="show_offline_contacts" label="Offline Contacts" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowOfflineContactsCommand" />
			<MenuCheck id="group_offline_contacts" label="Group Offline Contacts" event_handler="Galaxium.Protocol.Xmpp.GtkGui.GroupOfflineContactsCommand" />
			
			<MenuSeparator />
			
			<MenuCheck id="use_defaults" label="Use Defaults" event_handler="Galaxium.Protocol.Xmpp.GtkGui.UseDefaultListViewCommand" />
		</Condition>
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/Group">
		<!-- Not Implemented -->
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/Contact">
		<Condition id="ActiveSession" protocol="XMPP">

			<MenuItem id="open_chat" label="_Start Conversation" icon="galaxium-conversation" event_handler="Galaxium.Protocol.Xmpp.GtkGui.OpenChatCommand" />
			<MenuItem id="compose_message" label="Compose Message..." icon="galaxium-message" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ComposeMessageCommand" />

			<MenuItem id="send_file" label="Send File..." icon="galaxium-transfer-open" event_handler="Galaxium.Protocol.Xmpp.GtkGui.SendFileContactCommand" />

			<MenuCheck id="auto_accept" label="Auto-accept Files" event_handler="Galaxium.Protocol.Xmpp.GtkGui.AutoAcceptCommand" />

			<Menu id="manage_contact" label="Manage Contact">
				<MenuItem id="change_nickname" label="Change Nickname..." icon="galaxium-set" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ChangeNicknameCommand" />
				<MenuItem id="edit_groups" label="Edit Groups..." icon="galaxium-edit" event_handler="Galaxium.Protocol.Xmpp.GtkGui.EditGroupsCommand" />				

				<MenuSeparator />
				<Condition id="SubscriptionCondition" asking="false">
					<MenuItem id="request_subscription" label="Request Subscription..." icon="galaxium-add" event_handler="Galaxium.Protocol.Xmpp.GtkGui.RequestAuthCommand" />
				</Condition>
				<Condition id="SubscriptionCondition" asking="true">
					<MenuItem id="request_subscription_waiting" label="Request Subscription... (waiting)" icon="galaxium-add" event_handler="Galaxium.Protocol.Xmpp.GtkGui.RequestAuthCommand" />
				</Condition>

				<MenuItem id="approve_subscription" label="Approve Subscription" icon="galaxium-approve" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ApproveAuthCommand" />
				<Condition id="SubscriptionCondition" waiting="true">
					<MenuItem id="deny_subscription" label="Decline Subscription" icon="galaxium-block" event_handler="Galaxium.Protocol.Xmpp.GtkGui.DenyAuthCommand" />
				</Condition>
				<Condition id="SubscriptionCondition" waiting="false">
					<MenuItem id="cancel_subscription" label="Cancel Subscription" icon="galaxium-block" event_handler="Galaxium.Protocol.Xmpp.GtkGui.DenyAuthCommand" />
				</Condition>
				<MenuSeparator />
				
				<MenuItem id="remove" label="Remove" icon="galaxium-remove" event_handler="Galaxium.Protocol.Xmpp.GtkGui.RemoveContactCommand" />
			</Menu>

			<!--<Menu id="options" label="Options">
				<MenuCheck id="display_name" label="Display Name in the List" event_handler="Galaxium.Protocol.Xmpp.GtkGui.DisplayNameCommand" />
				<MenuCheck id="display_avatar" label="Display Avatar in the List" event_handler="Galaxium.Protocol.Xmpp.GtkGui.DisplayImageCommand" />
				</Menu>-->
				
			<MenuSeparator />
			
			<MenuItem id="log" label="Open Communication Log" event_handler="Galaxium.Protocol.Xmpp.GtkGui.DisplayLogCommand" />

			<MenuItem id="information" label="_Information" icon="galaxium-user-info" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ViewInfoCommand" />
		</Condition>
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/ConferenceContact">
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/Conference">
		<MenuItem id="open_channel" label="Open Channel" icon="galaxium-conversation" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ContactTreeOpenChannelCommand" />
		<MenuItem id="rename_channel" label="Rename Channel..." icon="galaxium-rename-channel" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ContactTreeRenameChannelCommand" />
		<MenuItem id="change_password" label="Change Password..." icon="galaxium-set" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ContactTreeChangeChannelPasswordCommand" />
		<MenuSeparator />
		<MenuItem id="join_channel" label="Join Channel" icon="galaxium-join-channel" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ContactTreeJoinChannelCommand" />
		<MenuItem id="part_channel" label="Part Channel" icon="galaxium-part-channel" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ContactTreePartChannelCommand" />
		<MenuSeparator />
		<MenuItem id="remove_from_list" label="Remove from List" icon="galaxium-delete" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ContactTreeRemoveChannelCommand" />
		<MenuSeparator />
		<MenuCheck id="persistent" label="Persistent Channel" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ContactTreeChannelPersistentCommand" />
		<MenuCheck id="auto_join" label="Auto-Join Channel" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ContactTreeChannelAutoJoinCommand" />
		<MenuSeparator />
		<MenuItem id="channel_details" label="Channel Details" icon="galaxium-info" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ContactTreeChannelDetailsCommand" />
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContainerWindow/Menu">
		<Menu id="Conversation" label="_Conversation">
			<MenuItem id="send_file" label="Send File..." icon="galaxium-transfer-open" event_handler="Galaxium.Protocol.Xmpp.GtkGui.SendFileCommand" />
			<MenuSeparator />
			<MenuItem id="view_logs" label="View Logs..." icon="galaxium-preferences-logging" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ViewLogsCommand" />
			<MenuItem id="find_text" label="Find Text..." icon="galaxium-find" event_handler="Galaxium.Protocol.Xmpp.GtkGui.FindTextCommand" />
			<MenuSeparator />
			<MenuItem id="close_conversation" label="Close Tab" icon="galaxium-close" accel_key="control+w" event_handler="Galaxium.Protocol.Xmpp.GtkGui.CloseConversationCommand" />
			<MenuItem id="close_window" label="Close Window" icon="galaxium-close" accel_key="Escape" event_handler="Galaxium.Protocol.Xmpp.GtkGui.CloseWindowCommand" />
		</Menu>
		
		<Menu id="View" label="_View">
			<MenuCheck id="show_action" label="Action Toolbar" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowActionToolbarCommand" />
			<MenuCheck id="show_input" label="Input Toolbar" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowInputToolbarCommand" />
			
			<MenuSeparator />
			
			<MenuCheck id="show_account_image" label="Account Image" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowAccountImageCommand" />
			<MenuCheck id="show_contact_image" label="Contact Image" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowContactImageCommand" />
			<MenuCheck id="show_personal" label="Contact Details" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowPersonalMessageCommand" />
			<!--<MenuCheck id="show_timestamps" label="Timestamps" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowTimestampsCommand" />-->
			
			<MenuSeparator />
			
			<MenuCheck id="use_view_defaults" label="Use Defaults" event_handler="Galaxium.Protocol.Xmpp.GtkGui.UseDefaultViewCommand" />
		</Menu>
		
		<Menu id="Options" label="_Options">
			<MenuCheck id="enable_logging" label="Enable Logging" event_handler="Galaxium.Protocol.Xmpp.GtkGui.EnableLoggingCommand" />
			<MenuCheck id="enable_sounds" label="Enable Sounds" event_handler="Galaxium.Protocol.Xmpp.GtkGui.EnableSoundsCommand" />
			
			<MenuSeparator />
			
			<MenuCheck id="use_settings_defaults" label="Use Defaults" event_handler="Galaxium.Protocol.Xmpp.GtkGui.UseDefaultSettingsCommand" />
		</Menu>
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContainerWindow/ConferenceMenu">
		<Menu id="Conversation" label="_Conversation">
			<MenuItem id="send_file" label="Send File..." icon="galaxium-transfer-open" event_handler="Galaxium.Protocol.Xmpp.GtkGui.SendFileCommand" />
			<MenuSeparator />
			<MenuItem id="view_logs" label="View Logs..." icon="galaxium-preferences-logging" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ViewLogsCommand" />
			<MenuItem id="find_text" label="Find Text..." icon="galaxium-find" event_handler="Galaxium.Protocol.Xmpp.GtkGui.FindTextCommand" />
			<MenuSeparator />
			<MenuItem id="close_conversation" label="Close Tab" icon="galaxium-close" accel_key="control+w" event_handler="Galaxium.Protocol.Xmpp.GtkGui.CloseConversationCommand" />
			<MenuItem id="close_window" label="Close Window" icon="galaxium-close" accel_key="Escape" event_handler="Galaxium.Protocol.Xmpp.GtkGui.CloseWindowCommand" />
		</Menu>
		
		<Menu id="View" label="_View">
			<MenuCheck id="show_action" label="Action Toolbar" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowActionToolbarCommand" />
			<MenuCheck id="show_input" label="Input Toolbar" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowInputToolbarCommand" />
			
			<MenuSeparator />
			
			<MenuCheck id="show_account_image" label="Account Image" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowAccountImageCommand" />
			<!--<MenuCheck id="show_timestamps" label="Timestamps" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowTimestampsCommand" />-->
			
			<MenuSeparator />
			
			<MenuCheck id="use_view_defaults" label="Use Defaults" event_handler="Galaxium.Protocol.Xmpp.GtkGui.UseDefaultViewCommand" />
		</Menu>
		
		<Menu id="Options" label="_Options">
			<MenuCheck id="enable_logging" label="Enable Logging" event_handler="Galaxium.Protocol.Xmpp.GtkGui.EnableLoggingCommand" />
			<MenuCheck id="enable_sounds" label="Enable Sounds" event_handler="Galaxium.Protocol.Xmpp.GtkGui.EnableSoundsCommand" />
			
			<MenuSeparator />
			
			<MenuCheck id="use_settings_defaults" label="Use Defaults" event_handler="Galaxium.Protocol.Xmpp.GtkGui.UseDefaultSettingsCommand" />
		</Menu>
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContainerWindow/PrivateConversationMenu">
		<Menu id="Conversation" label="_Conversation">
			<MenuItem id="send_file" label="Send File..." icon="galaxium-transfer-open" event_handler="Galaxium.Protocol.Xmpp.GtkGui.SendFileCommand" />
			<MenuSeparator />
			<MenuItem id="view_logs" label="View Logs..." icon="galaxium-preferences-logging" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ViewLogsCommand" />
			<MenuItem id="find_text" label="Find Text..." icon="galaxium-find" event_handler="Galaxium.Protocol.Xmpp.GtkGui.FindTextCommand" />
			<MenuSeparator />
			<MenuItem id="close_conversation" label="Close Tab" icon="galaxium-close" accel_key="control+t" event_handler="Galaxium.Protocol.Xmpp.GtkGui.CloseConversationCommand" />
			<MenuItem id="close_window" label="Close Window" icon="galaxium-close" accel_key="Escape" event_handler="Galaxium.Protocol.Xmpp.GtkGui.CloseWindowCommand" />
		</Menu>
		
		<Menu id="View" label="_View">
			<MenuCheck id="show_action" label="Action Toolbar" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowActionToolbarCommand" />
			<MenuCheck id="show_input" label="Input Toolbar" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowInputToolbarCommand" />
			
			<MenuSeparator />
			
			<MenuCheck id="show_account_image" label="Account Image" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowAccountImageCommand" />
			<!--<MenuCheck id="show_timestamps" label="Timestamps" event_handler="Galaxium.Protocol.Xmpp.GtkGui.ShowTimestampsCommand" />-->
			
			<MenuSeparator />
			
			<MenuCheck id="use_view_defaults" label="Use Defaults" event_handler="Galaxium.Protocol.Xmpp.GtkGui.UseDefaultViewCommand" />
		</Menu>
		
		<Menu id="Options" label="_Options">
			<MenuCheck id="enable_logging" label="Enable Logging" event_handler="Galaxium.Protocol.Xmpp.GtkGui.EnableLoggingCommand" />
			<MenuCheck id="enable_sounds" label="Enable Sounds" event_handler="Galaxium.Protocol.Xmpp.GtkGui.EnableSoundsCommand" />
			
			<MenuSeparator />
			
			<MenuCheck id="use_settings_defaults" label="Use Defaults" event_handler="Galaxium.Protocol.Xmpp.GtkGui.UseDefaultSettingsCommand" />
		</Menu>
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContainerWindow/Toolbar">
		<ToolButton id="invite_contact" important="true" label="Invite Contact" icon="galaxium-invite" tooltip="Invite a contact into the conversation." event_handler="Galaxium.Protocol.Xmpp.GtkGui.InviteContactToolCommand" />
		
		<ToolSeparator />
		
		<ToolButton id="send_file" important="true" label="Send File" icon="galaxium-transfer-open" tooltip="Send a file to this contact." event_handler="Galaxium.Protocol.Xmpp.GtkGui.SendFileToolCommand" />
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContainerWindow/ConferenceToolbar">
		<ToolButton id="invite_contact" important="true" label="Invite Contact" icon="galaxium-invite" tooltip="Invite a contact into the conversation." event_handler="Galaxium.Protocol.Xmpp.GtkGui.InviteContactToolCommand" />
		<ToolSeparator />
		<ToolButton id="send_file" important="true" label="Send File" icon="galaxium-transfer-open" tooltip="Send a file to this contact." event_handler="Galaxium.Protocol.Xmpp.GtkGui.SendFileToolCommand" />
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContainerWindow/PrivateConversationToolbar">
		<ToolButton id="send_file" important="true" label="Send File" icon="galaxium-transfer-open" tooltip="Send a file to this contact." event_handler="Galaxium.Protocol.Xmpp.GtkGui.SendFileToolCommand" />
	</Extension>
	
	<Extension path = "/Galaxium/Gui/XMPP/ContainerWindow/InputToolbar">
		<ToolButton id="font" label="Font" icon="galaxium-font-face" important="true" iconsize="Small" tooltip="Change the font of your messages." event_handler="Galaxium.Protocol.Xmpp.GtkGui.FontToolCommand" />
		<ToolButton id="reset" icon="galaxium-font-normal" iconsize="Small" tooltip="Reset the font settings back to default." event_handler="Galaxium.Protocol.Xmpp.GtkGui.ResetToolCommand" />
		<ToolSeparator />
		<ToolButton id="emoticon" label="Emoticon" icon="galaxium-insert-emoticon" important="true" iconsize="Small" tooltip="Insert an emoticon in your message." event_handler="Galaxium.Protocol.Xmpp.GtkGui.EmoticonToolCommand" />
		<ToolSeparator />
		<ToolButton id="spellcheck" label="Spelling" icon="galaxium-spellcheck" iconsize="Small" tooltip="Check the spelling of your message." event_handler="Galaxium.Protocol.Xmpp.GtkGui.SpellCheckToolCommand" />
		<ToolButton id="clear" label="Clear" icon="galaxium-clear" iconsize="Small" tooltip="Clear the message you are typing." event_handler="Galaxium.Protocol.Xmpp.GtkGui.ClearToolCommand" />
	</Extension>
</Addin>
