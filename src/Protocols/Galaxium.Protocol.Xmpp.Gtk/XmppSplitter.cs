//  
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.Collections.Generic;
using System.Text;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class XmppSplitter: IMessageSplitter
	{
		public XmppSplitter ()
		{
		}

		public List<IMessageChunk> Split (string text, IEntity source, IEntity dest)
		{
			var chunks = new List<IMessageChunk> ();
			chunks.Add (new TextMessageChunk (null, null, text));
			return chunks;
		}
		
		public string ChunkMarkup (IMessageChunk chunk)
		{
			if (chunk is ITextMessageChunk)
				return (chunk as TextMessageChunk).Text;
			else
				return String.Empty;
		}
	}
}
