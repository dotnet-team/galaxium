using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Galaxium.Protocol.Xmpp.Gtk")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Galaxium")]
[assembly: AssemblyCopyright("GNU General Public License v3")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("0.8.0.0")]

[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
