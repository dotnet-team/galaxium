/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Collections.Generic;

namespace Galaxium.Protocol.Irc
{
	//TODO: see http://www.mirc.net/raws
	public static class IrcConstants
	{
		public const int MaxCommandSize = 512;
		
		public static class Markers
		{
			public const char Ctcp = '\u0001';
			public const char Bold = '\u0002';
			public const char Underline = '\u001F';
			public const char Reverse = '\u0016';
			public const char Reset = '\u000F';
			public const char Color = '\u0003';
		}
		
		public static class Colors
		{
			// Tango Color Codes
			public const int White = 16777215;
			public const int Black = 0;
			public const int Blue = 2116231;
			public const int Green = 5151238;
			public const int LightRed = 15542569;
			public const int Brown = 9394434;
			public const int Purple = 6042982;
			public const int Orange = 16087296;
			public const int Yellow = 15586304;
			public const int LightGreen = 7590422;
			public const int Cyan = 233090;
			public const int LightCyan = 381633;
			public const int LightBlue = 7512015;
			public const int Pink = 11370408;
			public const int Grey = 5592915;
			public const int LightGrey = 12238262;
		}
		
		public static class Commands
		{
			public const string Password = "PASSWORD";
			public const string Nickname = "NICK";
			public const string User = "USER";
			public const string ServList = "SERVLIST";
			public const string ServQuery = "SQUERY";
			public const string Server = "SERVER";
			public const string Operator = "OPER";
			public const string Quit = "QUIT";
			public const string ServerQuit = "SQUIT";
			public const string Join = "JOIN";
			public const string Part = "PART";
			public const string Mode = "MODE";
			public const string MOTD = "MOTD";
			public const string Topic = "TOPIC";
			public const string Names = "NAMES";
			public const string List = "LIST";
			public const string LUsers = "LUSERS";
			public const string Invite = "INVITE";
			public const string Kick = "KICK";
			public const string Version = "VERSION";
			public const string Stats = "STATS";
			public const string Links = "LINKS";
			public const string Time = "TIME";
			public const string Connect = "CONNECT";
			public const string Trace = "TRACE";
			public const string Admin = "ADMIN";
			public const string Info = "INFO";
			public const string PrivateMessage = "PRIVMSG";
			public const string Notice = "NOTICE";
			public const string Who = "WHO";
			public const string WhoIs = "WHOIS";
			public const string WhoWas = "WHOWAS";
			public const string Wallops = "WALLOPS";
			public const string Kill = "KILL";
			public const string Ping = "PING";
			public const string Pong = "PONG";
			public const string Error = "ERROR";
			public const string Away = "AWAY";
			public const string Rehash = "REHASH";
			public const string Restart = "RESTART";
			public const string Summon = "SUMMON";
			public const string Users = "USERS";
			public const string Operwall = "WALLOPS";
			public const string Userhost = "USERHOST";
			public const string Ison = "ISON";
	
			public const string Ctcp = "CTCP";
			public const string CtcpFinger = "FINGER";
			public const string CtcpUserInfo = "USERINFO";
			public const string CtcpVersion = "VERSION";
			public const string CtcpSource = "SOURCE";
			public const string CtcpClientInfo = "CLIENTINFO";
			public const string CtcpErrorMessage = "ERRMSG";
			public const string CtcpPing = "PING";
			public const string CtcpTime = "TIME";
			
			public const string Dcc = "DCC";
			public const string DccChat = "CHAT";
			public const string DccSend = "SEND";
			public const string DccGet = "GET";
			public const string DccResume = "RESUME";
			public const string DccAccept = "ACCEPT";
		}
		
		public static class Replies
		{
			public const string HeaderWelcome = "001";
			public const string HeaderHost = "002";
			public const string HeaderServer = "003";
			public const string HeaderServerInfo = "004";
			public const string HeaderServerFlags = "005";
	
			// RPL_NONE
			// Dummy reply number. Not used.
			public const string ReplyNone = "300";
			// RPL_USERHOST
			// ":[<reply>{<space><reply>}]"
			// - Reply format used by USERHOST to list replies to the query list. The reply string is composed as follows:
			// <reply> ::= <nick>['*'] '=' <'+'|'-'><hostname>
			// The '*' indicates whether the client has registered as an Operator. The '-' or '+' characters represent whether the client has set an AWAY message or not respectively.
			public const string ReplyUserHost = "302";
			// RPL_ISON
			// ":[<nick> {<space><nick>}]"
			// - Reply format used by ISON to list replies to the query list.
			public const string ReplyIsOn = "303";
			// RPL_AWAY
			// "<nick> :<away message>"
			public const string ReplyAway = "301";
			// RPL_UNAWAY
			// ":You are no longer marked as being away"
			public const string ReplyUnAway = "305";
			// RPL_NOWAWAY
			// ":You have been marked as being away"
			// - These replies are used with the AWAY command (if allowed). RPL_AWAY is sent to any client sending a PRIVMSG to a client which is away. RPL_AWAY is only sent by the server to which the client is connected. Replies RPL_UNAWAY and RPL_NOWAWAY are sent when the client removes and sets an AWAY message.
			public const string ReplyNowAway = "306";
			// RPL_WHOISUSER
			// "<nick> <user> <host> * :<real name>"
			public const string ReplyWhoisUser = "311";
			// RPL_WHOISSERVER
			// "<nick> <server> :<server info>"
			public const string ReplyWhoisServer = "312";
			// RPL_WHOISOPERATOR
			// "<nick> :is an IRC operator"
			public const string ReplyWhoisOperator = "313";
			// RPL_WHOISIDLE
			// "<nick> <integer> :seconds idle"
			public const string ReplyWhoisIdle = "317";
			// RPL_ENDOFWHOIS
			// "<nick> :End of /WHOIS list"
			public const string ReplyEndOfWhois = "318";
			// RPL_WHOISCHANNELS
			// "<nick> :{[@|+]<channel><space>}"
			// - Replies 311 - 313, 317 - 319 are all replies generated in response to a WHOIS message. Given that there are enough parameters present, the answering server must either formulate a reply out of the above numerics (if the query nick is found) or return an error reply. The '*' in RPL_WHOISUSER is there as the literal character and not as a wild card. For each reply set, only RPL_WHOISCHANNELS may appear more than once (for long lists of channel names). The '@' and '+' characters next to the channel name indicate whether a client is a channel operator or has been granted permission to speak on a moderated channel. The RPL_ENDOFWHOIS reply is used to mark the end of processing a WHOIS message.
			public const string ReplyWhoisChannels = "319";
			// RPL_WHOWASUSER
			// "<nick> <user> <host> * :<real name>"
			public const string ReplyWhoWasUser = "314";
			// RPL_ENDOFWHOWAS
			// "<nick> :End of WHOWAS"
			// - When replying to a WHOWAS message, a server must use the replies RPL_WHOWASUSER, RPL_WHOISSERVER or ERR_WASNOSUCHNICK for each nickname in the presented list. At the end of all reply batches, there must be RPL_ENDOFWHOWAS (even if there was only one reply and it was an error).
			public const string ReplyEndOfWhoWas = "369";
			// RPL_LISTSTART
			// "Channel :Users Name"
			public const string ReplyListStart = "321";
			// RPL_LIST
			// "<channel> <# visible> :<topic>"
			public const string ReplyList = "322";
			// RPL_LISTEND
			// ":End of /LIST"
			// - Replies RPL_LISTSTART, RPL_LIST, RPL_LISTEND mark the start, actual replies with data and end of the server's response to a LIST command. If there are no channels available to return, only the start and end reply must be sent.
			public const string ReplyListEnd = "323";
			// RPL_CHANNELMODEIS
			// "<channel> <mode> <mode params>"
			public const string ReplyChannelModeIs = "324";
			
			public const string ReplyChannelUrl = "328";
			public const string ReplyChannelTime = "329";
	
			// RPL_NOTOPIC
			// "<channel> :No topic is set"
			public const string ReplyNoTopic = "331";
			// RPL_TOPIC
			// "<channel> :<topic>"
			// - When sending a TOPIC message to determine the channel topic, one of two replies is sent. If the topic is set, RPL_TOPIC is sent back else RPL_NOTOPIC.
			public const string ReplyTopic = "332";
			public const string ReplyChannelNicknameTime = "333";
			// RPL_INVITING
			// "<channel> <nick>"
			// - Returned by the server to indicate that the attempted INVITE message was successful and is being passed onto the end client.
			public const string ReplyInviting = "341";
			// RPL_SUMMONING
			// "<user> :Summoning user to IRC"
			// - Returned by a server answering a SUMMON message to indicate that it is summoning that user.
			public const string ReplySummoning = "342";
			// RPL_VERSION
			// "<version>.<debuglevel> <server> :<comments>"
			// - Reply by the server showing its version details. The <version> is the version of the software being used (including any patchlevel revisions) and the <debuglevel> is used to indicate if the server is running in "debug mode".
			// The "comments" field may contain any comments about the version or further version details.
			public const string ReplyVersion = "351";
			// RPL_WHOREPLY
			// "<channel> <user> <host> <server> <nick> <H|G>[*][@|+] :<hopcount> <real name>"
			public const string ReplyWho = "352";
			// RPL_ENDOFWHO
			// "<name> :End of /WHO list"
			// - The RPL_WHOREPLY and RPL_ENDOFWHO pair are used to answer a WHO message. The RPL_WHOREPLY is only sent if there is an appropriate match to the WHO query. If there is a list of parameters supplied with a WHO message, a RPL_ENDOFWHO must be sent after processing each list item with <name> being the item.
			public const string ReplyEndOfWho = "315";
			// RPL_NAMREPLY
			// "<channel> :[[@|+]<nick> [[@|+]<nick> [...]]]"
			public const string ReplyNames = "353";
			// RPL_ENDOFNAMES
			// "<channel> :End of /NAMES list"
			// - To reply to a NAMES message, a reply pair consisting of RPL_NAMREPLY and RPL_ENDOFNAMES is sent by the server back to the client. If there is no channel found as in the query, then only RPL_ENDOFNAMES is returned. The exception to this is when a NAMES message is sent with no parameters and all visible channels and contents are sent back in a series of RPL_NAMEREPLY messages with a RPL_ENDOFNAMES to mark the end.
			public const string ReplyEndOfNames = "366";
			// RPL_LINKS
			// "<mask> <server> :<hopcount> <server info>"
			public const string ReplyLinks = "364";
			// RPL_ENDOFLINKS
			// "<mask> :End of /LINKS list"
			// - In replying to the LINKS message, a server must send replies back using the RPL_LINKS numeric and mark the end of the list using an RPL_ENDOFLINKS reply.v
			public const string ReplyEndOfLinks = "365";
			// RPL_BANLIST
			// "<channel> <banid>"
			public const string ReplyBanList = "367";
			// RPL_ENDOFBANLIST
			// "<channel> :End of channel ban list"
			// - When listing the active 'bans' for a given channel, a server is required to send the list back using the RPL_BANLIST and RPL_ENDOFBANLIST messages. A separate RPL_BANLIST is sent for each active banid. After the banids have been listed (or if none present) a RPL_ENDOFBANLIST must be sent.
			public const string ReplyEndOfBanList = "368";
			// RPL_INFO
			// ":<string>"
			public const string ReplyInfo = "371";
			// RPL_ENDOFINFO
			// ":End of /INFO list"
			// - A server responding to an INFO message is required to send all its 'info' in a series of RPL_INFO messages with a RPL_ENDOFINFO reply to indicate the end of the replies.
			public const string ReplyEndOfInfo = "374";
			// RPL_MOTDSTART
			// ":- <server> Message of the day - "
			public const string ReplyMotdStart = "375";
			// RPL_MOTD
			// ":- <text>"
			public const string ReplyMotd = "372";
			// RPL_ENDOFMOTD
			// ":End of /MOTD command"
			// - When responding to the MOTD message and the MOTD file is found, the file is displayed line by line, with each line no longer than 80 characters, using RPL_MOTD format replies. These should be surrounded by a RPL_MOTDSTART (before the RPL_MOTDs) and an RPL_ENDOFMOTD (after).
			public const string ReplyEndOfMotd = "376";
			// RPL_YOUREOPER
			// ":You are now an IRC operator"
			// - RPL_YOUREOPER is sent back to a client which has just successfully issued an OPER message and gained operator status.
			public const string ReplyYouAreOperator = "381";
			// RPL_REHASHING
			// "<config file> :Rehashing"
			// - If the REHASH option is used and an operator sends a REHASH message, an RPL_REHASHING is sent back to the operator.
			public const string ReplyRehashing = "382";
			// RPL_TIME
			// "<server> :<string showing server's local time>"
			// - When replying to the TIME message, a server must send the reply using the RPL_TIME format above. The string showing the time need only contain the correct day and time there. There is no further requirement for the time string.
			public const string ReplyTime = "391";
			// RPL_USERSSTART
			// ":UserID Terminal Host"
			public const string ReplyUsersStart = "392";
			// RPL_USERS
			// ":%-8s %-9s %-8s"
			public const string ReplyUsers = "393";
			// RPL_ENDOFUSERS
			// ":End of users"
			public const string ReplyEndOfUsers = "394";
			// RPL_NOUSERS
			// ":Nobody logged in"
			// - If the USERS message is handled by a server, the replies RPL_USERSTART, RPL_USERS, RPL_ENDOFUSERS and RPL_NOUSERS are used. RPL_USERSSTART must be sent first, following by either a sequence of RPL_USERS or a single RPL_NOUSER. Following this is RPL_ENDOFUSERS.
			public const string ReplyNoUsers = "395";
			// RPL_TRACELINK
			// "Link <version & debug level> <destination> <next server>"
			public const string ReplyTraceLink = "200";
			// RPL_TRACECONNECTING
			// "Try. <class> <server>"
			public const string ReplyTraceConnecting = "201";
			// RPL_TRACEHANDSHAKE
			// "H.S. <class> <server>"
			public const string ReplyTraceHandshake = "202";
			// RPL_TRACEUNKNOWN
			// "???? <class> [<client IP address in dot form>]"
			public const string ReplyTraceUnknown = "203";
			// RPL_TRACEOPERATOR
			// "Oper <class> <nick>"
			public const string ReplyTraceOperator = "204";
			// RPL_TRACEUSER
			// "User <class> <nick>"
			public const string ReplyTraceUser = "205";
			// RPL_TRACESERVER
			// "Serv <class> <int>S <int>C <server> <nick!user|*!*>@<host|server>"
			public const string ReplyTraceServer = "206";
			// RPL_TRACENEWTYPE
			// "<newtype> 0 <client name>"
			public const string ReplyTraceNewType = "208";
			// RPL_TRACELOG
			// "File <logfile> <debug level>"
			// - The RPL_TRACE* are all returned by the server in response to the TRACE message. How many are returned is dependent on the the TRACE message and whether it was sent by an operator or not. There is no predefined order for which occurs first. Replies RPL_TRACEUNKNOWN, RPL_TRACECONNECTING and RPL_TRACEHANDSHAKE are all used for connections which have not been fully established and are either unknown, still attempting to connect or in the process of completing the 'server handshake'. RPL_TRACELINK is sent by any server which handles a TRACE message and has to pass it on to another server. The list of RPL_TRACELINKs sent in response to a TRACE command traversing the IRC network should reflect the actual connectivity of the servers themselves along that path. RPL_TRACENEWTYPE is to be used for any connection which does not fit in the other categories but is being displayed anyway.
			public const string ReplyTraceLog = "261";
			// RPL_STATSLINKINFO
			// "<linkname> <sendq> <sent messages> <sent bytes> <received messages> <received bytes> <time open>"
			public const string ReplyStatsLinkInfo = "211";
			// RPL_STATSCOMMANDS
			// "<command> <count>"
			public const string ReplyStatsCommands = "212";
			// RPL_STATSCLINE
			// "C <host> * <name> <port> <class>"
			public const string ReplyStatsCLine = "213";
			// RPL_STATSNLINE
			// "N <host> * <name> <port> <class>"
			public const string ReplyStatsNLine = "214";
			// RPL_STATSILINE
			// "I <host> * <host> <port> <class>"
			public const string ReplyStatsILine = "215";
			// RPL_STATSKLINE
			// "K <host> * <username> <port> <class>"
			public const string ReplyStatsKLine = "216";
			// RPL_STATSYLINE
			// "Y <class> <ping frequency> <connect frequency> <max sendq>"
			public const string ReplyStatsYLine = "218";
			// RPL_ENDOFSTATS
			// "<stats letter> :End of /STATS report"
			public const string ReplyEndOfStats = "219";
			// RPL_STATSLLINE
			// "L <hostmask> * <servername> <maxdepth>"
			public const string ReplyStatsLLine = "241";
			// RPL_STATSUPTIME
			// ":Server Up %d days %d:%02d:%02d"
			public const string ReplyStatsUptime = "242";
			// RPL_STATSOLINE
			// "O <hostmask> * <name>"
			public const string ReplyStatsOLine = "243";
			// RPL_STATSHLINE
			// "H <hostmask> * <servername>"
			public const string ReplyStatsHLine = "244";
			// RPL_UMODEIS
			// "<user mode string>"
			// - To answer a query about a client's own mode, RPL_UMODEIS is sent back.
			public const string ReplyUModeIs = "221";
			// Highest connection count: total (num clients)
			public const string ReplyHighestConnectionCount = "250";
			// RPL_LUSERCLIENT
			// ":There are <integer> users and <integer> invisible on <integer> servers"
			public const string ReplyLUserClient = "251";
			// RPL_LUSEROP
			// "<integer> :operator(s) online"
			public const string ReplyLUserOp = "252";
			// RPL_LUSERUNKNOWN
			// "<integer> :unknown connection(s)"
			public const string ReplyLUserUnknown = "253";
			// RPL_LUSERCHANNELS
			// "<integer> :channels formed"
			public const string ReplyLUserChannels = "254";
			// RPL_LUSERME
			// ":I have <integer> clients and <integer> servers"		
			// - In processing an LUSERS message, the server sends a set of replies from RPL_LUSERCLIENT, RPL_LUSEROP, RPL_USERUNKNOWN, RPL_LUSERCHANNELS and RPL_LUSERME. When replying, a server must send back RPL_LUSERCLIENT and RPL_LUSERME. The other replies are only sent back if a non-zero count is found for them.
			public const string ReplyLUserMe = "255";
			// RPL_ADMINME
			// "<server> :Administrative info"
			public const string ReplyAdminMe = "256";
			// RPL_ADMINLOC1
			// ":<admin info>"
			public const string ReplyAdminLoc1 = "257";
			// RPL_ADMINLOC2
			// ":<admin info>"
			public const string ReplyAdminLoc2 = "258";
			// RPL_ADMINEMAIL
			// ":<admin info>"
			// - When replying to an ADMIN message, a server is expected to use replies RLP_ADMINME through to RPL_ADMINEMAIL and provide a text message with each. For RPL_ADMINLOC1 a description of what city, state and country the server is in is expected, followed by details of the university and department (RPL_ADMINLOC2) and finally the administrative contact for the server (an email address here is required) in RPL_ADMINEMAIL.
			public const string ReplyAdminMail = "259";
			// Server load is temporarily too heavy. Please wait a while and try again.
			public const string ReplyHeavyServerLoad = "263";
			// Current local users: curr Max: max
			public const string ReplyCurrentLocalUsers = "265";
			// Current global users: curr Max: max
			public const string ReplyCurrentGlobalUsers = "266";
	
			/// <summary>
			/// ERR_NOSUCHNICK
			/// "<nickname> :No such nick/channel"
			/// Used to indicate the nickname parameter supplied to a command is currently unused.
			/// </summary>
			public const string ErrorNoSuchNick = "401";
			/// <summary>
			/// ERR_NOSUCHSERVER
			/// "<server name> :No such server"
			/// Used to indicate the server name given currently doesn't exist.
			/// </summary>
			public const string ErrorNoSuchServer = "402";
			/// <summary>
			/// ERR_NOSUCHCHANNEL
			/// "<channel name> :No such channel"
			/// Used to indicate the given channel name is invalid.
			/// </summary>
			public const string ErrorNoSuchChannel = "403";
			/// <summary>
			/// ERR_CANNOTSENDTOCHAN
			/// "<channel name> :Cannot send to channel"
			/// Sent to a user who is either (a) not on a channel which is mode +n or (b) not a chanop (or mode +v) on a channel which has mode +m set and is trying to send a PRIVMSG message to that channel.
			/// </summary>
			public const string ErrorCannotSendToChannel = "404";
			/// <summary>
			/// ERR_TOOMANYCHANNELS
			/// "<channel name> :You have joined too many channels"
			/// Sent to a user when they have joined the maximum number of allowed channels and they try to join another channel.
			/// </summary>
			public const string ErrorTooManyChannels = "405";
			/// <summary>
			/// ERR_WASNOSUCHNICK
			/// "<nickname> :There was no such nickname"
			/// Returned by WHOWAS to indicate there is no history information for that nickname.
			/// </summary>
			public const string ErrorWasNoSuchNick = "406";
			/// ERR_TOOMANYTARGETS
			/// "<target> :Duplicate recipients. No message delivered"
			/// Returned to a client which is attempting to send PRIVMSG/NOTICE using the user@host destination format and for a user@host which has several occurrences.
			public const string ErrorTooManyTargets = "407";
			/// ERR_NOORIGIN
			/// ":No origin specified"
			/// PING or PONG message missing the originator parameter which is required since these commands must work without valid prefixes.
			public const string ErrorNoOrigin = "409";
			/// ERR_NORECIPIENT
			/// ":No recipient given (<command>)"
			public const string ErrorNoRecipient = "411";
			/// ERR_NOTEXTTOSEND
			/// ":No text to send"
			public const string ErrorNoTextToSend = "412";
			/// ERR_NOTOPLEVEL
			/// "<mask> :No toplevel domain specified"
			public const string ErrorNoTopLevel = "413";
			/// ERR_WILDTOPLEVEL
			/// "<mask> :Wildcard in toplevel domain"
			/// 412 - 414 are returned by PRIVMSG to indicate that the message wasn't delivered for some reason. ERR_NOTOPLEVEL and ERR_WILDTOPLEVEL are errors that are returned when an invalid use of "PRIVMSG $<server>" or "PRIVMSG #<host>" is attempted.
			public const string ErrorWildTopLevel = "414";
			/// ERR_UNKNOWNCOMMAND
			/// "<command> :Unknown command"
			/// Returned to a registered client to indicate that the command sent is unknown by the server.
			public const string ErrorUnknownCommand = "421";
			/// ERR_NOMOTD
			/// ":MOTD File is missing"
			/// Server's MOTD file could not be opened by the server.
			public const string ErrorNoMotd = "422";
			/// ERR_NOADMININFO
			/// "<server> :No administrative info available"
			/// Returned by a server in response to an ADMIN message when there is an error in finding the appropriate information.
			public const string ErrorNoAdminInfo = "423";
			/// ERR_FILEERROR
			/// ":File error doing <file op> on <file>"
			/// Generic error message used to report a failed file operation during the processing of a message.
			public const string ErrorFileError = "424";
			/// ERR_NONICKNAMEGIVEN
			/// ":No nickname given"
			/// Returned when a nickname parameter expected for a command and isn't found.
			public const string ErrorNoNicknameGiven = "431";
			/// ERR_ERRONEUSNICKNAME
			/// "<nick> :Erroneus nickname"
			/// Returned after receiving a NICK message which contains characters which do not fall in the defined set. See section x.x.x for details on valid nicknames.
			public const string ErrorErroneusNickname = "432";
			/// ERR_NICKNAMEINUSE
			/// "<nick> :Nickname is already in use"
			/// Returned when a NICK message is processed that results in an attempt to change to a currently existing nickname.
			public const string ErrorNicknameInUse = "433";
			/// ERR_NICKCOLLISION
			/// "<nick> :Nickname collision KILL"
			/// Returned by a server to a client when it detects a nickname collision (registered of a NICK that already exists by another server).
			public const string ErrorNickCollision = "436";
			/// ERR_USERNOTINCHANNEL
			/// "<nick> <channel> :They aren't on that channel"
			/// Returned by the server to indicate that the target user of the command is not on the given channel.
			public const string ErrorUserNotInChannel = "441";
			/// ERR_NOTONCHANNEL
			/// "<channel> :You're not on that channel"
			/// Returned by the server whenever a client tries to perform a channel effecting command for which the client isn't a member.
			public const string ErrorNotOnChannel = "442";
			/// ERR_USERONCHANNEL
			/// "<user> <channel> :is already on channel"
			/// Returned when a client tries to invite a user to a channel they are already on.
			public const string ErrorUserOnChannel = "443";
			/// ERR_NOLOGIN
			/// "<user> :User not logged in"
			/// Returned by the summon after a SUMMON command for a user was unable to be performed since they were not logged in.
			public const string ErrorNoLogin = "444";
			/// ERR_SUMMONDISABLED
			/// ":SUMMON has been disabled"
			/// Returned as a response to the SUMMON command. Must be returned by any server which does not implement it.
			public const string ErrorSummonDisabled = "445";
			/// ERR_USERSDISABLED
			/// ":USERS has been disabled"
			/// Returned as a response to the USERS command. Must be returned by any server which does not implement it.
			public const string ErrorUsersDisabled = "446";
			/// ERR_NOTREGISTERED
			/// ":You have not registered"
			/// Returned by the server to indicate that the client must be registered before the server will allow it to be parsed in detail.
			public const string ErrorNotRegistered = "451";
			/// ERR_NEEDMOREPARAMS
			/// "<command> :Not enough parameters"
			/// Returned by the server by numerous commands to indicate to the client that it didn't supply enough parameters.
			public const string ErrorNeedMoreParams = "461";
			/// ERR_ALREADYREGISTRED
			/// ":You may not reregister"
			/// Returned by the server to any link which tries to change part of the registered details (such as password or user details from second USER message).
			public const string ErrorAlreadyRegistered = "462";
			/// ERR_NOPERMFORHOST
			/// ":Your host isn't among the privileged"
			/// Returned to a client which attempts to register with a server which does not been setup to allow connections from the host the attempted connection is tried.
			public const string ErrorNoPermForHost = "463";
			/// ERR_PASSWDMISMATCH
			/// ":Password incorrect"
			/// Returned to indicate a failed attempt at registering a connection for which a password was required and was either not given or incorrect.
			public const string ErrorPasswdMismatch = "464";
			/// ERR_YOUREBANNEDCREEP
			/// ":You are banned from this server"
			/// Returned after an attempt to connect and register yourself with a server which has been setup to explicitly deny connections to you.
			public const string ErrorYouAreBanned = "465";
			/// ERR_KEYSET
			/// "<channel> :Channel key already set"
			public const string ErrorKeySet = "467";
			/// ERR_CHANNELISFULL
			/// "<channel> :Cannot join channel (+l)"
			public const string ErrorChannelIsFull = "471";
			/// ERR_UNKNOWNMODE
			/// "<char> :is unknown mode char to me"
			public const string ErrorUnknownMode = "472";
			/// ERR_INVITEONLYCHAN
			/// "<channel> :Cannot join channel (+i)"
			public const string ErrorInviteOnlyChannel = "473";
			/// ERR_BANNEDFROMCHAN
			/// "<channel> :Cannot join channel (+b)"
			public const string ErrorBannedFromChannel = "474";
			/// ERR_BADCHANNELKEY
			/// "<channel> :Cannot join channel (+k)"
			public const string ErrorBadChannelKey = "475";
			/// ERR_NOPRIVILEGES
			/// ":Permission Denied- You're not an IRC operator"
			/// Any command requiring operator privileges to operate must return this error to indicate the attempt was unsuccessful.
			public const string ErrorNoPrivileges = "481";
			/// ERR_CHANOPRIVSNEEDED
			/// "<channel> :You're not channel operator"
			/// Any command requiring 'chanop' privileges (such as MODE messages) must return this error if the client making the attempt is not a chanop on the specified channel.
			public const string ErrorChannelOpPrivsNeeded = "482";
			/// ERR_CANTKILLSERVER
			/// ":You cant kill a server!"
			/// Any attempts to use the KILL command on a server are to be refused and this error returned directly to the client.
			public const string ErrorCantKillServer = "483";
			/// ERR_NOOPERHOST
			/// ":No O-lines for your host"
			/// If a client sends an OPER message and the server has not been configured to allow connections from the client's host as an operator, this error must be returned.
			public const string ErrorNoOperHost = "491";
			/// ERR_UMODEUNKNOWNFLAG
			/// ":Unknown MODE flag"
			/// Returned by the server to indicate that a MODE message was sent with a nickname parameter and that the a mode flag sent was not recognized.
			public const string ErrorUModeUnknownFlag = "501";
			/// ERR_USERSDONTMATCH
			/// ":Cant change mode for other users"
			/// Error sent to any user trying to view or change the user mode for a user other than themselves.
			public const string ErrorUsersDontMatch = "502";
		}
	}
}
