/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Irc
{
	[Flags]
	public enum ChannelModeFlags
	{
		None = 0,
		
		//rfc 1459
		PrivateChannel = 1, //p
		SecretChannel = 2, //s
		InviteOnlyChannel = 4, //i
		OnlyOperChangeTopic = 8, //t
		NoExternalMessages = 16, //n
		Moderated = 32, //m
		UserLimit = 64, //l
		BanMask = 128, //b
		BanException = 256, //e
		ChannelKey = 512, //k
		
		//rfc 1459
		Voice = 1024, //v
		Operator = 2048, //o
		//rfc2811
		ChannelCreator = 4096, //O
		//non official
		HalfOperator = 8192, //h
		
		//rfc2811
		AnonymousChannel = 16384, //a
		QuietChannel = 32768, //q
		ServerReopChannel = 65536, //r
		InvitationMask = 131072 //I
	}
}
