/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Irc
{
	[Flags]
	public enum UserModeFlags
	{
		None = 0,
		//rfc 1459
		Invisible = 1, //i
		ReceiveServerNotices = 2, //s
		ReceiveWallops = 4, //w
		Operator = 8, //o
		
		//rfc 2812
		Away = 16, //a
		RestrictedConnection = 32, //r
		LocalOperator = 64 //O
	}
}
