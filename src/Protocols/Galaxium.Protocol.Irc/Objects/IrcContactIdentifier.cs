/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.Protocol.Irc
{
	public sealed class IrcContactIdentifier
	{
		private string nickname;
		private string hostname;
		
		public IrcContactIdentifier (string ident)
		{
			if (ident == null)
				throw new ArgumentNullException ("ident");
			
			ParseIdent (ident);
		}
		
		public string Nickname
		{
			get { return nickname; }
		}
		
		public string Hostname
		{
			get { return hostname; }
		}
		
		public bool IsServerIdentifier
		{
			get { return nickname == null; }
		}
		
		private void ParseIdent (string ident)
		{
			int index = ident.IndexOf ("!");
			if (index < 0) { //server message
				hostname = ident;
			} else { //contact message
				nickname = ident.Substring (0, index);
				hostname = ident.Substring (index + 1);
			}
		}
		
		public override string ToString ()
		{
			if (IsServerIdentifier)
				return hostname;
			else
				return String.Concat (nickname, "!", hostname);
		}

	}
}