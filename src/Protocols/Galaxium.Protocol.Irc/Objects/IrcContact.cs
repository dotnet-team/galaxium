/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.Protocol.Irc
{
	public sealed class IrcContact : AbstractContact
	{
		private string ident;
		
		private IrcConversation conversation;
		
		private UserModeFlags userMode;
		private ChannelModeFlags channelUserMode;
		private IrcStoredContact _storedContact;
		
		internal IrcStoredContact StoredContact
		{
			get { return _storedContact; }
			set { _storedContact = value; }
		}
		
		internal IrcContact (IrcSession session, string ident, string name)
			: base (session, name, name, IrcPresence.Online)
		{
			this.ident = ident;
		}
		
		public IrcContact (IrcSession session, IrcConversation conversation, string ident, string name)
			: base (session, Guid.NewGuid ().ToString (), name, IrcPresence.Offline)
		{
			this.conversation = conversation;
			this.ident = ident;
		}
		
		public IrcContact (IrcSession session, IrcConversation conversation, string ident, string name, IPresence presence)
			: base (session, Guid.NewGuid ().ToString (), name, presence)
		{
			this.conversation = conversation;
			this.ident = ident;
		}
		
		public IrcConversation Conversation
		{
			get { return conversation; }
			set { conversation = value; }
		}
		
		public string Ident
		{
			get { return ident; }
			set { ident = value; }
		}
		
		public UserModeFlags UserMode
		{
			get { return userMode; }
			set { userMode = value; }
		}
		
		public ChannelModeFlags ChannelUserMode
		{
			get { return channelUserMode; }
			set { channelUserMode = value; }
		}
		
		public bool HasUserModes (UserModeFlags flags)
		{
			return (userMode & flags) == flags;
		}
		
		public bool HasChannelUserModes (ChannelModeFlags flags)
		{
			return (channelUserMode & flags) == flags;
		}
	}
}