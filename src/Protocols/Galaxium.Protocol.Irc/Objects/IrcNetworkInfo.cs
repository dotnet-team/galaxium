/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;
using System.Collections.Generic;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Protocol.Irc
{
	[Serializable]
	public class IrcNetworkInfo : IComparable<IrcNetworkInfo>
	{
		private string name;
		private IrcServerInfo[] mirrors;
		
		public IrcNetworkInfo ()
		{
		}
		
		public IrcNetworkInfo (string name)
		{
			if (name == null)
				throw new ArgumentNullException ("name");
			
			this.name = name;
		}
		
		[XmlAttribute ("name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}
	
		//[XmlElement ("Mirrors")]
		public IrcServerInfo[] Mirrors
		{
			get { return mirrors; }
			set { mirrors = value; }
		}
	
		public int CompareTo (IrcNetworkInfo other)
		{
			return name.CompareTo (other.Name);
		}
	}
	
	public class IrcNetworkInfoNameComparer : IPropertyComparer<IrcNetworkInfo, string>
	{
		public int Compare (IrcNetworkInfo info, string name)
		{
			return info.Name.CompareTo (name);
		}
	}
}
