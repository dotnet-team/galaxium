/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Protocol.Irc
{
	public static class IrcPresence
	{
		private static IPresence _online;
		private static IPresence _offline;
		private static IPresence _away;

		public static IPresence Online
		{
			get
			{
				if (_online == null)
					_online = new OnlinePresence ();
				return _online;
			}
		}

		public static IPresence Offline
		{
			get
			{
				if (_offline == null)
					_offline = new OfflinePresence ();
				return _offline;
			}
		}
		
		public static IPresence Away
		{
			get
			{
				if (_away == null)
					_away = new AwayPresence ();
				return _away;
			}
		}
		
		public static bool IsValidIrcPresence (IPresence presence)
		{
			if (presence == null)
				return false;

			return presence.Equals (_online) || presence.Equals (_offline)
				|| presence.Equals (_away);
		}

		public static bool IsValidIrcPresence (string presence)
		{
			if (String.IsNullOrEmpty (presence))
				return false;

			return String.Compare (presence, "NLN", true) == 0
				|| String.Compare (presence, "AWY", true) == 0
				|| String.Compare (presence, "FLN", true) == 0;
		}
		
		public static IPresence ParseIrcPresence (string presence)
		{
			if (String.Compare (presence, "NLN", true) == 0) {
				return Online;
			} else if (String.Compare (presence, "AWY", true) == 0) {
				return Away;
			} else if (String.Compare (presence, "FLN", true) == 0) {
				return Offline;
			}
			
			return UnknownPresence.Instance;
		}
		
		public static string GetIrcPresenceCode (IPresence presence)
		{
			if (presence.Equals (IrcPresence.Online))
				return "NLN";
			else if (presence.Equals (IrcPresence.Away))
				return "AWY";
			else
				return "FLN";
		}
	}
	
	public sealed class AwayPresence : AbstractPresence
	{
		private string state;
		
		internal AwayPresence ()
		{
			_basePresence = BasePresence.Away;
			state = "Away";
		}
		
		public AwayPresence (string state)
			: this ()
		{
			ThrowUtility.ThrowIfEmpty ("state", state);
			
			this.state = state;
		}
		
		public override string State
		{
			get { return state; }
		}
	}
	
	public sealed class OnlinePresence : AbstractPresence
	{
		internal OnlinePresence ()
		{
			_basePresence = BasePresence.Online; 
		}
		
		public override string State
		{
			get { return "Online"; }
		}
	}
	
	public sealed class OfflinePresence : AbstractPresence
	{
		internal OfflinePresence ()
		{
			_basePresence = BasePresence.Offline; 
		}
		
		public override string State
		{
			get { return "Offline"; }
		}
	}
}