
using System;

namespace Galaxium.Protocol.Irc
{
	public class ServerEventArgs : EventArgs
	{
		private IrcMessage _message;
		private string _output;
		
		public IrcMessage Message
		{
			get { return _message; }
		}
		
		public string Output
		{
			get { return _output; }
		}
		
		public ServerEventArgs (IrcMessage message, string output)
		{
			_message = message;
			_output = output;
		}
	}
}
