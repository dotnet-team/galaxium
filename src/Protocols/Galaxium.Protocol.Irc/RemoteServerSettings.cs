/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

namespace Galaxium.Protocol.Irc
{
	public class RemoteServerSettings
	{
		private int maxChannels;
		private int maxBans;
		private int nickLength;
		private int topicLength;
		private int kickLength;
		
		private string networkName;
		private char[] channelTypes;
		private char[] channelModes;

		public RemoteServerSettings ()
		{
			maxChannels = 20;
			maxBans = 50;
			nickLength = 20;
			topicLength = 200;
			kickLength = 75;
			
			networkName = "Unknown";
			channelTypes = new char[] {'#','&'};
			channelModes = new char[] {'o','p','s','i','t','n','m','l','b','e','v','k'};
		}
		
		public void Parse (IrcMessage msg)
		{
			//for example
			//:underworld2.no.quakenet.org 005 tremtest WHOX WALLCHOPS WALLVOICES USERIP CPRIVMSG CNOTICE SILENCE=15 MODES=6 MAXCHANNELS=20 MAXBANS=45 NICKLEN=15 MAXNICKLEN=15 :are supported by this server
			//:underworld2.no.quakenet.org 005 tremtest TOPICLEN=250 AWAYLEN=160 KICKLEN=250 CHANTYPES=#& PREFIX=(ov)@+ CHANMODES=b,k,l,imnpstrDdcCNu CASEMAPPING=rfc1459 NETWORK=QuakeNet :are supported by this server
			
			foreach (string param in msg.Parameters) {
				string upper = param.ToUpper ();
				
				if (upper.StartsWith ("MAXCHANNELS")) {
					maxChannels = GetValue (param, maxChannels);
				} else if (upper.StartsWith ("MAXBANS")) {
					maxBans = GetValue (param, maxBans);
				} else if (upper.StartsWith ("NICKLEN") || upper.StartsWith ("MAXNICKLEN")) {
					nickLength = GetValue (param, nickLength);
				} else if (upper.StartsWith ("TOPICLEN")) {
					topicLength = GetValue (param, topicLength);
				} else if (upper.StartsWith ("AWAYLEN")) {
					//TODO:
				} else if (upper.StartsWith ("KICKLEN")) {
					kickLength = GetValue (param, kickLength);
				} else if (upper.StartsWith ("CHANTYPES")) {
					string types = GetValue (param);
					channelTypes = types.ToCharArray ();
				} else if (upper.StartsWith ("PREFIX")) {
					//TODO:
				} else if (upper.StartsWith ("CHANMODES")) {
					string modes = GetValue (param);
					List<char> chanModes = new List<char> ();
					foreach (char c in modes)
						if (c != ',')
							chanModes.Add (c);
					channelModes = chanModes.ToArray ();
				} else if (upper.StartsWith ("NETWORK")) {
					networkName = GetValue (param);
				}
			}
		}
		
		public int MaxChannels
		{
			get { return maxChannels; }
		}
		
		public int MaxBans
		{
			get { return maxBans; }
		}
		
		public int NickLength
		{
			get { return nickLength; }
		}
		
		public int TopicLength
		{
			get { return topicLength; }
		}
		
		public int KickLength
		{
			get { return kickLength; }
		}
		
		public string NetworkName
		{
			get { return networkName; }
		}
		
		public char[] ChannelTypes
		{
			get { return channelTypes; }
		}
		
		public char[] ChannelModes
		{
			get { return channelModes; }
		}

		private string GetValue (string param)
		{
			int index = param.IndexOf ("=");
			return param.Substring (index + 1);
		}
		
		private int GetValue (string param, int defaultValue)
		{
			string value = GetValue (param);
			
			int i;
			if (int.TryParse (value, out i))
				return i;
			return defaultValue;
		}
	}
}
