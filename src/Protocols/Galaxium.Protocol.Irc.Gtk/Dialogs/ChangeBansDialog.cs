/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2009 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public sealed class ChangeBansDialog
	{
		[Widget ("ChangeBansDialog")]
		private Dialog _dialog;
		[Widget ("WindowImage")]
		private Image _imgDialog;
		[Widget ("AddButton")]
		private Button _addButton;
		[Widget ("RemoveButton")]
		private Button _removeButton;
		[Widget ("MaskEntry")]
		private Entry _maskEntry;
		[Widget ("BanWindow")]
		private ScrolledWindow _banWindow;
		
		private Gtk.TreeView _banList;
		private ListStore _listStore;
		
		public ChangeBansDialog (IrcSession session, IrcChannel channel)
		{
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (ChangeBansDialog).Assembly, "SetBansDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-rename-channel", IconSizes.Large);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-set", IconSizes.Small);
			
			//_applyButton.Sensitive = CheckInput();
			
			_listStore = new ListStore (typeof (String));
			_banList = new TreeView (_listStore);
			
			_banWindow.Add(_banList);
			
			session.ChangeMode (channel.DisplayName, "+b", String.Empty);
			channel.ChannelBanListChanged += HandleChannelBanListChanged;
			_dialog.ShowAll();
		}

		void HandleChannelBanListChanged(object sender, ChannelEventArgs e)
		{
			_listStore.Clear ();
			
			foreach (string mask in e.Channel.BanMasks)
			{
				_listStore.AppendValues (mask);
			}
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		private void DataChangedEvent (object sender, EventArgs args)
		{
			//_applyButton.Sensitive = CheckInput();
		}
		
		private bool CheckInput()
		{
			//if (_topicEntry.Text.Length == 0)
				//return false;
			//else
				return true;
		}
	}
}