/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2009 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public sealed class ChangeChannelModeDialog
	{
		[Widget ("ChangeChannelModeDialog")]
		private Dialog _dialog;
		[Widget ("WindowImage")]
		private Image _imgDialog;
		[Widget ("ApplyButton")]
		private Button _applyButton;
		
		[Widget ("CheckModerated")]
		private CheckButton _checkModerated;
		[Widget ("CheckPrivate")]
		private CheckButton _checkPrivate;
		[Widget ("CheckSecret")]
		private CheckButton _checkSecret;
		[Widget ("CheckInviteOnly")]
		private CheckButton _checkInviteOnly;
		[Widget ("CheckInternalOnly")]
		private CheckButton _checkInternalOnly;
		[Widget ("CheckRestrictTopic")]
		private CheckButton _checkRestrictedTopic;
		
		public bool Private
		{
			get { return _checkPrivate.Active; }
		}
		
		public bool Moderated
		{
			get { return _checkModerated.Active; }
		}
		
		public bool Secret
		{
			get { return _checkSecret.Active; }
		}
		
		public bool InviteOnly
		{
			get { return _checkInviteOnly.Active; }
		}
		
		public bool InternalOnly
		{
			get { return _checkInternalOnly.Active; }
		}
		
		public bool RestrictTopic
		{
			get { return _checkRestrictedTopic.Active; }
		}
		
		public ChangeChannelModeDialog (IrcSession session, IrcChannel channel)
		{
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (ChangeChannelModeDialog).Assembly, "SetChannelModeDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-rename-channel", IconSizes.Large);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-set", IconSizes.Small);
			
			_applyButton.Sensitive = CheckInput();
			
			// Lets setup the checks to match the channel's current state
			_checkModerated.Active = channel.HasChannelModes (ChannelModeFlags.Moderated);
			_checkPrivate.Active = channel.HasChannelModes (ChannelModeFlags.PrivateChannel);
			_checkSecret.Active = channel.HasChannelModes (ChannelModeFlags.SecretChannel);
			_checkInviteOnly.Active = channel.HasChannelModes (ChannelModeFlags.InviteOnlyChannel);
			_checkInternalOnly.Active = channel.HasChannelModes (ChannelModeFlags.NoExternalMessages);
			_checkRestrictedTopic.Active = channel.HasChannelModes (ChannelModeFlags.OnlyOperChangeTopic);
			
			_dialog.ShowAll();
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		private void DataChangedEvent (object sender, EventArgs args)
		{
			_applyButton.Sensitive = CheckInput();
		}
		
		private bool CheckInput()
		{
			return true;
		}
	}
}