/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Client;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public class ContactTreeChannelDetailsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			
		}
	}
	
	public class ContactTreeChangePasswordCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			//IrcAccount account = channel.Session.Account as IrcAccount;
			
			ChangePasswordDialog dialog = new ChangePasswordDialog (channel.Session as IrcSession, channel);
			ResponseType response = (ResponseType)dialog.Run ();
			
			if (response == ResponseType.Apply)
			{
				channel.Key = dialog.Password;
				channel.Save ();
			}
			else if (response == ResponseType.No)
			{
				channel.Key = String.Empty;
				channel.Save ();
			}
			
			dialog.Destroy();
		}
	}
	
	public class ContactTreeRemoveChannelCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			IrcAccount account = channel.Session.Account as IrcAccount;
			
			account.Session.RemoveChannel (channel);
		}
	}
	
	public class ContactTreeAutoJoinCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			//IrcAccount account = channel.Session.Account as IrcAccount;
			
			(MenuItem as CheckMenuItem).Active = channel.AutoJoin;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			//IrcAccount account = channel.Session.Account as IrcAccount;
			
			channel.AutoJoin = (MenuItem as CheckMenuItem).Active;
			
		}
	}
	
	public class ContactTreePersistentCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			//IrcAccount account = channel.Session.Account as IrcAccount;
			
			
			(MenuItem as CheckMenuItem).Active = channel.Persistent;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			//IrcAccount account = channel.Session.Account as IrcAccount;
			
			channel.Persistent = (MenuItem as CheckMenuItem).Active;
			
		}
	}
	
	public class ContactTreeOpenChannelCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			IrcSession session = channel.Session as IrcSession;
			
			session.JoinChannel (channel.DisplayName, channel.Key, true, false);
			
			var conversation = session.ObtainConversation (channel, true);
			WindowUtility<Widget>.Activate(conversation, true);
		}
	}
	
	public class ContactTreeJoinChannelCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			
			(MenuItem as MenuItem).Sensitive = channel.Presence == IrcPresence.Offline;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			IrcSession session = channel.Session as IrcSession;
			
			session.JoinChannel (channel.DisplayName, channel.Key, channel.AutoJoin, channel.Persistent);
		}
	}
	
	public class ContactTreePartChannelCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			
			(MenuItem as MenuItem).Sensitive = channel.Presence == IrcPresence.Online;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcChannel channel = context.Value as IrcChannel;
			IrcSession session = channel.Session as IrcSession;
			
			session.PartChannels (channel.DisplayName);
		}
	}
}