/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol.Irc;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public class AddContactCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{			
		}
		
		public override void Run ()
		{
			IrcSession session = SessionUtility.ActiveSession as IrcSession;
			if (session == null)
				return;
			
			AddContactDialog dialog = new AddContactDialog (session);
			ResponseType response = (ResponseType)dialog.Run ();
			 
			if (response == ResponseType.Ok)
			{
				session.AddContact (dialog.Nickname, dialog.Ident);
			}
			
			dialog.Destroy ();
		}
	}
	
	public class AddChannelCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{			
		}
		
		public override void Run ()
		{
			IrcSession session = SessionUtility.ActiveSession as IrcSession;
			if (session == null)
				return;
			
			AddChannelDialog dialog = new AddChannelDialog (session);
			ResponseType response = (ResponseType)dialog.Run ();
			 
			if (response == ResponseType.Ok)
			{
				session.AddChannel (dialog.Channel, dialog.Key, dialog.AutoJoin, dialog.Persistent);
			}
			
			dialog.Destroy ();
		}
	}
	
	public class BrowseChannelsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{			
		}
		
		public override void Run ()
		{
			IrcSession session = SessionUtility.ActiveSession as IrcSession;
			if (session == null)
				return;
			
			BrowseChannelsDialog dialog = new BrowseChannelsDialog (session);
			
			bool working = true;
			
			while (working)
			{
				ResponseType response = (ResponseType)dialog.Run ();
				
				if (response == ResponseType.Ok)
				{
					session.JoinChannel (dialog.Channel, dialog.Key, dialog.AutoJoin, dialog.Persistent); //auto added to channel collection+event emitted
					
					IrcAccount account = session.Account as IrcAccount;
					IrcStoredChannel storedChannel = new IrcStoredChannel (dialog.Channel, dialog.Key, dialog.AutoJoin, dialog.Persistent);
					account.Channels.Add (storedChannel);
				}
				else if (response == ResponseType.Apply)
				{
					dialog.Refresh ();
				}
				else
					working = false;
			}
			
			dialog.Destroy ();
		}
	}
	
	public class ViewConsoleCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{			
		}
		
		public override void Run ()
		{
			IrcSessionWidget widget = IrcWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as IrcSession);
			
			if (widget != null)
			{
				widget.ConsoleWindow.Visible = true;
			}
		}
	}
	
	public class NetworkDetailsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{			
		}
		
		public override void Run ()
		{
			IrcSessionWidget widget = IrcWidgetUtility.GetSessionWidget (SessionUtility.ActiveSession as IrcSession);
			
			if (widget != null)
			{
				
			}
		}
	}
}