/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using GDK=Gdk;
using Glade;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Protocol.Irc;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public class IrcSessionWidget : BasicSessionWidget
	{
		private Button _buttonServer;
		private ServerConsoleWindow _serverConsole;
		
		internal ServerConsoleWindow ConsoleWindow
		{
			get { return _serverConsole; }
		}
		
		internal IrcSessionWidget (IControllerWindow<Widget> window, IrcSession session) : base (window, session)
		{
			IrcWidgetUtility.AddSessionWidget(session, this);
			
			_buttonServer = new Button ();
			_buttonServer.Image = new Image (IconUtility.GetIcon ("galaxium-server", IconSizes.Small));
			
			_tree_view.RowActivated += new RowActivatedHandler(TreeRowActivated);
			
			_serverConsole = new ServerConsoleWindow (session);
			_serverConsole.Visible = (session.Account as IrcAccount).ShowConsole;
			
			session.Disconnected += SessionDisconnected;
			session.ConversationInitiated += HandleConversationInitiated;
			session.Account.PresenceChange += AccountPresenceChanged;
			session.Account.DisplayMessageChange += AccountMessageChanged;
			session.Account.DisplayNameChange += AccountNameChanged;
		}

		private void HandleConversationInitiated(object sender, ConversationEventArgs e)
		{
			// We have to check to see if we already have a chat widget, otherwise open.
			OnActivateChatWidget (new ChatEventArgs (e.Conversation));
		}
		
		private void AccountNameChanged (object sender, EntityChangeEventArgs<string> args)
		{
			ThreadUtility.Dispatch (() => {	
				_display_name_entry.Text = args.New;
				_display_name_entry.QueueDraw ();
			});
		}
		
		private void AccountMessageChanged (object sender, EntityChangeEventArgs<string> args)
		{
			ThreadUtility.Dispatch (() => {	
				_display_message_entry.Text = args.New;
				_display_message_entry.QueueDraw ();
			});
		}
		
		private void AccountPresenceChanged (object sender, EntityChangeEventArgs<IPresence> args)
		{
			ThreadUtility.Dispatch (() => {	
				if (_status_combo.GetSelectedItem() != args.New)
					_status_combo.Select (args.New);
			});
		}
		
		public override void Initialize ()
		{
			OmitDisplayImage = true;
			
			base.Initialize ();
			
			// We need to setup a IRC tree manager for our tree
			_tree_view.Manager = new IrcContactTreeManager ();
			
			// We have to insert the protocol specific statuses
			_status_combo.Append (IrcPresence.Online);
			_status_combo.Append (IrcPresence.Away);
			_status_combo.Select (Session.Account.Presence);
			
			IrcAccount account = Account as IrcAccount;
			IrcSession session = _session as IrcSession;
			
			// auto join channels
			foreach (IrcStoredChannel channel in account.Channels)
			{
				if (channel.AutoJoin)
					session.JoinChannel (channel.Name, channel.Key, channel.AutoJoin, channel.Persistent);
			}
			
			// Make sure everything is positioned properly.
			Update ();
		}
		
		public override IConversation CreateUsableConversation (IContact contact)
		{
			IrcSession session = _session as IrcSession;
			IrcConversation conv = session.ObtainConversation (contact, true);
			
			// If its a private conversation to a individual, lets attempt dcc.
			if (conv.IsPrivateConversation)
				conv.ConnectDCC ();
			
			return conv;
		}
		
		protected override void SetDisplayImage ()
		{
			// We do not use display images in IRC
		}
		
		protected override string PresenceTextLookup (IPresence item)
		{
			return item.State;
		}
		
		protected override Gdk.Pixbuf PresenceImageLookup (IPresence item, IIconSize size)
		{
			return IconUtility.StatusLookup (item, size);
		}
		
		private void SessionDisconnected (object sender, SessionEventArgs args)
		{
			_serverConsole.Destroy();
		}
		
		public override void TreeRowActivated (object sender, RowActivatedArgs args)
		{
			base.TreeRowActivated (sender, args);
			
			IrcSession session = _session as IrcSession;
			IContact entity = _tree_view.GetModelData (args.Path) as IContact;
			
			if (entity is IrcChannel)
			{
				IrcChannel channel = entity as IrcChannel;
				
				if (channel != null && channel.Presence == IrcPresence.Offline)
					session.JoinChannel (channel);
			}
			else if (entity is IrcContact)
			{
				// We doubleclicked a contact, but do we open DCC chat? or regular?
			}
		}
	}
}