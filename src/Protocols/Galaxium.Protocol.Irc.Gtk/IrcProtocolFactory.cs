/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public class IrcProtocolFactory : AbstractProtocolFactory, IProtocolGuiFactory<Widget>
	{
		public IrcProtocolFactory () : base (IrcProtocol.Instance)
		{
			
		}
		
		public ISessionWidget<Widget> CreateSessionWidget (IControllerWindow<Widget> window, ISession session)
		{
			ThrowUtility.ThrowIfNull ("session", session);

			return new IrcSessionWidget (window, session as IrcSession);
		}
		
		public IAccountWidget<Widget> CreateAccountWidget ()
		{
			return new IrcAccountWidget ();
		}
		
		public IChatWidget<Widget> CreateChatWidget (IContainerWindow<Widget> window, IConversation conversation)
		{
			return new IrcChatWidget (window, conversation as IrcConversation);
		}
		
		public override ISession CreateSession (IAccount account)
		{
			return new IrcSession (account as IrcAccount);
		}

		public override IAccount LoadAccount (IConfigurationSection config)
		{
			IrcAccount account = new IrcAccount (config.Name);
			
			// General settings
			account.Password = config.GetString (Configuration.Account.Password.Name, Configuration.Account.Password.Default);
			account.DisplayName = config.GetString (Configuration.Account.DisplayName.Name, account.UniqueIdentifier);
			account.DisplayMessage = config.GetString (Configuration.Account.DisplayMessage.Name, Configuration.Account.DisplayMessage.Default);
			account.Nickname = config.GetString (Configuration.Account.Nickname.Name, Configuration.Account.Nickname.Default);
			account.InitialPresence = IrcPresence.ParseIrcPresence (config.GetString (Configuration.Account.InitialPresence.Name, Configuration.Account.InitialPresence.Default));
			account.AutoConnect = config.GetBool (Configuration.Account.AutoConnect.Name, Configuration.Account.AutoConnect.Default);
			account.RememberPassword = config.GetBool (Configuration.Account.RememberPassword.Name, Configuration.Account.RememberPassword.Default);
			
			account.ViewByGroup = config.GetBool (Configuration.Account.ViewByGroup.Name, Configuration.Account.ViewByGroup.Default);
			account.DetailLevel = (ContactTreeDetailLevel)config.GetInt (Configuration.Account.DetailLevel.Name, Configuration.Account.DetailLevel.Default);
			account.SortAlphabetic = config.GetBool (Configuration.Account.SortAlphabetic.Name, Configuration.Account.SortAlphabetic.Default);
			account.SortAscending = config.GetBool (Configuration.Account.SortAscending.Name, Configuration.Account.SortAscending.Default);
			account.ShowOfflineContacts = config.GetBool (Configuration.Account.ShowOfflineContacts.Name, Configuration.Account.ShowOfflineContacts.Default);
			account.ShowEmptyGroups = config.GetBool (Configuration.Account.ShowEmptyGroups.Name, Configuration.Account.ShowEmptyGroups.Default);
			account.ShowSearchBar = config.GetBool (Configuration.Account.ShowSearchBar.Name, Configuration.Account.ShowSearchBar.Default);
			account.GroupOfflineContacts = config.GetBool (Configuration.Account.GroupOfflineContacts.Name, Configuration.Account.GroupOfflineContacts.Default);
			account.ShowDisplayImages = config.GetBool (Configuration.Account.ShowContactImages.Name, Configuration.Account.ShowContactImages.Default);
			account.ShowDisplayNames = config.GetBool (Configuration.Account.ShowContactNames.Name, Configuration.Account.ShowContactNames.Default);
			account.ShowPersonalMessages = config.GetBool (Configuration.Account.ShowContactMessages.Name, Configuration.Account.ShowContactMessages.Default);
			account.ShowNicknames = config.GetBool (Configuration.Account.ShowContactNicknames.Name, Configuration.Account.ShowContactNicknames.Default);
			account.UseDefaultListView = config.GetBool (Configuration.Account.UseDefaultListView.Name, Configuration.Account.UseDefaultListView.Default);
			
			// IRC Specific stuff
			
			IrcConnectionInfo info = null;
			bool isNetwork = config.GetBool ("IsNetwork", false);
			
			if (isNetwork)
			{
				string network = config.GetString ("Network", String.Empty);
				info = new IrcConnectionInfo (network);
			}
			else
			{
				string hostName = config.GetString ("HostName", String.Empty);
				int port = config.GetInt ("Port", 6667);
				info = new IrcConnectionInfo (hostName, port);
			}
			account.ConnectionInfo = info;
			
			account.AddChannels (config.GetList<IrcStoredChannel> ("Channels"));
			account.AddContacts (config.GetList<IrcStoredContact> ("Contacts"));
			
			account.ShowConsole = config.GetBool ("ShowConsole", false);
			
			return account;
		}
		
		public override void SaveAccount (IConfigurationSection config, IAccount acc)
		{
			IrcAccount account = acc as IrcAccount;
			
			// General account settings
			config.SetString ("Account", account.UniqueIdentifier);
			config.SetString (Configuration.Account.Password.Name, account.RememberPassword ? account.Password : String.Empty);
			config.SetString (Configuration.Account.InitialPresence.Name, IrcPresence.GetIrcPresenceCode (account.InitialPresence));
			config.SetString (Configuration.Account.DisplayName.Name, account.DisplayName);
			config.SetString (Configuration.Account.DisplayMessage.Name, account.DisplayMessage);
			config.SetString (Configuration.Account.Nickname.Name, account.Nickname);
			config.SetBool (Configuration.Account.AutoConnect.Name, account.AutoConnect);
			config.SetBool (Configuration.Account.RememberPassword.Name, account.RememberPassword);
			
			config.SetBool (Configuration.Account.ViewByGroup.Name, account.ViewByGroup);
			config.SetInt (Configuration.Account.DetailLevel.Name, (int)account.DetailLevel);
			config.SetBool (Configuration.Account.SortAlphabetic.Name, account.SortAlphabetic);
			config.SetBool (Configuration.Account.SortAscending.Name, account.SortAscending);
			config.SetBool (Configuration.Account.ShowOfflineContacts.Name, account.ShowOfflineContacts);
			config.SetBool (Configuration.Account.ShowEmptyGroups.Name, account.ShowEmptyGroups);
			config.SetBool (Configuration.Account.ShowSearchBar.Name, account.ShowSearchBar);
			config.SetBool (Configuration.Account.GroupOfflineContacts.Name, account.GroupOfflineContacts);
			config.SetBool (Configuration.Account.ShowContactImages.Name, account.ShowDisplayImages);
			config.SetBool (Configuration.Account.ShowContactNames.Name, account.ShowDisplayNames);
			config.SetBool (Configuration.Account.ShowContactMessages.Name, account.ShowPersonalMessages);
			config.SetBool (Configuration.Account.ShowContactNicknames.Name, account.ShowNicknames);
			config.SetBool (Configuration.Account.UseDefaultListView.Name, account.UseDefaultListView);
			
			config.SetBool ("IsNetwork", account.ConnectionInfo.IsNetwork);
			if (account.ConnectionInfo.IsNetwork)
			{
				config.SetString ("Network", account.ConnectionInfo.Network);
			}
			else
			{
				config.SetString ("HostName", account.ConnectionInfo.HostName);
				config.SetInt ("Port", account.ConnectionInfo.Port);
			}
			
			config.SetList<IrcStoredContact> ("Contacts", account.Contacts);
			config.SetList<IrcStoredChannel> ("Channels", account.Channels);
			
			config.SetBool ("ShowConsole", account.ShowConsole);
		}
		
		public override IAccountCache CreateAccountCache (IAccount account, string directory)
		{
			return null;
		}
	}
}