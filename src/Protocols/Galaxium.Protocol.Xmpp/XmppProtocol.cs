// 
// XmppProtocol.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppProtocol: AbstractProtocol
	{
		private static IProtocol _instance = new XmppProtocol ();

		private XmppProtocol ()
		{
		}
		
		public static IProtocol Instance
		{
			get { return _instance; }
		}
		
		public override string Name
		{
			get { return "XMPP"; }
		}
		
		public override string Image
		{
			get { return "galaxium-jabber"; }
		}
		
		public override string Description
		{
			get { return "Jabber/XMPP Network"; }
		}
	}
}
