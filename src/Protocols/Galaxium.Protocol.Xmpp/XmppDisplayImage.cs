//  
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.IO;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppDisplayImage: IDisplayImage
	{
		public string Checksum { get; private set; }
		public byte[] ImageBuffer { get; private set; }
		
		public string Filename { get; set; }
		
		public XmppDisplayImage (byte[] data)
		{
			Checksum = Library.Utility.Cryptography.Sha1HexHash (data);
			ImageBuffer = data;
		}
		
		public XmppDisplayImage (string filename)
		{
			if (File.Exists (filename))
				ImageBuffer = File.ReadAllBytes (filename);
			else
				ImageBuffer = new byte[0];
			Checksum = Library.Utility.Cryptography.Sha1HexHash (ImageBuffer);
		}
	}
}
