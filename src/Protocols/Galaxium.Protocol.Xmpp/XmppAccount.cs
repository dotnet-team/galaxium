// 
// XmppAccount.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppAccount: AbstractAccount
	{
		public JabberID Jid { get; set; }
		public int ConnectPort { get; set; }
		public string ConnectServer { get; set; }

		public bool AutoPriority { get; set; }
		public sbyte Priority { get; set; }
		
	//	private bool _register = false;
	//	private bool _use_http = false;
		
		public override IProtocol Protocol {
			get { return XmppProtocol.Instance; }
		}

		public override IPresence Presence {
			get { return base.Presence; }
			set { SetPresence (value); }
		}

		public override string DisplayMessage {
			get { return base.DisplayMessage; }
			set { SetMessage (value); }
		}

		public new XmppSession Session {
			get { return base.Session as XmppSession; }
		}

		public XmppAccount (string uid, string password, string display_name,
		                    bool auto_connect, bool remember_password)
			:base (null, uid, password, display_name, auto_connect, remember_password)
		{
			Jid = new JabberID (uid);
		}

		public XmppAccount (string uid)
			:base (null, uid)
		{
			Jid = new JabberID (uid);
		}

		private void SetPresence (IPresence presence)
		{
			if (!(presence is XmppPresence))
				throw new ArgumentException ("Must be an XmppPresence.");
			if (base.Presence == presence) return;
			
			base.Presence = presence;
			if (Session != null && Session.Client != null
			    && Session.Client.ConnectionState == ConnectionState.Connected)
				SetStatus ();
		}

		private void SetMessage (string message)
		{
			if (base.DisplayMessage == message) return;
			base.DisplayMessage = message;
			if (Session != null && Session.Client != null
			    && Session.Client.ConnectionState == ConnectionState.Connected)
				SetStatus ();
		}

		internal void SetStatus ()
		{
			Session.SetStatus ((Presence as XmppPresence).StatusID, DisplayMessage);
		}
	}
}
