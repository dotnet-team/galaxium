// 
//  Copyright © 2008, 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 

using System;

namespace Galaxium.Protocol.Xmpp.Library
{
	[Serializable]
	public class LoginCancelledException: ApplicationException
	{
	}
	
	[Serializable]
	public class LoginException: ApplicationException
	{
		public LoginException (string message): base (message)
		{
		}
	}

	[Serializable]
	public class ConnectionException: ApplicationException
	{
		public ConnectionException (string message): base (message)
		{
		}
	}

	[Serializable]
	public class TlsFailureException: ApplicationException
	{
		public TlsFailureException ()
			:base ()
		{
		}

		public TlsFailureException (Exception inner_exception)
			:base (null, inner_exception)
		{
		}
	}

	public class InternalErrorException: ApplicationException
	{
		public InternalErrorException ()
			:base ()
		{
		}
		
		public InternalErrorException (string message)
			:base (message)
		{
		}
	}
}