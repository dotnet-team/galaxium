// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 



using System;

namespace Galaxium.Protocol.Xmpp.Library.Utility
{
	internal static class IdGen
	{
		private static string m_uniq;
		private static int m_counter = -1;
		
		static IdGen ()
		{
			// this is technicaly just for case server decides to send a response
			// to some long forgotten stanza sent in one of previous sessions
			m_uniq = StringUtils.Random (4);
		}
		
		public static string GetID ()
		{
			m_counter ++;
			return m_uniq + m_counter.ToString ();
		}
	}
}
