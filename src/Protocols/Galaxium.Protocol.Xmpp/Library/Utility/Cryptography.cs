// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 



using System;
using System.Text;
using System.Security.Cryptography;

namespace Galaxium.Protocol.Xmpp.Library.Utility
{
	public static class Cryptography
	{
		public static string BytesToHexString (byte[] bytes)
		{
			var sb = new StringBuilder ();
			for (int i = 0; i < bytes.Length; i++)
				sb.AppendFormat ("{0:x2}", bytes [i]);
			return sb.ToString ();
		}
		
		public static string Md5HexHash (byte[] bytes)
		{
			return BytesToHexString (Md5Hash (bytes));
		}			
		
		public static string Md5HexHash (string str)
		{
			return Md5HexHash (Encoding.UTF8.GetBytes (str));
		}
		
		public static byte[] Md5Hash (byte[] bytes)
		{
			MD5 md5 = MD5CryptoServiceProvider.Create ();
			return md5.ComputeHash (bytes);
		}			
		
		public static byte[] Md5Hash (string str)
		{
			return Md5Hash (Encoding.UTF8.GetBytes (str));
		}
		
		public static string Sha1HexHash (byte[] data)
		{
			return BytesToHexString (SHA1.Create ().ComputeHash (data));
		}
		
		public static string Base64Encode (string str)
		{
			return Convert.ToBase64String (Encoding.UTF8.GetBytes (str));
		}
		
		public static string Base64Decode (string str)
		{
			return System.Text.Encoding.UTF8.GetString (Convert.FromBase64String (str));
		}
	}
}