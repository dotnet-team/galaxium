// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;

using Galaxium.Protocol.Xmpp.Library.Extensions.Disco;

namespace Galaxium.Protocol.Xmpp.Library.Extensions
{
	public class ServiceIdentity
	{
		public Service Service { get; private set; }
		
		public string Category { get; private set; }
		public string Type     { get; private set; }
		public string Name     { get; private set; }
		
		public ServiceIdentity (Service service, Identity identity)
		{
			Service = service;
			Category = identity.Category;
			Type = identity.Type;
			Name = identity.Name;
		}
	}
}
