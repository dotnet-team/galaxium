// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;

using Galaxium.Protocol.Xmpp.Library.Xml;

namespace Galaxium.Protocol.Xmpp.Library.Extensions
{
	public class vCardAddress
	{
		public vCardAddress ()
		{
		}
		
		public vCardAddress (Element elm)
		{
			if (elm.Name != "ADR")
				throw new ArgumentException ();
			
			IsHome = elm.FirstChild ("HOME", null) != null;
			IsWork = elm.FirstChild ("WORK", null) != null;
			IsDomestic = elm.FirstChild ("DOM", null) != null;
			IsInternational = elm.FirstChild ("INTL", null) != null;
			IsPostal = elm.FirstChild ("POSTAL", null) != null;
			IsParcel = elm.FirstChild ("PARCEL", null) != null;
			IsPreferred = elm.FirstChild ("PREF", null) != null;
			
			POBox = elm.GetTextChild ("POBOX", null);
			ExtendedAddress = elm.GetTextChild ("EXTADD", null);
			Street = elm.GetTextChild ("STREET", null);
			Locality = elm.GetTextChild ("LOCALITY", null);
			Region = elm.GetTextChild ("REGION", null);
			PostCode = elm.GetTextChild ("PCODE", null);
			Country = elm.GetTextChild ("CTRY", null);
		}
		
		public bool IsHome { get; set; }
		public bool IsWork { get; set; }
		public bool IsDomestic { get; set; }
		public bool IsInternational { get; set; }
		public bool IsPostal { get; set; }
		public bool IsParcel { get; set; }
		public bool IsPreferred { get; set; }
		
		public string POBox { get; set; }
		public string ExtendedAddress { get; set; }
		public string Street { get; set; }
		public string Locality { get; set; }
		public string Region { get; set; }
		public string PostCode { get; set; }
		public string Country { get; set; }
	
		public Element ToXml ()
		{
			var e = new Element ("ADR");
			
			if (IsHome) e.AppendChild (new Element ("HOME"));
			if (IsWork) e.AppendChild (new Element ("WORK"));
			if (IsDomestic) e.AppendChild (new Element ("DOM"));
			if (IsInternational) e.AppendChild (new Element ("INTL"));
			if (IsPostal) e.AppendChild (new Element ("POSTAL"));
			if (IsParcel) e.AppendChild (new Element ("PARCEL"));
			if (IsPreferred) e.AppendChild (new Element ("PREF"));
			
			if (!String.IsNullOrEmpty (POBox))
				e.AppendTextChild ("POBOX", null, POBox);
			if (!String.IsNullOrEmpty (ExtendedAddress))
				e.AppendTextChild ("EXTADD", null, ExtendedAddress);
			if (!String.IsNullOrEmpty (Street))
				e.AppendTextChild ("STREET", null, Street);
			if (!String.IsNullOrEmpty (Locality))
				e.AppendTextChild ("LOCALITY", null, Locality);
			if (!String.IsNullOrEmpty (Region))
				e.AppendTextChild ("REGION", null, Region);
			if (!String.IsNullOrEmpty (PostCode))
				e.AppendTextChild ("PCODE", null, PostCode);
			if (!String.IsNullOrEmpty (Country))
				e.AppendTextChild ("CTRY", null, Country);
			
			return e;
		}
	}
}
