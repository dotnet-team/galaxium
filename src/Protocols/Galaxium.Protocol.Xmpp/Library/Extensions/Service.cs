// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;
using System.Collections.Generic;

using Galaxium.Protocol.Xmpp.Library.Core;
using Galaxium.Protocol.Xmpp.Library.Extensions.Disco;

namespace Galaxium.Protocol.Xmpp.Library.Extensions
{
	public class Service
	{
		public bool CanRegister  { get; private set; }
		public bool IsRegistered { get; private set; }
		
		public JabberID Jid  { get; private set; }
		public string   Node { get; private set; }
		
		public IEnumerable<ServiceIdentity> Identities { get; private set; }
		public IEnumerable<string> Features { get; private set; }
		
		public Service (JabberID jid, string node, Info info, bool registered)
		{
			Jid = jid;
			Node = node;
			
			var identities = new List<ServiceIdentity> ();
			foreach (var identity in info.Identities)
				identities.Add (new ServiceIdentity (this, identity));
			Identities = identities;
			
			Features = info.Features;
			
			CanRegister = info.Supports (Namespaces.Registration);
			IsRegistered = registered;
		}
	}
}
