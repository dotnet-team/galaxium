// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;
using System.Collections.Generic;

using Galaxium.Protocol.Xmpp.Library.Xml;

namespace Galaxium.Protocol.Xmpp.Library.Extensions
{
	public class SearchResultForm
	{
		public SearchResultForm ()
		{
			Items = new List<SearchResultItem> ();
		}
		
		public SearchResultForm (Element source)
		{
			Items = new List<SearchResultItem> ();
			
			Title = source.GetTextChild ("title", null);
			Header = new SearchResultHeader (source.FirstChild ("reported", null));
			
			foreach (var child in source.EachChild ("item"))
				Items.Add (new SearchResultItem (child, Header));
		}
		
		public string Title { get; set; }
		
		public SearchResultHeader Header { get; set; }
		public List<SearchResultItem> Items { get; private set; }

		public Element ToXml ()
		{
			if (Header == null)
				throw new InvalidOperationException ("Table header is not present.");
				
			var x = new Element ("x", Namespaces.DataForms);
			x ["type"] = "result";
			x.AppendTextChild ("title", null, Title);
			
			x.AppendChild (Header.ToXml ());
			foreach (var item in Items)
				x.AppendChild (item.ToXml ());
			
			return x;
		}
	}
}
