// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;
using System.Collections;
using System.Collections.Generic;

using Galaxium.Protocol.Xmpp.Library.Xml;

namespace Galaxium.Protocol.Xmpp.Library.Extensions
{	
	public class SearchResultHeader: IEnumerable<DataField>
	{
		public SearchResultHeader ()
		{
			_fields = new List<DataField> ();
		}
		
		public SearchResultHeader (Element source)
		{
			if (source.Name != "reported")
				throw new ArgumentException ();
			
			foreach (var field in source) {
				if (field.Name != "field") continue;
				_fields.Add (new DataField (field));
			}
		}
		
		public List<DataField> _fields;
		
		public void AddField (string name, DataFieldType type, string label, string description)
		{
			var field = new DataField (name, type);
			field.Label = label;
			field.Description = description;
			_fields.Add (field);
		}
		
		public Element ToXml ()
		{
			var e = new Element ("reported");
			
			foreach (var field in _fields)
				e.AppendChild (field.ToXml (false));
			
			return e;
		}
		
		public IEnumerator<DataField> GetEnumerator ()
		{
			return _fields.GetEnumerator ();
		}
		
		IEnumerator IEnumerable.GetEnumerator ()
		{
			return GetEnumerator ();
		}
	}
}
