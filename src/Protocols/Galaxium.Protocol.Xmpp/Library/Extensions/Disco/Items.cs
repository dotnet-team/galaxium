// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;
using System.Collections.Generic;

using Galaxium.Protocol.Xmpp.Library.Core;
using Galaxium.Protocol.Xmpp.Library.Xml;
using Galaxium.Protocol.Xmpp.Library.Utility;

namespace Galaxium.Protocol.Xmpp.Library.Extensions.Disco
{
	public class Items: List<Item>
	{
		public Items (): base ()
		{
		}
		
		public Items (Element query): base ()
		{
			foreach (Element elm in query) {
				if (elm.Name != "item") continue;
				Add (new Item (elm ["jid"], elm ["node"], elm ["name"]));
			}
		}
		
		public static Iq CreateRequest (JabberID jid, string node)
		{
			var iq = new Iq (IqType.Get, Namespaces.DiscoItems);
			iq.To = jid;
			if (node != null)
				iq.Query ["node"] = node;
			return iq;
		}
		
		public Iq CreateResult (JabberID jid, string node, string id)
		{
			var iq = new Iq (IqType.Result, Namespaces.DiscoItems);
			iq.To = jid;
			iq.ID = id;
						
			foreach (var item in this) {
				var elm = new Element ("item");
				elm ["jid"] = item.Jid;
				elm ["node"] = item.Node;
				elm ["name"] = item.Name;
				iq.Query.AppendChild (elm);
			}
			
			return iq;
		}
	}
}
