// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 

using System;
using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp.Library
{
	public class TextEventArgs: EventArgs
	{
		public string Text { get; private set; }
		public TextEventArgs (string text) { Text = text; }
	}
	
	public class ContactPresenceEventArgs: EventArgs
	{
		public Contact Contact { get; private set; }
		public Presence Presence { get; private set; }
		
		public ContactPresenceEventArgs (Contact contact, Presence presence)
		{
			Contact = contact;
			Presence = presence;
		}
	}
	
	public class SubscriptionRequestEventArgs: EventArgs
	{
		public Contact Contact { get; private set; }
		public string Message { get; private set; }
		
		public SubscriptionRequestEventArgs (Contact contact, string message)
		{
			Contact = contact;
			Message = message;
		}
	}
	
	public class SubscriptionUpdateEventArgs: EventArgs
	{
		public Contact Contact { get; private set; }
		public SubscriptionState OldState { get; private set; }
		
		public SubscriptionUpdateEventArgs (Contact contact, SubscriptionState old_state)
		{
			Contact = contact;
			OldState = old_state;
		}
	}
	
	public class ContactGroupEventArgs: EventArgs
	{
		public Contact Contact { get; private set; }
		public string Group { get; private set; }
		
		public ContactGroupEventArgs (Contact contact, string group)
		{
			Contact = contact;
			Group = group;
		}
	}
	
	public class ContactNameEventArgs: EventArgs
	{
		public Contact Contact { get; private set; }
		public string OldName { get; private set; }
		
		public ContactNameEventArgs (Contact contact, string old_name)
		{
			Contact = contact;
			OldName = old_name;
		}
	}
	
	public class StatusChangeEventArgs: EventArgs
	{
		public Status Status { get; private set; }
		public string Description { get; private set; }
		public sbyte Priority { get; private set; }
		
		public StatusChangeEventArgs (Status status, string description, sbyte priority)
		{
			Status = status;
			Description = description;
			Priority = priority;
		}
	}
	
	public class AvatarChangeEventArgs: EventArgs
	{
		public Contact Contact { get; private set; }
		public string Type { get; private set; }
		public byte[] Data { get; private set; }
		
		public AvatarChangeEventArgs (Contact contact, string type, byte[] data)
		{
			Contact = contact;
			Type = type;
			Data = data;
		}
	}
}