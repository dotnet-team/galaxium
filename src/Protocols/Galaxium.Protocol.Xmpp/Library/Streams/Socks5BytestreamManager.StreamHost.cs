// 
//   Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//  
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as
//   published by the Free Software Foundation, either version 3 of the
//   License, or (at your option) any later version.
//  
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//  
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  
//

using System;
using Galaxium.Protocol.Xmpp.Library.Xml;

namespace Galaxium.Protocol.Xmpp.Library.Streams
{
	public partial class Socks5BytestreamManager: IBytestreamManager
	{
		private class StreamHost {
			public JabberID UID { get; private set; }
			public string Address { get; private set; }
			public ushort Port { get; private set; }
			public string ZeroConf { get; private set; }
			
			public StreamHost (Element elm)
			{
				UID = (JabberID) elm ["jid"];
				Address = elm ["host"];
				try { Port = UInt16.Parse (elm ["port"]); }
				catch { Port = 0; }
				ZeroConf = elm ["zeroconf"];
			}
			
			public StreamHost (JabberID uid, string addr, ushort port, string zeroconf)
			{
				UID = uid;
				Address = addr;
				Port = port;
				ZeroConf = zeroconf;
			}
			
			public Element ToXml ()
			{
				var e = new Element ("streamhost");
				e ["jid"] = UID;
				e ["host"] = Address;
				if (Port != 0)
					e ["port"] = Port.ToString ();
				e ["zeroconf"] = ZeroConf;
				return e;
			}
		}
	}
}