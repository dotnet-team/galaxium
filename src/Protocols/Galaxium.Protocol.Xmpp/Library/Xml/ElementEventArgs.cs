
using System;

namespace Galaxium.Protocol.Xmpp.Library.Xml
{
	public class ElementEventArgs: EventArgs
	{
		public Element Element { get; private set; }
		
		public ElementEventArgs (Element element)
		{
			Element = element;
		}
	}
}
