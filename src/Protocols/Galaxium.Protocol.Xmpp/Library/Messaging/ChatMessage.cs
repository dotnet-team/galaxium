// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 

using System;
using Galaxium.Protocol.Xmpp.Library.Xml;
using Galaxium.Protocol.Xmpp.Library.Core;
using Galaxium.Protocol.Xmpp.Library.Messaging;

namespace Galaxium.Protocol.Xmpp.Library
{
	public class ChatMessage: XmlMessage
	{
		public ChatState ChatState {
			get { return GetChatState (); }
		}
		
		public ChatMessage (JabberID jid, string thread, string text)
			:base (MsgType.Chat, jid, thread, null, text)
		{
		}
		
		public ChatMessage ()
			:this (null, null, null)
		{
		}
		
		internal ChatMessage (Element message)
			:base (message)
		{
		}
		
		private ChatState GetChatState ()
		{
			try {
				var state_elm = FirstChild (null, Namespaces.ChatStates);
				return (ChatState) Enum.Parse (typeof (ChatState), state_elm.Name, true);
			}
			catch {
				return ChatState.Unknown;
			}
		}
		
		public void AppendChatState (ChatState state)
		{
			AppendChild (new Element (state.ToString ().ToLower (), Namespaces.ChatStates));
		}
	}
}
