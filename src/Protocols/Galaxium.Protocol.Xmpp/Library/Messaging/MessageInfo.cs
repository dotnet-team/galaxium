// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 



using System;
using System.Collections.Generic;
using Galaxium.Protocol.Xmpp.Library.Core;
using Galaxium.Protocol.Xmpp.Library.Xml;
using Galaxium.Protocol.Xmpp.Library.Utility;

namespace Galaxium.Protocol.Xmpp.Library.Messaging
{
	// TODO: allow multiple recipients
	public class MessageInfo
	{
		public DateTime    Time         { get; private set; }
		public MsgType     Type         { get; private set; }
		public string      Thread       { get; private set; }
		public string      Subject      { get; private set; }
		public JabberID    Source       { get; private set; }
		public JabberID    Destination  { get; private set; }
		public JabberID    Client       { get; private set; }
		public JabberID    Contact      { get; private set; }
		public string      Body         { get; private set; }
		public bool        IsOutgoing   { get; private set; }
		public Element[]   Extensions   { get; private set; }
		public StanzaError Error        { get; private set; }
		public bool        IsUnread     { get;         set; }
		
		public MessageInfo (XmlMessage message, bool outgoing)
		{
			Time = DateTime.Now;
			Type = message.Type;
			Subject = message.Subject;
			Body = message.Body;
			Thread = message.Thread ?? String.Empty;
			Error = message.Error;
			IsOutgoing = outgoing;
			
			Source = message.From;
			Destination = message.To;
			Client  = IsOutgoing ? Source : Destination;
			Contact = IsOutgoing ? Destination : Source;
			
			IsUnread = !IsOutgoing;
			
			var exts = new List<Element> ();
			foreach (Element elm in message) {
				if (elm.IsAnonymous
				    || String.IsNullOrEmpty (elm.Namespace)
				    || elm.Namespace == Namespaces.Client)
					continue;
				exts.Add (elm.Clone ());
			}
			Extensions = exts.ToArray ();
		}
	}
}
