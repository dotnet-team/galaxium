// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 

using System;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Extensions;
using Galaxium.Protocol.Xmpp.Library.Extensions.Disco;

namespace Galaxium.Protocol.Xmpp
{	
	public class RoomInfo
	{
		Info _info;
		
		public bool SupportsMucRegisterForm {
			get { return _info.Supports ("http://jabber.org/protocol/muc#register"); }
		}
		
		public bool SupportsMucRoomConfigForm {
			get { return _info.Supports ("http://jabber.org/protocol/muc#roomconfig"); }
		}
		
		public bool SupportsMucRoomInfoForm {
			get { return _info.Supports ("http://jabber.org/protocol/muc#roominfo"); }
		}
		
		public bool IsPublic {
			get { return _info.Supports ("muc_public"); }
			// !Supports ("muc_hidden");
		}
		
		public bool IsMembersOnly {
			get { return _info.Supports ("muc_membersonly"); }
			// !Supports ("muc_open");
		}
		
		public bool IsModerated {
			get { return _info.Supports ("muc_moderated"); }
			// !Supports ("muc_unmoderated"); }
		}
		
		public bool IsNonAnonymous {
			get { return _info.Supports ("muc_nonanonymous"); }
		}
		
		public bool IsSemiAnonymous {
			get { return _info.Supports ("muc_semianonymous"); }
		}
		
		public bool IsPasswordProtected {
			get { return _info.Supports ("muc_passwordprotected"); }
			// !Supports ("muc_unsecured")
		}
		
		public bool IsPersistent {
			get { return _info.Supports ("muc_persistent"); }
			// !Supports ("muc_temporary");
		}
		
		private DataField GetField (string field)
		{
			var form = _info.GetForm ("http://jabber.org/protocol/muc#roominfo");
			if (form == null) return null;
			if (form.ContainsField (field))
				return form.GetField (field);
			else return null;
		}
		
		public IEnumerable<JabberID> ContactIDs {
			get {
				var field = GetField ("muc#roominfo_contactjid");
				if (field == null) return new JabberID [0];
				return field.GetJids ();
			}
		}
		
		public string Description {
			get {
				var field = GetField ("muc#roominfo_description");
				if (field == null) return null;
				return field.GetString ();
			}
		}
		
		public string Language {
			get {
				var field = GetField ("muc#roominfo_lang");
				if (field == null) return null;
				return field.GetString ();
			}
		}
		
		public string LdapGroup {
			get {
				var field = GetField ("muc#roominfo_ldapgroup");
				if (field == null) return null;
				return field.GetString ();
			}
		}
		
		public string LogUrl {
			get {
				var field = GetField ("muc#roominfo_logs");
				if (field == null) return null;
				return field.GetString ();
			}
		}
		
		public int? Occupants {
			get {
				var field = GetField ("muc#roominfo_occupants");
				if (field == null) return null;
				try { return Int32.Parse (field.GetString ()); }
				catch { return null; }
			}
		}
		
		public string Subject {
			get {
				var field = GetField ("muc#roominfo_subject");
				if (field == null) return null;
				return field.GetString ();
			}
		}
		
		public bool? SubjectIsModifiable {
			get {
				var field = GetField ("muc#roominfo_subjectmod");
				if (field == null) return null;
				return field.GetBool ();
			}
		}
		
		public RoomInfo (Info info)
		{
			ThrowUtility.ThrowIfNull ("info", info);
			_info = info;
		}
	}
}
