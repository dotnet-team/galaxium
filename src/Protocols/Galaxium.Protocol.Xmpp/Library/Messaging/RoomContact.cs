// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;

using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Messaging;
using Galaxium.Protocol.Xmpp.Library.Extensions;

namespace Galaxium.Protocol.Xmpp
{
	public class RoomContact
	{
		public JabberID RoomUID { get; internal set; }
		public JabberID UID { get; internal set; }
		public string Nickname {
			get { return RoomUID.Resource; }
		}
		
		public RoomAffiliation Affiliation { get; internal set; }
		public RoomRole Role { get; internal set; }
		
		public ChatState ChatState { get; internal set; }
		
		public Status Status { get; internal set; }
		public string StatusDescription { get; internal set; }
		
		public bool IsSelfContact { get; internal set; }
		
		internal RoomContact ()
		{
		}
		
		public vCard GetVCard ()
		{
			throw new NotImplementedException ();
		}
	}
}
