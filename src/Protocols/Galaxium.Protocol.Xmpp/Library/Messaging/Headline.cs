
using System;
using Galaxium.Protocol.Xmpp.Library.Xml;
using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp.Library
{
	public class Headline: XmlMessage
	{
		internal Headline (Element message)
			:base (message)
		{
		}
	}
}
