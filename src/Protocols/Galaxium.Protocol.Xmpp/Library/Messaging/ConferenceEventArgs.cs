// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;

using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp.Library
{
	public class JoinEventArgs: EventArgs
	{
		public string Nickname { get; private set; }
		public bool IsAssignedNickname { get; private set; }
		public bool RoomIsLogged { get; private set; }
		public bool RoomIsNonAnonymous { get; private set; }
		public bool RoomWasCreated { get; private set; }
		
		public JoinEventArgs (string nickname, bool assigned, bool logged,
		                      bool non_anonymous, bool room_created)
		{
			Nickname = nickname;
			IsAssignedNickname = assigned;
			RoomIsLogged = logged;
			RoomIsNonAnonymous = non_anonymous;
			RoomWasCreated = room_created;
		}
	}
	
	public enum JoinFailureReason {
		NoNickname, NicknameConflict, InvalidPassword, MembersOnly, Banned,
		OccupantNumberLimit, LockedRoom, QueryFailed, Cancelled, Other
	}
	
	public class JoinFailedEventArgs: EventArgs
	{
		public JoinFailureReason Reason { get; private set; }
		public StanzaError Error { get; private set; }
		
		public JoinFailedEventArgs (JoinFailureReason reason, StanzaError error)
		{
			Reason = reason;
			Error = error;
		}
	}
	
	public enum MucExitReason {
		Left, Kicked, Banned, AffiliationChange, IsntMember, SystemShutdown
	}
	
	public class MucExitEventArgs: EventArgs
	{
		public MucExitReason Reason { get; private set; }
		public JabberID Actor { get; private set; }
		public string ActorsReason { get; private set; }
		
		public MucExitEventArgs (MucExitReason reason, JabberID actor, string act_reason)
		{
			Reason = reason;
			Actor = actor;
			ActorsReason = act_reason;
		}
	}
	
	public class MucMessageEventArgs: EventArgs
	{
		public JabberID UID { get; private set; }
		public string Nickname { get; private set; }
		public string Thread { get; private set; }
		public string Body { get; private set; }
		public DateTime TimeStamp { get; private set; }
		public bool IsHistory { get; private set; }
		public bool IsPrivate { get; private set; }
		
		public MucMessageEventArgs (string nickname, JabberID source, DateTime timestamp,
		                            string thread, string body, bool is_history, bool is_private)
		{
			Nickname = nickname;
			UID = source;
			TimeStamp = timestamp;
			Thread = thread;
			Body = body;
			IsHistory = is_history;
			IsPrivate = is_private;
		}
	}
	
	public class MucErrorEventArgs: EventArgs
	{
		public string Text { get; private set; }
		public MucErrorEventArgs (string text) { Text = text; }
	}
	
	public class MucSubjectEventArgs: EventArgs
	{
		public JabberID UID { get; private set; }
		public string Nickname { get; private set; }
		public DateTime TimeStamp { get; private set; }
		public string Subject { get; private set; }
		
		public MucSubjectEventArgs (string nickname, JabberID uid, DateTime stamp, string subject)
		{
			Nickname = nickname;
			UID = uid;
			TimeStamp = stamp;
			Subject = subject;
		}
	}
	
	public class NicknameRequestEventArgs: EventArgs
	{
		public string Nickname { get; set; }
		public NicknameRequestEventArgs () {}
	}
		
	public class PasswordRequestEventArgs: EventArgs
	{
		public string Password { get; set; }
		public PasswordRequestEventArgs () {}
	}
	
	public class RoomContactEventArgs: EventArgs
	{
		public RoomContact Contact { get; private set; }
		public RoomContactEventArgs (RoomContact contact) { Contact = contact; }
	}
	
	public class RoomContactNicknameEventArgs: EventArgs
	{
		public RoomContact Contact { get; private set; }
		public string OldNickname { get; private set; }
		
		public RoomContactNicknameEventArgs (RoomContact contact, string old_nickname)
		{
			Contact = contact;
			OldNickname = old_nickname;
		}
	}
}