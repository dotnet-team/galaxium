// 
//  Copyright © 2008, 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 

using System;

namespace Galaxium.Protocol.Xmpp.Library
{
	public static class Namespaces
	{
		public const string Stream  = "http://etherx.jabber.org/streams";
		public const string Client  = "jabber:client";
		public const string Tls     = "urn:ietf:params:xml:ns:xmpp-tls";
		public const string Bind    = "urn:ietf:params:xml:ns:xmpp-bind";
		public const string Sasl    = "urn:ietf:params:xml:ns:xmpp-sasl";
		public const string Session = "urn:ietf:params:xml:ns:xmpp-session";
		public const string Stanzas = "urn:ietf:params:xml:ns:xmpp-stanzas";
		public const string Roster  = "jabber:iq:roster";
		
		public const string Ping = "urn:xmpp:ping";
		
		public const string DiscoInfo = "http://jabber.org/protocol/disco#info";
		public const string DiscoItems = "http://jabber.org/protocol/disco#items";
		public const string Capabilities = "http://jabber.org/protocol/caps";
		
		public const string Blocking = "urn:xmpp:blocking";
		
		public const string Version = "jabber:iq:version";
		
		public const string DataForms = "jabber:x:data";
		public const string Registration = "jabber:iq:register";
		
		public const string XmlStorage = "jabber:iq:private";
		public const string Bookmarks = "storage:bookmarks";
		
		public const string ChatStates = "http://jabber.org/protocol/chatstates";
		
		public const string Delay = "urn:xmpp:delay";
		public const string OldDelay = "jabber:x:delay";
		
		public const string vCard = "vcard-temp";
		public const string vCardUpdate = "vcard-temp:x:update";
		
		public const string Muc = "http://jabber.org/protocol/muc";
		public const string MucUser = "http://jabber.org/protocol/muc#user";
		
		public const string StreamInitiation = "http://jabber.org/protocol/si";
		public const string Socks5Bytestream = "http://jabber.org/protocol/bytestreams";
		public const string InBandBytestream = "http://jabber.org/protocol/ibb";
		public const string OutOfBand = "jabber:iq:oob";
		public const string FeatureNegotiation = "http://jabber.org/protocol/feature-neg";
		public const string SIFileTransfer = "http://jabber.org/protocol/si/profile/file-transfer";
	}
}
