// 
// XmppPresence.cs
//
// Copyright © 2007 Ben Motmans <ben.motmans@gmail.com>
// Copyright © 2008 Philippe Durand <draekz@gmail.com>
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Galaxium.Protocol.Xmpp.Library;

using Galaxium.Core;

// FIXME: proper tray icons

namespace Galaxium.Protocol.Xmpp
{
	public class XmppPresence: AbstractPresence
	{
		private string _state;
		private Status _status_id;
		private int    _priority;

		public override string State  { get { return _state;       }}
		public Status StatusID        { get { return _status_id;   }}
		public int    Priority        { get { return _priority;    }}

		public static XmppPresence Chat      { get; private set; }
		public static XmppPresence Online    { get; private set; }
		public static XmppPresence Away      { get; private set; }
		public static XmppPresence Dnd       { get; private set; }
		public static XmppPresence XA        { get; private set; }
		public static XmppPresence Error     { get; private set; }
		public static XmppPresence Offline   { get; private set; }
		public static XmppPresence Unknown   { get; private set; }
		
		private XmppPresence (BasePresence base_presence, string state, Status status_id, int priority)
		{
			_basePresence = base_presence;
			_state = state;
			_status_id = status_id;
			_priority = priority;
		}
		
		static XmppPresence ()
		{
			Chat = new XmppPresence (BasePresence.Online,
			                         "Free for Chat", Status.Chat, 7);

			Online = new XmppPresence (BasePresence.Online,
			                           "Online", Status.Online, 6);
			
			Away = new XmppPresence (BasePresence.Idle,
			                         "Away", Status.Away, 5);

			XA = new XmppPresence (BasePresence.Away,
			                       "Extended Away", Status.XA, 4);
			
			Dnd = new XmppPresence (BasePresence.Busy,
			                        "Do Not Disturb", Status.Dnd, 3);
			
			Error = new XmppPresence (BasePresence.Unknown,
			                          "Error", Status.Error, 2);
			
			Unknown = new XmppPresence (BasePresence.Unknown,
			                            "Unknown", Status.Unknown, 1);
			
			Offline = new XmppPresence (BasePresence.Offline,
			                            "Offline", Status.Offline, 0);
		}

		public static XmppPresence Get (BasePresence basePresence)
		{
			switch (basePresence) {
			case BasePresence.Online:    return Online;
			case BasePresence.Away:      return XA;
			case BasePresence.Busy:      return Dnd;
			case BasePresence.Idle:      return Away;
			case BasePresence.Invisible: return null;
			case BasePresence.Offline:   return Offline;
			case BasePresence.Unknown:   return Unknown;
			default:                     return null;
			}
		}

		public static XmppPresence Get (Status status)
		{
			switch (status) {
			case Status.Chat:    return Chat;
			case Status.Online:  return Online;
			case Status.Away:    return Away;
			case Status.XA:      return XA;
			case Status.Dnd:     return Dnd;
			case Status.Offline: return Offline;
			case Status.Error:   return Error;
			case Status.Unknown: return Unknown;
			default:             throw new InternalErrorException ();
			}
		}
	}
}
