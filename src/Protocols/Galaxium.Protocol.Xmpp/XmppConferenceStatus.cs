
using System;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppConferenceStatus: AbstractPresence
	{
		private string _state;
		
		public override bool IsCustomPresence {
			get { return true; }
		}

		public override string State {
			get { return _state; }
		}
		
		public static XmppConferenceStatus Online { get; private set; }
		public static XmppConferenceStatus Offline { get; private set; }
		
		private XmppConferenceStatus ()
		{
		}
		
		static XmppConferenceStatus ()
		{
			Online = new XmppConferenceStatus ();
			Online._state = "Connected";
			Offline = new XmppConferenceStatus ();;
			Offline._state = "Disconnected";
		}
	}
}
