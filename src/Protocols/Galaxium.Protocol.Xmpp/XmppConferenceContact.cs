
using System;

using Galaxium.Protocol.Xmpp.Library;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppConferenceContact: AbstractContact
	{
		public new XmppSession Session {
			get { return base.Session as XmppSession; }
		}

		public override IPresence Presence {
			get {
				return Connected
					? XmppConferenceStatus.Online
					: XmppConferenceStatus.Offline; 
			}
			set {
				throw new InvalidOperationException ();
			}
		}
		
		public override string DisplayMessage {
			get { return Conference.Subject; }
			set { throw new InvalidOperationException (); }
		}
		
		public bool Connected {
			get { return Conference.Connected; }
		}
		
		public int MessagesAvailable {
			get { return 0; }
		}
		
		public XmppConference Conference { get; private set; }
		
		public XmppConferenceContact (XmppSession session, JabberID uid)
			:base (session, uid, uid, XmppConferenceStatus.Offline)
		{
			Conference = new XmppConference (session, this);
		}
		
		public void Join ()
		{
			Conference.Join ();
		}
		
		public void Leave ()
		{
			Conference.Leave ();
		}
	}
}
