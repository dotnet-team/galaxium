// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;

using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;
using Galaxium.Protocol.Xmpp.Library.Messaging;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Gui;

using Anculus.Core;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppPrivateConversation: AbstractConversation
	{
		private List<Message> _messages =
			new List<Message> ();
		
		public override bool IsChannelConversation {
			get { return true; }
		}

		public override bool IsPrivateConversation {
			get { return true; }
		}
		
		public new XmppMucContact PrimaryContact {
			get { return base.PrimaryContact as XmppMucContact; }
		}

		public XmppPrivateConversation (XmppMucContact contact, XmppSession session)
			:base (contact, session)
		{
		}
		
		public override bool Active {
			get { return base.Active; }
			set {
				base.Active = value;
				if (value)
					PrimaryContact.PendingMessages = 0;
			}
		}
		
		public override void Close ()
		{
		}
		
		internal void HandleMessage (string message)
		{
			var msg = new Message (MessageFlag.Message, PrimaryContact, Session.Account, DateTime.Now);
			msg.SetMarkup (message, null);
			_messages.Add (msg);
			ThreadUtility.Dispatch (() => {
				OnMessageReceived (new MessageEventArgs (msg, PrimaryContact));
			});
			if (!Active) {
				ThreadUtility.Dispatch (() => {
					PrimaryContact.PendingMessages ++;
					PrimaryContact.Conference.EmitPrivateNotify ();
				});
			}
			SoundSetUtility.Play (Sound.MessageReceived);
		}
		
		public IEnumerable<Message> GetHistory ()
		{
			return _messages;
		}
		
		public void Send (Message message)
		{
			PrimaryContact.Conference.SendPrivateMessage (PrimaryContact.DisplayName, message.Text);
			_messages.Add (message);
			SoundSetUtility.Play (Sound.MessageSent);
		}

		public override void InviteContact (IContact contact)
		{
			throw new System.InvalidOperationException ();
		}
		
		protected override void LogMessage (IMessage msg, bool read)
		{
			
		}
	}
}
