/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using G=Gtk;

using Mono.Addins;

using Galaxium.Core;
using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	[ExtensionNode ("ToggleButton")]
	public class ToggleButtonExtension : ToolbarExtension
	{
		[NodeAttribute]
		string label;
		
		[NodeAttribute]
		string icon;
		
		[NodeAttribute]
		string iconsize = "MediumSmall";
		
		[NodeAttribute]
		bool important = false;
		
		[NodeAttribute]
		bool active = false;
		
		[NodeAttribute]
		string tooltip = String.Empty;
		
		[NodeAttribute]
		string event_handler;
		
		G.ToggleToolButton _toolButton;
		
		public override G.ToolItem GetToolItem ()
		{
			IIconSize size = typeof (IconSizes).GetProperty (iconsize).GetValue (null, null) as IIconSize;
			
			_toolButton = new G.ToggleToolButton ();
			_toolButton.Label = label;
			_toolButton.IconWidget = new G.Image(IconUtility.GetIcon(icon, size));
			_toolButton.Active = active;
			_toolButton.TooltipText = tooltip;
			_toolButton.IsImportant = important;
			
			if(event_handler != null && event_handler != String.Empty)
			{
				AbstractMenuCommand command = Addin.CreateInstance (event_handler, false) as AbstractMenuCommand;
				
				if (command != null)
				{
					if(Context != null)
						command.Context = Context;
					
					command.MenuItem = _toolButton;
					_toolButton.Clicked += delegate {
						command.Run();
					};
				}
				else
				{
					Log.Error ("Could not find event handler '{0}'", event_handler);
				}
			}
			
			return _toolButton;
		}
	}
}