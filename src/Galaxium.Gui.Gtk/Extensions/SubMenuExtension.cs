/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using G=Gtk;
using Mono.Addins;

namespace Galaxium.Gui.GtkGui
{
	[ExtensionNode ("Menu")]
	[ExtensionNodeChild (typeof(MenuItemExtension))]
	[ExtensionNodeChild (typeof(MenuSeparatorExtension))]
	[ExtensionNodeChild (typeof(MenuRadioExtension))]
	[ExtensionNodeChild (typeof(MenuCheckExtension))]
	[ExtensionNodeChild (typeof(SubMenuExtension))]
	public class SubMenuExtension : MenuExtension
	{
		[NodeAttribute]
		string label;
		
		public override G.MenuItem GetMenuItem (G.MenuItem item)
		{
			G.MenuItem it = new G.MenuItem (label);
			G.Menu submenu = new G.Menu ();
			Dictionary<string, G.MenuItem> masterItems = new Dictionary<string, G.MenuItem> ();
			
			foreach (MenuExtension node in ChildNodes)
			{
				node.Context = this.Context;
				
				G.MenuItem nodeItem;
				
				if (node is MenuRadioExtension)
				{
					MenuRadioExtension radioNode = node as MenuRadioExtension;
					
					if (radioNode.Group == null || radioNode.Group == String.Empty)
					{
						nodeItem = radioNode.GetMenuItem (null);
						masterItems.Add(radioNode.Id, nodeItem);
					}
					else
						nodeItem = radioNode.GetMenuItem (masterItems[radioNode.Group]);
				}
				else
					nodeItem = node.GetMenuItem (null);
				
				submenu.Insert (nodeItem, -1);
			}
			
			it.Submenu = submenu;
			
			it.Visible = true;
			
			return it;
		}
	}
}