/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using G=Gtk;

using Mono.Addins;

using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	[ExtensionNode ("MenuToolButton")]
	public class MenuToolButtonExtension : ToolbarExtension
	{
		[NodeAttribute]
		string label;
		
		[NodeAttribute]
		string icon;
		
		[NodeAttribute]
		bool important = false;
		
		[NodeAttribute]
		string tooltip = String.Empty;
		
		[NodeAttribute]
		string builder = null;
		
		G.MenuToolButton _toolButton;
		
		public override G.ToolItem GetToolItem ()
		{
			_toolButton = new G.MenuToolButton (new G.Image(IconUtility.GetIcon(icon, IconSizes.MediumSmall)), label);

			_toolButton.TooltipText = tooltip;
			
			_toolButton.IsImportant = important;
			
			G.Menu submenu = new G.Menu ();
			Dictionary<string, G.MenuItem> masterItems = new Dictionary<string, G.MenuItem> ();
			
			foreach (MenuExtension node in ChildNodes)
			{
				node.Context = this.Context;
				
				G.MenuItem nodeItem;
				
				if (node is MenuRadioExtension)
				{
					MenuRadioExtension radioNode = node as MenuRadioExtension;
					
					if (radioNode.Group == null || radioNode.Group == String.Empty)
					{
						nodeItem = radioNode.GetMenuItem (null);
						masterItems.Add(radioNode.Id, nodeItem);
					}
					else
						nodeItem = radioNode.GetMenuItem (masterItems[radioNode.Group]);
				}
				else
					nodeItem = node.GetMenuItem (null);
				
				submenu.Insert (nodeItem, -1);
			}
			
			_toolButton.Menu = submenu;
			
			if (!string.IsNullOrEmpty (builder))
			{
				IPopupMenuBuilder build = Addin.CreateInstance (builder, false) as IPopupMenuBuilder;
				
				if (build != null)
				{
					_toolButton.ShowMenu += delegate
					{
						build.Build (submenu);
					};
				}
				else
					Log.Warn ("Invalid builder value {0}", builder);
			}
			
			return _toolButton;
		}
	}
}
