// DragReceivedEventArgs.cs created with MonoDevelop
// User: draek at 7:27 P 17/03/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

namespace Galaxium.Gui.GtkGui
{
	public class DragReceivedEventArgs : EventArgs
	{
		private string[] _uriList;
		
		public string[] UriList
		{
			get { return _uriList; }
		}
		
		public DragReceivedEventArgs (string[] list)
		{
			_uriList = list;
		}
	}
}
