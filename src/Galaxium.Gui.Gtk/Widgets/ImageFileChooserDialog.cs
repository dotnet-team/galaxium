/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gtk;

using Galaxium.Core;

namespace Galaxium.Gui.GtkGui
{
	public class ImageFileChooserDialog: FileChooserDialog
	{
		Image _preview;
		
		public ImageFileChooserDialog(Gtk.Window parent)
			: base(GettextCatalog.GetString ("Select Image"), parent, FileChooserAction.Open, Stock.Cancel, ResponseType.Cancel, Stock.Apply, ResponseType.Accept)
		{
			FileFilter filter = new Gtk.FileFilter();
			filter.AddPixbufFormats();
			this.Filter = filter;
			
			_preview = new Image();
			this.PreviewWidget = _preview;
		}
		
		protected override void OnUpdatePreview()
		{
			if (string.IsNullOrEmpty(PreviewFilename))
			{
				_preview.Hide();
				return;
			}
			
			try
			{
				Gdk.Pixbuf pixbuf = new Gdk.Pixbuf(PreviewFilename);
				double scale = 1;
				
				if (pixbuf.Width > 150)
					scale = 150 / (double)pixbuf.Width;
				if (pixbuf.Height > 300)
				{
					double hscale = 300 / (double)pixbuf.Height;
					
					if (hscale < scale)
						scale = hscale;
				}
				
				if (scale != 1)
					pixbuf = pixbuf.ScaleSimple((int)(pixbuf.Width * scale), (int)(pixbuf.Height * scale), Gdk.InterpType.Bilinear);
				
				_preview.Pixbuf = pixbuf;
				_preview.Show();
			}
			catch
			{
				_preview.Hide();
			}
		}
	}        
}