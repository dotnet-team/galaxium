/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Gtk;
using Cairo;
using Pango;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public enum EntityDisplayEntryMode { Name, Message }
	
	public class EntityDisplayEntry : Entry
	{
		IEntity _entity;
		bool _editing;
		List<IMessageChunk> _chunks;
		string _text;
		EntityDisplayEntryMode _mode;
		
		public new string Text
		{
			get { return _text; }
			set
			{
				_text = value;
				
				if (string.IsNullOrEmpty (value))
					_chunks = null;
				else
					_chunks = Message.Split (GLib.Markup.EscapeText (value), _entity, null, null);
				
				if (_editing)
					base.Text = value;
			}
		}
		
		public EntityDisplayEntry (IEntity entity, EntityDisplayEntryMode mode)
		{
			_entity = entity;
			_mode = mode;
			
			_editing = false;
			
			if (mode == EntityDisplayEntryMode.Name)
			{
				Text = _entity.DisplayName;
				_entity.DisplayNameChange += EntityStringChange;
			}
			else
			{
				Text = _entity.DisplayMessage;
				_entity.DisplayMessageChange += EntityStringChange;
			}
			
			base.Text = " ";
		}
		
		protected override bool OnExposeEvent (Gdk.EventExpose evnt)
		{
			bool ret = base.OnExposeEvent (evnt);
			
			if ((_chunks != null) && !_editing)
			{
				Cairo.Context cx = Gdk.CairoHelper.Create (evnt.Window);
				
				GtkUtility.RenderFormatted (_chunks, _entity, true,
				                            Style, State, cx,
				                            PangoContext,
				                            2, 2, Allocation.Width - 2, Allocation.Height - 2);
				
				((IDisposable)cx.Target).Dispose ();
				((IDisposable)cx).Dispose ();
			}
			
			return ret;
		}
		
		protected override bool OnFocusInEvent (Gdk.EventFocus evnt)
		{
			_editing = true;
			base.Text = _text;
			
			return base.OnFocusInEvent (evnt);
		}
		
		protected override bool OnFocusOutEvent (Gdk.EventFocus evnt)
		{
			_editing = false;
			_text = base.Text;
			base.Text = " ";
			
			if (_mode == EntityDisplayEntryMode.Name)
				_entity.DisplayName = _text;
			else if (_mode == EntityDisplayEntryMode.Message)
				_entity.DisplayMessage = _text;
			
			return base.OnFocusOutEvent (evnt);
		}
		
		protected override bool OnKeyPressEvent (Gdk.EventKey evnt)
		{
			if (evnt.Key == Gdk.Key.Return || evnt.Key == Gdk.Key.KP_Enter)
			{
				Toplevel.ChildFocus (DirectionType.TabForward);
				return true;
			}
			else
				return base.OnKeyPressEvent (evnt);
		}
		
		void EntityStringChange (object sender, EntityChangeEventArgs<string> args)
		{
			if (!_editing)
				Text = args.New;
		}
	}
}
