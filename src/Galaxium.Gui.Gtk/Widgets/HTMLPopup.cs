/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gtk;

namespace Galaxium.Gui.GtkGui
{
	public class HTMLPopup : Gtk.Window
	{
		IHTMLWidget htmlWidget;
		
		public HTMLPopup (Gtk.Widget position, string url)
			: base (Gtk.WindowType.Popup)
		{
			htmlWidget = HTMLUtility.CreateHTMLWidget ();
			htmlWidget.LoadUrl (url);
			
			this.Add (htmlWidget as Gtk.Widget);
			
			Position (position);
		}
		
		public void Position (Gtk.Widget position)
		{
			Gtk.Window win = (Gtk.Window)position.Toplevel;
			
			int topX, topY;
			position.GdkWindow.GetPosition (out topX, out topY);
			
			int allocX, allocY;
			win.TranslateCoordinates (position.Toplevel,
			                                0, 0,
			                                out allocX,
			                                out allocY);
			
			this.Move (topX + allocX, topY + allocY);
		}
	}
}
