// ThumbnailView.cs created with MonoDevelop
// User: draek at 10:42 A 26/10/2007
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;
using G=Gtk;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.Gui.GtkGui
{
	public class ThumbnailView : G.IconView
	{
		const int COL_PATH = 0;
		const int COL_PIXBUF = 1;
		
		private G.ListStore _store;
		
		public ThumbnailView ()
		{
			// path, filename, pixbuf
			_store = new G.ListStore (typeof (string), typeof (Gdk.Pixbuf));
			
			Model = _store;
			SelectionMode = G.SelectionMode.Single;
			
			PixbufColumn = COL_PIXBUF;
		}
		
		public string GetSelectedImagePath ()
		{
			string path = String.Empty;
			G.TreeIter iter;
			
			if (SelectedItems.Length > 0)
			{
				if (_store.GetIter(out iter, SelectedItems[0]))
				{
					path = _store.GetValue(iter, COL_PATH) as string;
				}
			}
			
			return path;
		}
		
		public void InsertFile (string path, Gdk.Pixbuf pixbuf)
		{
			// TODO: here we could manipulate the pixbuf to make it look nice?
			
			_store.AppendValues (path, pixbuf);
		}
		
		public void ClearFiles ()
		{
			_store.Clear();
		}
	}
}
