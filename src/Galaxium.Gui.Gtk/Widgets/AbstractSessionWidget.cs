/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Text;

using Gtk;
using Gdk;
using Glade;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Gui.GtkGui
{
	public enum SessionWidgetPositions { Header, Content, Footer, Search };
	
	public abstract class AbstractSessionWidget : ISessionWidget<Widget>
	{
		public event EventHandler<ChatEventArgs> ActivateChatWidget;
		
		[Widget("HeaderBox")]
		protected Gtk.VBox _header_box;
		[Widget("ContentBox")]
		protected Gtk.VBox _content_box;
		[Widget("FooterBox")]
		protected Gtk.VBox _footer_box;
		[Widget("SearchBox")] 
		protected Gtk.HBox _search_box;
		
		protected Widget _native_widget;
		protected IControllerWindow<Widget> _window;
		protected ISession _session;
		protected IConfigurationSection _config = Configuration.ContactList.Section;
		
		public ISession Session
		{
			get { return _session; }
		}
		
		public IAccount Account
		{
			get { return _session.Account; }
		}
		
		public Widget NativeWidget
		{
			get { return _native_widget; }
		}
		
		public IControllerWindow<Widget> Window
		{
			get { return _window; }
		}
		
		public AbstractSessionWidget (IControllerWindow<Widget> window, ISession session)
		{
			_window = window;
			_session = session;
			
			_session.ContactReverse += SessionContactReverse;
			_session.ContactPresenceChanged += SessionContactPresenceChanged;
			_session.TransferInvitationReceived += SessionTransferInvitationReceived;
			_session.TransferInvitationSent += SessionTransferInvitationSent;
			_session.ContactChanged += SessionContactChanged;
			
			_session.Account.DisplayNameChange += DisplayNameChange;
			_session.Account.DisplayMessageChange += DisplayMessageChange;
			_session.Account.DisplayImageChange += DisplayImageChange;
			_session.Account.PresenceChange += PresenceChange;
			_session.Account.DisplaySettingsChange += DisplaySettingsChange;
		}
		
		public virtual void Initialize ()
		{
			_native_widget = GladeUtility.ExtractWidget<Widget> (GladeUtility.GetGladeResourceStream (typeof (AbstractSessionWidget).Assembly, "AbstractSessionWidget.glade"), "Widget", this);
		}
		
		public abstract void SwitchTo ();
		
		public abstract void Update ();
		
		public void ShowBox (SessionWidgetPositions position)
		{
			Box box = GetBox (position);
			box.Show ();
		}
		
		public void HideBox (SessionWidgetPositions position)
		{
			Box box = GetBox (position);
			box.Hide ();
		}
		
		public Gtk.Box GetBox (SessionWidgetPositions position)
		{
			switch (position)
			{
				case SessionWidgetPositions.Header:
					return _header_box;
				case SessionWidgetPositions.Content:
					return _content_box;
				case SessionWidgetPositions.Footer:
					return _footer_box;
				case SessionWidgetPositions.Search:
					return _search_box;
			}
			
			return null;
		}
		
		public void RemoveSessionWidget (SessionWidgetPositions position)
		{
			Box box = GetBox (position);
			box.Remove (box.Children[0]);
		}
		
		public void SetSessionWidget(SessionWidgetPositions position, Widget widget)
		{
			SetSessionWidget (position, widget, true, true, 0);
		}
		
		public virtual void SetSessionWidget (SessionWidgetPositions position, Widget widget, bool expand, bool fill, uint padding)
		{
			ThrowUtility.ThrowIfNull ("widget", widget);
			
			Box box = GetBox (position);
			box.PackStart (widget, expand, fill, padding);
			
			widget.ShowAll ();
		}
		
		protected virtual void SessionContactReverse (object sender, ContactEventArgs args)
		{
		
		}
		
		protected virtual void SessionContactPresenceChanged (object sender, EntityChangeEventArgs<IPresence> args)
		{
			
		}
		
		protected virtual void SessionTransferInvitationReceived (object sender, FileTransferEventArgs args)
		{
			
		}
		
		protected virtual void SessionTransferInvitationSent (object sender, FileTransferEventArgs args)
		{
			
		}
		
		protected virtual void SessionContactChanged (object sender, ContactEventArgs args)
		{
			
		}
		
		protected virtual void DisplaySettingsChange (object sender, PropertyEventArgs args)
		{
			switch (args.Property)
			{
				default:
					break;
			}
		}
		
		protected virtual void DisplayImageChange (object sender, EventArgs args)
		{
			
		}
		
		protected virtual void DisplayNameChange (object sender, EventArgs args)
		{
			
		}
		
		protected virtual void DisplayMessageChange (object sender, EventArgs args)
		{
			
		}
		
		protected virtual void PresenceChange (object sender, EventArgs args)
		{
			
		}
		
		public void OnActivateChatWidget (ChatEventArgs args)
		{
			if (ActivateChatWidget != null)
				ActivateChatWidget (this, args);
		}
	}
}