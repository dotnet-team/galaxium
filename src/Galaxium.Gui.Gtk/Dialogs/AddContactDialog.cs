/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public sealed class AddContactDialog
	{
		[Widget ("AddContactDialog")]
		private Dialog _dialog = null;
		[Widget ("entryAddress")]
		private Entry _entryAddress = null;
		[Widget ("entryAlias")]
		private Entry _entryAlias = null;
		[Widget ("boxGroup")]
		private HBox _group_box = null;
		[Widget ("chkBlock")]
		private CheckButton _chkBlock = null;
		[Widget ("imgDialog")]
		private Image _imgDialog = null;
		[Widget ("btnAdd")]
		private Button _btnAdd = null;
		
		private ComboBoxEntry _groups_combo_entry;
		private ComboBox _groups_combo;
		private ISession _session;
		
		public string Address { get { return _entryAddress.Text; } }
		public string Alias { get { return _entryAlias.Text; } }
		public bool Block { get { return _chkBlock.Active; } }
		public string SelectedGroup { get { return GetSelectedGroup(); } }
		
		public AddContactDialog (ISession session, bool enable_groupedit)
		{
			_session = session;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (AddContactDialog).Assembly, "AddContactDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_entryAddress.Changed += new EventHandler (DataChangedEvent);
			_entryAlias.Changed += new EventHandler (DataChangedEvent);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-add", IconSizes.Small);
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-add-contact", IconSizes.Large);
			
			if (enable_groupedit)
			{
				_groups_combo_entry = ComboBoxEntry.NewText ();
				_group_box.PackStart(_groups_combo_entry, true, true, 0);
				
				foreach (IGroup group in _session.GroupCollection)
					_groups_combo_entry.AppendText (group.Name);
				
				_groups_combo_entry.Active = 0;
			}
			else
			{
				_groups_combo = ComboBox.NewText ();
				_group_box.PackStart(_groups_combo, true, true, 0);
				
				foreach (IGroup group in _session.GroupCollection)
					_groups_combo.AppendText (group.Name);
				
				_groups_combo.Active = 0;
			}
			
			_btnAdd.Sensitive = CheckInput();
			
			_group_box.ShowAll();
			_dialog.ShowAll();
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		private string GetSelectedGroup ()
		{
			if (_groups_combo_entry != null)
				return _groups_combo_entry.ActiveText;
			else if (_groups_combo != null)
				return _groups_combo.ActiveText;
			
			return string.Empty;
		}
		
		private void DataChangedEvent (object sender, EventArgs args)
		{
			_btnAdd.Sensitive = CheckInput();
		}
		
		private bool CheckInput()
		{
			if (_entryAddress.Text.Length == 0)
				return false;
			else if (_entryAddress.Text.IndexOf(' ') > -1 || _entryAddress.Text.IndexOf('@') < 0)
				return false;
			else
				return true;
		}
	}
}