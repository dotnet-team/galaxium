/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public sealed class RemoveContactDialog
	{
		[Widget ("RemoveContactDialog")]
		private Dialog _dialog;
		[Widget ("imgDialog")]
		private Image _imgDialog;
		[Widget ("InstructionsLabel")]
		private Label _instructionLabel;
		[Widget ("BlockCheck")]
		private CheckButton _blockCheck;
	
		// CHECKTHIS
	//	private ISession _session;
		
		public bool Block
		{
			get { return _blockCheck.Active; }
		}
		
		public RemoveContactDialog (ISession session, bool forceRemoveAll, bool isBlocked)
		{
		//	_session = session;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (RemoveContactDialog).Assembly, "RemoveContactDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-delete", IconSizes.Small);
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-remove-contact", IconSizes.Large);
			
			if (isBlocked)
				_blockCheck.Visible = false;
			
			if (forceRemoveAll)
			{
				_dialog.Title = GettextCatalog.GetString ("Remove from List");
				_instructionLabel.Text = GettextCatalog.GetString ("Removing a contact will remove them from all groups and permanently remove from then from your contact list.");
			}
			else
			{
				_dialog.Title = GettextCatalog.GetString ("Remove from Group");
				_instructionLabel.Text = GettextCatalog.GetString ("Removing a contact from a group will not remove it from your list. It will be put in the 'Other Contacts' group if it does not appear in any other groups.");
				_blockCheck.Visible = false;
			}
			
			_dialog.Show();
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
	}
}