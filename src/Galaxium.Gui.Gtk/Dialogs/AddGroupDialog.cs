/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public sealed class AddGroupDialog
	{
		[Widget ("AddGroupDialog")]
		private Dialog _dialog = null;
		[Widget ("entryName")]
		private Entry _entryName = null;
		[Widget ("imgDialog")]
		private Image _imgDialog = null;
		[Widget ("btnAdd")]
		private Button _btnAdd = null;
		
		public string Name { get { return _entryName.Text; } }
	
		// CHECKTHIS
	//	private ISession _session;
		
		public AddGroupDialog (ISession session)
		{
		//	_session = session;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (AddGroupDialog).Assembly, "AddGroupDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_entryName.Changed += new EventHandler (DataChangedEvent);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-add", IconSizes.Small);
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-add-group", IconSizes.Large);
			
			_btnAdd.Sensitive = CheckInput();
			
			_dialog.ShowAll();
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		private void DataChangedEvent (object sender, EventArgs args)
		{
			_btnAdd.Sensitive = CheckInput();
		}
		
		private bool CheckInput()
		{
			return _entryName.Text.Length > 0;
		}
	}
}