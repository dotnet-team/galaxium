/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
using System;
using Glade;
using G=Gtk;

using Galaxium.Protocol;
using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	public sealed class SetNicknameDialog
	{
		[Widget("SetNicknameDialog")] G.Window _window;
		[Widget("WindowImage")] G.Image _window_image;
		[Widget("NicknameEntry")] G.Entry _nickname_entry;
		[Widget("CancelButton")] G.Button _cancel_button;
		[Widget("ApplyButton")] G.Button _apply_button;
		[Widget("ClearButton")] G.Button _clear_button;

		private IContact _contact;
		
		public SetNicknameDialog (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);
			
			_contact = contact;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (SetNicknameDialog).Assembly, "SetNicknameDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_window);
			
			_window.Icon = IconUtility.GetIcon ("galaxium-set", IconSizes.Small);
			
			_nickname_entry.Text = !String.IsNullOrEmpty (contact.Nickname) ? contact.Nickname : contact.DisplayName;
			
			_nickname_entry.SelectRegion (0, _nickname_entry.Text.Length);
			
			_window_image.Pixbuf = IconUtility.GetIcon("galaxium-set-nickname", IconSizes.Large);
			_window_image.Show();
			
			_cancel_button.Clicked += OnCancelButtonActivated;
			_cancel_button.Image = new G.Image(IconUtility.GetIcon("galaxium-cancel", IconSizes.Small));
			_cancel_button.Label = GettextCatalog.GetString ("_Cancel");
			
			_clear_button.Clicked += OnClearButtonActivated;
			_clear_button.Image = new G.Image(IconUtility.GetIcon("galaxium-clear", IconSizes.Small));
			_clear_button.Label = GettextCatalog.GetString ("C_lear");
			
			_apply_button.Clicked += OnApplyButtonActivated;
			_apply_button.Image = new G.Image(IconUtility.GetIcon("galaxium-apply", IconSizes.Small));
			_apply_button.Label = GettextCatalog.GetString ("_Apply");
			
			_window.ShowAll();
		}
	
		private void OnApplyButtonActivated (object sender, EventArgs args)
		{
			_contact.Nickname = _nickname_entry.Text;
			_contact.Save ();
			
			_window.Destroy();
		}
		
		private void OnClearButtonActivated (object sender, EventArgs args)
		{
			_nickname_entry.Text = _contact.DisplayName;
			_contact.Nickname = String.Empty;
			_contact.Save ();
			
			_window.Destroy();
		}
		
		private void OnCancelButtonActivated (object sender, EventArgs args)
		{
			_window.Destroy();
		}
	}
}