/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public sealed class NewContactDialog
	{
		[Widget ("NewContactDialog")]
		private Dialog _dialog;
		[Widget ("labelContact")]
		private Label _contact_label;
		[Widget ("entryAlias")]
		private Entry _nickname_entry;
		[Widget ("boxGroup")]
		private HBox _group_box;
		[Widget ("imgDialog")]
		private Image _dialog_image;
	//	[Widget ("btnAdd")]
	//	private Button _add_button;
	//	[Widget ("checkAccept")]
	//	private CheckButton _add_check;
		[Widget ("radioAccept")]
		private RadioButton _accept_radio;
	//	[Widget ("lblNickname")]
	//	private Label _nickname_label;
	//	[Widget ("lblGroup")]
	//	private Label _group_label;
		[Widget ("addTable")]
		private Table _addition_table;
		[Widget ("authTable")]
		private Table _auth_table;
		[Widget ("dialogLabel")]
		private Label _dialog_label;
		
		private ComboBoxEntry _groups_combo_entry;
		private ComboBox _groups_combo;
		private ISession _session;
		private string _address;
		
		public string Address { get { return _address; } }
		public string Nickname { get { return _nickname_entry.Text; } }
		public bool AuthAccept { get { return _accept_radio.Active; } }
		public string SelectedGroup { get { return GetSelectedGroup(); } }
		
		public NewContactDialog(ISession session, string address, bool enable_addition, bool enable_groupedit)
		{
			_address = address;
			_session = session;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (NewContactDialog).Assembly, "NewContactDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-add", IconSizes.Small);
			_dialog_image.Pixbuf = IconUtility.GetIcon ("galaxium-add-contact", IconSizes.Large);
			
			if (enable_addition)
				_dialog.Title = GettextCatalog.GetString ("New Contact");
			else
				_dialog.Title = GettextCatalog.GetString ("Subscription Request");
			
			_contact_label.Text = address;
			
			if (enable_groupedit)
			{
				_groups_combo_entry = ComboBoxEntry.NewText ();
				_group_box.PackStart(_groups_combo_entry, true, true, 0);
				
				foreach (IGroup group in _session.GroupCollection)
					_groups_combo_entry.AppendText (group.Name);
				
				_groups_combo_entry.Active = 0;
			}
			else
			{
				_groups_combo = ComboBox.NewText ();
				_group_box.PackStart(_groups_combo, true, true, 0);
				
				foreach (IGroup group in _session.GroupCollection)
					_groups_combo.AppendText (group.Name);
				
				_groups_combo.Active = 0;
			}
			
			_dialog.ShowAll();
			
			if (enable_addition)
			{
				_dialog_label.Text = GettextCatalog.GetString ("Someone has added you to their contact list and is requesting to see your status, what would you like to do?");
				_addition_table.Visible = true;
				_auth_table.Visible = false;
			}
			else
			{
				_dialog_label.Text = GettextCatalog.GetString ("Someone on your contact list is requesting to see your status, what would you like to do?");
				_addition_table.Visible = false;
				_auth_table.Visible = true;
			}
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		private string GetSelectedGroup ()
		{
			if (_groups_combo_entry != null)
				return _groups_combo_entry.ActiveText;
			else if (_groups_combo != null)
				return _groups_combo.ActiveText;
			
			return string.Empty;
		}
	}
}