/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public sealed class CopyMoveGroupDialog
	{
		[Widget ("CopyMoveGroupDialog")]
		private Dialog _dialog;
		[Widget ("boxGroups")]
		private HBox _group_box;
		[Widget ("imgDialog")]
		private Image _imgDialog;
		[Widget ("btnApply")]
		private Button _btnApply;
		
		private ComboBoxEntry _groups_combo_entry;
		private ComboBox _groups_combo;
		private ISession _session;
		private bool _docopy;
		
		public string SelectedGroup { get { return GetSelectedGroup(); } }
		
		public CopyMoveGroupDialog(ISession session, IContact contact, bool docopy, bool enable_groupedit)
		{
			_session = session;
			_docopy = docopy;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (CopyMoveGroupDialog).Assembly, "CopyMoveGroupDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			if (_docopy)
			{
				_dialog.Icon = IconUtility.GetIcon ("galaxium-copy", IconSizes.Small);
				_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-copy-contact", IconSizes.Large);
				_dialog.Title = GettextCatalog.GetString ("Copy To Group");
				_btnApply.Label = GettextCatalog.GetString ("Copy");
				_btnApply.Image = new Image(IconUtility.GetIcon ("galaxium-copy", IconSizes.Small));
			}
			else
			{
				_dialog.Icon = IconUtility.GetIcon ("galaxium-move", IconSizes.Small);
				_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-move-contact", IconSizes.Large);
				_dialog.Title = GettextCatalog.GetString ("Move To Group");
				_btnApply.Label = GettextCatalog.GetString ("Move");
				_btnApply.Image = new Image(IconUtility.GetIcon ("galaxium-move", IconSizes.Small));
			}
			
			if (enable_groupedit)
			{
				_groups_combo_entry = ComboBoxEntry.NewText ();
				_group_box.PackStart(_groups_combo_entry, true, true, 0);
				
				foreach (IGroup group in _session.GroupCollection)
				{
					if (group.UniqueIdentifier != "0" && !group.Contains (contact))
						_groups_combo_entry.AppendText (group.Name);
				}
				
				_groups_combo_entry.Active = 0;
				_groups_combo_entry.Entry.Changed += ComboEntryChanged;
				
				if (string.IsNullOrEmpty(_groups_combo_entry.Entry.Text))
					_btnApply.Sensitive = false;
				else
					_btnApply.Sensitive = true;
			}
			else
			{
				_groups_combo = ComboBox.NewText ();
				_group_box.PackStart(_groups_combo, true, true, 0);
				
				foreach (IGroup group in _session.GroupCollection)
				{
					if (group.UniqueIdentifier != "0" && !group.Contains (contact))
						_groups_combo.AppendText (group.Name);
				}
				
				_groups_combo.Active = 0;
			}
			
			_group_box.ShowAll();
			_dialog.ShowAll();
		}
		
		private void ComboEntryChanged (object sender, EventArgs args)
		{
			if (string.IsNullOrEmpty(_groups_combo_entry.Entry.Text))
				_btnApply.Sensitive = false;
			else
				_btnApply.Sensitive = true;
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		private string GetSelectedGroup ()
		{
			if (_groups_combo_entry != null)
				return _groups_combo_entry.ActiveText;
			else if (_groups_combo != null)
				return _groups_combo.ActiveText;
			
			return string.Empty;
		}
	}
}