/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Gui;

using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	public abstract class ConversationMenuCommand : AbstractMenuCommand
	{
		public abstract void SetDefaultMenuItem ();
		protected IChatWidget<Widget> _chat_widget = null;
		protected IConfigurationSection _config = Configuration.Conversation.Section;
		
		public bool UsingDefaultView
		{
			get { return _chat_widget.UseDefaultView; }
		}
		
		public bool UsingDefaultSettings
		{
			get { return _chat_widget.Conversation.UseDefaultSettings; }
		}
		
		public override void SetMenuItem ()
		{
			if (MenuItem == null && Object == null)
			{
				Log.Error ("Not going to process this menu item.");
				return;
			}
			
			_chat_widget = Object as IChatWidget<Widget>;
			
			if (_chat_widget == null)
			{
				Log.Error ("Chat widget not present in its menu command!");
				return;
			}
			
			SetDefaultMenuItem ();
		}
		
		public abstract void RunDefault ();
		
		public override void Run ()
		{
			if (MenuItem == null && Object == null)
				return;
			
			RunDefault ();
		}
	}
}