/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Gdk;
using GLib;

using Galaxium.Core;

namespace Galaxium.Gui.GtkGui
{
	public sealed class ExpanderCellRenderer : CellRenderer
	{
		public override void GetSize (Widget widget, ref Rectangle cell_area, out int x_offset, out int y_offset, out int width, out int height)
		{
			int size = (int)widget.StyleGetProperty ("expander-size");
			
			width = (int)Xpad * 2 + size;
			height = (int)Ypad * 2 + size;
			
			x_offset = 0;
			y_offset = 0;
			if (!cell_area.Equals (Rectangle.Zero)) {
				x_offset = (int)(Xalign * (cell_area.Width - width));
				x_offset = Math.Max (x_offset, 0);
				
				y_offset = (int)(Yalign * (cell_area.Height - height));
				y_offset = Math.Max (y_offset, 0);
			}
		}

		protected override void Render (Drawable window, Widget widget, Rectangle background_area, Rectangle cell_area, Rectangle expose_area, CellRendererState flags)
		{
			int width = 0, height = 0, x_offset = 0, y_offset = 0;
			StateType state;
			GetSize (widget, ref cell_area, out x_offset, out y_offset, out width, out height);
			
			if (widget.HasFocus)
				state = StateType.Active;
			else
				state = StateType.Normal;

			width -= (int)Xpad * 2;
			height -= (int)Ypad * 2;
			Rectangle clipping_area = new Rectangle ((int) (cell_area.X + x_offset + Xpad), (int) (cell_area.Y + y_offset + Ypad), width - 1, height - 1);
			Style.PaintExpander (widget.Style, (Gdk.Window)window, state, clipping_area, widget, "treeview", (int)(cell_area.X + Xpad + (width / 2)), (int)(cell_area.Y + Ypad + (height / 2)), ExpanderStyle.Expanded);
		}
	}
}