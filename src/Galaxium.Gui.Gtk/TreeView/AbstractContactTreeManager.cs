/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gtk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public abstract class AbstractContactTreeManager : IContactTreeManager
	{
		protected ISession _session;
		protected IConversation _conversation;

		public virtual ISession Session
		{
			get { return _session; }
			set { _session = value; }
		}
		
		public virtual IConversation Conversation
		{
			get { return _conversation; }
			set { _conversation = value; }
		}
		
		public abstract IConfigurationSection Config { get; }
		
		public abstract void Init (GalaxiumTreeView tree);
		
		public abstract void RenderText (object data, CellRendererContact renderer);
		public abstract void RenderLeftImage (object data, CellRendererPixbuf renderer);
		public abstract void RenderRightImage (object data, CellRendererPixbuf renderer);
		
		public virtual string GetMenuExtensionPoint (object data)
		{
			return null;
		}
		
		public virtual InfoTooltip GetTooltip (object data)
		{
			return null;
		}
		
		public abstract int Compare (object data1, object data2);
		
		public abstract bool Visible (object data, string filter, bool caseSensitive);
	}
}
