/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using GLib;
using Gdk;
using Gtk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public class BasicContactTreeManager : AbstractContactTreeManager
	{
		ContactTreeView _tree;
		
		protected ContactTreeView Tree
		{
			get { return _tree; }
		}
		
		public override IConfigurationSection Config
		{
			get
			{
				if (Session.Account.UseDefaultListView)
					return Configuration.ContactList.Section;
				
				return ConfigurationUtility.Accounts[_tree.Manager.Session.Account.Protocol.Name][_tree.Manager.Session.Account.UniqueIdentifier];
			}
		}
		
		protected virtual ContactTreeDetailLevel Detail
		{
			get { return (ContactTreeDetailLevel)Config.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default); }
		}
		
		public override void Init (GalaxiumTreeView tree)
		{
			_tree = tree as ContactTreeView;
		}
		
		public override void RenderText (object data, CellRendererContact renderer)
		{
			if (data is IContact)
			{
				IContact contact = data as IContact;
				renderer.Contact = contact;
				renderer.ShowEmoticons = true;
				renderer.Markup = string.Format ("{0} <i>({1})</i>", Markup.EscapeText (contact.DisplayIdentifier), contact.Presence.State);
			}
			else if (data is ContactTreeRealGroup)
			{
				ContactTreeRealGroup treeGroup = data as ContactTreeRealGroup;
				renderer.Contact = null;
				renderer.ShowEmoticons = false;
				renderer.Markup = string.Format ("<b>{0}</b> ({1}/{2})", Markup.EscapeText (treeGroup.Group.Name), treeGroup.Group.OnlineCount, treeGroup.Group.Count);
			}
			else if (data is ContactTreeGroup)
			{
				renderer.Contact = null;
				renderer.ShowEmoticons = false;
				renderer.Markup = string.Format ("<b>{0}</b> ({1})", Markup.EscapeText ((data as ContactTreeGroup).Name), (data as ContactTreeGroup).Count);
			}
			else
			{
				renderer.Contact = null;
				renderer.ShowEmoticons = false;
				renderer.Markup = "<b>ERROR: Unknown row</b>";
			}
		}
		
		public override void RenderLeftImage (object data, CellRendererPixbuf renderer)
		{
			Pixbuf pbuf = null;
			
			if (data is IContact)
			{
				try
				{
					byte[] buf = (data as IContact).DisplayImage.ImageBuffer;
					Pixbuf originalPbuf = new Pixbuf (buf);
					pbuf = PixbufUtility.GetFramedPixbuf (originalPbuf, _session.Account.DetailLevel);
					originalPbuf.Dispose ();
				}
				catch { }
			}
			
			if (pbuf != null)
			{
				renderer.Pixbuf = pbuf;
				renderer.Width = pbuf.Width;
				renderer.Visible = true;
			}
			else
			{
				renderer.Pixbuf = null;
				renderer.Visible = false;
			}
		}
		
		public override void RenderRightImage (object data, CellRendererPixbuf renderer)
		{
			renderer.Pixbuf = null;
			renderer.Visible = false;
		}
		
		public override string GetMenuExtensionPoint (object data)
		{
			if (data is IContact)
				return "/Galaxium/Gui/" + _session.Account.Protocol.Name + "/ContactTree/ContextMenu/Contact";
			if (data is ContactTreeRealGroup)
				return "/Galaxium/Gui/" + _session.Account.Protocol.Name + "/ContactTree/ContextMenu/Group";
			
			return base.GetMenuExtensionPoint (data);
		}
		
		public override int Compare (object data1, object data2)
		{
			if (data1 is IContact)
				return (data1 as IContact).DisplayIdentifier.CompareTo ((data2 as IContact).DisplayIdentifier);
			else if (data1 is ContactTreeGroup)
			{
				if (data1 is ContactTreeOfflineVirtualGroup)
					return (_tree.SortOrder == SortOrder.Ascending) ? int.MaxValue : int.MinValue;
				if (data2 is ContactTreeOfflineVirtualGroup)
					return (_tree.SortOrder == SortOrder.Ascending) ? int.MinValue : int.MaxValue;
				
				string name1 = (data1 as ContactTreeGroup).Name;
				string name2 = (data2 as ContactTreeGroup).Name;
				
				return name1.CompareTo (name2);
			}
			
			return 0;
		}
		
		public override bool Visible (object data, string filter, bool caseSensitive)
		{
			if (data is ContactTreeGroup)
				return true;
			
			if (data is IContact)
			{
				if (caseSensitive)
					return (data as IContact).DisplayIdentifier.Contains (filter);
				else
					return (data as IContact).DisplayIdentifier.ToLower ().Contains (filter.ToLower ());
			}
			
			return false;
		}
	}
}
