/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using GLib;
using Gtk;

namespace Galaxium.Gui.GtkGui
{
	public partial class GalaxiumTreeView : TreeView
	{
		uint _tooltipTimeout;
		InfoTooltip _currentTooltip;
		Gdk.Rectangle _tooltipRectangle;
		
		protected override bool OnLeaveNotifyEvent (Gdk.EventCrossing evnt)
		{
			DestroyTooltip ();
			return base.OnLeaveNotifyEvent (evnt);
		}
		
		protected override bool OnMotionNotifyEvent (Gdk.EventMotion evnt)
		{
			TreePath treePath;
			int iX = (int)evnt.X;
			int iY = (int)evnt.Y;
			
			if (_tooltipTimeout != 0)
			{
				if ((iY > _tooltipRectangle.Y) && ((iY - _tooltipRectangle.Height) < _tooltipRectangle.Y))
					return base.OnMotionNotifyEvent (evnt);
					
				DestroyTooltip ();
			}
			
			if (!GetPathAtPos (iX, iY, out treePath))
				return base.OnMotionNotifyEvent (evnt);
			
			_tooltipRectangle = GetCellArea (treePath, null);
			
			object data = GetModelData (treePath);
			
			if (data == null)
				return base.OnMotionNotifyEvent (evnt);

			_tooltipTimeout = GLib.Timeout.Add (1000, delegate { ShowTooltip (treePath, data); return false; });
			return false;
		}
		
		protected virtual InfoTooltip CreateTooltip (TreePath treePath, object data)
		{
			return null;
		}
		
		private void ShowTooltip (TreePath treePath, object data)
		{
			InfoTooltip ttip = CreateTooltip (treePath, data);
			
			if (ttip == null)
				return;
			
			_currentTooltip = ttip;
			
			Gdk.Rectangle oRectangle;
			int iOriginX, iOriginY;
			
			if (GdkWindow != null)
			{
				GdkWindow.GetOrigin (out iOriginX, out iOriginY);
				
				oRectangle = new Gdk.Rectangle ();
				oRectangle.X = iOriginX + _tooltipRectangle.X;
				oRectangle.Y = iOriginY + _tooltipRectangle.Y;
				oRectangle.Width = _tooltipRectangle.Width;
				oRectangle.Height = _tooltipRectangle.Height;
				
				_currentTooltip.PositionToRectangle (oRectangle, Screen);
				_currentTooltip.ShowAll ();
			}
			else
			{
				Anculus.Core.Log.Error ("Unable to obtain the GDK window and its origin to display the tooltip!");
			}
		}
		
		void DestroyTooltip ()
		{
			if (_currentTooltip != null)
			{
				_currentTooltip.Destroy ();
				_currentTooltip = null;
			}
			
			if (_tooltipTimeout > 0)
				GLib.Source.Remove (_tooltipTimeout);
		}
	}
}
