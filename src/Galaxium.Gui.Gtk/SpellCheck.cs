using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace GtkSpell
{
	public enum Error { Backend }
	
	public class SpellCheck : GLib.Opaque
	{
#region DllImports
	// CHECKTHIS
		
	//	[DllImport ("libgtkspell.dll")]
	//	static extern void gtkspell_attach (IntPtr view);
		
	//	[DllImport ("libgtkspell.dll")]
	//	static extern int gtkspell_init ();
		
		[DllImport ("libgtkspell.dll")]
		static extern void gtkspell_detach (IntPtr raw);

		[DllImport ("libgtkspell.dll")]
		static extern int gtkspell_error_quark ();

		[DllImport ("libgtkspell.dll")]
		static extern bool gtkspell_set_language (IntPtr raw, IntPtr lang, out IntPtr error);

		[DllImport ("libgtkspell.dll")]
		static extern void gtkspell_recheck_all (IntPtr raw);

		[DllImport ("libgtkspell.dll")]
		static extern IntPtr gtkspell_get_from_text_view (IntPtr view);

		[DllImport ("libgtkspell.dll")]
		static extern IntPtr gtkspell_new_attach (IntPtr view, IntPtr lang, out IntPtr error);
#endregion

		public SpellCheck (IntPtr raw)
			: base (raw)
		{
		}

		public SpellCheck (Gtk.TextView view, string lang) 
		{
			IntPtr native_lang = (lang == null) ? IntPtr.Zero : GLib.Marshaller.StringToPtrGStrdup (lang);
			IntPtr error = IntPtr.Zero;
			
			Raw = gtkspell_new_attach (view == null ? IntPtr.Zero : view.Handle, native_lang, out error);
			
			if (native_lang != IntPtr.Zero)
				GLib.Marshaller.Free (native_lang);
			
			if (error != IntPtr.Zero)
				throw new GLib.GException (error);
		}
		
		public void Detach ()
		{
			gtkspell_detach (Handle);
		}
		
		public static int ErrorQuark ()
		{
			return gtkspell_error_quark ();
		}
		
		public bool SetLanguage (string lang)
		{
			IntPtr native_lang = GLib.Marshaller.StringToPtrGStrdup (lang);
			IntPtr error = IntPtr.Zero;
			
			bool ret = gtkspell_set_language(Handle, native_lang, out error);
			
			GLib.Marshaller.Free (native_lang);
			
			if (error != IntPtr.Zero)
				throw new GLib.GException (error);
			
			return ret;
		}
		
		public void RecheckAll ()
		{
			gtkspell_recheck_all (Handle);
		}
		
		public static GtkSpell.SpellCheck GetFromTextView (Gtk.TextView view)
		{
			IntPtr raw_ret = gtkspell_get_from_text_view (view == null ? IntPtr.Zero : view.Handle);
			GtkSpell.SpellCheck ret = (raw_ret == IntPtr.Zero) ? null : (GtkSpell.SpellCheck) GLib.Opaque.GetOpaque (raw_ret, typeof (GtkSpell.SpellCheck), false);
			return ret;
		}
	}
}
