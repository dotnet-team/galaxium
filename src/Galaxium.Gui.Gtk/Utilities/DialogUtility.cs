/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Mono.Addins;

using Gtk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.Gui.GtkGui
{
	public static class DialogUtility
	{
		public static bool ShowConfirmationDialog (Window parent, string identifier, bool defaultValue, string title, string format, params object[] args)
		{
			if (identifier == null)
				throw new ArgumentNullException ("identifier");
			if (format == null)
				throw new ArgumentNullException ("format");
			
			IConfigurationSection section = ConfigurationUtility.General["Gui"]["ConfirmationDialogs"];
			bool hasDefaultReply = section.GetBool (identifier, false);
			
			if (hasDefaultReply)
				return defaultValue;
			
			DialogFlags flags = parent == null ? DialogFlags.Modal : DialogFlags.DestroyWithParent;
			MessageDialog dlg = new MessageDialog (parent, flags, Gtk.MessageType.Question, ButtonsType.YesNo, format, args);
			dlg.Title = title;

			CheckButton chkDontAsk = new CheckButton ("Never ask this again.");
			
			VBox box = dlg.VBox;
			box.PackStart (chkDontAsk, false, true, 0);
			box.ShowAll ();
			
			bool reply = false;
			
			try {
				ResponseType rt = (ResponseType)dlg.Run ();
				reply = rt == ResponseType.Yes;
				
				section.SetBool (identifier, chkDontAsk.Active);
			} finally {
				dlg.Destroy ();
			}
			
			return reply;
		}
	}
}