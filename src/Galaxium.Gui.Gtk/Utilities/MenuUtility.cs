/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;

using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	public static class MenuUtility
	{
		static Dictionary<Gtk.Window, AccelGroup> _accelGroups;
		
		static MenuUtility ()
		{
			_accelGroups = new Dictionary<Gtk.Window, AccelGroup> ();
		}
		
		public static AccelGroup GetAccelGroup (Gtk.Window window)
		{
			if (!_accelGroups.ContainsKey (window))
				_accelGroups.Add (window, new AccelGroup ());
			
			return _accelGroups[window];
		}
		
		public static Toolbar CreateToolbar (string extensionPoint, IExtensionContext extensionContext)
		{
			Toolbar toolBar = new Toolbar ();
			
			FillToolBar (extensionPoint, extensionContext, toolBar);
			
			return toolBar;
		}
		
		public static MenuBar CreateMenuBar (string extensionPoint, IExtensionContext extensionContext)
		{
			MenuBar menuBar = new MenuBar ();
			
			FillMenuBar (extensionPoint, extensionContext, menuBar);
			
			return menuBar;
		}
		
		public static Menu CreateContextMenu (string extensionPoint, IExtensionContext extensionContext)
		{
			Menu menu = new Menu ();
			
			FillContextMenu (extensionPoint, extensionContext, menu);
			
			return menu;
		}
		
		public static void ClearToolBar (Toolbar toolBar)
		{
			foreach (Widget child in toolBar.Children)
				toolBar.Remove(child);
		}
		
		public static void FillToolBar (string extensionPoint, IExtensionContext extensionContext, Toolbar toolBar)
		{
			ThrowUtility.ThrowIfEmpty ("extensionPoint", extensionPoint);
			ThrowUtility.ThrowIfNull ("toolBar", toolBar);
			
			ClearToolBar (toolBar);
			
			foreach (ToolbarExtension node in AddinUtility.GetContextExtensionNodes (extensionPoint, extensionContext))
			{
				ToolItem item = node.GetToolItem ();
				toolBar.Insert(item, -1);
				item.ShowAll();
			}
		}
		
		public static void ClearMenuBar (MenuBar menuBar)
		{
			foreach (Widget child in menuBar.Children)
				menuBar.Remove(child);
		}
		
		public static void FillMenuBar (string extensionPoint, IExtensionContext extensionContext, MenuBar menuBar)
		{
			ThrowUtility.ThrowIfEmpty ("extensionPoint", extensionPoint);
			ThrowUtility.ThrowIfNull ("menuBar", menuBar);
			
			ClearMenuBar (menuBar);
			
			foreach (MenuExtension node in AddinUtility.GetContextExtensionNodes (extensionPoint, extensionContext))
			{
				MenuItem item = node.GetMenuItem(null);
				menuBar.Insert(item, -1);
			}
		}
		
		public static void ClearContextMenu (Menu menu)
		{
			foreach (Widget child in menu.Children)
				menu.Remove(child);
		}
		
		public static void FillContextMenu (string extensionPoint, IExtensionContext extensionContext, Menu menu)
		{
			ThrowUtility.ThrowIfEmpty ("extensionPoint", extensionPoint);
			ThrowUtility.ThrowIfNull ("menu", menu);
			
			ClearContextMenu (menu);
			
			foreach (MenuExtension node in AddinUtility.GetContextExtensionNodes (extensionPoint, extensionContext))
			{
				MenuItem item = node.GetMenuItem (null);
				menu.Insert(item, -1);
			}
		}
	}
}