/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2003 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Startup
{
	public class Program
	{
#if DEBUG
		internal const string DefaultAppName = "GalaxiumDebug";
#else
		internal const string DefaultAppName = "Galaxium";
#endif
		
		internal static void Main (string[] args)
		{
			Console.WriteLine ("Galaxium Messenger");
			Console.WriteLine ("Copyright (C) 2003-2008 Philippe Durand <draekz@gmail.com>");
			Console.WriteLine ("Copyright (C) 2006-2009 Ben Motmans <ben.motmans@gmail.com>");
			Console.WriteLine ("Copyright (C) 2007-2008 Paul Burton <paulburton89@gmail.com>");
			Console.WriteLine (String.Empty);
			
			bool debug = true;
			string appName = DefaultAppName;
			
			foreach (string arg in args)
			{
				if (arg.StartsWith ("--debug"))
				{
					debug = !arg.EndsWith ("false");
					continue;
				}
				if (arg.StartsWith ("--appName="))
				{
					appName = arg.Substring (10);
					continue;
				}
			}
			
			if (debug)
				Console.WriteLine ("Running in DEBUG mode, appName {0}", appName);
			
			try {
				new Program (debug, appName);
			} catch (Exception e) {
				Console.WriteLine ("Fatal exception while running Galaxium.");
				
				Exception loop = e;
				while (loop != null) {
					Console.WriteLine ("{0}: {1}", loop.GetType().Name, loop.Message);
					Console.WriteLine (loop.StackTrace);
		
					loop = loop.InnerException;
				}

				throw e;
			}
		}
		
		public Program (bool debug, string appName)
		{
			GLibLogging.Enabled = true;
			
			Assembly exe = typeof (Program).Assembly;
			
			string configDir = Path.GetFullPath (Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData), appName));
			string lockFile = Path.Combine (configDir, "pid.lock");

			bool instanceRunning = DetectInstances (lockFile, appName);
			if (instanceRunning) {
				Gtk.Application.Init ();

				Gtk.MessageDialog md = new Gtk.MessageDialog (null, Gtk.DialogFlags.Modal, Gtk.MessageType.Warning, Gtk.ButtonsType.Close,
					GettextCatalog.GetString ("An instance of Galaxium with configuration profile '{0}' is already running.{1}If you really want to run 2 seperate instances, use the \"--appName=GalaxiumXXX\" command line parameter",
						appName, Environment.NewLine));
				md.Run ();
				md.Destroy ();
				md.Dispose ();

				md.Close += delegate(object sender, EventArgs e) {
					Gtk.Application.Quit ();
				};

				Gtk.Application.Run ();
			} else {
				CoreUtility.Initialize (exe, appName, debug);
				WriteInstancePid (lockFile);
				
				AddinUtility.Initialize ();
			}
		}

		private void WriteInstancePid (string lockFile)
		{
			try {
				Process currentProcess = Process.GetCurrentProcess ();
				if (File.Exists (lockFile))
					File.Delete (lockFile);

				File.WriteAllText (lockFile, currentProcess.Id.ToString ());
			} catch (Exception e) {
				Log.Error (e, "WriteInstancePid");
			}
		}

		private bool DetectInstances (string lockFile, string appName)
		{
			if (File.Exists (lockFile)) {
				string content = File.ReadAllText (lockFile);
				int pid = -1;
				if (int.TryParse (content, out pid)) {
					//found process id, make sure the process is still running

					Process other = null;
					try {
						other = Process.GetProcessById (pid);
					} catch {}

					if (other == null)
						return false;

					string otherAppName = GetAppNameFromProcess (other);
					
					if (Environment.OSVersion.Platform == PlatformID.Unix) {
						//case sensitive
						return appName == otherAppName;
					} else {
						//case insensitive
						return String.Compare (appName, otherAppName, true) == 0;
					}
				}
			}

			return false;
		}

		private string GetAppNameFromProcess (Process p)
		{
			if (String.IsNullOrEmpty (p.StartInfo.Arguments))
				return DefaultAppName;

			string[] args = p.StartInfo.Arguments.Split (new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
			foreach (string arg in args) {
				if (arg.StartsWith ("--appName="))
					return arg.Substring (10);
			}

			return DefaultAppName;
		}
	}
}