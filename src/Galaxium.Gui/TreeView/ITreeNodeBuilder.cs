/*
 * Copyright (C) 2005-2007  Ben Motmans  <ben.motmans@gmail.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Gui
{
	public interface ITreeNodeBuilder<T> where T : ITreeNodeRenderer
	{
		Type NodeType { get; }

		string ContextMenuExtensionPoint { get; }

		void RenderIcon (TreeNodeContext context, T renderer, ITreeNodeWidget widget);

		void RenderText (TreeNodeContext context, T renderer, ITreeNodeWidget widget);

		bool HasDynamicChildNodes (TreeNodeContext context);

		void BuildDynamicChildNodes (TreeNodeContext context);

		void NodeActivated (TreeNodeContext context);

		string GetComparableContent (TreeNodeContext context);
	}
}