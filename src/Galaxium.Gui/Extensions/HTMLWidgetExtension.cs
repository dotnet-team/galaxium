using System;

using Mono.Addins;
using Galaxium.Core;

namespace Galaxium.Gui
{
	public class HTMLWidgetExtension: ExtensionNode
	{
		[NodeAttribute]
		string type = null;
		
		public Type WidgetType
		{
			get { return Addin.GetType (type); }
		}
		
		public IHTMLWidget GetWidget ()
		{
			return Activator.CreateInstance (WidgetType) as IHTMLWidget;
		}
	}
}
