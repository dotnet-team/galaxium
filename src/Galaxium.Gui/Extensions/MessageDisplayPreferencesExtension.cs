using System;
using System.IO;

using Mono.Addins;

using Galaxium.Core;

namespace Galaxium.Gui
{
	public class MessageDisplayPreferencesExtension: TypeExtensionNode
	{
		[NodeAttribute]
		string id = null;
		
		public string Name
		{
			get { return id; }
		}

		public override bool Equals (object o)
		{
			return base.Equals(o) || id.Equals(o);
		}
		
		public override int GetHashCode ()
		{
			return id.GetHashCode ();
		}
	}
}
