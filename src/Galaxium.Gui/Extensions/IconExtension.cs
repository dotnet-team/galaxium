/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using Mono.Addins;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Gui
{
	public class IconExtension<IconType> : ContextExtension
	{
		[NodeAttribute]
		private string _id = null;
		
		[NodeAttribute]
		private string _theme = null;
		
		[NodeAttribute]
		private string _resource = null;
		
		[NodeAttribute]
		private string _iconSize = null;
		
		[NodeAttribute]
		private bool _lazyLoad = false;
		
		public string Identifier {
			get { return _id; }
		}
		
		public string Theme {
			get { return _theme; }
		}
		
		public string Resource {
			get { return _resource; }
		}
		
		public IIconSize IconSize {
			get { return GetIconSize(_iconSize); }
		}
		
		public bool LazyLoad {
			get { return _lazyLoad; }
		}
		
		public object Object {
			get { return GetIcon (); }
		}
		
		public virtual IconType GetIcon()
		{
			return default (IconType);
		}
		
		public virtual IIconSize GetIconSize(string iconSize)
		{
			Log.Warn("GetIconSize Called!");
			return IconSizes.Medium;
		}
	}
}