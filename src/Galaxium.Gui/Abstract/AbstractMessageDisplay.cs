/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui
{
	public abstract class AbstractMessageDisplay : IMessageDisplay
	{
		IConversation _conversation;
		bool _storeMessages;
		List<IMessage> _storedMessages;
		object _widget;
		
		public IConversation Conversation
		{
			get { return _conversation; }
		}
		
		public object Widget
		{
			get { return _widget; }
		}
		
		protected AbstractMessageDisplay (object widget, bool storeMessages)
		{
			ThrowUtility.ThrowIfNull ("widget", widget);
			
			_storeMessages = storeMessages;
			_widget = widget;
			
			if (_storeMessages)
				_storedMessages = new List<IMessage> ();
		}
		
		public virtual void Init (IConversation conversation)
		{
			_conversation = conversation;
		}
		
		public virtual void Dispose ()
		{
			if (_storeMessages)
				_storedMessages.Clear ();
		}
		
		public void AddMessage (IMessage msg)
		{
			if (_storeMessages)
				_storedMessages.Add (msg);
			
			DisplayMessage (msg);
		}
		
		public void Clear ()
		{
			DisplayClear ();
			
			if (_storeMessages)
				_storedMessages.Clear ();
		}
		
		public virtual void Reload ()
		{
			DisplayClear ();
			
			if (_storeMessages)
			{
				foreach (IMessage msg in _storedMessages)
					DisplayMessage (msg);
			}
			else
				Log.Error ("Message display {0} does not override Reload", GetType ().Name);
		}
		
		public virtual bool Save (string filename)
		{
			return false;
		}
		
		public abstract void DisplayClear ();
		public abstract void DisplayMessage (IMessage msg);
		public abstract void UpdateEmoticon (IEmoticon emot);
	}
}
