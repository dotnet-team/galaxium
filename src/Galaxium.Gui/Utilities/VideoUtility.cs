/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Anculus.Core;

using Galaxium.Core;

namespace Galaxium.Gui
{
	public static class VideoUtility
	{
		static List<IVideoBackend> _backends = new List<IVideoBackend> ();
		
		static IVideoBackend _activeBackend;
		static IWebcamDevice _activeDevice;
		
		public static IVideoBackend ActiveBackend
		{
			get { return _activeBackend; }
		}
		
		public static IWebcamDevice ActiveDevice
		{
			get { return _activeDevice; }
		}
		
		public static void Initialize ()
		{
			LoadBackends ();
		}
		
		static void LoadBackends()
		{
			_backends.Clear ();
			_activeBackend = null;
			_activeDevice = null;
							
			foreach (VideoBackendExtension node in AddinUtility.GetExtensionNodes ("/Galaxium/Backends/Video"))
			{
				try
				{
					IVideoBackend backend = (IVideoBackend)node.GetInstance ();
					_backends.Add (backend);
				}
				catch (Exception ex)
				{
					Log.Error (ex, "Unable to load video backend " + node.Name);
				}
			}
			
			if (_backends.Count > 0)
			{
				_activeBackend = _backends[0];
				
				Log.Debug ("Using video backend '{0}'", _activeBackend);
				
				SelectDevice ();
			}
			else
				Log.Warn ("Unable to load any video backends");
		}
		
		public static void SelectDevice ()
		{
			_activeDevice = null;
			
			if (_activeBackend == null)
				return;
			
			foreach (IWebcamDevice device in _activeBackend.Devices)
			{
				_activeDevice = device;
				
				Log.Debug ("Using video device '{0}'", _activeDevice.Name);
				
				foreach (IVideoFormat format in _activeDevice.VideoFormats)
				{
					string rates = string.Empty;
					foreach (float rate in format.Framerates)
						rates += string.Format ("{0:0.00}Hz, ", rate);
					
					if (rates.Length > 0)
						rates = rates.Substring (0, rates.Length - 2);
					
					Log.Debug ("\t{0}\t {1}x{2}\t {3}", format.MimeType, format.Width, format.Height, rates);
				}
			}
			
			if (_activeDevice == null)
				Log.Debug ("No video devices found");
		}
		
		public static IVideoPlayer GetPlayer (bool local)
		{
			if (local)
				return _activeBackend.GetPlayer (_activeDevice, DecideOptimumVideoFormat (_activeDevice.VideoFormats));
			
			return _activeBackend.GetPlayer (null, null);
		}
		
		static IVideoFormat DecideOptimumVideoFormat (IEnumerable<IVideoFormat> formats)
		{
			IVideoFormat best = null;
			int bestSize = 0;
			
			foreach (IVideoFormat format in formats)
			{
				int formatSize = format.Width * format.Height;
				
				// We choose the largest video format available
				
				// GalaxiumVideoSink only supports RGB/BGR formats,
				// however the pipeline can convert from YUV to RGB
				// if necessary
				
				// But, if we have 2 formats of equal size, one of
				// which is an RGB format, we should always prefer
				// the RGB one to avoid conversion
				
				if ((best == null) ||
				    (formatSize > bestSize) ||
				    ((formatSize == bestSize) && (best.MimeType != "video/x-raw-rgb") && (format.MimeType == "video/x-raw-rgb")))
				{
					best = format;
					bestSize = formatSize;
				}
			}
			
			return best;
		}
	}
}
